<?php include( dirname(__FILE__) . "/inc/common.inc.php");
$page=$_REQUEST['page'];

if (empty($page)) $page='login';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="<?php echo $_SESSION['lingua']; ?>">
<head>
<title><?=$nome_banca?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="keywords" content="<?=$nome_banca?>" />
<meta name="description" content="<?=$nome_banca?>" />


<link type="text/css" media="screen" rel="stylesheet" href="css/style.css" />
</head>

<body class="bodyPrint">

<div style="margin:5px;">
<?php
// se non � il login controllo l'accesso

if ($page!='login') {
    if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],$page) && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
        $goPage->alertback(ACCESSO_NEGATO, false);
        exit();
    }
    
}

if (file_exists($path_www."inc/pages/".$page.".inc.php")) include($path_www."inc/pages/".$page.".inc.php"); 
else $goPage->alertback(PAGE_NOT_FOUND, false);
    

?>
</div>
<script type="text/javascript" language="javascript">

window.onload = function()
{
    if (typeof(window.print) != 'undefined') {
        window.print();
    }
}

</script>
</body>
</html>