<?php
# download.php
include( dirname(__FILE__) . "/inc/common.inc.php");

if (!empty($_REQUEST['fn']) && file_exists($path_www.'apply-online/'.$_REQUEST['fn'])) {
			
	header("Pragma: public"); 
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false); 
	header("Content-Type: application/pdf");
	header("Content-Disposition: attachment; filename=\"".$_REQUEST['fn']."\";" );
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: ".filesize("".$path_www."apply-online/".$_REQUEST['fn']));
	readfile("".$path_www."apply-online/".$_REQUEST['fn']);
	exit();
	
} else {
	
	$goPage->alertClose("File not Found!");
	exit();
	
}
?>
