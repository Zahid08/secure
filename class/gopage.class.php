<?php
//classe di reindirizzamento della pagina
class goPage {
	
	var $s='';

	function alertgo($message, $location, $return=false) {

		$s ='<script type="text/javascript">';
		$s .="\n".'<!--';
		$s .="\n\t".'alert("'.$message.'");';
		$s .="\n\t".'window.location="'.$location.'";';
		$s .="\n".'//-->';
		$s .="\n".'</script>';
		
		if ($return) return $s;
		else echo $s;
		
	}

	function alertback($message, $return=false) {

		$s ='<script type="text/javascript">';
		$s .="\n".'<!--';
		$s .="\n\t".'alert("'.$message.'");';
		$s .="\n\t".'history.back();';
		$s .="\n".'//-->';
		$s .="\n".'</script>';
		
		if ($return) return $s;
		else echo $s;

	}

	function go_to($url, $return=false) {

		$s ='<script type="text/javascript">';
		$s .="\n".'<!--';
		$s .="\n\t".'window.location="'.$url.'";';
		$s .="\n".'//-->';
		$s .="\n".'</script><br />Se il tuo browser non supporta javascript clicca <a href="'.$url.'">qui</a>';
		
		if ($return) return $s;
		else echo $s;

	}

	function alert($message, $return=false) {

		$s ='<script type="text/javascript">';
		$s .="\n".'<!--';
		$s .="\n\t".'alert("'.$message.'");';
		$s .="\n".'//-->';
		$s .="\n".'</script>';
		
		if ($return) return $s;
		else echo $s;
		
	}

} // end class

?>