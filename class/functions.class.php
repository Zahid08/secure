<?php
// functions.class.php

class functions {

	/**
         * function file_upload($nome_campo,$percorso,$prefisso_file=null){
         * 
         * @global type $goPage
         * @global type $path_www
         * @param type $nome_campo
         * @param type $percorso
         * @param type $prefisso_file
         * @param type $tipo_upload
         * @return type
         */
	function upload_file($nome_campo,$percorso,$prefisso_file=null,$tipo_upload) {
		global $goPage,$path_www;
		
		$error = '';
		
		$arrayErrorFiles[0]="Nessun errore";
		$arrayErrorFiles[1]="Il file inviato eccede le dimensioni consetite dal server (30Mb)."; // php.ini
		$arrayErrorFiles[2]="Il file inviato eccede le dimensioni consentite (".$_REQUEST['MAX_FILE_SIZE']." Mb)."; // MAX_FILE_SIZE
		$arrayErrorFiles[3]="Upload eseguito parzialmente.";
		$arrayErrorFiles[4]="Nessun file � stato inviato.";
		$arrayErrorFiles[6]="Mancanza della cartella temporanea in cui uploadare i files.";
		
		if ($_FILES[$nome_campo]["name"]){
		
			if($_FILES[$nome_campo]["error"]>0){
				$error = $arrayErrorFiles[$_FILES[$nome_campo]["error"]];
			}
			
			$arraySearch = array('�','�','�','�','�','�');
			$arrayReplace = array('a','e','e','i','o','u');
			
			switch($tipo_upload){	
			
				case "image":
				
					
					$type_file = array("image/pjpeg","image/jpeg","image/gif", "image/png", "image/x-png");
					if (!in_array($_FILES[$nome_campo]["type"], $type_file)) {
						
						$error = 'Formato file non ammesso';
						
					} else {
								
						# elaboro il nome
						$nome_tmp=str_replace(" ","-",ereg_replace('[^0-9 A-Z a-z . _ -]+','',str_replace($arraySearch,$arrayReplace,$_FILES[$nome_campo]["name"])));
						//$nome_tmp='';
						$estensione = substr(strrchr($_FILES[$nome_campo]["name"], '.'), 0);
						$nome_file = $prefisso_file.$nome_tmp;//.$estensione;
				
						move_uploaded_file($_FILES[$nome_campo]["tmp_name"],$percorso.$nome_file);
						
						# ridimensiono il file
						list($width, $height, $type, $attr) = getimagesize($percorso.$nome_file);
				
						// SE L'IMMAGINE E' TROPPO GROSSA LA SCALO
						if ($width>800) {
							
							// includi classi per ridimensionamento
							include_once $path_www."class/save_image.class.php";

							$filetemp = $percorso.$nome_file;
							$thumb=new thumbnail($filetemp); // prepare to generate "shiegege.jpg" in directory "/www"
							$thumb->size_auto(800);
							$thumb->jpeg_quality(90);		   // set width for thumbnail with 100 pixels
							$thumb->save($percorso.$nome_file);	   // save my  thumbnail to file "huhu.jpg" in directory "/www/thumb
						}
					
					}
							
				
				
				break;
				
				case "document":
					
					$type_file = array("application/pdf","text/html","application/msword","text/plain", "video/quicktime", "audio/mpeg", "video/mpeg", "audio/mpeg3", "application/powerpoint", "application/mspowerpoint", "application/plain", "application/excel", "application/x-msexcel", "application/zip", "application/x-compressed", "application/x-zip-compressed", "application/x-excel","image/pjpeg","image/jpeg","image/gif", "image/png", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/force-download");
					if (!in_array($_FILES[$nome_campo]["type"], $type_file) ) {
					
						$error = 'Formato file non ammesso'.$_FILES[$nome_campo]["type"];
						
					} else {
						
						# elaboro il nome
						$nome_tmp=str_replace(" ","-",ereg_replace('[^0-9A-Za-z._-]+','',str_replace($arraySearch,$arrayReplace,$_FILES[$nome_campo]["name"])));
						$nome_file = $prefisso_file.$nome_tmp;
				
						move_uploaded_file($_FILES[$nome_campo]["tmp_name"],$percorso.$nome_file);
						
					}
				
				break;
					
					
				
				
			}
			
			
			
		}
	
		if ($error) $goPage->alert("ERRORE: ".$error); else return ($nome_file);
	
	} // end func upload_file






	
	function sendMail($intestazioni,$oggetto,$messaggio,$allegati = NULL) {
		
		global $name_admin, $mail_webmaster;
		/*echo '<pre>';
		print_r($intestazioni);
		echo '</pre>';
*/
		$mailtestuale = strip_tags($messaggio);
		// se nel messaggio ci sono immagini devo rimappare il src
		$oggetto = strip_tags($oggetto);
	
		
		$mailPHP = new PHPMailer();
		//$mailPHP->IsSMTP();
		//$mailPHP->SMTPDebug  = 2;  // enables SMTP debug information (for testing)
	
		//$mailPHP->SetLanguage('it');
		 $mailPHP->Host = '89.46.111.23'; //'192.168.52.10';
			
                
		$mailPHP->From = $intestazioni['from_email'];
		if (!empty($intestazioni['from_name'])) 
                    $mailPHP->FromName = $intestazioni['from_name']; 
                else 
                    $mailPHP->FromName = $name_admin;
		
                
                if('DEV' == ENV ){
                    
                    $mailPHP->AddReplyTo($mail_webmaster, (!empty($intestazioni['reply_name'])?$intestazioni['reply_name']:$name_admin));
                    
                    $mailPHP->AddAddress($mail_webmaster, $intestazioni['to_name']);
                    $mailPHP->Sender = $mail_webmaster; // ritorno email non recapitate
                    
                    $footer_dev = PHP_EOL . "TO EMAIL: ". $intestazioni['to_email'] . ' ' . $intestazioni['to_name'] ;
                    
                    if (!empty($intestazioni['reply_email'])) 
                        $footer_dev .= PHP_EOL . "TO EMAIL: ". $intestazioni['reply_email'] . ' ' . (!empty($intestazioni['reply_name'])?$intestazioni['reply_name']:$name_admin);
                    
                    if (!empty($intestazioni['return_email'])) 
                        $footer_dev .= PHP_EOL."RETURN_EMAIL: " . $intestazioni['return_email'] ;
                    if (!empty($intestazioni['cc_email'])) 
                        $footer_dev .= PHP_EOL . "CC_EMAIL: " . $intestazioni['cc_email'] . ' ' . (!empty($intestazioni['cc_name'])?$intestazioni['cc_name']:'');

                    if (!empty($intestazioni['bcc_email'])) 
                        $footer_dev .= PHP_EOL . "BCC_EMAIL: " . $intestazioni['bcc_email'] . ' ' . (!empty($intestazioni['bcc_name'])?$intestazioni['bcc_name']:'');
                    
                    $oggetto = "[DEV][caribbeancapitalreserve] " .$oggetto ;
                    $messaggio .= nl2br($footer_dev); 
                    $mailtestuale .= $footer_dev; 
                    
                }else{ //produzione
                    if (!empty($intestazioni['reply_email'])) 
                        $mailPHP->AddReplyTo($intestazioni['reply_email'], (!empty($intestazioni['reply_name'])?$intestazioni['reply_name']:$name_admin));
                    
                    $mailPHP->AddAddress($intestazioni['to_email'], $intestazioni['to_name']);

                    if (!empty($intestazioni['return_email'])) 
                        $mailPHP->Sender = $intestazioni['return_email']; // ritorno email non recapitate

                    if (!empty($intestazioni['cc_email'])) 
                        $mailPHP->AddCC($intestazioni['cc_email'], (!empty($intestazioni['cc_name'])?$intestazioni['cc_name']:''));

                    if (!empty($intestazioni['bcc_email'])) 
                        $mailPHP->AddBCC($intestazioni['bcc_email'], (!empty($intestazioni['bcc_name'])?$intestazioni['bcc_name']:''));

                }	
			
		// set word wrap to 50 characters				
		$mailPHP->WordWrap = 50;
			
		if(is_array($allegati)){
			foreach($allegati as $nome_file_ori => $filename_upload) $mailPHP->AddAttachment($nome_file_ori, $filename_upload);         // add attachments
		}

		$mailPHP->IsHTML(true);                                  // set email format to HTML
		
		
		// AGGIUSTO IL FORMATO DEL MESSAGGIO
		/*$messaggio = str_replace("<tr","\n<tr",$messaggio);
		$messaggio = str_replace("<font","\n<font",$messaggio);
		$messaggio = str_replace("<a","\n<a",$messaggio);
		$messaggio = str_replace("<br","\n<br",$messaggio);
		$messaggio = str_replace("<img","\n<img",$messaggio);*/
		
		$mailPHP->Subject = $oggetto; // Oggetto del mailing
		$mailPHP->Body    = $messaggio;  // Mail in HTML
		$mailPHP->AltBody = $mailtestuale; // Mail in formato testuale
		
		$invio = $mailPHP->Send();
		
		/*echo '<pre>';
		print_r($mailPHP);
		echo '</pre>';*/
		
		//if (!$invio) echo '***'.$mailPHP->ErrorInfo.'***';

/*
			if(!$mailPHP->Send()){
				
			  	echo '<br />'.date('Y-m-d H:i:s'). "Impossibile spedire l'email a ".$mailto." - ";
			    echo '<br />'."Error: " . $mailPHP->ErrorInfo."\r\n";
			   
			    echo '<br />'. $l.') '.date('Y-m-d H:i:s'). "Impossibile spedire l'email a ".$mailto." - Error: " . $mailPHP->ErrorInfo."<br>";
			   //exit;
			}else{
				 echo '<br />'. $l.') '.date('Y-m-d H:i:s')." - ".$mailto." inviata !\r\n";	
				
				 echo '<br />'. $l.') '.date('Y-m-d H:i:s')." - ".$mailto." inviata !<br>";	
				
			}*/

		$mailPHP->SmtpClose();
		
		return $invio;
		
	} // end func sendMail














	function formatData($dataora, $formato) {
		
		$dataFormat='';
		if ($dataora==null || $dataora=='0000-00-00 00:00:00') return $dataFormat;
		// riceve data in formato datetime o date
		// Y-m-d H:i:s
		$data='';
		$ora='';
		$d=date('d');
		$m=date('m');
		$Y=date('Y');
		$H=date('H');
		$i=date('i');
		$s=date('s');
		list($data, $ora)=explode(" ", $dataora);
		
		if (!empty($data)) list($Y, $m, $d)=explode("[-/.]", $data);
		if (!empty($ora)) list($H, $i, $s)=explode("[:,.]", $ora);
		//echo "$H,$i,$s,$m,$d,$Y";
		$dataFormat = date($formato, mktime($H,$i,$s,$m,$d,$Y));
		
		return $dataFormat;

	} // end func formatData

	/**
	function cutText($modString, $maxLength)
	*/
	function cutText($modString, $maxLength) {
		
		$modString=strip_tags($modString);
		if (strlen($modString) > $maxLength) {
			$modString = substr($modString,0,$maxLength);
			$lastSpace = strrpos ($modString, " ");
			// se non trova spazi, non tronca il testo
			if (empty($lastSpace)) $lastSpace=$maxLength;
			$modString = substr($modString,0,$lastSpace);
			$modString .= "...";
		}
		return $modString;
	} // end func

	/**
	function clear_string($string, $separator=null)
	*/
	function clear_string($string, $separator='-'){
				
		// a capo
		$clean_string = nl2br($string);
		
		// elimino gli spazi agli estremi
		$clean_string = trim($clean_string);
		
		// elimino i tag
		$clean_string = strip_tags($clean_string);
		
		// converto in minuscolo
		$clean_string =strtolower($clean_string);
		
		// converto i caratteri speciali in entities
		$clean_string = htmlspecialchars($clean_string);
		
		// converto tutti i caratteri possibli in entities
		//$clean_string = htmlentities($clean_string);

		// a questo punto devo prevedere tutte le entities minuscole
		$arrayEntity = array(
		'&nbsp;',
		'&curren;',
		'&sect;',
		'&ordf;',
		'&shy;',
		'&deg;',
		'&sup3;',
		'&para;',
		'&sup1;',
		'&frac14;',
		'&iquest;',
		'&times;',
		'&divide;',
		'&cent;',
		'&Agrave;',
		'&Auml;',
		'&Eacute;',
		'&Euml;',
		'&Igrave;',
		'&Oacute;',
		'&Oslash;',
		'&THORN;',
		'&Ugrave;',
		'&aacute;',
		'&agrave;',
		'&auml;',
		'&ecirc;',
		'&euml;',
		'&igrave;',
		'&oacute;',
		'&oslash;',
		'&szlig;',
		'&ucirc;',
		'&yacute;',
		'&lt;',
		'&amp;',
		'&iexcl;',
		'&yen;',
		'&uml;',
		'&laquo;',
		'&reg;',
		'&plusmn;',
		'&acute;',
		'&middot;',
		'&ordm;',
		'&frac12;',
		'&Aring;',
		'&Ccedil;',
		'&Ecirc;',
		'&Iacute;',
		'&Iuml;',
		'&Ocirc;',
		'&Otilde;',
		'&Uacute;',
		'&Uuml;',
		'&acirc;',
		'&aring;',
		'&ccedil;',
		'&egrave;',
		'&iacute;',
		'&iuml;',
		'&ocirc;',
		'&otilde;',
		'&thorn;',
		'&ugrave;',
		'&yuml;',
		'&gt;',
		'&quot;',
		'&Atilde;',
		'&ETH;',
		'&Egrave;',
		'&Icirc;',
		'&Ntilde;',
		'&Ograve;',
		'&Ouml;',
		'&Ucirc;',
		'&Yacute;',
		'&aelig;',
		'&atilde;',
		'&eacute;',
		'&eth;',
		'&icirc;',
		'&ntilde;',
		'&ograve;',
		'&ouml;',
		'&uacute;',
		'&uuml;',
		'&pound;',
		'&brvbar;',
		'&copy;',
		'&not;',
		'&macr;',
		'&sup2;',
		'&micro;',
		'&cedil;',
		'&raquo;',
		'&frac34;'
		);
		
		// ricontrollo anche i caratteri
		$arrayCaratteri = array(
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'<',
		'&',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'>',
		'"	"',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�',
		'�'
		);
		
		$arraySostituzioni = array(
		' ',
		'',
		'',
		'',
		'',
		'',
		'3',
		'',
		'1',
		'1-4',
		'',
		'',
		'',
		'c',
		'a',
		'a',
		'e',
		'e',
		'i',
		'o',
		'o',
		'p',
		'u',
		'a',
		'a',
		'a',
		'e',
		'e',
		'i',
		'o',
		'o',
		'b',
		'u',
		'y',
		'',
		'and',
		'',
		'y',
		'u',
		'',
		'',
		'',
		'',
		'',
		'',
		'1-2',
		'a',
		'c',
		'e',
		'i',
		'i',
		'o',
		'o',
		'u',
		'u',
		'a',
		'a',
		'c',
		'e',
		'i',
		'i',
		'o',
		'o',
		'p',
		'u',
		'y',
		'',
		'',
		'a',
		'd',
		'e',
		'i',
		'n',
		'o',
		'o',
		'u',
		'y',
		'ae',
		'a',
		'e',
		'd',
		'i',
		'n',
		'o',
		'o',
		'u',
		'u',
		'l',
		'',
		'c',
		'',
		'',
		'2',
		'm',
		'',
		'',
		'3-4'
		);

		// entities
		$clean_string = str_replace($arrayEntity, $arraySostituzioni, $clean_string);

		// caratteri speciali
		$clean_string = str_replace($arrayCaratteri, $arraySostituzioni, $clean_string);
	
		// separatore
		if (!$separator) $separator = "-";
		
		// spazi
		$regexp='/[\s]+/'; 
		$clean_string = preg_replace($regexp, $separator, $clean_string);
		
		// trattini
		$regexp='/[-]+/'; 
		$clean_string = preg_replace($regexp, $separator, $clean_string);

		
		// tutto il resto lo tolgo

		$clean_string=ereg_replace('[^0-9A-Z a-z._-]+','',$clean_string);
	
		return($clean_string);
	}


 


}

?>