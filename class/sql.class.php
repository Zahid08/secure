<?php

# sql.class.php

// funziona con le DB PEAR
// riceve dati dalle quickform

class sql {

	function PrimaryKey ($tabella) {
		global $db;

		$res=$db->query("show full columns from ".$tabella);
		
		$chiave_primaria='';
		while ($table_struct =& $res->fetchRow()) {
			if ($table_struct['Key']=="PRI" && $table_struct['Extra']=="auto_increment") return $table_struct["Field"];
		}
	
	} // end func PrimaryKey

	function prepareQuery ($tabella, $arrayValori, $tipo, $condizioni='') {
		
		global $db;

		$res=$db->query("show full columns from ".$tabella);
		
		$campi_tabella=array();
		$tipi_campi=array();
		$chiave_primaria='';
		while ($table_struct =& $res->fetchRow()) {
			$campi_tabella[] = $table_struct["Field"];
			$tipi_campi[] = $table_struct["Type"];
			// chiave
			if ($table_struct['Key']=="PRI" && $table_struct['Extra']=="auto_increment") $chiave_primaria=$table_struct["Field"];
		}

		// where
		$where='';
		//	inizializzo la condizione di aggiornamento
		if ( !empty($condizioni) ) {
			if ( is_array($condizioni) ) {
				reset($condizioni);
				$where = " where ";
				$and="";
				while ( list($chiave,$valore) = each($condizioni) ) {
					$where .= $chiave."='".addslashes(stripslashes(trim($valore)))."'";
					$and=" and ";
				}
			} elseif ( is_string($condizioni) ) {
				$condizioni = trim($condizioni);
				if ( substr($condizioni,0,5) != 'where' ) $where = " where ".$condizioni;
				else $where = $condizioni;
			}

		} else {
			if (!empty($chiave_primaria)) $where .= " where ".$chiave_primaria."='".addslashes(stripslashes(trim($arrayValori[$chiave_primaria])))."'";
		}

		//print_r($campi_tabella);


		foreach($campi_tabella as $key => $valore) {

			if (isset($arrayValori[$valore]) && $valore!=$chiave_primaria) {

				$elenco_campi .= $valore.",";
				$value_to_insert=$this->prepareField($arrayValori[$valore],$tipi_campi[$key]);
				$elenco_valori .= "'".addslashes(stripslashes(trim($value_to_insert)))."',";
				$elenco_campi_update .= $valore."= '".addslashes(stripslashes(trim($value_to_insert)))."',";

			}
		}

		$elenco_campi = substr($elenco_campi,0,-1);
		$elenco_valori = substr($elenco_valori,0,-1);
		$elenco_campi_update = substr($elenco_campi_update,0,-1);

		switch($tipo) {

			case "insert":

				$querysql = "INSERT INTO ".$tabella." (".$elenco_campi.") VALUES (".$elenco_valori.")";
				
			break;

			case "update":

				$querysql = "UPDATE ".$tabella." SET ".$elenco_campi_update.$where;

			break;
			
			case "delete":

				$querysql = "delete from ".$tabella.$where;

			break;

		} // end switch

		return($querysql);


	} // end func prepareQuery


	function prepareField ($valore, $tipo) {
		
		$tipo=strtolower($tipo);
		
		if (strstr($tipo, 'int')) $tipo='int';
		if (strstr($tipo, 'float')) $tipo='float';
		
		switch ($tipo) {
			
			case "int":
				//if (is_int($valore)) return $valore; else return 'errore';
				return intval($valore);
			break;

			case "float":
				return floatval($valore);
			break;

			default:
				
				return $valore;

		} // end switch 

	} // end func prepareField


	function sqlPage($query,$records_pp=20,$pg,$link_extra_param,$nr_pg_per_view=10) {
	
		global $db, $view_links, $tot_record; 
		
		$view_links='';
		$ins_back = '&laquo;&nbsp;';
		$ins_next = '&nbsp;&raquo;';
		$tot_record=0;
		if ($query) {

			if ($link_extra_param) $agg_link_extra_param .= "&amp;".$link_extra_param;
			
			// eseguo la query
			///echo '<!--'.$query.' -->';
			$resSql=$db->query($query);
			//echo '<!--';
			//var_dump($resSql);
			//echo ' -->';
			$tot_record = $resSql->numRows();

			if (!$tot_record) $pgs = 1;
			else $pgs = ceil($tot_record / $records_pp);

			if ($pgs <= $nr_pg_per_view) {
				$pg_start = 1;
				$pg_end = $pgs;
			} else {

				if ((!$pg) || ($pg < ($nr_pg_per_view / 2))) $pg_start = 1;
				else {
					if ($pg < ($pgs - $nr_pg_per_view))  $pg_start = $pg - ($nr_pg_per_view / 2);
					else $pg_start = $pgs - $nr_pg_per_view;
				}
				$pg_end = $nr_pg_per_view + $pg_start;
				
			}

			for ($i = $pg_start; $i <= $pg_end; $i++) {

				if (!$pg) $pg = 1;

				$begin = ($records_pp * ($pg - 1));

				if ($pg == 1) {
					$n = $pg + 1;
					$next = '&nbsp;<a href="?pg='.$n.$agg_link_extra_param.'" title="'.$ins_next.'">'.$ins_next.'</a>&nbsp;';
					$back = "&nbsp;";
				} elseif (($pg > 1) && ($pg < $pgs)) {
					$n = $pg + 1;
					$b = $pg - 1;
					$next = '&nbsp;<a href="?pg='.$n.$agg_link_extra_param.'" title="'.$ins_next.'">'.$ins_next.'</a>&nbsp;';
					$back = '&nbsp;<a href="?pg='.$b.$agg_link_extra_param.'" title="'.$ins_back.'">'.$ins_back.'</a>&nbsp;';
				} else {
					$b = $pg - 1;
					$next = "&nbsp;";
					$back = '&nbsp;<a href="?pg='.$b.$agg_link_extra_param.'" title="'.$ins_back.'">'.$ins_back.'</a>&nbsp;';
				}

				if ($i == $pg) $view_page .= '&nbsp;['.$i.']&nbsp;';	
				else $view_page .= '&nbsp;<a href="?pg='.$i.$agg_link_extra_param.'" title="'.$i.'">'.$i.'</a>&nbsp;';

			}

			if ($pg == 1) $query .= " LIMIT 0,".$records_pp;
			else $query .= " LIMIT ".$begin.",".$records_pp;
				
			if ($pgs>1) $view_links = $back.'&nbsp;'.$view_page.'&nbsp;'.$next;
			else $view_links = "&nbsp;";
				
			$resSql=$db->query($query);
			return $resSql;

		} // end if
		else echo "<script language=\"javascript\">alert(\"Attenzione, manca la query. \\nErrore. Fimp01\")</script>";

	} // end func sqlPage


} // end class sql

?>