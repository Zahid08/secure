<?php

class caribbean {

    private $newAccountNumeration = 925000;
    private $lastOldAccountId = 390;

    /**
     * 
     * @param type $db injection del db
     * @param type $sql injection del $sql
     * @param type $goPage injection del $goPage
     */
    public function __construct($db, $sql, $goPage) {
        $this->db = $db;
        $this->sql = $sql;
        $this->goPage = $goPage;
    }

    public function setPath_www($path_www) {
        $this->path_www = $path_www;
    }

    /**
     * ALE BALDUCCI 13/03/2012 NON RITORNA UN ARRAY!! MA SOLO UN RECORD
     * @author ALE BALDUCCI <alessandro.balducci@titanka.com>
     * @param type $id_cliente
     * @return type
     */
    function getContoDataCli($id_cliente) {

        $res = $this->db->query("select * from conti where id_cliente = " . $id_cliente);
        $output = $res->fetchRow();
        return $output;
    }

    /**
     * ALE BALDUCCI 13/03/2012 NON RITORNA UN ARRAY!! MA SOLO UN RECORD
     * @author ALE BALDUCCI <alessandro.balducci@titanka.com>
     * @param type $id_conto
     * @return type
     */
    function getDepositoDataCli($id_conto) {

        $res = $this->db->query("select * from investimenti where id_conto = " . $id_conto);
        $output = $res->fetchRow();
        return $output;
    }

    /*
      ALE BALDUCCI 13/3/2012 VISUALIZZA SOLO UN DEPOSITO .. NON SCORRE UN VERO ARRAY
      function getInteressiClienti($id_cliente){


      $cData = $this->getContoDataCli($id_cliente);
      if($cData['id_conto']){
      $valuta = $this->getValuta($cData['id_valuta']);
      $dData = $this->getDepositoDataCli($cData['id_conto']);
      }

      //intestazioni
      $output = '<table style="width:100%"><tr>
      <td><strong>Type</strong></td>
      <td><strong>Currency</strong></td>
      <td><strong>Owner</strong></td>
      <td><strong>Account number/Deposit</strong></td>
      <td><strong>Activation date</strong></td>
      <td><strong>Balance</strong></td>
      <td><strong>Rate applied</strong></td>
      <td><strong>Quote Interests</strong></td>
      </tr>';

      //recupero interessi per eventuali conti
      if($cData && is_array($cData) ){
      $qry = "select * from interessi_correnti where tipo = 'C' and id_conto_investimento = ".$cData['id_conto'];
      $res = $this->db->query($qry);
      while($rowConti = $res->fetchRow()){
      $output .='<tr>
      <td>Account</td>
      <td>'.$valuta.'</td>
      <td>'.$cData['intestatario'].'</td>
      <td>'.$cData['numero_conto'].'</td>
      <td>'.$cData['data_attivazione'].'</td>
      <td>'.$cData['saldo_attuale'].'</td>
      <td>'.$rowConti['tasso_applicato'].'%</td>
      <td>'.$rowConti['ammontare_corrente'].'</td>
      </tr>';
      }
      }else{
      $output .='<tr><td colspan="7">No information found</td></tr>';
      }

      //recupero interessi per eventuali depositi
      if($dData && is_array($dData) ){
      $qry = "select * from interessi_correnti where tipo = 'D' and id_conto_investimento = ".$dData['id_investimento'];
      $res = $this->db->query($qry);
      while($rowDepositi = $res->fetchRow()){
      $valutaDep = $this->getValuta($dData['id_valuta']);
      $output .='<tr>
      <td>Deposit</td>
      <td>'.$valutaDep.'</td>
      <td>'.$dData['intestatario'].'</td>
      <td>'.$dData['numero_investimento'].'</td>
      <td>'.$dData['data_attivazione'].'</td>
      <td>'.$dData['saldo_attuale'].'</td>
      <td>'.$rowDepositi['tasso_applicato'].'%</td>
      <td>'.$rowDepositi['ammontare_corrente'].'</td>
      </tr>';
      }
      }else{
      $output .='<tr><td colspan="7">No information</td></tr>';
      }

      $output .='</table>';
      return $output;
      }


      function aggiornoContoNoInteressi($id_deposito){

      $res=$this->db->query("select id_conto, saldo_iniziale from investimenti where id_investimento = ".$id_deposito);
      $row = $res->fetchRow();
      $conto = $row['id_conto'];
      $saldoIniziale = $row['saldo_iniziale'];

      $res=$this->db->query("update conti set saldo_attuale = saldo_attuale + ".$saldoIniziale." where id_conto = ".$conto);

      }
     */

    /*
      originario ( errori sql )
      function fineDeposito($id,$tipo,$idC){


      //accredito interesse maturato nel deposito (investimento)

      //recupero la quota da versare
      $res=$this->db->query("select ammontare_corrente from interessi_correnti where tipo = '".$tipo."' and id_conto_investimento = ".$idC." and id = ".$id);
      $row = $res->fetchRow();
      $quotaMese = $row['ammontare_corrente'];

      //aggiorno saldo del DEPOSITO
      $res=$this->db->query("update investimenti set saldo_attuale = saldo_attuale + ".$quotaMese.", saldo_iniziale = saldo_iniziale + ".$quotaMese." where id_conto = ".$idC);

      //accredito del saldo del deposito nel conto corrente
      $res=$this->db->query("select * from investimenti where id_conto= ".$idC);
      $row = $res->fetchRow();
      $saldoDeposito = $row['saldo_attuale'];
      $conto = $row['id_conto'];
      $res=$this->db->query("update conti set saldo_attuale = saldo_attuale + ".$saldoDeposito." where id = ".$conto);

      //eliminazione del deposito (attivo = 0)
      $res=$this->db->query("update investimenti set attivo = 0 where id = ".$idC);
      //eliminazione dell'interesse ad esso correlato (attivo = 0)
      $res=$this->db->query("update interessi_correnti set attivo = 0 where id = ".$id);
      }
     */

//********************************* FUNZIONI PROCEDURA BATCH ********************************************************

    function fineDeposito($idInteressi, $tipo, $idInvestimento) {


        /*         * ********* FUNZIONAMENTO (ale balducci 17/02/2012) **************************************************
          INTERESSI --> DEPOSITO --> CONTO
          1) Determino l'ammontare corrente degli interessi per l'investimento (deposito) corrente
          2) Aggiorno il saldo attuale ed iniziale dell'investimento sommandogli l'ammontare corrente di cui al punto 1)
          3) Al saldo attuale del conto legato all'investimento sommo il saldo attuale dell'investimento come calcolato al punto 2)
          4) Disattivo l'investimento
          5) Disattivo il record interessi di partenza
         * **************************************************************************************************** */

        //1) recupero la quota da versare
        $res = $this->db->query("select ammontare_corrente from interessi_correnti where  id = " . $idInteressi); //where tipo = '".$tipo."' and id_conto_investimento = ".$idInvestimento."  ale balducci 18/06/2013        
        $row = $res->fetchRow();
        $quotaMese = $row['ammontare_corrente'];

        //2) aggiorno saldo del DEPOSITO
        $res = $this->db->query("update investimenti set saldo_attuale = saldo_attuale + " . $quotaMese . ", saldo_iniziale = saldo_iniziale + " . $quotaMese . " where id_investimento = " . $idInvestimento);

        /*                 $sql = "update investimenti set saldo_attuale = saldo_attuale + ".$quotaMese.", saldo_iniziale = saldo_iniziale + ".$quotaMese." where id_investimento = ".$idInvestimento;
          echo '<br>fineDeposito1 : ' . $sql;
         */

        //3) accredito del saldo del deposito nel conto corrente (rileggo i dati appena sopra aggiornati)
        $res = $this->db->query("select * from investimenti where id_investimento= " . $idInvestimento);
        $row = $res->fetchRow();
        $saldoDeposito = $row['saldo_attuale'];
        $id_conto = $row['id_conto'];

        $res = $this->db->query("update conti set saldo_attuale = saldo_attuale + " . $saldoDeposito . " where id_conto = " . $id_conto);

        /* $sql = "update conti set saldo_attuale = saldo_attuale + ".$saldoDeposito." where id_conto = ".$id_conto;
          echo '<br>fineDeposito2 : ' . $sql;
         */

        //4) eliminazione del deposito (attivo = 0) 
        $res = $this->db->query("update investimenti set attivo = 0 where id_investimento = " . $idInvestimento);
        /*     $sql = "update investimenti set attivo = 0 where id_investimento = ".$idInvestimento;
          echo '<br>fineDeposito3 : ' . $sql;
         */


        //5) eliminazione dell'interesse ad esso correlato (attivo = 0)
        $res = $this->db->query("update interessi_correnti set attivo = 0 where id = " . $idInteressi);
        /*                 $sql = "update interessi_correnti set attivo = 0 where id = ".$idInteressi;
          echo '<br>fineDeposito4 : ' . $sql;
         */
    }

    function aggiornaSaldiConto($id, $tipo, $idConto) {


        /*         * ********* FUNZIONAMENTO (ale balducci 17/02/2012) **************************************************
          INTERESSI --> CONTO
          1) Determino l'ammontare corrente degli interessi per il CONTO in questione
          2) Aggiorno il saldo attuale del CONTO sommandogli l'ammontare corrente di cui al punto 1)
          3) Azzero l'ammontare corrente del record interessi collegato al conto in questione
         * **************************************************************************************************** */

        //1) recupero la quota da versare
        $sql = "select ammontare_corrente from interessi_correnti where id = " . $id;
        //." AND tipo = '".$tipo."' and id_conto_investimento = ".$idConto."  "; ALE 18/06/2013
        $res = $this->db->query($sql);
        $row = $res->fetchRow();
        $quotaMese = $row['ammontare_corrente'];

        //2) aggiorno saldo del conto
        $res = $this->db->query("update conti set saldo_aggiornato=1 , saldo_attuale = saldo_attuale + " . $quotaMese . " where id_conto = " . $idConto);


        //3) reimposto l'ammontare
        $res = $this->db->query("update interessi_correnti set ammontare_corrente = 0 where id= " . $id);
        //tipo = '".$tipo."' and id_conto_investimento = ".$idConto." and ALE BALDUCCI 18/06/2013 NON SERVE A UNA CIPPA
    }

    function accreditaQuota($idInteressi, $tipo, $idT, $quota) {

        $res = $this->db->query("update interessi_correnti set ammontare_corrente = ammontare_corrente + " . $quota . " where id= " . $idInteressi); //where tipo = '".$tipo."' and id_conto_investimento = ".$idT."  ale balducci
    }

    function getT($idRes) {

        $res = $this->db->query("select int_annuo from tipi_conti where id_tipo_conto = " . $idRes);
        $row = $res->fetchRow();
        return $row['int_annuo'];
    }

    function cambiaTasso($idInteressi, $tipo, $idConto) {


        /*         * ********* FUNZIONAMENTO (ale balducci 17/02/2012) **************************************************
          CONTI --> TIPI_CONTI --> INTERESSI
          1) Determino il tipo conto del Conto in questione
          2) Determino il tasso interesse annuo relativo al tipo conto di cui punto 1)
          3) Aggiorno il tasso applicato nel record interessi in question
         * **************************************************************************************************** */
        switch ($tipo) {
            case 'C':


                $res = $this->db->query("select id_tipo_conto from conti where id_conto = " . $idConto);
                $row = $res->fetchRow();

                if ($row['id_tipo_conto']) {
                    $nuovoTasso = $this->getT($row['id_tipo_conto']); // va a pescare l'int. annuo 
                    $res = $this->db->query("update interessi_correnti set tasso_applicato = " . $nuovoTasso . " where id = " . $idInteressi); //id_conto_investimento =".$idConto." and tipo ='".$tipo."' and  ale balduccci 18/06/2013
                    //questo serve perche' va eseguito solo 1 volta ovvero dopo il 3 mese dall'attivazione (vd. il batch)
                    $res = $this->db->query("update conti set tasso_cambiato=1 where id_conto = " . $idConto);
                }

                break;
        }
    }

//****************************************** FINE FUNZIONI BATCH *******************************************************/    



    function getTimeStamp($data) {
        $temp = explode(' ', $data);
        $miadata = explode('-', $temp[0]);
        $miaora = explode(':', $temp[1]);
        $output = mktime($miaora[0], $miaora[1], $miaora[2], $miadata[1], $miadata[2], $miadata[0]);
        return $output;
    }

    function getInfoConto($tipo, $id) {

        switch ($tipo) {
            case 'C':
                $q = "select * from conti where attivo = 1 and stato = 'A' and id_conto = " . $id;
                $res = $this->db->query($q);
                $row = $res->fetchRow();
                break;
            case 'D':
                $q = "select * from investimenti where attivo = 1 and stato = 'A' and id_investimento = " . $id;
                $res = $this->db->query($q);
                $row = $res->fetchRow();
                break;
        }
        return $row;
    }

    function getSaldoConto($id) {

        $q = "select saldo_attuale from conti where id_conto = " . $id;
        $res = $this->db->query($q);
        $row = $res->fetchRow();
        return $row['saldo_attuale'];
    }

    function getTassoConto($tipoConto) {


        $q = "SELECT int_3_mesi from tipi_conti where id_tipo_conto = " . $tipoConto;
        $res = $this->db->query($q);
        $row = $res->fetchRow();
        return $row['int_3_mesi'];
    }

    function getTasso($data, $id_valuta, $saldoIniziale, $lang) {

        $val = explode('|', $data);
        $descrizione = $val[0];
        $durata = $val[1];
        $id_tipo = $val[2];
        $valuta = $this->getValuta($id_valuta);

        //die($saldoIniziale);

        $q = "SELECT DISTINCT
rel_interessi_investimenti.tasso_" . strtolower($valuta) . ", rel_interessi_investimenti.minimo_deposito FROM rel_interessi_investimenti Inner Join tipi_investimenti_lingue ON tipi_investimenti_lingue.id_tipo_investimento = rel_interessi_investimenti.id_tipo_investimento WHERE rel_interessi_investimenti.id_durata_minima =  '" . $durata . "' AND tipi_investimenti_lingue.sigla_lingua =  '" . $lang . "' AND tipi_investimenti_lingue.descrizione =  '" . $descrizione . "'";

        //die($q);

        $res = $this->db->query($q);

        while ($row = $res->fetchRow()) {

            if ($saldoIniziale >= $row['minimo_deposito']) {
                $vEffettiva = $row['tasso_' . strtolower($valuta)];
            }
        }
        //die($vEffettiva);
        return $vEffettiva;
    }

    function getAttivazione($id) {

        $q = "select data_attivazione from investimenti where id_investimento = " . $id;
        $res = $this->db->query($q);
        $row = $res->fetchRow();
        return $row['data_attivazione'];
    }

    function getAttivazioneConto($id) {

        $q = "select data_attivazione from conti where id_conto = " . $id;
        $res = $this->db->query($q);
        $row = $res->fetchRow();
        return $row['data_attivazione'];
    }

    function getInvestimentoNome($id, $lang) {

        $res = $this->db->query("select nome_tipo from tipi_investimenti_lingue where id_tipo_investimento = " . $id . " and sigla_lingua = '" . $lang . "' ");
        $row = $res->fetchRow();
        return $row['nome_tipo'];
    }

    function getContoNome($id, $lang) {

        $res = $this->db->query("select nome_tipo from tipi_conti_lingue where id_tipo_conto = " . $id . " and sigla_lingua = '" . $lang . "' ");
        $row = $res->fetchRow();
        return $row['nome_tipo'];
    }

    function getDurate() {

        $res = $this->db->query("select * from durate_investimenti");
        $output = array();
        while ($row = $res->fetchRow()) {
            $output[$row['valore']] = $row['descrizione'];
        }
        return $output;
    }

    /**
      function getLingue()
     */
    function getLingue() {



        $qry = "SELECT * FROM lingue order by ordinamento";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();

        while ($row = & $res->fetchRow()) {
            $record[$row['sigla_lingua']] = $row;
        }


        return $record;
    }

// end func getLingue

    /**
      function getValute()
     */
    function getValute($attivo = false, $vuoto = true) {



        //$qry = "SELECT * FROM valute".($attivo==true?" where attivo=1":'')." order by ordinamento"; ale balducci 05/07/2011
        $qry = "SELECT * FROM valute where attivo=1 order by ordinamento";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();

        if ($vuoto)
            $record[''] = '';
        while ($row = & $res->fetchRow()) {
            $record[$row['id_valuta']] = $row['nome_valuta'];
        }


        return $record;
    }

// end func getValute
    //func getValuta
    function getValuta($id) {



        $qry = "SELECT * from valute where id_valuta = " . $id;
        $res = $this->db->query($qry);
        $row = & $res->fetchRow();

        return $row['nome_valuta'];
    }

// end func getValuta

    /**
      function getPaesi()
     */
    function getPaesi($vuoto = true) {



        $qry = "SELECT * FROM paesi order by nome_paese";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();
        if ($vuoto)
            $record[''] = '';
        while ($row = & $res->fetchRow()) {
            $record[$row['codice_iso2']] = $row['nome_paese'];
        }


        return $record;
    }

// end func getPaesi

    /**
      function getClienti()
     */
    function getClienti($attivo = false) {



        $qry = "SELECT * FROM clienti where 1 = 1 " . ($attivo == true ? " and attivo=1" : '') . " and data_attivazione is not null order by cognome, nome, ragione_sociale";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();

        $record[''] = '';
        while ($row = & $res->fetchRow()) {
            $record[$row['id_cliente']] = $row['cognome'] . ' ' . $row['nome'] . (!empty($row['ragione_sociale']) ? ' - ' . $row['ragione_sociale'] : '');
        }


        return $record;
    }

// end func getClienti

    /**
      function getClienti()
     */
    function getConti($attivo = true) {



        $qry = "SELECT * FROM conti where 1 = 1 " . ($attivo == true ? " and attivo=1" : '') . " and stato = 'A' order by intestatario";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();

        $record[''] = '';
        while ($row = & $res->fetchRow()) {

            $valuta = $this->getValuta($row['id_valuta']);

            $record[$row['id_conto']] = $valuta . '| conto n. ' . $row['numero_conto'] . ' di: ' . $row['intestatario'];
        }


        return $record;
    }

// end func getClienti    

    /**
      function getClientiConti()
     */
    function getClientiConti($attivo = false) {



        $qry = "SELECT * FROM clienti as cli, conti as con where cli.id_cliente=con.id_cliente and con.attivo=1" . ($attivo == true ? " and cli.attivo=1" : '') . " and con.stato='A' group by cli.id_cliente order by cli.cognome, cli.nome, cli.ragione_sociale";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();

        $record[''] = '';
        while ($row = & $res->fetchRow()) {
            $record[$row['id_cliente']] = $row['cognome'] . ' ' . $row['nome'] . ' - ' . $row['ragione_sociale'] . '';
        }


        return $record;
    }

// end func getClientiConti

    function getContiClienti($attivo = true) {


        $optionValute = $this->getValute();

        $qry = "SELECT * FROM conti where (1=1)" . ($attivo == true ? " and stato='A' and attivo=1 and numero_conto is not null and numero_conto<>''" : '') . " order by numero_conto";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();

        $record[''] = '';
        while ($row = & $res->fetchRow()) {
            $record[$row['id_cliente']][''] = '---';
            //$record[$row['id_cliente']][$row['id_conto']]['']='---';
            $record[$row['id_cliente']][$row['id_conto']] = $row['numero_conto'] . ' (' . $optionValute[$row['id_valuta']] . ')';
        }

        return $record;
    }

// end func getContiClienti

    function getCarteClienti($attivo = true, $type = '') {


        $optionValute = $this->getValute();

        $qry = "SELECT * FROM carte where (1=1)" . ($attivo == true ? " and stato='A' and attivo=1 and numero_carta is not null and numero_carta<>''" : '') . (!empty($type) ? " and debito_credito = '" . $type . "'" : '') . " order by numero_carta";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();

        $record[''] = '';
        while ($row = & $res->fetchRow()) {
            $record[$row['id_cliente']][''][''] = '---';
            $record[$row['id_cliente']][$row['id_conto']][''] = '---';
            $record[$row['id_cliente']][$row['id_conto']][$row['id_carta']] = $row['numero_carta'] . '';
        }

        return $record;
    }

// end func getCarteClienti

    function getContiClientiPortfolio($attivo = true) {


        $optionValute = $this->getValute();

        $qry = "SELECT * FROM conti where abilita_prelievo=1 and abilita_deposito=1" . ($attivo == true ? " and stato='A' and attivo=1 and numero_conto is not null and numero_conto<>''" : '') . " order by numero_conto";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();

        $record[''] = '';
        while ($row = & $res->fetchRow()) {
            $record[$row['id_cliente']][$row['id_conto']] = $row['numero_conto'] . ' (' . $optionValute[$row['id_valuta']] . ')';
        }

        return $record;
    }

// end func getContiClientiPortfolio

    function getContiDati($attivo, $id_cliente = '') {


        $qry = "SELECT * FROM conti where (1=1)" . ($attivo == true ? " and attivo=1 and stato='A'" : '') . (!empty($id_cliente) ? " and id_cliente=" . $id_cliente : '') . " order by numero_conto";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();

        while ($row = & $res->fetchRow()) {
            $record[$row['id_conto']] = $row;
        }

        return $record;
    }

// end func getContiDati

    /**
      function getTypes()
     */
    function getTypes($tbl, $lingua, $attivo = true, $vuoto = true) {



        $PryKey = $this->sql->PrimaryKey($tbl);

        $qry = "SELECT * FROM " . $tbl . " as tab, " . $tbl . "_lingue as tab_lingue where tab." . $PryKey . "=tab_lingue." . $PryKey . "" . ($attivo == true ? " and tab.attivo=1" : '') . " and tab_lingue.sigla_lingua='" . $lingua . "' order by tab.ordinamento, tab_lingue.nome_tipo";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();
        if ($vuoto)
            $record[''] = '';

        while ($row = & $res->fetchRow()) {
            $record[$row[$PryKey]] = $row['nome_tipo'];
        }


        return $record;
    }

// end func getTypes

    function getInvestimentiDurate($lingua, $attivo = true, $vuoto = true) {





        $qry = "SELECT DISTINCT
tipi_investimenti_lingue.descrizione,
rel_interessi_investimenti.id_durata_minima,
rel_interessi_investimenti.id_tipo_investimento
FROM
tipi_investimenti_lingue
Inner Join rel_interessi_investimenti ON rel_interessi_investimenti.id_tipo_investimento = tipi_investimenti_lingue.id_tipo_investimento
WHERE
tipi_investimenti_lingue.sigla_lingua =  '" . $lingua . "'";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();
        if ($vuoto)
            $record[''] = '';

        while ($row = & $res->fetchRow()) {
            $record[$row['descrizione'] . '|' . $row['id_durata_minima'] . '|' . $row['id_tipo_investimento']] = $row['descrizione'] . ' della durata di: ' . ($row['id_durata_minima'] / 2592000) . ' mese/i';
        }


        return $record;
    }

// end func getTipiInvestimenti

    function getTipiInvestimenti($tbl, $lingua, $attivo = true, $vuoto = true) {



        $PryKey = $this->sql->PrimaryKey($tbl);

        $qry = "SELECT * FROM " . $tbl . " as tab, " . $tbl . "_lingue as tab_lingue where tab." . $PryKey . "=tab_lingue." . $PryKey . "" . ($attivo == true ? " and tab.attivo=1" : '') . " and tab_lingue.sigla_lingua='" . $lingua . "' order by tab.ordinamento, tab_lingue.nome_tipo";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();
        if ($vuoto)
            $record[''] = '';

        while ($row = & $res->fetchRow()) {
            $record[$row[$PryKey]] = $row;
        }


        return $record;
    }

// end func getTipiInvestimenti

    function generaChiave($length = 8) {
        /*
          $chiave = "";
          For($I=0;$I<$length;$I++){
          do{
          $N = Ceil(rand(48,122));
          } while(!((($N >= 48) && ($N <= 57)) || (($N >= 65) && ($N <= 90)) || (($N >= 97) && ($N <= 122))));

          $chiave = $chiave.Chr ($N);
          }
          return $chiave;
         */
        $template = "1234567890QWERTYUIPASDFGHJKLZXCVBNM";

        settype($length, "integer");
        settype($rndstring, "string");
        settype($a, "integer");
        settype($b, "integer");

        for ($a = 0; $a < $length; $a++) {
            $b = rand(0, strlen($template) - 1);
            $rndstring .= $template[$b];
        }

        return $rndstring;
    }

// end func generaChiave

    /**
     * ritorna il nuovo numero di conto
     * aggiornato alla nuova numerazione a partire da 925001 
     * @param type $id
     * @param type $dataora
     * @return string
     */
    public function generaNumeroConto($id, $dataora = FALSE) {
        //$this->lastOldAccountId;

        $query = "SELECT numero_conto 
            FROM conti 
            WHERE id_conto > ? AND numero_conto IS NOT NULL 
            ORDER BY numero_conto DESC LIMIT 1";
        $accountNumber = $this->db->getOne($query, array($this->lastOldAccountId));

        if (!$accountNumber || preg_match("/^2013(\d)+/", $accountNumber))
            $accountNumber = $this->newAccountNumeration;
        $accountNumber += 1;
        return $accountNumber;

//            $chiave = "";
//            list($data, $ora) = explode(" ", $dataora);
//            if (!empty($data))
//                list($Y, $m, $d) = preg_split("/[-\/.]/", $data);
//            $chiave = $Y . sprintf("%04d", $id);
//
//            return $chiave;
//        
    }

    /**
     * ritorna il nuovo numero di investimento
     * @param type $id
     * @param type $dataora
     * @return string
     */
    public function generaNumeroInvestimento($id, $dataora) {


        $chiave = "";
        list($data, $ora) = explode(" ", $dataora);
        if (!empty($data))
            list($Y, $m, $d) = preg_split("/[-\/.]/", $data);
        $chiave = $Y . sprintf("%04d", $id);

        return $chiave;
    }

// end func generaNumeroConto
    ### attivazioni

    function attivaPortfolio() {


        // invio email attivazione
        # dati cliente
        $qry = "select * from clienti as cli, conti as con, portfolio as por where cli.id_cliente=con.id_cliente and cli.id_cliente=por.id_cliente and con.id_conto=por.id_conto and por.id_portfolio=" . $_REQUEST['id_portfolio'];
        $res = $this->db->query($qry);
        $record = $res->fetchRow();

        $data = array();
        // modifica tabella portfolio
        $data['stato'] = 'A';
        $data['data_attivazione'] = date("Y-m-d H:i:s");

        $query = $this->sql->prepareQuery('portfolio', $data, 'update', "id_portfolio='" . $_REQUEST['id_portfolio'] . "'");
        //echo '<br />'.$query;
        $res = $this->db->query($query);

        ### inserisco il portfolio come transazione per averne traccia nell'estratto conto
        $data = array();
        $data['id_tipo_transazione'] = 6; // trade 
        //$data['id_tipo_commissione']=0;

        $data['id_valuta'] = $record['id_valuta'];
        $data['importo_transazione'] = $record['numero_quote'] * $record['valore_quota_iniziale'];

        $optionInvestimenti = $this->getTipiInvestimenti('tipi_investimenti', $_SESSION['lingua'], $GLOBALS['stato_opzioni'], false);

        $data['descrizione'] = $optionInvestimenti[$record['id_tipo_investimento']]['nome_tipo'] . ' ' . $optionInvestimenti[$record['id_tipo_investimento']]['descrizione'];
        $data['data_richiesta'] = $record['data_richiesta'];
        $data['data_attivazione'] = date("Y-m-d H:i:s");
        $data['data_transazione'] = date("Y-m-d H:i:s");
        $data['id_cliente_da'] = $record['id_cliente'];
        $data['id_conto_da'] = $record['id_conto'];
        //$data['id_cliente_a']=0;
        //$data['id_conto_a']=0;
        //$data['id_gestione_offshore']=0;
        $data['stato'] = 'A';
        $data['id_tipo_investimento'] = $record['id_tipo_investimento'];
        $data['valore_quota'] = $record['valore_quota_iniziale'];
        $data['numero_quote'] = $record['numero_quote'];
        $data['buy_sell'] = 'B';

        $query = $this->sql->prepareQuery('transazioni', $data, 'insert');
        //echo '<br />'.$query;
        $res = $this->db->query($query);

      
        // salvo la comunicazione in db
        $data = array();
        $data['id_cliente_mittente'] = 1; // admin
        $data['id_cliente_destinatario'] = $record['id_cliente'];
        $data['email_mittente'] = $param['from'];
        $data['email_destinatario'] = $param['to'];
        $data['oggetto_messaggio'] = $param['subject'];
        $data['testo_messaggio'] = $dati;
        $data['stato_messaggio_mittente'] = 'I';
        $data['stato_messaggio_destinatario'] = 'N';
        $data['sigla_lingua'] = $_SESSION['lingua'];
        $data['data_creazione_messaggio'] = date("Y-m-d H:i:s");
        $data['data_invio_messaggio'] = date("Y-m-d H:i:s");
        $data['notifica_automatica'] = 1;
        $data['notifica_email'] = 1;
        $query = $this->sql->prepareQuery('messaggi', $data, 'insert');
        //echo '<br />'.$query;
        //    $res=$this->db->query($query);
        # insert
        $query = $this->sql->prepareQuery('transazioni', $dataTrans, 'insert');
        //echo '<br />'.$query;
        $res = $this->db->query($query);

        $this->goPage->alertgo(ATTIVAZIONE_OK_PORTFOLIO, 'index.php?page=' . (!empty($_REQUEST['pageR']) ? $_REQUEST['pageR'] . '&request_type=' . $_REQUEST['request_type'] : $_REQUEST['page']) . '&act=list');
    }

// end func attivaPortfolio

    function attivaDeposito() {

        ;
        $data = array();

        // modifica tabella
        $data['stato'] = 'A';
        $data['data_attivazione'] = date("Y-m-d H:i:s");
        $query = $this->sql->prepareQuery('investimenti', $data, 'update', "id_investimento='" . $_REQUEST['id_investimento'] . "'");
        //echo '<br />'.$query;
        $res = $this->db->query($query);

        $this->db->query("update interessi_correnti set data_attivazione = '" . $data['data_attivazione'] . "' where tipo = 'C' and id_conto_investimento='" . $_REQUEST['id_investimento'] . "'");

        // invio email attivazione
        # dati cliente
        $qry = "select * from clienti as cli, investimenti as invest where cli.id_cliente=" . $_REQUEST['id_cliente'] . " and invest.id_investimento=" . $_REQUEST['id_investimento'];
        $res = $this->db->query($qry);
        $record = $res->fetchRow();

        // salvo la comunicazione in db
        $data = array();
        $data['id_cliente_mittente'] = 1; // admin
        $data['id_cliente_destinatario'] = $record['id_cliente'];
        $data['email_mittente'] = $param['from'];
        $data['email_destinatario'] = $param['to'];
        $data['oggetto_messaggio'] = $param['subject'];
        $data['testo_messaggio'] = $dati;
        $data['stato_messaggio_mittente'] = 'I';
        $data['stato_messaggio_destinatario'] = 'N';
        $data['sigla_lingua'] = $_SESSION['lingua'];
        $data['data_creazione_messaggio'] = date("Y-m-d H:i:s");
        $data['data_invio_messaggio'] = date("Y-m-d H:i:s");
        $data['notifica_automatica'] = 1;
        $data['notifica_email'] = 1;
        $query = $this->sql->prepareQuery('messaggi', $data, 'insert');
        //echo '<br />'.$query;
        //    $res=$this->db->query($query);

        $this->goPage->alertgo(ATTIVAZIONE_OK_CONTO, 'index.php?page=' . (!empty($_REQUEST['pageR']) ? $_REQUEST['pageR'] . '&request_type=' . $_REQUEST['request_type'] : $_REQUEST['page']) . '&act=list');
    }

// end func attivaDeposito

    function attivaConto() {


        $resSaldo = $this->db->query("select saldo_iniziale from conti where id_conto = " . $_REQUEST['id_conto']);
        $rowSaldo = $resSaldo->fetchRow();

        $data = array();

        // modifica tabella
        $data['stato'] = 'A';
        $data['data_attivazione'] = date("Y-m-d H:i:s");
        $data['saldo_attuale'] = $rowSaldo['saldo_iniziale'];
        $query = $this->sql->prepareQuery('conti', $data, 'update', "id_conto='" . $_REQUEST['id_conto'] . "'");
        //echo '<br />'.$query;
        $res = $this->db->query($query);


        //query=$this->sql->prepareQuery ('interessi_correnti', $data['data_attivazione'], 'update', "tipo = 'C' and id_conto_investimento='".$_REQUEST['id_conto']."'");
        //echo '<br />'.$query;
        $this->db->query("update interessi_correnti set data_attivazione = '" . $data['data_attivazione'] . "' where tipo = 'C' and id_conto_investimento='" . $_REQUEST['id_conto'] . "'");
        //$res=$this->db->query($query);
        // invio email attivazione
        # dati cliente
        $qry = "select * from clienti as cli, conti as con where cli.id_cliente=con.id_cliente and con.id_conto=" . $_REQUEST['id_conto'];
        $res = $this->db->query($qry);
        $record = $res->fetchRow();



        // salvo la comunicazione in db
        $data = array();
        $data['id_cliente_mittente'] = 1; // admin
        $data['id_cliente_destinatario'] = $record['id_cliente'];
        $data['email_mittente'] = $param['from'];
        $data['email_destinatario'] = $param['to'];
        $data['oggetto_messaggio'] = $param['subject'];
        $data['testo_messaggio'] = $dati;
        $data['stato_messaggio_mittente'] = 'I';
        $data['stato_messaggio_destinatario'] = 'N';
        $data['sigla_lingua'] = $_SESSION['lingua'];
        $data['data_creazione_messaggio'] = date("Y-m-d H:i:s");
        $data['data_invio_messaggio'] = date("Y-m-d H:i:s");
        $data['notifica_automatica'] = 1;
        $data['notifica_email'] = 1;
        $query = $this->sql->prepareQuery('messaggi', $data, 'insert');
        //echo '<br />'.$query;
        //$res=$this->db->query($query);

        $this->goPage->alertgo(ATTIVAZIONE_OK_CONTO, 'index.php?page=' . (!empty($_REQUEST['pageR']) ? $_REQUEST['pageR'] . '&request_type=' . $_REQUEST['request_type'] : $_REQUEST['page']) . '&act=list');
    }

// end func attivaConto

    function attivaCarta() {


        $data = array();

        // modifica tabella
        $data['stato'] = 'A';
        $data['data_attivazione'] = date("Y-m-d H:i:s");
        $query = $this->sql->prepareQuery('carte', $data, 'update', "id_carta='" . $_REQUEST['id_carta'] . "'");
        //echo '<br />'.$query;
        $res = $this->db->query($query);

        // invio email attivazione
        # dati cliente
        $qry = "select * from clienti as cli, conti as con, carte as car where cli.id_cliente=con.id_cliente and cli.id_cliente=car.id_cliente and con.id_conto=car.id_conto and car.id_carta=" . $_REQUEST['id_carta'];
        $res = $this->db->query($qry);
        $record = $res->fetchRow();

 

        // salvo la comunicazione in db
        $data = array();
        $data['id_cliente_mittente'] = 1; // admin
        $data['id_cliente_destinatario'] = $record['id_cliente'];
        $data['email_mittente'] = $param['from'];
        $data['email_destinatario'] = $param['to'];
        $data['oggetto_messaggio'] = $param['subject'];
        $data['testo_messaggio'] = $dati;
        $data['stato_messaggio_mittente'] = 'I';
        $data['stato_messaggio_destinatario'] = 'N';
        $data['sigla_lingua'] = $_SESSION['lingua'];
        $data['data_creazione_messaggio'] = date("Y-m-d H:i:s");
        $data['data_invio_messaggio'] = date("Y-m-d H:i:s");
        $data['notifica_automatica'] = 1;
        $data['notifica_email'] = 1;

        $query = $this->sql->prepareQuery('messaggi', $data, 'insert');
        //echo '<br />'.$query;
        //    $res=$this->db->query($query);

        $this->goPage->alertgo(ATTIVAZIONE_OK_CARTA, 'index.php?page=' . (!empty($_REQUEST['pageR']) ? $_REQUEST['pageR'] . '&request_type=' . $_REQUEST['request_type'] : $_REQUEST['page']) . '&act=list');
    }

// end func attivaCarta

    function attivaMovimentoCarta() {


        $data = array();

        // modifica tabella
        $data['stato'] = 'A';
        $data['data_attivazione'] = date("Y-m-d H:i:s");
        $query = $this->sql->prepareQuery('movimenti_carte', $data, 'update', "id_movimento='" . $_REQUEST['id_movimento'] . "'");
        //echo '<br />'.$query;
        $res = $this->db->query($query);

       
    }

// end func attivaMovimentoCarta

    function attivaTransazione() {


        $data = array();

        // modifica tabella
        $data['stato'] = 'A';
        $data['data_attivazione'] = date("Y-m-d H:i:s");
        $data['data_transazione'] = $data['data_attivazione'];
        $query = $this->sql->prepareQuery('transazioni', $data, 'update', "id_transazione='" . $_REQUEST['id_transazione'] . "'");
        //echo '<br />'.$query;
        $res = $this->db->query($query);

        // invio email attivazione
        # dati
        $qry = "select * from transazioni where id_transazione=" . $_REQUEST['id_transazione'];
        $res = $this->db->query($qry);
        $record = $res->fetchRow();


        $optionClienti = $this->getClienti(false);
        $optionConti = $this->getContiDati(false);
        $optionValute = $this->getValute(true, false);
        $optionTransazioni = $this->getTypes('tipi_transazioni', $_SESSION['lingua'], true);
 

          $param['from']=$GLOBALS['mail_admin'];
         
        ### cliente da        
        $qry2 = "select * from clienti where id_cliente = " . $record['id_cliente_da'];
        $res2 = $this->db->query($qry2);
        $record2 = $res2->fetchRow();
        /*
          $param['to'] = $record2['email'];
          $param['subject']=$titolo;

          $param['text']='';

          $sendmail = new sendMail();
          $sendmail->testa = $testa.$msg;
          $sendmail->piede = $piede;
          $sendmail->files = $file;
          $sendmail->set(html, true);
          $sendmail->getParams($param);
          $sendmail->parseBody();
          $sendmail->setHeaders();
          //$sendmail->send(); */

        // salvo la comunicazione in db
        $data = array();
        $data['id_cliente_mittente'] = 1; // admin
        $data['id_cliente_destinatario'] = $record2['id_cliente'];
        $data['email_mittente'] = $param['from'];
        $data['email_destinatario'] = $param['to'];
        $data['oggetto_messaggio'] = $param['subject'];
        $data['testo_messaggio'] = $dati;
        $data['stato_messaggio_mittente'] = 'I';
        $data['stato_messaggio_destinatario'] = 'N';
        $data['data_creazione_messaggio'] = date("Y-m-d H:i:s");
        $data['data_invio_messaggio'] = date("Y-m-d H:i:s");
        $data['notifica_automatica'] = 1;
        $data['notifica_email'] = 1;
        $data['sigla_lingua'] = $_SESSION['lingua'];
        //    $query=$this->sql->prepareQuery ('messaggi', $data, 'insert');
        //echo '<br />'.$query;
        //    $res=$this->db->query($query);
        # cliente A
        if ($record['id_cliente_da'] != $record['id_cliente_a'] && !empty($record['id_cliente_a'])) {
            $qry2 = "select * from clienti where id_cliente = " . $record['id_cliente_a'];
            $res2 = $this->db->query($qry2);
            $record2 = $res2->fetchRow();

            /* $param['to'] = $record2['email'];
              $param['subject']=$titolo;

              $param['text']='';

              $sendmail = new sendMail();
              $sendmail->testa = $testa.$msg;
              $sendmail->piede = $piede;
              $sendmail->files = $file;
              $sendmail->set(html, true);
              $sendmail->getParams($param);
              $sendmail->parseBody();
              $sendmail->setHeaders();
              //$sendmail->send(); */

            // salvo la comunicazione in db
            $data = array();
            $data['id_cliente_mittente'] = 1; // admin
            $data['id_cliente_destinatario'] = $record2['id_cliente'];
            $data['email_mittente'] = $param['from'];
            $data['email_destinatario'] = $param['to'];
            $data['oggetto_messaggio'] = $param['subject'];
            $data['testo_messaggio'] = $dati;
            $data['stato_messaggio_mittente'] = 'I';
            $data['stato_messaggio_destinatario'] = 'N';
            $data['sigla_lingua'] = $_SESSION['lingua'];
            $data['data_creazione_messaggio'] = date("Y-m-d H:i:s");
            $data['data_invio_messaggio'] = date("Y-m-d H:i:s");
            $data['notifica_automatica'] = 1;
            $data['notifica_email'] = 1;
            $query = $this->sql->prepareQuery('messaggi', $data, 'insert');
            //echo '<br />'.$query;
            //    $res=$this->db->query($query);
        }

        $this->goPage->alertgo(ATTIVAZIONE_OK_TRANSAZIONE, 'index.php?page=' . $_REQUEST['page'] . '&=list&id_tipo_transazione=' . $record['id_tipo_transazione'] . '&title=' . $_SESSION['title']);
    }

// end func attivaTransazione

    function getCarte($id_cliente = NULL, $id_conto = NULL, $attivo = true) {


        $qry = "SELECT * FROM carte where (1=1)" . (!empty($id_cliente) ? " and id_cliente=" . $id_cliente : "") . (!empty($id_conto) ? " and id_conto=" . $id_conto : "") . ($attivo ? " and attivo = 1 and numero_carta<>''" : '') . " order by numero_carta";
        $res = $this->db->query($qry);

        // ... se si verifica un errore, lo scriviamo
        if (DB::isError($res)) {
            print "Attenzione! Si � verificato un errore durante l'esecuzione della query " . $qry . ".";
            die($res->getMessage());
        }

        $record = array();

        while ($row = & $res->fetchRow()) {
            $record[$row['id_carta']] = $row['numero_carta'];
        }


        return $record;
    }

// end func getCarte

    /**
     * crea il pdf dal testo passato
     * @param DOMPDF $dompdf
     * @param string $html
     * @param string $pathfile
     */
    function createpdf($dompdf, $html, $pathfile) {
 
        $dompdf->load_html($html);
        $dompdf->render();
        // The next call will store the entire PDF as a string in $pdf
        $pdf = $dompdf->output();
        // You can now write $pdf to disk, store it in a database or stream it
        // to the client.
 
        file_put_contents($pathfile, $pdf);
    }

// end func createpdf

    function PasswordCasuale($lunghezza = 6) {
        $caratteri_disponibili = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789";
        $password = "";
        for ($i = 0; $i < $lunghezza; $i++) {
            $password = $password . substr($caratteri_disponibili, rand(0, strlen($caratteri_disponibili) - 1), 1);
        }
        return $password;
    }

    function intero($valore) {

        if (!preg_match('/^([0-9])+$/i', $valore, $matches))
            return false;
        return true;
    }

// end func intero($valore)

    function doppione($email) {

        $sql_doppio = "select id_cliente from clienti where trim(email)='" . trim($_REQUEST['email']) . "' ";
        $res_doppio = $this->db->query($sql_doppio);
        $row_doppio = $res_doppio->fetchRow();

        return $row_doppio;
    }

    // ************ REPORT INTERESSI - CONTO - DEPOSITI ALL'APERTURA DEL BANKING *******************
    // funzioni ale balducci Marzo 2012
    function reportInteressi($id_cliente) {

        global $arrayImgStati;

        //$cData = $this->getContoDataCli($id_cliente);

        $res_conti = $this->db->query("select * from conti where attivo =1 and  id_cliente = " . $id_cliente . " order by id_conto ");
        $dati_cliente = $this->getInfoCliente($id_cliente);



        $optionTipiConti = $this->getTypes('tipi_conti', 'en', $GLOBALS['stato_opzioni'], false);
        $optionInvestimenti = $this->getTipiInvestimenti('tipi_investimenti', 'en', $GLOBALS['stato_opzioni'], false);

        //************* VISUALIZZO I CONTI CORRENTI *******************
//        $out .= '<B><span style="font-size:14px; color:#28597A;">Report Accounts/Deposits/Interests</span></B><BR><BR>';
        $out .= '<table  align="left" style="width:100%; border:1px solid #DDDDDD;" cellpadding="5"  cellspacing="5"  border="1" bordercolor="#DDDDDD" bgcolor="#CCCCCC">
            <tr>
            <td><strong>Account Name</strong></td>
            <td><strong>My Reference</strong></td>
            <td><strong>Type</strong></td>
            <td align="center"><strong>Activation date</strong></td>
            <td align="center"><strong>Currency</strong></td>                        
            <td align="center"><strong>Minimum Balance</strong></td>
            <td align="center"><strong>Actual Balance</strong></td>
            <td align="center"><strong>Rate applied</strong></td>                                    
            <td align="center"><strong>Quote interests</strong></td>
            <td align="center"><strong>Status</strong></td>                                                
            </tr>';



        //if($cData['id_conto']){
        while ($row_conti = $res_conti->fetchRow()) {
            $bg_col1 = '#EEEEEE';
            $col1 = '#000';
            $bg_col2 = '#F3F2F1';
            $col2 = '#008000';

            $valuta = $this->getValuta($row_conti['id_valuta']);

            //determino gli interessi collegati al conto che sto leggendo (andranno sulla sua stessa riga)
            $qry = "select * from interessi_correnti where tipo = 'C' and id_conto_investimento = " . $row_conti['id_conto'];
            //    echo '<br>'.$qry;


            $res = $this->db->query($qry);
            $rowInteressi = $res->fetchRow();
            //    while($rowInteressi = $res->fetchRow()) in teoria per un conto ho solo una riga di interessi
            //    {

            $tasso = $rowInteressi['tasso_applicato'];
            $quota = $rowInteressi['ammontare_corrente'];

            $tasso_applicato = '';
            $quota_interessi = '';

            if ($tasso)
                $tasso_applicato = $tasso . ' %';
            if ($quota)
                $quota_interessi = $quota;

            if ($row_conti['id_tipo_conto'] == 14) { // FOREX ANONYMOUS ACCOUNT    
                $bg_col1 = '#C8FB5E';
                $bg_col2 = '#C8FB5E';
                $tasso_applicato = '0,05% - 0,15%';
            }



            $out .= '<tr>
                        <td bgcolor="' . $bg_col1 . '">' . $row_conti['intestatario'] . '</B></td>
                        <td bgcolor="' . $bg_col1 . '"><B>' . $row_conti['numero_conto'] . '</B></td>
                        <td bgcolor="' . $bg_col1 . '">' . $optionTipiConti[$row_conti['id_tipo_conto']] . '</td>
                        <td align="center" bgcolor="' . $bg_col1 . '">' . $row_conti['data_attivazione'] . '</td>
                        <td align="center" bgcolor="' . $bg_col1 . '">' . $valuta . '</td>
                        <td align="center" bgcolor="' . $bg_col1 . '">' . $row_conti['saldo_iniziale'] . '</td>
                        <td align="center" bgcolor="' . $bg_col1 . '">' . $row_conti['saldo_attuale'] . '</td>
                        <td align="center" bgcolor="' . $bg_col2 . '"><span style="color:' . $col2 . ';">' . $tasso_applicato . '</SPAN></td>
                        <td align="center" bgcolor="' . $bg_col2 . '"><span style="color:' . $col2 . ';">' . $quota_interessi . '</span></td>    
                        <td align="center" bgcolor="' . $bg_col1 . '">' . $arrayImgStati[$row_conti['stato']] . '</td>            
                    </tr>';
            //}
            //********** ESTRAPOLO EVENTUALI INVESTIMENTI CON RELATIVI INTERESSI **********************
            $sql_investimenti = "select * from investimenti where id_conto = " . $row_conti['id_conto'];
            $res_investimenti = $this->db->query($sql_investimenti);
            $investimenti_trovati = $res_investimenti->numRows();


            if ($investimenti_trovati) {

                $bg_col1 = '#FFFFFF';
                $col1 = '#000';
                $bg_col2 = '#F3F2F1';
                $col2 = '#008000';

                $out .= '<tr><td colspan="10" bgcolor="#FFF">';

                $out.='<B></B>';
                $out .= '
                        <div style="margin-left:0px;margin-bottom:20px;">
                        <table  align="center"  style="width:95%; border:1px solid #DDDDDD;" cellpadding="5"  cellspacing="5"  border="1" bordercolor="#DDDDDD" bgcolor="#CCCCCC">
                        <tr>
                        <td align="center"><strong>Deposit Number</strong></td>
                        <td align="center"><strong>Deposit Type</strong></td>
                        <td align="center"><strong>Activation date</strong></td>
                        <td align="center"><strong>Time Deposit</strong></td>
                        <td align="center"><strong>Balance</strong></td>
                        <td align="center"><strong>Rate applied</strong></td>                                    
                        <td align="center"><strong>Quote interests</strong></td>                                                
                        </tr>';

                while ($row_investimenti = $res_investimenti->fetchRow()) {
                    $dati_dep = explode('|', $row_investimenti['id_tipo_investimento']);
                    $id_tipo_investimento = $dati_dep[0];
                    $tempoDeposito = ($dati_dep[1] / 2592000);
                    $num_investimento = $row_investimenti['numero_investimento'];
                    $attivazione = $row_investimenti['data_attivazione'];

                    $qry_int = "select * from interessi_correnti where tipo = 'D' and id_conto_investimento = " . $row_investimenti['id_investimento'];
                    $res_int = $this->db->query($qry_int);
                    $interessi_trovati = $res_int->numRows();
                    //echo $qry_int .'<br>';
                    if ($interessi_trovati) {
                        while ($rowInteressi = $res_int->fetchRow()) {


                            $out .='<tr>
                                        <td bgcolor="' . $bg_col1 . '" >' . $num_investimento . ' </td>
                                        <td  bgcolor="' . $bg_col1 . '" >' . $id_tipo_investimento . '</td>
                                        <td bgcolor="' . $bg_col1 . '" align="center">' . $attivazione . '</td>
                                        
                                        <td  bgcolor="' . $bg_col1 . '" align="center">' . $tempoDeposito . ' Monthes</td>
                                        
                                        <td  bgcolor="' . $bg_col1 . '" align="center">' . $row_investimenti['saldo_attuale'] . '</td>
                                        <td  bgcolor="' . $bg_col2 . '" align="center"><span style="color:' . $col2 . ';">' . $rowInteressi['tasso_applicato'] . ' %</span></td>
                                        <td  bgcolor="' . $bg_col2 . '" align="center"><span style="color:' . $col2 . ';">' . $rowInteressi['ammontare_corrente'] . '</span></td>                
                                    
                                    </tr>';
                        }
                    } else { //se non ci sono interessi ancora collegati all investimento lo stampo comunque
                        $out .='<tr>
                                        <td bgcolor="' . $bg_col1 . '" >' . $num_investimento . '</td>
                                        <td  bgcolor="' . $bg_col1 . '" >' . $id_tipo_investimento . '</td>
                                        <td bgcolor="' . $bg_col1 . '" align="center">' . $attivazione . '</td>
                                        
                                        <td  bgcolor="' . $bg_col1 . '" align="center">' . $tempoDeposito . ' Monthes</td>
                                        
                                        <td  bgcolor="' . $bg_col1 . '" align="center">' . $row_investimenti['saldo_attuale'] . '</td>
                                        <td  bgcolor="' . $bg_col2 . '" align="center"><span style="color:' . $col2 . ';"></td>
                                        <td  bgcolor="' . $bg_col2 . '" align="center"><span style="color:' . $col2 . ';"></td>                
                                    
                                    </tr>';
                    }
                } // CHIUDE WHILE INVESTIMENTI

                $out.='</table></div>'; // fine elenco investimenti per il conto
                $out .='</td></tr>';
            }
        }

        $out .= '</table></br>
        <div style="clear:both;"></div>
        ';
        $out .='<div style="clear:both;"></div>';

        return $out;
    }

    // funzione che ritorna la lista dei log di modifica agli account (solo x admin)
    function getModAccount() {


        $sql = "select * from log_modaccount order by data_mod";
        $res = $this->db->query($sql);
        $indice = 0;

        $output = array();
        while ($row = $res->fetchRow()) {

            $output[$indice]['id_utente'] = $row['id_utente'];
            $output[$indice]['data_mod'] = $row['data_mod'];
            $output[$indice]['new_username'] = $row['new_username'];
            $output[$indice]['new_pwd'] = $row['new_pwd'];
            $indice = $indice + 1;
        }

        return $output;
    }

    function getInfoCliente($id_cliente) {

        $sql = "select * from clienti where id_cliente =" . $id_cliente;

        $res = $this->db->query($sql);
        $row = $res->fetchRow();

        return $row;
    }

    function getDepositoDataClienti($id_conto) {
        $output = array();
        if ($id_conto) {
            $res = $this->db->query("select * from investimenti where id_conto = " . $id_conto);

            while ($row = $res->fetchRow()) {
                $output[] = $row;
            }
        }

        return $output;
    }

    function getContiDataClienti($id_cliente) {

        $res = $this->db->query("select * from conti where id_cliente = " . $id_cliente);

        while ($row = $res->fetchRow()) {
            $output[] = $row;
        }

        return $output;
    }

    function getInteressiClienti($id_cliente) {


        $conti = $this->getContiDataClienti($id_cliente);
//            var_dump($conti);
        /*             if($cData['id_conto']){
          $valuta = $this->getValuta($cData['id_valuta']);
          $dData = $this->getDepositoDataCli2($cData['id_conto']);
          }
         */
        //intestazioni
        $output = '<table style="width:100%"><tr>
            <td><strong>Type</strong></td>
            <td><strong>Currency</strong></td>
            <td><strong>Owner</strong></td>
            <td><strong>Account number/Deposit</strong></td>            
            <td><strong>Activation date</strong></td>                        
            <td><strong>Balance</strong></td>                                    
            <td><strong>Rate applied</strong></td>                                                
            <td><strong>Quote Interests</strong></td>                                                            
            </tr>';

        foreach ($conti as $indice => $cData) {
            $id_conto = $cData['id_conto'];

            //recupero interessi per eventuali conti
            $qry = "select * from interessi_correnti where tipo = 'C' and id_conto_investimento = " . $id_conto;
            $res = $this->db->query($qry);
            while ($rowConti = $res->fetchRow()) {
                $output .='<tr>
                        <td>Account</td>
                        <td>' . $valuta . '</td>
                        <td>' . $cData['intestatario'] . '</td>
                        <td>' . $cData['numero_conto'] . '</td>
                        <td>' . $cData['data_attivazione'] . '</td>
                        <td>' . $cData['saldo_attuale'] . '</td>
                        <td>' . $rowConti['tasso_applicato'] . '%</td>
                        <td>' . $rowConti['ammontare_corrente'] . '</td>                
                    </tr>';
            }


            $valuta = $this->getValuta($cData['id_valuta']);
            $depositi = $this->getDepositoDataClienti($id_conto);

            foreach ($depositi as $indice_dep => $dData) {
                //recupero interessi per eventuali depositi
                $qry = "select * from interessi_correnti where tipo = 'D' and id_conto_investimento = " . $dData['id_investimento'];
                $res = $this->db->query($qry);
                while ($rowDepositi = $res->fetchRow()) {
                    $valutaDep = $this->getValuta($dData['id_valuta']);
                    $output .='<tr>
                            <td>Deposit</td>
                            <td>' . $valutaDep . '</td>
                            <td>' . $dData['intestatario'] . '</td>
                            <td>' . $dData['numero_investimento'] . '</td>
                            <td>' . $dData['data_attivazione'] . '</td>
                            <td>' . $dData['saldo_attuale'] . '</td>
                            <td>' . $rowDepositi['tasso_applicato'] . '%</td>
                            <td>' . $rowDepositi['ammontare_corrente'] . '</td>                
                        </tr>';
                }
            }
        } // fine giro dei conti
        $output .='</table>';
        return $output;
    }

    //Funzione che su post iscrizione profilo aggancia un conto (da sito pubblico per il forex anonymous)
    function registra_conto($id_utente, $id_tipo_conto, $min_dep, $id_valuta, $attivo = 0) {

        $dati_cliente = $this->getInfoCliente($id_utente);

        $data['stato'] = 'P';
        $data['attivo'] = 1;
        $data['id_valuta'] = $id_valuta;
        $data['id_tipo_conto'] = $id_tipo_conto;
        $data['data_richiesta'] = date("Y-m-d H:i:s");
        $data['data_creazione'] = date("Y-m-d H:i:s");
        $data['id_cliente'] = $id_utente;
        $data['minimo_deposito'] = $min_dep;
        $data['intestatario'] = $dati_cliente['nome'] . ' ' . $dati_cliente['cognome'];
        $data['saldo_iniziale'] = $min_dep;
        $data['abilita_prelievo'] = 1;
        $data['abilita_deposito'] = 1;


        $query = $this->sql->prepareQuery('conti', $data, 'insert');
        $res = $this->db->query($query);
        $lastid = mysql_insert_id();

        if ($attivo == 1)
            $this->completa_conto($lastid);
    }

    /**
     *  ALE B 30/04/2012 questa funzione crea un numero di conto e inserisce un record INTERESSI CORRENTI legato a questo conto
     * @param type $id_conto
     * @param type $azione
     */
    function completa_conto($id_conto, $azione) {

        // registrazione su interessi correnti per procedura batch
        $dataAttiva = date("Y-m-d H:i:s"); //$this->getAttivazioneConto($lastid);
        $intStart = array();

        if ($id_conto) {
            //dati del conto
            $q = "select * from conti where id_conto = " . $id_conto;
            $res = $this->db->query($q);
            $dati_conto = $res->fetchRow();

            // HA GIA' UN NUMERO CONTO ? senno lo definisco
            if (!($dati_conto['numero_conto'])) {
                $numero_conto = $this->generaNumeroConto($id_conto, date("Y-m-d H:i:s"));
                $query = "update conti set numero_conto = '" . $numero_conto . "' where id_conto='" . $id_conto . "'";
                $res = $this->db->query($query);
            }


            // INSERISCO IN INTERESSI CORRENTI LA RELATIVA RIGA
            $intStart['id_conto_investimento'] = $id_conto;
            $intStart['tipo'] = 'C';
            $intStart['data_attivazione'] = $dataAttiva;
            $intStart['tasso_applicato'] = $this->getTassoConto($dati_conto['id_tipo_conto']);

            if ($azione == 'mod') {
                $intStart['data_attivazione'] = $this->getAttivazioneConto($id_conto);
                $query = $this->sql->prepareQuery('interessi_correnti', $intStart, 'update', "id_conto_investimento='" . $id_conto . "'");
            } else {
                $query = $this->sql->prepareQuery('interessi_correnti', $intStart, 'insert');
            }


            $res = $this->db->query($query);
        }
    }


}

 

// end class
?>