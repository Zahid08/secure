<?php

# http://rodomontano.altervista.org/captcha.php

# Configurazione

/* Sfondo con righe spesse colorate
   0  non attivo.
   1  attivo.  
   */
$LinearBackgroud = 0;

/* Disturbo granulare
   0  non attivo
   1  attivo 
   */
$GranularNoise = 0;


/* Disturbo rigato
   0  non attivo
   1  attivo  
   */
$LinearNoise = 0;


/* Disurbo quadrettato
   0  non attivo
   1  attivo 
   */
$SquareNoise = 0;

/* Distorsione orizzontale dei caratteri
   0  non attiva
   1  attiva  
   */
$distorsion_hor = 0;


/* Distorsione verticale dei caratteri
   0  non attiva.
   1  attiva.  
   */
$distorsion_ver = 0;


/* Colore dei caratteri
   0  caratteri neri.
   1  colori casuali.  
   */
$CharacterColorMode = 1;

/* sottoinsieme del codice di sicurezza 
   0  L'utente verr� richiesto di inserire l'intero codice.
   1  L'utente deve inserire solo una parte dei caratteri mostratigli: quelli colorati di rosso 
   
   NOTA - Questa opzione � ovviamente incompatibile con l'opzione precedente che genera caratteri di vario colore 
   */
$subset = 0;


/* Caratteri sfalsati
   0  non attivo.
   1  attivo.  
   */
$staggering = 1;


/* Dimensione dei caratteri. 
   0.5   dimensione casuale tra 50 e 100%.
   0.6   dimensione casuale tra 60 to 100%.
   etc
   1     Dimensione fissa. 
   */
$CharacterSizeMode = 0.6;



/* Angolo di rotazione dei caratteri.
   0  nessuna rotazione
   10 rotation csuale tra -10� e +10�
   20 rotation csuale tra -20� e +20�
   etc
   */
$CharacterRotationMode = 0;

/*  Default font.
    Il font di default pu� essere cambiato, ma deve
    trovarsi nella stessa directory dello script 
   */
//$font = 'ball.ttf';
//$font = 'box.ttf';
//$font = 'dayplanner.ttf';
//$font = 'outlined.ttf';
$font = 'sans.ttf';
//$font = 'shades.ttf';
//$font = 'sp.ttf';


/*  Scelta casuale del font. 
Fonts must be in same directory of script 
   0 non attivo. Viene caricato sempre il font di default
   1 attivo
   */
$randomfont = 0;
$fontlist = array ("sans.ttf","box.ttf","ball.ttf","shades.ttf","outlined.ttf","sp.ttf","dayplanner.ttf");


/* numero e tipo di caratteri usati per generare il codice di sicurezza. 
   */
$Codelength = 6;
//$Characters = "0123456789abcdefghjklmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
$Characters = "23456789abcdefghjklmnpqrstuvwxyz";


/* Altezza dell'immagine  (la larghezza viene determinata automaticamente) 
   */
$height = 45;


/* Colore dello sfondo
    0 Bianco
    1 Grigio
    2 Sfumatura colorata casuale
   */
$BackgroudColorMode = 0;

# Non cambiare nulla qui sotto se non sai quello che fai

############################################################




// Genero il codice di sicurezza
  for ($i = 0; $i < $Codelength; $i++) {
    $SecurityCode[$i] = substr($Characters,mt_rand(0,strlen($Characters) - 1),1);
    $pass=$pass.$SecurityCode[$i];
    $rand = mt_rand(0,1);
    if ($i == 1) {$rand = 1;}
    if ($rand > 0) 
    {$pass2=$pass2.$SecurityCode[$i]; 
     $select[$i] = $i;
     
     
    } 
    else
    { 
     $select[$i] = -1;
    }
    
    
    
    
    }
if ($randomfont > 0) {$font = $fontlist[mt_rand(0,6)];}



//  Determino la dimensione del carattere in base all'altezza dell'immagine
$fontsize = $height * 0.6;	


// Determino la larghezza dell'immagine e la creo
$textbox2 = imagettfbbox($fontsize, 0, $font, $pass) or die('Error in imagettfbbox function');
$width = (abs($textbox2[4] - $textbox2[0]))*1.25;
$im = imagecreatetruecolor($width, $height);


// Creo alcuni colori
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 238, 238, 238);
$black = imagecolorallocate($im, 0, 0, 0);
$red = imagecolorallocate($im, 255, 0, 0);

// Inserisco il colore di sfondo

ImageFill($im, 0, 0, $grey);

if ($BackgroudColorMode < 1) {ImageFill($im, 0, 0, $white);}

if ($BackgroudColorMode > 1) {

// sfondo sfumato
$red_start   = mt_rand(0,255);
$red_end     = mt_rand($red_start,255);

$green_start = mt_rand(0,255);
$green_end   = mt_rand($green_start,255);

$blue_start  = mt_rand(0,255);
$blue_end    = mt_rand($blue_start,255);

function dif ($start,$end)
{
	if ($start >= $end)
		$dif = $start - $end;
	else
		$dif = $end - $start;

	return $dif;
}


function draw($start,$end,$pos,$step_width)
{
	if ($start > $end)	
		$color = $start - $step_width * $pos;
	else
		$color = $start + $step_width * $pos;
		
	return $color;
}	

$dif_red = dif($red_start,$red_end);
$dif_green = dif($green_start,$green_end);
$dif_blue = dif($blue_start,$blue_end);

$step_red = $dif_red / $width;
$step_green = $dif_green / $width;
$step_blue = $dif_blue / $width;

$height = $height-1;


for ($pos=0; $pos<=$width; $pos++)
{
	$color = ImageColorAllocate($im,draw($red_start,$red_end,$pos,$step_red),
						   			draw($green_start,$green_end,$pos,$step_green),
						   			draw($blue_start,$blue_end,$pos,$step_blue));	
	
	imageline($im,$pos,"0",$pos,$height,$color);
	
}


$height = $height+1;

}


/* genero lo sfondo a righe colorate spesse */

if ($LinearBackgroud > 0) {
		 imagesetthickness($im, $height/10);
		for( $i=0; $i<$width*$height/200; $i++ ) {
		$Color = imagecolorallocate($im, mt_rand(100,250), mt_rand(100,250), mt_rand(100,250));
   
			imageline($im, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $Color);
		}
}


// sfasatura cifre
$x = $width/18;
for ($i = 0; $i < $Codelength; $i++) {
 $Color = $black;
 if ($CharacterColorMode > 0)  $Color = imagecolorallocate($im, mt_rand(0,250), mt_rand(0,250), mt_rand(0,250));
 
 $textbox = imagettfbbox($fontsize, 0, $font, $SecurityCode[$i]) or die('Error in imagettfbbox function');
 $y = ($height - $textbox[5])/2;
 $w = abs($textbox[4] - $textbox[0]);
 
 $Size = mt_rand($fontsize*$CharacterSizeMode,$fontsize);
 $Angle = mt_rand(-$CharacterRotationMode,$CharacterRotationMode);
 if ($staggering > 0) { 
  $x = $x + rand($w-$w/18 , $w+$w/18);
 $y = rand($y-($height/6) ,$y+($height/6));  
 
 if ($subset < 1) {ImageTtfText($im,$Size,$Angle,$x-$w,$y,$Color,$font,$SecurityCode[$i]); } 
 else {
 
 if ($select[$i] > -1) {ImageTtfText($im,$Size,$Angle,$x-$w,$y,$red,$font,$SecurityCode[$i]); }
              else {ImageTtfText($im,$Size,$Angle,$x-$w,$y,$Color,$font,$SecurityCode[$i]);}
 
 }
 } 
 else {

 $y = $height - $height/4;
 $x = $x + $w*1.1;
  
 if ($subset < 1) {ImageTtfText($im,$Size,$Angle,$x-$w,$y,$Color,$font,$SecurityCode[$i]); } 
 else {
 
 if ($select[$i] > -1) {ImageTtfText($im,$Size,$Angle,$x-$w,$y,$red,$font,$SecurityCode[$i]); }
              else {ImageTtfText($im,$Size,$Angle,$x-$w,$y,$Color,$font,$SecurityCode[$i]);}
 
 }
 
 }
}


/* 
Ampiezza e Frequenza della distorsione verticale, 
Valori troppo elevati rendono i caratteri illeggibili
*/
$ampl_y = 5; 
$freq_y = 10;

/* 
Ampiezza e Frequenza della distorsione verticale, 
Valori troppo elevati rendono i caratteri illeggibili
*/
$ampl_x = 5; 
$freq_x = 10; 


if ($distorsion_ver > 0) {
//Apply vertical distorsion
for ($i=0;$i<$width;$i+=2){
            imagecopy($im,$im,
                $xx+$i-2,$yy+sin($i/$freq_y)*$ampl_y,     //dest
                $xx+$i,$yy,             //src
                2,$height);
        }
 }  
if ($distorsion_hor > 0) {      
//Apply horizontal distorsion
for ($i=0;$i<$height;$i+=1){
            imagecopy($im,$im,
                $xx+sin($i/$freq_x)*$ampl_x,$yy+$i-1,     //dest
                $xx,$yy+$i,             //src
                $width,1);
        }
}


// disturbo quadrettato
if ($SquareNoise > 0) {
imagesetthickness($im, 1);
for($i = 0; $i <= $width; $i += $height/5) {
        @ImageLine($im, $i, 0, $i, $height, $black);
      }
      for($i = 0; $i <= $height; $i += $height/5) {
        @ImageLine($im, 0, $i, $width, $i, $black);
      }
}



/* disturbo rigato */
if ($LinearNoise > 0) {
imagesetthickness($im, 1);
		for( $i=0; $i<$height; $i++ ) {
			imageline($im, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $black);
		}
}

# disturbo granulare
if ($GranularNoise > 0) {
 for ($i=1;$i<($width*$width/10);$i++)
            {
                $cor_x = mt_rand(1,$width);
                $cor_y = mt_rand(1,$height);
                imagesetpixel($im,$cor_x,$cor_y,$black);
            }
}

// genero l'immagine 
header('Content-Type: image/jpeg');
		imagejpeg($im);
		imagedestroy($im);
		
if ($subset > 0) {$pass = $pass2;}

// memorizzo il codice per la successiva verifica
session_start();
$_SESSION['code'] = $pass;

?> 

