<?php
session_start();

header('Content-type: image/png');

$x = 190;
$y = 58;
$code = base64_decode(strrev(urldecode($_REQUEST['sessid'])));



//$code = substr($code,0,4);
$space = $x / (strlen($code)+1);

/*
Credo che questa prima parte sia abbastanza semplice da capire;
- Con la funzione header() indichiamo al browser che dovr� aspettarsi un�immagine di tipo png.
- Assegnamo i valori di larghezza e altezza del riquadro che andremo a produrre
- Facciamo in modo che il codice che vogliamo generare abbia una lunghezza massima di 9 caratteri
- Calcoliamo lo spazio che deve esserci tra un carattere e il successivo
*/

$img = imagecreatetruecolor($x,$y);

//Usiamo la funzione imagecreatetruecolor() per creare il riquadro con 256 toni di rosso, verde e blu (da 0 a 255).


$bg = imagecolorallocate($img,255,255,255);
$border = imagecolorallocate($img,0,0,0);
$colors[] = imagecolorallocate($img,128,64,192);
$colors[] = imagecolorallocate($img,192,64,128);
$colors[] = imagecolorallocate($img,108,192,64);

//Con questo, invece provvediamo a definire i colori dello sfondo (bianco), del bordo (nero) e un array di 3 colori che useremo per la renderizzazione del testo.


imagefilledrectangle($img,1,1,$x-2,$y-2,$bg);
imagerectangle($img,0,0,$x-1,$y-2,$border);

//Tramite queste funzioni andiamo a definire il background e il bordo del riquadro rettangolare


for ($i=0; $i< strlen ($code); $i++)
{
$color = $colors[$i % count($colors)];
imagettftext($img,28+rand(0,8),-20+rand(0,40),($i+0.3)*$space,40,$color,dirname(__FILE__) . '/sans.ttf',$code{$i});
}

//Con questo ciclo provvediamo a scrivere il testo nel riquadro.
//Ho usato il font alba.ttf ma si pu� usare qualsiasi font, l�importante � inserire il file nella stessa directory in cui inseriamo questo script.


/*for($i=0;$i<400;$i++)
{
$x1 = rand(3,$x-3);
$y1 = rand(3,$y-3);
$x2 = $x1-2-rand(0,8);
$y2 = $y1-2-rand(0,8);
imageline($img,$x1,$y1,$x2,$y2,$colors[rand(0,count($colors)-1)]);
}
*/
//Quest�altro ciclo for ci serve per disegnare 400 linee di coordinate casuali che serviranno a confondere le idee ai bot nel caso volessero cercare di decifrare la nostra scritta.

//Infine non ci resta che chiamare la funzione imagepng() per visualizzare i dati dell�immagine nel browser

imagepng($img);
?>