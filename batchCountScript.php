<?php

//inserimento della lista degli indirizzi IP
include( dirname(__FILE__) . "/inc/common.inc.php");

$esegui = $_REQUEST['exec'];

//per evitare che venga lanciato da qualche robot
if ($esegui) {
    $arraymodalita['0'] = 'CON ESECUZIONE';
    $arraymodalita['1'] = 'TEST';

    $disabilita_exec = 0; //1 serve solo in caso di test 
	$mail_webmaster = 'amadi.azikiwe@mwangacb.com';
	
    $headers = array();
    $headers['to_email'] = $mail_webmaster;
    $headers['from_name'] = 'Procedura bacth  banking Mwanga Community Bank';
    $headers['from_email'] = $mail_webmaster;


    $messaggio = 'INIZIO PROCEDURA vedi file batchCountScript.php<BR><BR>';
    $func->sendMail($headers, 'Inizio Procedura ' . $arraymodalita[$disabilita_exec] . ' Batch banking dubai', $messaggio, '');



    /*     * ********* FUNZIONAMENTO (ale balducci 17/02/2012) **************************************************

      Gli interessi correnti possono essere collegati ad un conto o ad un investimento (deposito)
      Il parametro che passo nelle funzioni sara' quindi l'id del conto o del deposito sulla base del parametro Tipo

      ----- CAMBIA TASSO --------------------------------------------------------------------------------------------
      CONTI --> TIPI_CONTI --> INTERESSI
      1) Determino il tipo conto del Conto in questione
      2) Determino il tasso interesse annuo relativo al tipo conto di cui punto 1)
      3) Aggiorno il tasso applicato nel record interessi in question

      ----- AGGIORNA SALDI CONTO --------------------------------------------------------------------------------------------
      INTERESSI --> CONTO
      1) Determino l'ammontare corrente degli interessi per il CONTO in questione
      2) Aggiorno il saldo attuale del CONTO sommandogli l'ammontare corrente di cui al punto 1)
      3) Azzero l'ammontare corrente del record interessi collegato al conto in questione

      ----- FINE DEPOSITO --------------------------------------------------------------------------------------------
      INTERESSI --> DEPOSITO --> CONTO
      1) Determino l'ammontare corrente degli interessi per l'investimento (deposito) corrente
      2) Aggiorno il saldo attuale ed iniziale dell'investimento sommandogli l'ammontare corrente di cui al punto 1)
      3) Al saldo attuale del conto legato all'investimento sommo il saldo attuale dell'investimento come calcolato al punto 2)
      4) Disattivo l'investimento
      5) Disattivo il record interessi di partenza

      ----- ACCREDITA QUOTA --------------------------------------------------------------------------------------------
      INTERESSI
      Aggiorno l'ammontare corrente di un record INTERESSI sommandogli il valore quota calcolato...

     * **************************************************************************************************** */

    $dataCorrente = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));

    $q = "select * from interessi_correnti where attivo = 1   ";
    $res = $db->query($q);


    while ($row = $res->fetchRow()) {

        $infoCD = $zealandCredit->getInfoConto($row['tipo'], $row['id_conto_investimento']);
        $dataRif = $infoCD['data_attivazione'];

        if ($infoCD['saldo_attuale'] > 20000) { //come da richiesta di alex 05/2013 procedo solo se actual balance è 20.000

            $TimeStampAttivazione = $zealandCredit->getTimeStamp($dataRif);

            $operazioni .= '*********<br>TIPO INTERESSE ' . $row['tipo'] . ' ID: ' . $row['id'] . ' , ID CONTO:' . $row['id_conto_investimento'] . '<br>';

            switch ($row['tipo']) {
                case 'C':



                    //DI DEFAULT 
                    $m = ($row['tasso_applicato'] / 365);

                    /*                     * ********* SE SONO PASSATI 30 GG DALL'ATTIVAZIONE **************************************************
                      INTERESSI --> CONTO
                      alla scadenza del primo mese applico l'interesse al saldo attuale del conto e azzero il contatore

                      1) Determino l'ammontare corrente degli interessi per il CONTO in questione
                      2) Aggiorno il saldo attuale del CONTO sommandogli l'ammontare corrente di cui al punto 1)
                      3) Azzero l'ammontare corrente del record interessi collegato al conto in questione
                     * **************************************************************************************************** */
                    if ($infoCD['saldo_aggiornato'] == 0) { //ale b. 27/06/2013 nuovo campo senno entro sempre qui !
                        if (($dataCorrente - $TimeStampAttivazione) >= 2592000) {

                            //applicazione interesse & azzeramento quota_corrente
                            if (!$disabilita_exec)
                                $zealandCredit->aggiornaSaldiConto($row['id'], $row['tipo'], $row['id_conto_investimento']);

                            //$m = ($row['tasso_applicato']); ALE B - NON VA DIVISO PER 365 ?? 27/06/2013

                            $operazioni .= 'Aggiorno saldo conto<br>';
                        }
                    }

                    /*                     * ********* DOPO IL TERZO MESE **************************************************
                      CONTI --> TIPI_CONTI --> INTERESSI
                      1) Determino il tipo conto del Conto in questione
                      2) Determino il tasso interesse annuo relativo al tipo conto di cui punto 1)
                      3) Aggiorno il tasso applicato nel record interessi in question
                     * **************************************************************************************************** */
                    //alla scadenza del terzo mese imposto il tasso annuale del conto
                    if ($infoCD['tasso_cambiato'] == 0) { //ale b. 27/06/2013 nuovo campo senno entro sempre qui !
                        if (($dataCorrente - $TimeStampAttivazione) >= 7776000) {
                            if (!$disabilita_exec)
                                $zealandCredit->cambiaTasso($row['id'], $row['tipo'], $row['id_conto_investimento']);

                            //$m = ($row['tasso_applicato']/365); //ale balducci 10/08/2012 leggo quello annuale salvato negli interessi 
                            $operazioni .= '<br>Cambio tasso<br>';
                        }
                    }

                    $imponibile = $infoCD['saldo_attuale'];

                    //il conto base NON ha interessi
                    if ($row['tasso_applicato'] > 0) {

                        $m = $m / 100;

                        $quota = round(($imponibile * $m), 2); //ale balducci 10/08/2012
                        //incremento quota interesse ad interessi correnti
                        if (!$disabilita_exec) {

                            $zealandCredit->accreditaQuota($row['id'], $row['tipo'], $row['id_conto_investimento'], $quota);
                        }
                        $operazioni .= '<br>Accredito ' . $quota . '<br>';
                    }

                    break;
                case 'D':

                    //controllo la data: se coincide con la data di scadenza (1 6 12 24 mesi) accredito il deposito sul saldo attuale del conto azzero deposito e interessi correnti
                    $infoDeposito = explode('|', $infoCD['id_tipo_investimento']);
                    $durataInvestimento = $infoDeposito[1];

                    if (($dataCorrente - $TimeStampAttivazione) >= $durataInvestimento) {

                        //$zealandCredit->fineDeposito($row['id'],$row['tipo'],$row['id_conto_investimento']);
                        $operazioni .= '<br>Fine deposito <br>';
                    } else {
                        $imponibile = $infoCD['saldo_attuale'];

                        $m = ($row['tasso_applicato']);
                        $m = $m / 100;
                        $quota = round(($imponibile * $m), 2);

                        //$m = (($row['tasso_applicato']/100)/360)+1;			
                        //$quota = round(($imponibile * $m)-$imponibile,2);
                        //incremento quota interesse
                        if (!$disabilita_exec)
                            $zealandCredit->accreditaQuota($row['id'], $row['tipo'], $row['id_conto_investimento'], $quota);

                        $operazioni .= '<br>Accredito quota ' . $quota . '<br>';
                    }
                    break;
            }
        }
        /* 	else
          $operazioni .= '<br>Operazione fermata su conto '.$row['id_conto_investimento'] .'per actual balance = '.$infoCD['saldo_attuale'].'<br>'; */
    } //end while


    //echo $operazioni;
    # NOTIFICA DI ESECUZIONE BATCH
    $messaggio = $operazioni . '<BR><BR>===== FINE PROCEDURA ' . $arraymodalita[$disabilita_exec] . ' dal server..vedi file batchCountScript.php =======';

    # invio email all'admin
    $func->sendMail($headers, 'FINE Procedura Batch GIBANK', $messaggio, '');
	
	if (!$funz)
	{
	  /*  echo '<pre>';
		var_dump($headers);*/
		echo 'EMAIL NON INVIATA';
	}
		
}