<?php
# menu.inc.php

switch ($_SESSION['utente']['id_tipo_utente']) {

	case "1":
		$stato_opzioni=false;
		
		// richieste pendenti
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=requests" title="'.LINK_MENU_SN_REQUESTS.'"><img src="'.$path_web.'img/icone/app_options.png" alt="'.LINK_MENU_SN_REQUESTS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=requests" title="'.LINK_MENU_SN_REQUESTS.'">'.LINK_MENU_SN_REQUESTS.'</a>';
		
		// messages
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=messages" title="'.LINK_MENU_SN_MESSAGES.'"><img src="'.$path_web.'img/icone/mail.png" alt="'.LINK_MENU_SN_MESSAGES.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=messages" title="'.LINK_MENU_SN_MESSAGES.'">'.LINK_MENU_SN_MESSAGES.'</a>';

		// profiles
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=profiles" title="'.LINK_MENU_SN_PROFILES.'"><img src="'.$path_web.'img/icone/users.png" alt="'.LINK_MENU_SN_PROFILES.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=profiles" title="'.LINK_MENU_SN_PROFILES.'">'.LINK_MENU_SN_PROFILES.'</a>';

		// accounts
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=accounts" title="'.LINK_MENU_SN_ACCOUNTS.'"><img src="'.$path_web.'img/icone/money.png" alt="'.LINK_MENU_SN_ACCOUNTS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=accounts" title="'.LINK_MENU_SN_ACCOUNTS.'">'.LINK_MENU_SN_ACCOUNTS.'</a>';

		// cards
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=cards" title="'.LINK_MENU_SN_CARDS.'"><img src="'.$path_web.'img/icone/credit_card.png" alt="'.LINK_MENU_SN_CARDS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=cards" title="'.LINK_MENU_SN_CARDS.'">'.LINK_MENU_SN_CARDS.'</a>';

		// transactions
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=transactions" title="'.LINK_MENU_SN_TRANSACTIONS.'"><img src="'.$path_web.'img/icone/refresh.png" alt="'.LINK_MENU_SN_TRANSACTIONS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=transactions" title="'.LINK_MENU_SN_TRANSACTIONS.'">'.LINK_MENU_SN_TRANSACTIONS.'</a>';
		
		// portfolio
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=portfolio" title="'.LINK_MENU_SN_PORTFOLIO.'"><img src="'.$path_web.'img/icone/app_chart.png" alt="'.LINK_MENU_SN_PORTFOLIO.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=portfolio" title="'.LINK_MENU_SN_PORTFOLIO.'">'.LINK_MENU_SN_PORTFOLIO.'</a>';
		
		// reports
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=reports" title="'.LINK_MENU_SN_REPORTS.'"><img src="'.$path_web.'img/icone/apps.png" alt="'.LINK_MENU_SN_REPORTS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=reports" title="'.LINK_MENU_SN_REPORTS.'">'.LINK_MENU_SN_REPORTS.'</a>';
		
		// settings
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=settings" title="'.LINK_MENU_SN_SETTINGS.'"><img src="'.$path_web.'img/icone/configure.png" alt="'.LINK_MENU_SN_SETTINGS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=settings" title="'.LINK_MENU_SN_SETTINGS.'">'.LINK_MENU_SN_SETTINGS.'</a>';
        
		// logout
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=logout" title="'.LINK_MENU_SN_LOGOUT.'"><img src="'.$path_web.'img/icone/export.png" alt="'.LINK_MENU_SN_LOGOUT.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=logout" title="'.LINK_MENU_SN_LOGOUT.'">'.LINK_MENU_SN_LOGOUT.'</a>';
		
	break;

	case "2":
		$stato_opzioni=true;
		// messages
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=mymessages" title="'.LINK_MENU_SN_MESSAGES.'"><img src="'.$path_web.'img/icone/mail.png" alt="'.LINK_MENU_SN_MESSAGES.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=mymessages" title="'.LINK_MENU_SN_MESSAGES.'">'.LINK_MENU_SN_MESSAGES.'</a>';

		// profiles
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=myprofile" title="'.LINK_MENU_SN_MY_PROFILE.'"><img src="'.$path_web.'img/icone/users.png" alt="'.LINK_MENU_SN_MY_PROFILE.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=myprofile" title="'.LINK_MENU_SN_MY_PROFILE.'">'.LINK_MENU_SN_MY_PROFILE.'</a>';

		// accounts
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=myaccounts" title="'.LINK_MENU_SN_MY_ACCOUNTS.'"><img src="'.$path_web.'img/icone/money.png" alt="'.LINK_MENU_SN_MY_ACCOUNTS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=myaccounts" title="'.LINK_MENU_SN_MY_ACCOUNTS.'">'.LINK_MENU_SN_MY_ACCOUNTS.'</a>';

		// cards
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=mycards" title="'.LINK_MENU_SN_MY_CARDS.'"><img src="'.$path_web.'img/icone/credit_card.png" alt="'.LINK_MENU_SN_MY_CARDS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=mycards" title="'.LINK_MENU_SN_MY_CARDS.'">'.LINK_MENU_SN_MY_CARDS.'</a>';

		// transactions
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=mytransactions" title="'.LINK_MENU_SN_MY_TRANSACTIONS.'"><img src="'.$path_web.'img/icone/refresh.png" alt="'.LINK_MENU_SN_MY_TRANSACTIONS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=mytransactions" title="'.LINK_MENU_SN_MY_TRANSACTIONS.'">'.LINK_MENU_SN_MY_TRANSACTIONS.'</a>';
		
		// portfolio
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=myportfolio" title="'.LINK_MENU_SN_MY_PORTFOLIO.'"><img src="'.$path_web.'img/icone/app_chart.png" alt="'.LINK_MENU_SN_MY_PORTFOLIO.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=myportfolio" title="'.LINK_MENU_SN_MY_PORTFOLIO.'">'.LINK_MENU_SN_MY_PORTFOLIO.'</a>';
	
		// reports
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=myreports" title="'.LINK_MENU_SN_MY_REPORTS.'"><img src="'.$path_web.'img/icone/apps.png" alt="'.LINK_MENU_SN_MY_REPORTS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=myreports" title="'.LINK_MENU_SN_MY_REPORTS.'">'.LINK_MENU_SN_MY_REPORTS.'</a>';
		
		// logout
		echo "\n".'<br /><a href="'.$path_web.'index.php?page=logout" title="'.LINK_MENU_SN_LOGOUT.'"><img src="'.$path_web.'img/icone/export.png" alt="'.LINK_MENU_SN_LOGOUT.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=logout" title="'.LINK_MENU_SN_LOGOUT.'">'.LINK_MENU_SN_LOGOUT.'</a>';
	break;

}
?>