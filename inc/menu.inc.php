<?php
# menu.inc.php
 
switch ($_SESSION['utente']['id_tipo_utente']) {

	case "1":
		$stato_opzioni=false;
		
		// richieste pendenti
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=requests" title="'.LINK_MENU_SN_REQUESTS.'">'.LINK_MENU_SN_REQUESTS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=requests&amp;request_type=profiles" title="'.LINK_MENU_SN_PROFILES.'">'.LINK_MENU_SN_PROFILES.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=requests&amp;request_type=accounts" title="'.LINK_MENU_SN_ACCOUNTS.'">'.LINK_MENU_SN_ACCOUNTS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=requests&amp;request_type=cards" title="'.LINK_MENU_SN_CARDS.'">'.LINK_MENU_SN_CARDS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=requests&amp;request_type=transactions" title="'.LINK_MENU_SN_TRANSACTIONS.'">'.LINK_MENU_SN_TRANSACTIONS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=requests&amp;request_type=portfolio" title="'.LINK_MENU_SN_PORTFOLIO.'">'.LINK_MENU_SN_PORTFOLIO.'</a></div>
		';
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=requests" title="'.LINK_MENU_SN_REQUESTS.'"><img src="'.$path_web.'img/icone/app_options.png" alt="'.LINK_MENU_SN_REQUESTS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=requests" title="'.LINK_MENU_SN_REQUESTS.'">'.LINK_MENU_SN_REQUESTS.'</a>';
		
		// messages
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=messages" title="'.LINK_MENU_SN_MESSAGES.'"><img src="'.$path_web.'img/icone/mail.png" alt="'.LINK_MENU_SN_MESSAGES.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=messages" title="'.LINK_MENU_SN_MESSAGES.'">'.LINK_MENU_SN_MESSAGES.'</a>';

		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=messages" title="'.LINK_MENU_SN_MESSAGES.'">'.LINK_MENU_SN_MESSAGES.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=messages&amp;msg_type=sent" title="'.SENT_MES.'">'.SENT_MES.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=messages&amp;msg_type=inbox" title="'.INBOX_MES.'">'.INBOX_MES.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=messages&amp;msg_type=draft" title="'.DRAFT_MES.'">'.DRAFT_MES.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=messages&amp;msg_type=trash" title="'.TRASH_MES.'">'.TRASH_MES.'</a></div>
		';

		// profiles
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=profiles" title="'.LINK_MENU_SN_PROFILES.'"><img src="'.$path_web.'img/icone/users.png" alt="'.LINK_MENU_SN_PROFILES.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=profiles" title="'.LINK_MENU_SN_PROFILES.'">'.LINK_MENU_SN_PROFILES.'</a>';

		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=profiles" title="'.LINK_MENU_SN_PROFILES.'">'.LINK_MENU_SN_PROFILES.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=profiles" title="'.LIST_TABLE.'">'.LIST_TABLE.'</a></div>
		<!--<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=profiles&amp;act=form" title="'.INSERT_NEW.'">'.INSERT_NEW.'</a></div>-->
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=profiles&amp;act=form&amp;tipo_cliente=single-personal" title="'.INSERT_NEW_PERSONAL.'">'.INSERT_NEW_PERSONAL.'</a></div>
		
		
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=profiles&amp;act=form&amp;tipo_cliente=corporate" title="'.INSERT_NEW_CORPORATE.'">'.INSERT_NEW_CORPORATE.'</a></div>
		
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=mod_account" title="lista modifiche ai dati di accesso utenti">View login data modifies</a></div>
		';
	
	

		// accounts
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=accounts" title="'.LINK_MENU_SN_ACCOUNTS.'"><img src="'.$path_web.'img/icone/money.png" alt="'.LINK_MENU_SN_ACCOUNTS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=accounts" title="'.LINK_MENU_SN_ACCOUNTS.'">'.LINK_MENU_SN_ACCOUNTS.'</a>';

		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=accounts" title="'.LINK_MENU_SN_ACCOUNTS.'">'.LINK_MENU_SN_ACCOUNTS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=accounts" title="'.LIST_TABLE.'">'.LIST_TABLE.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=accounts&amp;act=form" title="'.INSERT_NEW.'">'.INSERT_NEW.'</a></div>
		';

		//investimenti/depositi
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=interest" title="'.MENU_SN_INTERESSI_MATURATI.'">'.MENU_SN_INTERESSI_MATURATI.'</a></div>';
		
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=invest" title="'.LINK_MENU_SN_INVEST.'">'.LINK_MENU_SN_INVEST.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=invest" title="'.LIST_TABLE.'">'.LIST_TABLE.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=invest&amp;act=form" title="'.INSERT_NEW.'">'.INSERT_NEW.'</a></div>';



		// cards
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=cards" title="'.LINK_MENU_SN_CARDS.'"><img src="'.$path_web.'img/icone/credit_card.png" alt="'.LINK_MENU_SN_CARDS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=cards" title="'.LINK_MENU_SN_CARDS.'">'.LINK_MENU_SN_CARDS.'</a>';

		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=cards" title="'.LINK_MENU_SN_CARDS.'">'.LINK_MENU_SN_CARDS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=cards" title="'.LIST_TABLE.'">'.LIST_TABLE.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=cards&amp;act=form" title="'.INSERT_NEW.'">'.INSERT_NEW.'</a></div>
		
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=rec-cc" title="'.MENU_RECUPERO_DATI_CC.'">'.MENU_RECUPERO_DATI_CC.'</a></div>
		';
/*<div class="voceMenu"><a href="'.$path_web.'?page=form-cc" title="'.MENU_DATI_CC.'">'.MENU_DATI_CC.'</a></div>*/
		// transactions
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=transactions" title="'.LINK_MENU_SN_TRANSACTIONS.'"><img src="'.$path_web.'img/icone/refresh.png" alt="'.LINK_MENU_SN_TRANSACTIONS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=transactions" title="'.LINK_MENU_SN_TRANSACTIONS.'">'.LINK_MENU_SN_TRANSACTIONS.'</a>';

		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=transactions" title="'.LINK_MENU_SN_TRANSACTIONS.'">'.LINK_MENU_SN_TRANSACTIONS.'</a></div>';
		
		//$optionTransazioni = $zealandCredit->getTypes('tipi_transazioni', $_SESSION['lingua'], $stato_opzioni);
		$optionTransazioni = $zealandCredit->getTypes('tipi_transazioni', 'en', $stato_opzioni); //ale baldu 29/02/2012 -solo inglese

		foreach ($optionTransazioni as $id=>$nome) echo (!empty($nome)?'<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=transactions&amp;act=list&amp;id_tipo_transazione='.$id.'&amp;title='.$nome.'" title="'.$nome.'">'.$nome.'</a></div>':'');


		
		// portfolio
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=portfolio" title="'.LINK_MENU_SN_PORTFOLIO.'"><img src="'.$path_web.'img/icone/app_chart.png" alt="'.LINK_MENU_SN_PORTFOLIO.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=portfolio" title="'.LINK_MENU_SN_PORTFOLIO.'">'.LINK_MENU_SN_PORTFOLIO.'</a>';
		

		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=portfolio" title="'.LINK_MENU_SN_PORTFOLIO.'">'.LINK_MENU_SN_PORTFOLIO.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=portfolio" title="'.LIST_TABLE.'">'.LIST_TABLE.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=portfolio&amp;act=form" title="'.INSERT_NEW.'">'.INSERT_NEW.'</a></div>
		';

		// reports
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=reports" title="'.LINK_MENU_SN_REPORTS.'"><img src="'.$path_web.'img/icone/apps.png" alt="'.LINK_MENU_SN_REPORTS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=reports" title="'.LINK_MENU_SN_REPORTS.'">'.LINK_MENU_SN_REPORTS.'</a>';
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=reports" title="'.LINK_MENU_SN_REPORTS.'">'.LINK_MENU_SN_REPORTS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=reports&amp;report_type=transactions" title="'.LINK_MENU_SN_TRANSACTIONS.'">'.LINK_MENU_SN_TRANSACTIONS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=reports&amp;report_type=cards" title="'.LINK_MENU_SN_CARDS.'">'.LINK_MENU_SN_CARDS.'</a></div>		
		';
		
	 
		
		// settings
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=settings" title="'.LINK_MENU_SN_SETTINGS.'"><img src="'.$path_web.'img/icone/configure.png" alt="'.LINK_MENU_SN_SETTINGS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=settings" title="'.LINK_MENU_SN_SETTINGS.'">'.LINK_MENU_SN_SETTINGS.'</a>';
 
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=settings" title="'.LINK_MENU_SN_SETTINGS.'">'.LINK_MENU_SN_SETTINGS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=settings&amp;act=list&amp;tbl=tipi_gruppi&amp;title='. GRUPPI_UTENTI.'" title="'. GRUPPI_UTENTI.'">'. GRUPPI_UTENTI.'</a></div>

		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=settings&amp;act=list&amp;tbl=tipi_documenti&amp;title='. TIPI_DOCUMENTI.'" title="'. TIPI_DOCUMENTI.'">'. TIPI_DOCUMENTI.'</a></div>

		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=settings&amp;act=list&amp;tbl=tipi_titoli&amp;title='. TITOLI.'" title="'. TITOLI.'">'. TITOLI.'</a></div>

		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=settings&amp;act=list&amp;tbl=tipi_conti&amp;title='. TIPI_CONTI.'" title="'. TIPI_CONTI.'">'. TIPI_CONTI.'</a></div>
       
	    <div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=settings&amp;act=list&amp;tbl=tipi_carte&amp;title='. TIPI_CARTE.'" title="'. TIPI_CARTE.'">'. TIPI_CARTE.'</a></div>
      
	    <div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=settings&amp;act=list&amp;tbl=tipi_transazioni&amp;title='. TIPI_TRANSAZIONI.'" title="'. TIPI_TRANSAZIONI.'">'. TIPI_TRANSAZIONI.'</a></div>
       
	    <div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=settings&amp;act=list&amp;tbl=tipi_commissioni&amp;title='. TIPI_COMMISSIONI.'" title="'. TIPI_COMMISSIONI.'">'. TIPI_COMMISSIONI.'</a></div>
        
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=settings&amp;act=list&amp;tbl=tipi_investimenti&amp;title='. TIPI_INVESTIMENTI.'" title="'. TIPI_INVESTIMENTI.'">'. TIPI_INVESTIMENTI.'</a></div>
      
	    <div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=settings&amp;act=list&amp;tbl=valute&amp;title='. VALUTE.'" title="'. VALUTE.'">'. VALUTE.'</a></div>
      
	   <div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=settings&amp;act=list&amp;tbl=lingue&amp;title='. LINGUE.'" title="'. LINGUE.'">'. LINGUE.'</a></div>';
 
        
		// logout
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=logout" title="'.LINK_MENU_SN_LOGOUT.'"><img src="'.$path_web.'img/icone/export.png" alt="'.LINK_MENU_SN_LOGOUT.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=logout" title="'.LINK_MENU_SN_LOGOUT.'">'.LINK_MENU_SN_LOGOUT.'</a>';
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=logout" title="'.LINK_MENU_SN_LOGOUT.'">'.LINK_MENU_SN_LOGOUT.'</a></div>';
		
	break;

	case "2":
		$stato_opzioni=true;
		// messages
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=mymessages" title="'.LINK_MENU_SN_MESSAGES.'"><img src="'.$path_web.'img/icone/mail.png" alt="'.LINK_MENU_SN_MESSAGES.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=mymessages" title="'.LINK_MENU_SN_MESSAGES.'">'.LINK_MENU_SN_MESSAGES.'</a>';
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=mymessages" title="'.LINK_MENU_SN_MESSAGES.'">'.LINK_MENU_SN_MESSAGES.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=mymessages&amp;msg_type=sent" title="'.SENT_MES.'">'.SENT_MES.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=mymessages&amp;msg_type=inbox" title="'.INBOX_MES.'">'.INBOX_MES.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=mymessages&amp;msg_type=draft" title="'.DRAFT_MES.'">'.DRAFT_MES.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=mymessages&amp;msg_type=trash" title="'.TRASH_MES.'">'.TRASH_MES.'</a></div>
		';

		// profiles
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=myprofile" title="'.LINK_MENU_SN_MY_PROFILE.'"><img src="'.$path_web.'img/icone/users.png" alt="'.LINK_MENU_SN_MY_PROFILE.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=myprofile" title="'.LINK_MENU_SN_MY_PROFILE.'">'.LINK_MENU_SN_MY_PROFILE.'</a>';
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=myprofile" title="'.LINK_MENU_SN_MY_PROFILE.'">'.LINK_MENU_SN_MY_PROFILE.'</a></div>';

		// accounts
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=myaccounts" title="'.LINK_MENU_SN_MY_ACCOUNTS.'"><img src="'.$path_web.'img/icone/money.png" alt="'.LINK_MENU_SN_MY_ACCOUNTS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=myaccounts" title="'.LINK_MENU_SN_MY_ACCOUNTS.'">'.LINK_MENU_SN_MY_ACCOUNTS.'</a>';
		
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=myaccounts" title="'.LINK_MENU_SN_MY_ACCOUNTS.'">'.LINK_MENU_SN_MY_ACCOUNTS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=myaccounts" title="'.LIST_TABLE.'">'.LIST_TABLE.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=myaccounts&amp;act=form" title="'.INSERT_NEW.'">'.INSERT_NEW.'</a></div>
		';


	//investimenti/depositi
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=interest" title="'.MENU_SN_INTERESSI_MATURATI.'">'.MENU_SN_INTERESSI_MATURATI.'</a></div>';
		
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=invest" title="'.LINK_MENU_SN_INVEST.'">'.LINK_MENU_SN_INVEST.'</a></div>';
		



		// cards
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=mycards" title="'.LINK_MENU_SN_MY_CARDS.'"><img src="'.$path_web.'img/icone/credit_card.png" alt="'.LINK_MENU_SN_MY_CARDS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=mycards" title="'.LINK_MENU_SN_MY_CARDS.'">'.LINK_MENU_SN_MY_CARDS.'</a>';

		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=mycards" title="'.LINK_MENU_SN_MY_CARDS.'">'.LINK_MENU_SN_MY_CARDS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=mycards" title="'.LIST_TABLE.'">'.LIST_TABLE.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=mycards&amp;act=form" title="'.INSERT_NEW.'">'.INSERT_NEW.'</a></div>
		';

		// transactions
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=mytransactions" title="'.LINK_MENU_SN_MY_TRANSACTIONS.'"><img src="'.$path_web.'img/icone/refresh.png" alt="'.LINK_MENU_SN_MY_TRANSACTIONS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=mytransactions" title="'.LINK_MENU_SN_MY_TRANSACTIONS.'">'.LINK_MENU_SN_MY_TRANSACTIONS.'</a>';
		
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=mytransactions" title="'.LINK_MENU_SN_MY_TRANSACTIONS.'">'.LINK_MENU_SN_MY_TRANSACTIONS.'</a></div>';


		### transazioni accessibli ai clienti
//		$qry="select * from tipi_transazioni as tt, tipi_transazioni_lingue as ttl where tt.fra_utenti=1 and tt.id_tipo_transazione=ttl.id_tipo_transazione and tt.attivo=1 and ttl.sigla_lingua='".$_SESSION['lingua']."' order by tt.ordinamento";
	
		$qry="select * from tipi_transazioni as tt, tipi_transazioni_lingue as ttl where tt.fra_utenti=1 and tt.id_tipo_transazione=ttl.id_tipo_transazione and tt.attivo=1 and ttl.sigla_lingua='en' order by tt.ordinamento";
	
		$res=$db->query($qry);
		
		while ($record=& $res->fetchRow()) 
			$transazioniUtente[$record['id_tipo_transazione']]=$record;
		
		
		foreach ($transazioniUtente as $id_tipo => $dati) echo "\n\t".'<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=mytransactions&amp;act=form&amp;id_tipo_transazione='.$dati['id_tipo_transazione'].'&amp;title='.$dati['nome_tipo'].'" title="'.$dati['nome_tipo'].'">'.$dati['nome_tipo'].'</a></div>';
		echo '<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=mytransactions&amp;act=list" title="'.ALL_PENDING_REQUESTS.'">'.ALL_PENDING_REQUESTS.'</a></div>';
		
		// portfolio
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=myportfolio" title="'.LINK_MENU_SN_MY_PORTFOLIO.'"><img src="'.$path_web.'img/icone/app_chart.png" alt="'.LINK_MENU_SN_MY_PORTFOLIO.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=myportfolio" title="'.LINK_MENU_SN_MY_PORTFOLIO.'">'.LINK_MENU_SN_MY_PORTFOLIO.'</a>';

		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=myportfolio" title="'.LINK_MENU_SN_MY_PORTFOLIO.'">'.LINK_MENU_SN_MY_PORTFOLIO.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'index.php?page=myportfolio" title="'.LIST_TABLE.'">'.LIST_TABLE.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=myportfolio&amp;act=form" title="'.INSERT_NEW.'">'.INSERT_NEW.'</a></div>
		';
	
		// reports
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=myreports" title="'.LINK_MENU_SN_MY_REPORTS.'"><img src="'.$path_web.'img/icone/apps.png" alt="'.LINK_MENU_SN_MY_REPORTS.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=myreports" title="'.LINK_MENU_SN_MY_REPORTS.'">'.LINK_MENU_SN_MY_REPORTS.'</a>';

		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=myreports" title="'.LINK_MENU_SN_MY_REPORTS.'">'.LINK_MENU_SN_MY_REPORTS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=myreports&amp;report_type=transactions" title="'.LINK_MENU_SN_TRANSACTIONS.'">'.LINK_MENU_SN_TRANSACTIONS.'</a></div>
		<div class="voceMenu"><b>&raquo;</b>&nbsp;<a href="'.$path_web.'?page=myreports&amp;report_type=cards" title="'.LINK_MENU_SN_CARDS.'">'.LINK_MENU_SN_CARDS.'</a></div>		
		';
	 
		
		// logout
		//echo "\n".'<br /><a href="'.$path_web.'index.php?page=logout" title="'.LINK_MENU_SN_LOGOUT.'"><img src="'.$path_web.'img/icone/export.png" alt="'.LINK_MENU_SN_LOGOUT.'" width="24" height="24" border="0" style="margin:5px; vertical-align:middle; display:inline" /></a>&nbsp;<a href="'.$path_web.'index.php?page=logout" title="'.LINK_MENU_SN_LOGOUT.'">'.LINK_MENU_SN_LOGOUT.'</a>';
		
		echo "\n".'<div class="titoloMenu"><a href="'.$path_web.'index.php?page=logout" title="'.LINK_MENU_SN_LOGOUT.'">'.LINK_MENU_SN_LOGOUT.'</a></div>';
		
	break;

}
?>