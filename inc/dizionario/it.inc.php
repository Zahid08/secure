<?

# dizionario

@setlocale(LC_TIME, 'en_US.ISO_8859-1');



/*VOCI INSERITE DA ALE*/

define('INT_PRIMI_3_MESI', 'Introductory Interest (3 month only)');

define('INT_ANNUO', 'Interest rate p.a.');

define('LABEL_MINIMO_DEPOSITO', 'Please insert Minimum Deposit');

define('LINK_MENU_SN_INVEST', 'Deposit');

define('SETT_INVEST', 'Manage min. dept & Int. Rates');

define('GEST_TASSI', 'Manage Parameters');

define('MIN_DEP', 'Minimum Deposit');

define('DURATA_INV', 'Period of Deposit');

define('T_NZD', 'NZD rate');

define('T_USD', 'USD rate');

define('T_CHF', 'CHF rate');

define('T_EUR', 'EUR rate');

define('T_GBP', 'GBP rate');

define('LABEL_STATO_INVESTIMENTO', 'State of Deposit');

define('LABEL_TIPO_INVESTIMENTO', 'Type of Deposit');

define('LABEL_NUMERO_INVESTIMENTO', 'Deposit Number');

define('LABEL_IDCONTO', 'Account Ref.');

define('LABEL_NUMERODEPOSITO', 'Deposit Number');

define('LABEL_TIPOINVESTIMENTO', 'Deposit Type');

define('LABEL_DURATA', 'Time Deposit');

define('LABEL_AMMONTARE', 'Amount');

define('MENU_SN_INTERESSI_MATURATI', 'Interest');

define('VISUALIZZA_INTERESSI', 'Show interest details');













/* menù sn */

define('LINK_MENU_SN_MESSAGES', 'Messages');

define('LINK_MENU_SN_PROFILES', 'Profiles');

define('LINK_MENU_SN_ACCOUNTS', 'Accounts');

define('LINK_MENU_SN_CARDS', 'Debit/Credit Cards');

define('LINK_MENU_SN_TRANSACTIONS', 'Transactions');

define('LINK_MENU_SN_PORTFOLIO', 'Portfolio');

define('LINK_MENU_SN_REPORTS', 'Reports');

define('LINK_MENU_SN_SETTINGS', 'Settings');

define('LINK_MENU_SN_LOGOUT', 'Logout');

define('LINK_MENU_SN_REQUESTS', 'Pending Requests');



define('LINK_MENU_SN_MY_PROFILE', 'My Profile');

define('LINK_MENU_SN_MY_MESSAGES', 'My Messages');

define('LINK_MENU_SN_MY_ACCOUNTS', 'My Accounts');

define('LINK_MENU_SN_MY_CARDS', 'My Debit/Credit Cards');

define('LINK_MENU_SN_MY_TRANSACTIONS', 'My Transactions');

define('LINK_MENU_SN_MY_PORTFOLIO', 'My Portfolio');

define('LINK_MENU_SN_MY_REPORTS', 'My Reports');

define('LINK_MENU_SN_MY_SETTINGS', 'My Settings');



define('USERNAME', 'Username');

define('PASSWORD', 'Password');





/* ETICHETTE FORM */

define('LABEL_DETTAGLI','Details');

define('LABEL_TELEFONO','Telephone');

define('LABEL_FAX','Fax');

define('LABEL_EMAIL','E-mail');

define('LABEL_SITO_WEB','Web Site');



define('LABEL_CLOSE','close');

define('LABEL_INFO_UTENTE', 'User Information');

define('LABEL_NOME','First Name');

define('LABEL_COGNOME','Last Name');

define('LABEL_EMAIL_DOPPIA','Email exists in Database!');

define('LABEL_DATA_NASCITA','Date of Birth');

define('LABEL_PAESE_NAZIONALITA','Country of Citizenship');

define('LABEL_RESIDENZA','Country of Residence');

define('LABEL_TIPO_DOCUMENTO','Document Type');

define('LABEL_CODICE_DOCUMENTO','Document Number');



define('LABEL_PROFILO_UTENTE','Profile Settings');

define('LABEL_INDIRIZZO_FISICO','Physical Address');

define('LABEL_INDIRIZZO_POSTA','Mailing Address');

define('LABEL_PROVINCIA_STATO_REGIONE','State / Province / Region');

define('LABEL_CAP','Zip / Postal Code');

define('LABEL_NOME2','Name');

define('MAIL_SAME_PHYSICAL', 'Same As Physical Address');

define('LABEL_PAESE','Country');





define('LABEL_NOTE_AMMINISTRATORE','Administrator\'s Notes');



define('LABEL_INVIA','Send');

define('LABEL_ENTRA','Enter');

define('LABEL_ANNULLA','Reset');

define('LABEL_TRATT_PRIVACY','Personal data processing');

define('LABEL_ACCETTO','Accetto');

define('LABEL_PRIVACY','Privacy');



define('LABEL_GENERE','Gender');

define('LABEL_FEMMINA','Female');

define('LABEL_MASCHIO','Male');



define('REQUIRED_FIELDS', 'Required fields *');



define('TESTO_PRIVACY', 'CONSENT PURSUANT TO Legislative Decree n. 196/03<br />Personal data will be processed by Mwanga Community Bank pursuant to Legislative decree n. 196 dated 30 June 2003 (New Privacy policy). Data collection is carried out according to the terms and conditions stated in the above mentioned law for both marketing and promotional purposes. Collected data won\'t be communicated or disclosed to third parties. In reference to article 7 of the Legislative decree n. 196/03 (Personal Rights), you can freely request the control of your data and demand modification or cancellation thereof. Otherwise you can forbid the use thereof by writing a message to the above mentioned address. I herby consent to the keeping of my personal data and agree to the forwarding of information, advertising or promotional material concerning the company and its products, in compliance with law N°  675/96 dated 31.12.1996 "Privacy Policy".');

					

define('ERRORE_JS', "Some compulsory fields haven't been filled in or haven't been filled in properly.");

define('ERRORE_EMAIL', "Incorrect e-mail format");

define('CONFERMA_INVIO', "The form was successfully sent.\nThank you for contacting us.");

define('TORNA', "Back");



define('LABEL_TITOLO', 'Title');

define('LABEL_RAGIONE_SOCIALE', 'Business Name');

define('LABEL_PERSONA_RIFERIMENTO', 'Referent');

define('LABEL_INDIRIZZO', 'Address');

define('LABEL_INDIRIZZO_POBOX', 'Street or P. O. Box');







define('LABEL_CAP', 'Post Code');

define('LABEL_CITTA', 'City');

define('LABEL_PROVINCIA', 'Province');

define('LABEL_PARTITA_IVA', 'VAT Registration Number');

define('CONFERMA_PASSWORD', 'Confirm Password');

define('LABEL_PASSWORD','Password (6 characters)');

define('ERROR_LUNG_PASSWORD','Password must include 6 characters..');

define('ERROR_CONF_PASSWORD','Passwords do not match.');

define('ATTIVO_CLIENTE', 'Active');

define('NON_ATTIVO_CLIENTE', 'Not Active');

define('ONLY_RAG_SOC', '<br /><small>(Only if Company is Account Owner)</small>');



define('LABEL_TEL_CASA', 'Home Phone');

define('LABEL_TEL_UFFICIO', 'Office Phone');

define('LABEL_TEL_MOBILE', 'Mobile Phone');

define('LABEL_LINGUA', 'Language');



define('LABEL_CHIAVE', 'Security Key');



define('LABEL_TIPO_UTENTE', 'User Type');

define('LABEL_NOTE', 'Notes');

define('LABEL_GRUPPI_UTENTE', 'Profile Groups');

define('LABEL_GRUPPI_UTENTE_JS', 'Please, select at least one group.');



define('GENERAZIONE_AUTOMATICA', 'Automatically Generated');

define('GENERA_ORA', 'Generate Now');

define('INVIA_EMAIL_USER', 'Sends an e-mail to user');



define('LABEL_NO', 'no');

define('LABEL_SI', 'yes');



define('LABEL_SELEZIONA', 'select');

define('LABEL_ALTRO', 'other');



define('ACCESSO_NEGATO', 'Access denied!');

define('PAGE_NOT_FOUND', 'Page not found'); //'La pagina ('.$page.') non esiste!'





/* settings */

define('GRUPPI_UTENTI', 'User Profiles - Groups');

define('TIPI_CONTI', 'Account Types');

define('TIPI_CARTE', 'Card Account Types');

define('WIREIN', 'Wire-in Instructions');

define('VALUTE', 'Currency');

define('WIREOUT', 'Wire-out Instructions');

define('TIPI_TRANSAZIONI', 'Transactions');

define('STATI_RECORD', 'Status');

define('TIPI_COMMISSIONI', 'Fees Types');

define('TIPI_DOCUMENTI', 'Documents Types');

define('TIPI_INDIRIZZI', 'Address Types');

define('TIPI_INVESTIMENTI', 'Investment Types and Market Values');

define('STATI_CARTE', 'Cards Status');

define('STATI_CONTI', 'Accounts Status');

define('ALTRI_SETTAGGI', 'Other Settings');

define('LINGUE', 'Languages');

define('TITOLI', 'Titles');





/* elenco */

define('TABELLA_VUOTA', 'Table is empty');

define('INSERT_NEW', 'Insert New');

define('LIST_TABLE', 'List');

define('MODIFY', 'Modify');

define('DELETE', 'Delete');

define('VIEW_MOD', 'View/Modify');

define('FILTRA', 'Filter');

define('NUM_RECORDS', 'Records');

define('ALERT_NUM', 'Please, insert valid sort numbers!');

define('NOT_FOUND', 'Match not found');

define('VIEW', 'View');



define('ORDINAMENTO', 'Sort');

define('ATTIVO', 'Visible');

define('NOME', 'Name');

define('VALORE', 'Value');

define('CONFIRM_DEL', 'Confirm delete?');

define('UPDATE', 'Update');

define('SAVE', 'Save');

define('BACK_LIST', 'Back to List');

define('RESET', 'Reset');



define('DESCRIZIONE', 'Description');





define('UPDATE_OK', 'Record has been saved!');

define('CHARSET', 'CharSet');

define('SIGLA_LINGUA', 'Language Code (2 characters)');

define('DELETE_OK', 'Record has been deleted!');

define('UPDATES_OK', 'Records have been saved!');



define('LABEL_TUTTI', 'All');

define('LABEL_DATA_RICHIESTA', 'Request Date');

define('LABEL_DATA_ATTIVAZIONE', 'Activation Date');



define('NO_RECORD', 'No record selected!');

define('LABEL_STATUS', 'Status');



define('ATTIVA_CLIENTE', 'Active User');





define('LABEL_CLIENTE', 'User');

define('LABEL_TIPO_CONTO', 'Account Type');

define('LABEL_VALUTA', 'Currency');

define('LABEL_DATA_CREAZIONE', 'Creation Date');

define('LABEL_NUMERO_CONTO', 'My Reference');

define('LABEL_SALDO_INIZIALE', 'Minimum Balance');

define('LABEL_SALDO_ATTUALE', 'Actual Balance');

define('LABEL_ABILITA_PRELIEVO', 'Allow Withdrawals');

define('LABEL_ABILITA_DEPOSITO', 'Allow Deposits');

define('LABEL_DESCRIZIONE', 'Description');

define('LABEL_STATO_CONTO', 'Account Status');

define('LABEL_STATO_CARTA', 'Card Status');



define('LABEL_CONTO', 'Account');

define('LABEL_TIPO_CARTA', 'Card Type');

define('LABEL_NUMERO_CARTA', 'Card Number');

         

define('TITOLO_MAIL_ATTIVAZIONE_UTENTE', 'Attivazione Utente Mwanga Community Bank');

//define('TESTO_MAIL_ATTIVAZIONE_UTENTE', 'Dear [_NOME_COGNOME],<br />il tuo profilo è stato attivato.<br />Per accedere all\'Area Riservata utilizza questi dati:<ul><li>username: [_USERNAME]</li><li>password: [_PASSWORD]</li></ul>Il numero cliente è : [_CHIAVE].<br />.......');



define('ATTIVAZIONE_OK_CLIENTE', 'L\'utente è stato attivato. E\' necessario comunicare all\'utente i dati di accesso.');

define('CONFIRM_ATT_CLIENTE', 'Do you confirm profile activation ?');

          

define('TITOLO_MAIL_ATTIVAZIONE_CONTO', 'Attivazione Conto Mwanga Community Bank');

define('TESTO_MAIL_ATTIVAZIONE_CONTO', 'Dear [_NOME_COGNOME],<br />your account is now active.<br />n° account: [_NUMERO_CONTO]<br />Currency: [_VALUTA].......');



define('ATTIVAZIONE_OK_CONTO', 'The account is active now.');

define('CONFIRM_ATT_CONTO', 'Do you confirm card attivation ?');





define('ATTIVAZIONE_OK_CARTA', 'The Card is active now. ');

define('CONFIRM_ATT_CARTA', 'Do you confirm Card activation?');



define('TITOLO_MAIL_ATTIVAZIONE_CARTA', 'UG Bank Card Attivation');

define('TESTO_MAIL_ATTIVAZIONE_CARTA', 'Dear [_NOME_COGNOME],<br />the card [_TIPO_CARTA], N° [_NUMERO_CARTA] on account n° [_NUMERO_CONTO] is now active.<br />.......');



define('ATTIVAZIONE_OK_PORTFOLIO', 'The Operation was Sent. ');

define('CONFIRM_ATT_PORTFOLIO', 'Do you confrim the operation ?');



// primo acquisto quote

define('TITOLO_MAIL_ATTIVAZIONE_PORTFOLIO', 'Portfolio activation Mwanga Community Bank');

define('TESTO_MAIL_ATTIVAZIONE_PORTFOLIO', 'Dear [_NOME_COGNOME],<br />a Portfolio for [_TIPO_INVESTIMENTO] ([_DESCRIZIONE_INVESTIMENTO])<br />has been actived . Numbero of quotes buyed: [_NUMERO_QUOTE]<br />quote value : [_VALORE_QUOTA]<br />tot. investment: [_TOTALE]<br />.......');





// trade

define('TITOLO_MAIL_TRADE_ACQ', 'Portfolio operation confirm Mwanga Community Bank');

define('TESTO_MAIL_TRADE_ACQ', 'Dear [_NOME_COGNOME],<br /> the following operation has been proceeded on your portfolio:

<br />Investimento: [_TIPO_INVESTIMENTO] ([_DESCRIZIONE_INVESTIMENTO])]<br />numero quote acquistate: [_NUMERO_QUOTE]<br />valore quota: [_VALORE_QUOTA]<br />totale investimento: [_TOTALE]<br />.......');



define('TITOLO_MAIL_TRADE_VEN', 'Conferma Operazione Portfolio Mwanga Community Bank');

define('TESTO_MAIL_TRADE_VEN', 'Dear [_NOME_COGNOME],<br />è stata eseguita la seguante operazione sul tuo Portfolio:

<br />Investimento: [_TIPO_INVESTIMENTO] ([_DESCRIZIONE_INVESTIMENTO])<br />numero quote vendute: [_NUMERO_QUOTE]<br />valore quota: [_VALORE_QUOTA]<br />totale ricavato: [_TOTALE]<br />.......');





define('ATTIVA_CONTO', 'Active the account');

define('ATTIVA_CARTA', 'Active the card');

define('ATTIVA_PORTFOLIO', 'Active Portfolio operation');

define('ATTIVA_MOVIMENTO', 'Active operation');



define('ABILITA_CONTO', 'Account Attivation');

define('ABILITA_CARTA', 'Card Attivation');

define('ABILITA_PORTFOLIO', 'Portfolio Operation Activation');

define('ABILITA_MOVIMENTO', 'Operation Activation');





define('SELECT_INVEST', 'Please, select the Quote Market');

define('INSERT_NUM_QUOTE', 'Please, insert the number of quotes.');

define('GIACENZA_INSUFF', 'La giacenza attuale del conto selezionato non è sufficiente per l\'acquisto del numero di quote scelte.');

define('VALUTE_MATCH', 'Le valuta del conto e quella dell\'investimento devono coincidere.');

define('CONTO_ABILITATO', 'Il conto scelto non è abilitato al prelievo/deposito.');



define('NOT_NUM', 'is not a valid number');



define('LABEL_STATO_PORTFOLIO', 'Portfolio Status');

define('LABEL_NUMERO_QUOTE', 'Numero Quote');

define('LABEL_VALORE_INIZIALE', 'Valore Iniziale Quote');



define('LABEL_TIPO_INVESTIMENTO', 'Investment type');



define('LABEL_NOME_SOCIETA', 'Company or Full Name');

define('LABEL_FULL_NAME', 'Full Name');



define('LABEL_NOME_INVESTIMENTO', 'Investment Name');

define('LABEL_VALORE_QUOTA', 'Market Value');

define('LABEL_TOT_INVESTIMENTO', 'Total Amount');

define('LABEL_TRADE', 'Trade');

define('TRADE_MARKER', 'Trade Marker');

define('LABEL_AMOUNT', 'Amount');



define('LABEL_CHIAVE_RANGE', 'The key is to be 6 characters long.');

define('LABEL_CHIAVE_CHARS', 'The key must only contain letters and numbers.');





define('FRA_UTENTI', 'Visibile a Utenti');

define('SELEZIONA_TIPO_TRANS', 'Select the transaction types');

define('FROM', 'From');

define('TO', 'To');

define('LABEL_IMPORTO', 'Amount');



define('LABEL_AGG_PSW', 'Last password update');



/* stati delle operazioni */

define('PENDING', 'Pending');

define('ACTIVE', 'Active');

define('DELETED', 'Deleted');

define('RESETTED', 'Resetted');





define('CLIENTE_DA', 'Customer that order the transaction');

define('CONTO_DA', 'Account from wich go the transaction');



define('CLIENTE_A', 'Customer receiving the Transaction');

define('CONTO_A', 'Recipient of the Transaction');



define('CARTA_A', 'Card Addressed to the transaction');

define('CARTA_DA', 'Card from wich go the transaction');



define('LABEL_AVANTI', 'Next Step');

define('LABEL_STATO_TRANSAZIONE', 'Transaction Status');





define('CLIENTE_FROM', 'From');

define('CLIENTE_TO', 'To');

define('CONTO_FROM', 'From Account');

define('CONTO_TO', 'To Account');

define('CARTA_FROM', 'From the card');

define('CARTA_TO', 'To the card');

define('CARTA_A', 'Carta destinatario della transazione');

define('CARTA_DA', 'Carta da cui parte la transazione');



define('ATT_TRANSAZIONE', 'Active the transaction');

define('ATTIVAZIONE_OK_TRANSAZIONE', 'The transaction is now active. ');

define('CONFIRM_ATT_TRANSAZIONE', 'Do you confirm the transaction ?');



// primo acquisto quote

define('TITOLO_MAIL_ATTIVAZIONE_TRANSAZIONE', 'Wire Mwanga Community Bank');

define('TESTO_MAIL_ATTIVAZIONE_TRANSAZIONE', 'Dear [_NOME_COGNOME],<br />è stato eseguita la seguente Transazione:<br />da:[_DA]<br />a:[_A]<br /><br />valuta:[_VALUTA]<br />importo:[_IMPORTO]<br />descrizione:[_DESCRIZIONE]<br />.......');

define('TESTO_MAIL_OFFSHORE', '');





define('LABEL_BANCA', 'Bank');

define('LABEL_SWIFT', 'SWIFT');

define('LABEL_NCN', 'NCN');

define('LABEL_ABA', 'ABA (only U.S.)');

define('LABEL_IBAN', 'IBAN');

define('LABEL_INTESTATARIO_CONTO', 'Account name');





define('INBOX_MES', 'Inbox');

define('SENT_MES', 'Sent');

define('DRAFT_MES', 'Draft');

define('TRASH_MES', 'Trash');



/* Stati dei messaggi */

define('LABEL_LETTO', 'Read');

define('LABEL_NON_LETTO', 'Not Read');

define('LABEL_CESTINATO', 'Trashed');

define('LABEL_ELIMINATO', 'Deleted');

define('LABEL_BOZZA', 'Draft');

define('LABEL_INVIATO', 'Sent');

define('LABEL_RISPOSTO', 'Answered');





define('LABEL_LEGGI', 'Read');

define('LABEL_RISPONDI', 'Answer');

define('LABEL_SCRIVI', 'Write New');

//define('MODIFY', 'Modify');

//define('DELETE', 'Delete');

//define('SAVE', 'Save');

define('LABEL_LEGGI_MSG', 'Read the message');



define('TO_ADMIN', 'To Admin');

define('FROM_ADMIN', 'From Admin');



define('LABEL_DATA_INVIO', 'Sending Date');

define('LABEL_DATA_LETTURA', 'Read date');



define('LABEL_NOTIFICA_AUTOMATICA', 'Automatic Notification');

define('OBJECT', 'Object');

define('TEXT', 'Text');

define('SAVE_DRAFT', 'Save Draft');



define('DATA_RICEZIONE', 'On receipt date');

define('DATA_INVIO', 'Sending date');

define('DATA_CREAZIONE', 'Creation Date');

define('DATA_MESSAGGIO', 'Message date');



define('RICHIEDI_NUOVO_CONTO', 'New Account request');

define('RICHIEDI_NUOVA_CARTA', 'New Card request');

define('RICHIEDI_NUOVA_TRANSAZIONE', 'New Order request');



define('RICHIESTA_INOLTRATA', 'Your order is sent, you will receive our answer soon.');



define('TITOLO_MAIL_NUOVO_CONTO', 'New Account Request');

define('TITOLO_MAIL_NUOVA_CARTA', 'New Card Request');

define('TITOLO_MAIL_NUOVA_TRANSAZIONE', 'New order request');

define('TITOLO_MAIL_NUOVO_PORTFOLIO', 'New Trade request');





define('CREDIT_CARD', 'Credit Card');

define('DEBIT_CARD', 'Debit Card');

define('LABEL_DEBITO_CREDITO', 'Debit/Credit');



define('COMMISSIONI', 'Fees');

define('LABEL_INTESTATARIO', 'Account Name');



define('LABEL_CAUSALE_COMMISSIONE', 'Fee detail');

define('LABEL_CAUSALE', 'Detail');



define('LABEL_INVIO_MULTIPLO', 'Multiple Sending');



define('SCEGLI_DESTINATARI', 'Please, choose one o more group of customers');



define('BUY', 'Buy');

define('SELL', 'Sell');

define('UTENTE', 'User');

define('CONTO', 'Account');



define('AMOUNT_TO_BUY', 'Amount to Buy');

define('AMOUNT_TO_SELL', 'Amount to Sell');



define('LABEL_DATA', 'Date');

define('TIPO_COMMISSIONE', 'Fee Type');

define('CAUSALE', 'Causale');



define('LABEL_PRINT', 'Print');

//define('PRINT_FAX', 'Please, print this page and send via fax to ');

define('PRINT_FAX', '');

define('VIEW_REQUESTS', 'View Requests');

define('LABEL_TIPO_TRANSAZIONE', 'Transaction Type');

define('VIEW_REPORTS', 'View Reports');



define('NORMAL', 'Normal');

define('EXPRESS', 'Express');



define('DATI_BONIFICO_OUT', 'Details of outgoing wire transfer');

define('DATI_BONIFICO_IN', 'Details of income wire transfer');



define('MOVIMENTI_CARD', 'Card Extreact');

define('LABEL_RICHIEDI_NUOVA_TRANSAZIONE', 'Make a new order');

define('ALL_PENDING_REQUESTS', 'All Pending Requests');

define('LABEL_RICHIEDENTE', 'Applicant');



define('LABEL_SEARCH', 'Search');

define('FINO_AL', 'To');

define('WRITE_DATE', 'Please, write dates in this format: dd-mm-yyyy');

define('MOVIMENTI_CARDS', 'Cards extract');

define('MESSAGE_SENT', 'Sent Message!');

define('MESSAGE_SAVED', 'Message has been saved in drafts.');



define('LABEL_CODICE_SICUREZZA', 'Security Key');

define('LABEL_RICOPIA_CODICE_SICUREZZA', 'Please, copy the Security Key.');

define('LABEL_CODICE_SICUREZZA_NON_CORRETTO', "Security Key is not correct!");





define('LABEL_PERSONAL_DATA','Personal data');

define('LABEL_RICHIEDENTE_SHORT','Applicant');

define('LABEL_DOCUMENT','Document');





define('LABEL_SKYPE', 'Skype');

define('LABEL_SEGNALATORE', 'Segnalatore');

define('LABEL_ALLEGATO', 'Attachment file');

define('DOWNLOAD_MODULE', 'Download your module');

define('RISPOSTA_APPLY', 'Your request was sent succesfully.<br />Our bank staff will contact you in 48 business hours.');





/* modifica la psw */

define('PSW_NON_MODIFICATA', 'The new password is the same of the old one, please modify it again.');

define('ATTENZIONE_MODIFICA_PSW', 'ATTENTION !! You are using the same password from more of 30 days.<br />For your security change it immediately. You can not login without change the password.');





/* dati carta di credito */

define('MENU_DATI_CC', 'Sending data card');

define('TITOLO_DATI_CC', 'DATA ENTRY CARD');

define('LABEL_NUMERO_CARTA', 'Card Number');

define('LABEL_VALIDA_DAL', 'Valid from');

define('LABEL_SCADENZA', 'Expiry');

define('LABEL_NOME_CARTA', 'Name on the card');

define('LABEL_PROCEDI', 'Proceed');

define('ERRORE_CODICE', "The code of the credit card is not correct!");

define('ERRORE_SCADENZA', 'The Expiry date is not correct');

define('ERRORE_CVC', 'Security code invalid');

define('ERRORE_NOME', "Name on the card is required");

define('ERRORE_EMAIL', "Email invalid");

define('ERRORE_VALIDITA_CARTA', 'Validity date of the card invalid');

define('OK_DATI', "The data was successfully sent.");



define('ERRORE_CCERROR_NOERROR', "No error");

define('ERRORE_CCERROR_UNKNOWN', "Credit card unknown");

define('ERRORE_CCERROR_WRONGFORMAT', "The credit card number does not have a correct format");

define('ERRORE_CCERROR_WRONGLENGTH', "The credit card number does not have a correct length");

define('ERRORE_CCERROR_CHECKSUM', "Error in Checksum");



define('TITOLO_RECUPERO_DATI_CC', 'Credit card data recovery');

define('NUMERO_RICHIESTA', 'Request number');

define('MENU_RECUPERO_DATI_CC', 'Credit card data recovery');

define('ERRORE_RICHIESTA_PSW', "Request number and password are necessaries");

define('ERRORE_RICHIESTA_PSW_NONTROVATI', "Request number and password are not present in the database");





/* nuovo apply online */

define('LABEL_ISCRIZIONE_SINGOLA', 'Single Signatory, each Account holder single');

define('LABEL_ISCRIZIONE_ANONYMOUS', 'New Forex Anonymous Account');



define('LABEL_ISCRIZIONE_CONGIUNTA', 'Jointly Signatory, both Account holders jointly');

define('LABEL_ISCRIZIONE_AZIENDA', 'Corporate Signatory');

define('LABEL_PROFESSIONE', 'Profession');

define('LABEL_NAZIONALITA', 'Nationality');

define('LABEL_CITTA_NASCITA', 'City of Birth');

define('LABEL_COGNOME_NUBILE', 'Maiden Name');

define('LABEL_STATO_CIVILE', 'Martial Status');

define('LABEL_SEX', 'Sex');

define('LABEL_RILASCIATO_IL', 'Issued');

define('LABEL_DA_RILASCIATO', 'by');



define('LABEL_SINGLE','Single');

define('LABEL_MARRIED','Married');

define('LABEL_SE_DIVERSO', '(if different)');



define('LABEL_DOCUMENTO_VERIFICA', 'Verified by');



# testo invio contratto pdf

define('EMAIL_OBJ_CONTRATTO','Your Application Form for Signing');

define('EMAIL_TEXT_CONTRATTO','<font color="#2D0059">Dear ,<br><br />To complete your application:<br />1. print out the attached application form<br />2. to be signed by you in presence og and certified by a justice of peace or a bank<br />3. mail the completed application form with a copy of your ID document or passport to:<br /><br /><strong>Mwanga Community Bank, Torres de Las Americas , Punta Pacifica 23th Floor - Panama City (Republic of Panama) </strong><br /><br />For any question please email : <a href="mailto:cards@mwangacb.com">cards@mwangacb.com</a><br /><br />Kind Regards,<br /> <br />Mwanga Community Bank Ltd. Support Team</font>');

define('EMAIL_TEXT_CONTRATTO_FOREX','<font color="#2D0059"><strong>Dear [SIGLA] [CLIENTE]</strong>, <br /><br>

First of all, we would like to thank you for choosing Mwanga Community Bank.<br /><br />

We welcome you and invite you to send photocopies of the following documents to complete the opening of your <strong><span style="text-decoration:underline">Forex Anonymous account</span></strong>.<br /><br />

 

<strong>1.  Signed application form (sent to the Applicant\'s e-mail as an attachment after filling out the online Registration Form) .*</strong><br /> 

<br />

<strong>2.  Signed copy of valid passport or ID .* </strong><br />

<br />

<strong>3. Scanner of the Utility Bill or Bank Statement as a proof of address for the Account Holder.</strong><br>

<br />

To expedite the opening of the account you can anticipate by sending scanned documents attached to the email:<br><a href="mailto:cards@mwangacb.com">cards@mwangacb.com</a><br><br>



After receiving by email or by fax and verification of the documentation our staff will proceed with opening an account within 2 business days<br>** We will send you the bank details and and access codes to your private area (Home Banking) to operate online.<br>From this moment your account will be operational.<br /><br />



After completing the opening with the deposit of the minimum amount, depending on the type of account you choose, you will be sent<br> a debit card.<br /><br />



<strong>The minimum amount, depending on the type of account you have chosen, should be transferred within 30 days from the time<br>of opening the account.</strong><br /><br />



* For any questions regarding the documents required or the difficulty in obtaining these documents please contact one of our consultants<br> by email or telephone. <br /><br />

** Days estimated from receipt of email with the required documentation, Saturdays and Sundays excluded. <br /><br />



Kind regards, <br /><br /></FONT>

');



define('EMAIL_TEXT_CONTRATTO_PERSONAL','<font color="#2D0059"><strong>Dear [SIGLA] [CLIENTE]</strong>, <br /><br>

First of all, we would like to thank you for choosing Mwanga Community Bank.<br /><br />

We welcome you and invite you to send photocopies of the following documents to complete the opening of your <strong><span style="text-decoration:underline">Personal account</span></strong>.<br /><br />

 

<strong>1.  Signed application form (sent to the Applicant\'s e-mail as an attachment after filling out the online Registration Form) .*</strong><br /> 

<br />

<strong>2.  Signed copy of valid passport or ID .* </strong><br /><BR>

<strong>3. Scanner of the Utility Bill or Bank Statement as a proof of address for the Account Holder</strong><br>

<br />

To expedite the opening of the account you can anticipate by sending scanned documents attached to the email:<br><a href="mailto:cards@mwangacb.com">cards@mwangacb.com</a><br><br>



After receiving by email or by fax and verification of the documentation our staff will proceed with opening an account within 2 business days<br>** We will send you the bank details and and access codes to your private area (Home Banking) to operate online.<br>From this moment your account will be operational.<br /><br />



After completing the opening with the deposit of the minimum amount, depending on the type of account you choose, you will be sent<br> a debit card.<br /><br />



<strong>The minimum amount, depending on the type of account you have chosen, should be transferred within 30 days from the time<br>of opening the account.</strong><br /><br />



* For any questions regarding the documents required or the difficulty in obtaining these documents please contact one of our consultants<br> by email or telephone. <br /><br />

** Days estimated from receipt of email with the required documentation, Saturdays and Sundays excluded. <br /><br />



Kind regards, <br /><br /></FONT>

');



define('EMAIL_TEXT_CONTRATTO_CORPORATE','<font color="#2D0059"><strong>Dear [SIGLA] [CLIENTE]</strong>,<br /><br> 

First of all, we would like to thank you for choosing Mwanga Community Bank. <br /><br />

We welcome you and invite you to send photocopies of the following documents to complete the opening of your <strong><span style="text-decoration:underline">Corporate account</span></strong>.<br>

<strong>  

  <font color="#2D0059"><strong>  <strong>1. 

    Signed application form</strong> (sent to the Applicant\'s e-mail as an attachment after filling out the online Registration Form)*<br />

  <br />

  <strong>2. Copy of valid passport or ID of the beneficial owner(s), directors and authorized signatory*.</strong><br />

  <br />

  3. Scanner of the Utility Bill or Bank Statement as a proof of address for the Account Holder<br />

  <br />

  <strong>4. Certificate of Incorporation or Registration Document of the company*. NB: The Certificate has to be apostilled.</strong><br />

  <br />

  <strong>5. Memorandum and Articles of Association or Statues of the company* </strong><br />

  </strong></font>

 

</strong></font>

<p><font color="#2D0059">

  

  To expedite the opening of the account you can anticipate by sending scanned documents attached to the email:<br>

  <a href="mailto:cards@mwangacb.com">cards@mwangacb.com</a><br /><br />

  

  After receiving by email or fax and verification of the documentation our staff will proceed with opening an account within 2 business days**.<br> We will send you the bank details and access codes to your private area (Home Banking) to operate online.<br>From this moment your account will be operational.<br /><br />

  

  After completing the opening with the deposit of the minimum amount, depending on the type of account you choose, you will be sent<br> a debit card. <br /><br />

  

  <strong><span style="text-decoration:underline">The minimum amount, depending on the type of account you have chosen, should be transferred within 30 days from the time of<br>opening the account.</span></strong><br /><br />

  

  * For any questions regarding the documents required or the difficulty in obtaining these documents please contact one of our consultants<br> by email or telephone. <br />

  ** Days estimated from receipt of email with the required documentation, Saturdays and Sundays excluded.<br /><br />

  

  Kind regards, <br /><br />

</font></p>

');











# testo conferma iscrizione

//define('TEXT_CONFIRM','Thank you for your account opening application. You\'ll receive an e-mail from us, there we invite you to activate your new account by clicking the link in the lower part of the e-mail !');

define('TEXT_CONFIRM','Thank you for your account opening application. You\'ll receive an e-mail from us, there we\'ll tell you how to complete your application.');





define('EMAIL_OBJ_CONFIRM','Your Application Form for Signing');

define('EMAIL_TEXT_CONFIRM','With thanks we received your completed account applicantion form.<br />Please activate your account and get your account number by clicking this link: http://script.alsafar-partners-dubai-lawyers.com/confirm.php?oId=[OID]');



define('LABEL_COMPANY','Company');

define('LABEL_COMPANY_NAME','Company Name');

define('LABEL_AUTHORIZED_PERSON','Authorized Person');

define('LABEL_PAESE','Country');

define('LABEL_CODICE_COMPANY','Company Registration N.');



define('LABEL_COMPANY_TELEFONO','Company Phone');

define('LABEL_COMPANY_FAX','Company Fax');

define('LABEL_COMPANY_EMAIL','Company E-mail');

define('LABEL_COMPANY_PAESE','Company Country');

define('LABEL_COMPANY_CAP','Company Zip / Postal Code');

define('LABEL_COMPANY_CITTA','Company City');

define('LABEL_COMPANY_INDIRIZZO','Company Address');

define('LABEL_COMPANY_PROVINCIA_STATO','Company State / Province / Region');





define('INSERT_NEW_PERSONAL', 'Insert New Personal');

define('INSERT_NEW_CORPORATE', 'Insert New Corporate');



?>