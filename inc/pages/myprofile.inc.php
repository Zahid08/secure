<?php
# myprofile.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

$optionTitoli = $zealandCredit->getTypes('tipi_titoli', $_SESSION['lingua'], $stato_opzioni);
$optionDocumenti = $zealandCredit->getTypes('tipi_documenti', $_SESSION['lingua'], $stato_opzioni);
$optionPaesi = $zealandCredit->getPaesi();

$optionLingue['']='';
foreach ($lingue as $sigla => $dati) $optionLingue[$sigla] = $dati['nome_lingua'];

echo '<div class="titoloAdmin">'.LINK_MENU_SN_MY_PROFILE.'</div>';

switch ($_REQUEST['act']) {

	case "upd": // insert / update
			
			echo '<h1>'.UPDATE.'</h1>';
			/*if ($_SESSION['utente']['id_cliente'] == 57) {
				echo '<pre>';
				print_r($_REQUEST);
				echo '</pre>';
				exit();
			}*/
			$data = $_REQUEST;
			$data['username'] = $_REQUEST['user'];
			$data['password'] = trim($_REQUEST['psw']['password']);
			
			# date
			//$data['data_di_nascita']=(!empty($_REQUEST['data_di_nascita']['Y']) && !empty($_REQUEST['data_di_nascita']['m']) && !empty($_REQUEST['data_di_nascita']['d'])?$_REQUEST['data_di_nascita']['Y'].'-'.$_REQUEST['data_di_nascita']['m'].'-'.$_REQUEST['data_di_nascita']['d']:null);
			
			if (!empty($_REQUEST['id_cliente']) && $_REQUEST['id_cliente']==$_SESSION['utente']['id_cliente']) {
				
				if ($_REQUEST['psw']['password']!=$_REQUEST['old_psw']) {
					
					$data['data_aggiornamento_psw']=date("Y-m-d H:i:s");
					
					$query = "update clienti set username = '".$data['username']."', password = '".$data['password']."', data_aggiornamento_psw = '".$data['data_aggiornamento_psw']."' where id_cliente='".$_SESSION['utente']['id_cliente']."'";
					//echo '<br />'.$query;
					$res=$db->query($query);
					$_SESSION['utente']['aggiornare_psw'] = false;
					
					// aggiorno la sessione
					
					$queryAut="select * from tipi_utenti_lingue as tul, tipi_utenti as tu, clienti as cli where cli.id_cliente='".$_SESSION['utente']['id_cliente']."' and tul.id_tipo_utente=tu.id_tipo_utente and tul.sigla_lingua=cli.sigla_lingua and tul.id_tipo_utente=cli.id_tipo_utente";
					$res=$db->query($queryAut);
					$record = $res->fetchRow();
					$_SESSION['utente']=$record;
		
					$goPage->alertgo(UPDATE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');
				} else {
					$goPage->alertback(PSW_NON_MODIFICATA);
				}
					
				# update
				// clienti
				/*$query=$sql->prepareQuery ('clienti', $data, 'update', "id_cliente='".$_REQUEST['id_cliente']."'");
				//echo '<br />'.$query;
				$res=$db->query($query);
				$lastid=$_REQUEST['id_cliente'];
				
				// gruppi utenti
				// cancello
				$query="delete from clienti_tipi_gruppi where id_cliente='".$_REQUEST['id_cliente']."'";
				//echo '<br />'.$query;
				$res=$db->query($query);
				
				// reinserisco
				# clienti_tipi_gruppi
				$data=array();
				if (is_array($_REQUEST['clienti_tipi_gruppi'])) {
					foreach ($_REQUEST['clienti_tipi_gruppi'] as $id => $value) {
						$data['id_cliente']=$lastid;
						$data['id_tipo_gruppo']=$id;
						$query=$sql->prepareQuery ('clienti_tipi_gruppi', $data, 'insert');
						//echo '<!--'.$query.'-->';
						$res=$db->query($query);
					}
				}*/
				
			}
			
	break;

	default: // form con dati modificabili
	
		$record=array();
		
		if (!empty($_SESSION['utente']['id_cliente'])) {
			echo '<h1>'.VIEW_MOD.'</h1>';
			# dati cliente
			$qry="select * from clienti as cli where cli.id_cliente=".$_SESSION['utente']['id_cliente'];
			$res=$db->query($qry);
			$record = $res->fetchRow();
			$record['psw']['password']=$record['password'];
			$record['user']=$record['username'];
			
			# gruppi
			$qry="select * from clienti_tipi_gruppi where id_cliente=".$_SESSION['utente']['id_cliente'];
			$res=$db->query($qry);
			while ($record2 =& $res->fetchRow()) {
				$record['clienti_tipi_gruppi'][$record2['id_tipo_gruppo']]=$record2['id_tipo_gruppo'];
			}
			
			if ($_SESSION['utente']['aggiornare_psw'] && $_SESSION['utente']['id_cliente'] == 57) echo '<div style="background-color:#ee0000; color:#fff; font-weight:bold; padding:4px;">'.ATTENZIONE_MODIFICA_PSW.'</div>';
			
			echo '<fieldset style="width:45%; margin:5px 0px 10px 0px;"><legend class="titoloForm">'.LABEL_PROFILO_UTENTE.'</legend>';

			$form = new HTML_QuickForm('FormApply', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
			$form->addElement('hidden', 'page', $_REQUEST['page']);
			$form->addElement('hidden', 'act', 'upd');
			$form->addElement('hidden', 'id_cliente', $_REQUEST['id_cliente']);
			$form->addElement('hidden', 'old_psw', $record['password']);
			$form->addElement('hidden', 'old_user', $record['user']);

			//$form->addElement('header', 'ProfileSettings', LABEL_PROFILO_UTENTE);
			
			//$form->addElement('html', '<fieldset style="width:45%"><legend class="titoloForm">'.LABEL_PROFILO_UTENTE.'</legend>');
			
			$form->addElement('text', 'user', USERNAME,' class="textbox" size="25" maxlength="15"');
			$form->addRule('user', USERNAME, 'required', FALSE,'client');
			//$form->addElement('html', '<div class="labelForm">'.USERNAME.'</div><div class="fieldForm">'.$record['username'].'</div><div class="clearBoth"></div>');
			
			//$form->addElement('text', 'segnalatore', LABEL_SEGNALATORE,' class="textbox" size="25""');
			
			
			$group_psw[]=&HTML_QuickForm::createElement('text', 'password', LABEL_PASSWORD,' class="textbox" size="25" maxlength="6"');
			if (empty($record['psw']['password'])) $group_psw[]=&HTML_QuickForm::createElement('checkbox', 'genera', null, GENERA_ORA, ' onclick="generaPsw(this.form.name);"');
		
			$form->addGroup($group_psw, 'psw', LABEL_PASSWORD, '&nbsp;');
			$form->addGroupRule('psw', LABEL_PASSWORD, 'required', null, 1, 'client', true);	
			
			if (!empty($record['data_aggiornamento_psw'])) $form->addElement('text', 'data_aggiornamento_psw', LABEL_AGG_PSW,' class="textbox" size="25" disabled="disabled"');
			
			//if (!empty($record['chiave'])) $form->addElement('text', 'chiave', LABEL_CHIAVE,' class="textbox" size="25" maxlength="6"');
			//else $form->addElement('html', '<div class="labelForm">'.LABEL_CHIAVE.'</div><div class="fieldForm">'.GENERAZIONE_AUTOMATICA.'</div><div class="clearBoth"></div>');
			
			//$form->addElement('select', 'id_tipo_utente', LABEL_TIPO_UTENTE, $optionTipoUtente, ' class="textbox" '.($record['id_cliente']==1?' disabled="disabled"':''));
			
			// a group of checkboxes
			//foreach ($optionGruppiUtente as $id => $nome) $checkbox[] = &HTML_QuickForm::createElement('checkbox', $id, null, $nome);
		
			//$form->addGroup($checkbox, 'clienti_tipi_gruppi', LABEL_GRUPPI_UTENTE, '<br />');
			//$form->addGroupRule('clienti_tipi_gruppi', LABEL_GRUPPI_UTENTE_JS, 'required', null, 1, 'client', true);	
			
			//$form->addRule('user', USERNAME, 'required', FALSE,'client');
			$form->addRule('psw', LABEL_PASSWORD, 'required', FALSE,'client');
			//$form->addRule('chiave', LABEL_CHIAVE, 'required', FALSE,'client');
			//$form->addRule('chiave', LABEL_CHIAVE_RANGE, 'rangelength',array(6,6), 'client');
			//$form->addRule('chiave', LABEL_CHIAVE_CHARS, 'alphanumeric', 'client');
			//$form->addRule('id_tipo_utente', LABEL_TIPO_UTENTE, 'required', FALSE,'client');
		
			//$form->addElement('textarea', 'note_admin', LABEL_NOTE_AMMINISTRATORE,' class="textbox" cols="60" rows="5"');
	
	
			$form->setDefaults($record); // da tabella
		
			
			$form->setRequiredNote(REQUIRED_FIELDS);
			$form->setJsWarnings(ERRORE_JS,'');
			
			// CREO I PULSANTI
			$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', SAVE,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
			
			if ($_SESSION['utente']['id_tipo_utente']==1) $buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
			
			$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
		
			//$form->addElement('html', '</fieldset>');
			//$form->freeze();
			// mostro il form
			$form->display();
			
			echo '</fieldset>';
			
			
			$record['datadinascita'] = $record['data_di_nascita'];
			$record['datascadenzadocumento'] = $record['data_scadenza_documento'];
			$record['datarilasciodocumento'] = $record['data_rilascio_documento'];
			$record['statocivile'] = $record['stato_civile'];
			$record['sex'] = $record['sesso'];
			
			$form = new HTML_QuickForm('FormApply', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
			$form->addElement('hidden', 'page', $_REQUEST['page']);
			$form->addElement('hidden', 'act', 'upd');
			$form->addElement('hidden', 'id_cliente', $_REQUEST['id_cliente']);
			$form->addElement('hidden', 'old_psw', $record['password']);
			$form->addElement('hidden', 'tipo_cliente', $record['tipo_cliente']);
			
			$form->addElement('header', 'UserInformation', LABEL_INFO_UTENTE);
			//$form->addElement('date', 'data_richiesta', LABEL_DATA_RICHIESTA, $optionDate,' class="textbox" style="margin-left:2px;"');
			//$form->addElement('date', 'data_attivazione', LABEL_DATA_ATTIVAZIONE, $optionDate,' class="textbox" style="margin-left:2px;"');
	
			$form->addElement('hidden', 'data_attivazione', $record['data_attivazione']);
			$form->addElement('hidden', 'data_richiesta', $record['data_richiesta']);
			
			$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
			
			$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_ATTIVAZIONE.'</div><div class="fieldForm">'.(!empty($record['data_attivazione'])?$func->formatData($record['data_attivazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
			
			$form->addElement('hidden', 'attivo', $record['attivo']);
		//	$form->addElement('html', '<div class="labelForm">'.ATTIVO_CLIENTE.'</div><div class="fieldForm">'.($record['attivo']==1?LABEL_SI:LABEL_NO).'</div><div class="clearBoth"></div>');
			
			//$form->addElement('select', 'attivo', ATTIVO_CLIENTE, $optionBool, ' class="textbox"');
			
			$form->addElement('select', 'sigla_lingua', LABEL_LINGUA, $optionLingue, ' class="textbox" ');


echo '****' . $record['tipo_cliente'];

			switch ($record['tipo_cliente']) {
				case "":
				case "anonymous-forex":
				case "single-personal":
				case "chequing":
				case "children":
				case "bsa":
				case "sa":
				case "cda":
				case "gia":
				case "sia":
				
				

					$form->addElement('select', 'id_tipo_titolo', LABEL_TITOLO, $optionTitoli, ' class="textbox" ');
					
					$form->addElement('text', 'cognome', LABEL_COGNOME,' class="textbox" size="50"');
					$form->addRule('cognome', LABEL_COGNOME, 'required', FALSE,'client');
					
					$form->addElement('text', 'nome', LABEL_NOME,' class="textbox" size="50"');
					$form->addRule('nome', LABEL_NOME, 'required', FALSE,'client');
					
					$form->addElement('select', 'codice_paese_residenza', LABEL_RESIDENZA, $optionPaesi, ' class="textbox" ');
					$form->addRule('codice_paese_residenza', LABEL_RESIDENZA, 'required', FALSE,'client');
					
					$form->addElement('text', 'provincia_stato_physical', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="50"');
					$form->addRule('provincia_stato_physical', LABEL_PROVINCIA_STATO_REGIONE, 'required', FALSE,'client');
					
					$form->addElement('text', 'cap_physical', LABEL_CAP,' class="textbox" size="15"');
					$form->addRule('cap_physical', LABEL_CAP, 'required', FALSE,'client');
					
					$form->addElement('text', 'citta_physical', LABEL_CITTA,' class="textbox" size="50"');
					$form->addRule('citta_physical', LABEL_CITTA, 'required', FALSE,'client');
					
					$form->addElement('text', 'indirizzo_physical', LABEL_INDIRIZZO,' class="textbox" size="50"');
					$form->addRule('indirizzo_physical', LABEL_INDIRIZZO, 'required', FALSE,'client');
					
					//$form->addElement('text', 'telefono_casa', LABEL_TEL_CASA,' class="textbox" size="30"');
					$form->addElement('text', 'telefono_physical', LABEL_TELEFONO,' class="textbox" size="30"');
					$form->addRule('telefono_physical', LABEL_TELEFONO, 'required', FALSE,'client');
					
					$form->addElement('text', 'fax', LABEL_FAX,' class="textbox" size="30"');
					
					$form->addElement('text', 'telefono_cellulare', LABEL_TEL_MOBILE,' class="textbox" size="50"');
					$form->addRule('telefono_cellulare', LABEL_TEL_MOBILE, 'required', FALSE,'client');
					
					$form->addElement('text', 'email', LABEL_EMAIL,' class="textbox" size="30"');
					$form->addRule('email', LABEL_EMAIL, 'required', FALSE,'client');
					$form->addRule('email', LABEL_EMAIL, 'email', FALSE,'client');
					
					$form->addElement('text', 'professione', LABEL_PROFESSIONE,' class="textbox" size="50"');
					
					/*$form->addElement('date', 'datadinascita', LABEL_DATA_NASCITA, $optionDate,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_di_nascita\');"');
					$form->addElement('hidden', 'data_di_nascita', '');
					$form->addRule('datadinascita', LABEL_DATA_NASCITA, 'required', FALSE,'client');
					$form->addRule('data_di_nascita', LABEL_DATA_NASCITA, 'required', FALSE,'client');*/

					$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_NASCITA.'</div><div class="fieldForm">'.(!empty($record['data_di_nascita'])?$func->formatData($record['data_di_nascita'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');



					$form->addElement('text', 'citta_nascita', LABEL_CITTA_NASCITA,' class="textbox" size="50"');
					$form->addRule('citta_nascita', LABEL_CITTA_NASCITA, 'required', FALSE,'client');
					
					$form->addElement('select', 'codice_paese_cittadinanza', LABEL_PAESE_NAZIONALITA, $optionPaesi, ' class="textbox" ');
					$form->addRule('codice_paese_cittadinanza', LABEL_PAESE_NAZIONALITA, 'required', FALSE,'client');
					
					$form->addElement('text', 'cognome_da_nubile', LABEL_COGNOME_NUBILE,' class="textbox" size="50"');
					//$form->addRule('cognome_da_nubile', LABEL_COGNOME_NUBILE, 'required', FALSE,'client');
					
					$form->addElement('text', 'nazionalita', LABEL_NAZIONALITA,' class="textbox" size="50"');
					$form->addRule('nazionalita', LABEL_NAZIONALITA, 'required', FALSE,'client');
					
					$sex[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_MASCHIO,'M', ' onclick="this.form.elements[\'sesso\'].value = this.value;"');
					$sex[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_FEMMINA,'F', ' onclick="this.form.elements[\'sesso\'].value = this.value;"');
					$form->addGroup($sex,'sex',LABEL_SEX,'&nbsp;');
					$form->addElement('hidden', 'sesso', 'M');
					$form->addRule('sesso', LABEL_SEX, 'required', FALSE,'client');
					$form->addRule('sex', LABEL_SEX, 'required', FALSE,'client');
					
					$statocivile[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_SINGLE,'single', ' onclick="this.form.elements[\'stato_civile\'].value = this.value;"');
					$statocivile[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_MARRIED,'married', ' onclick="this.form.elements[\'stato_civile\'].value = this.value;" ');
					$form->addGroup($statocivile,'statocivile',LABEL_STATO_CIVILE,'&nbsp;');
					$form->addElement('hidden', 'stato_civile', 'single');
					$form->addRule('stato_civile', LABEL_STATO_CIVILE, 'required', FALSE,'client');
					$form->addRule('statocivile', LABEL_STATO_CIVILE, 'required', FALSE,'client');
					
					
					$form->addElement('header', 'MailingAddress', LABEL_INDIRIZZO_POSTA.' '.LABEL_SE_DIVERSO); // id_tipo_indirizzo = 2
					
					//$form->addElement('checkbox', 'mail_same_physical', '', MAIL_SAME_PHYSICAL,' onclick="popolaMailAddress();"');
					//$form->addElement('text', 'nome_mail', LABEL_NOME2,' class="textbox" size="30"');
					$form->addElement('text', 'indirizzo_mail', LABEL_INDIRIZZO_POBOX,' class="textbox" size="50"');
					$form->addElement('text', 'cap_mail', LABEL_CAP,' class="textbox" size="15"');
					$form->addElement('text', 'citta_mail', LABEL_CITTA,' class="textbox" size="50"');
					$form->addElement('text', 'provincia_stato_mail', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="50"');
					//$form->addElement('text', 'telefono_mail', LABEL_TELEFONO,' class="textbox" size="30"');
					$form->addElement('select', 'codice_paese_mail', LABEL_PAESE, $optionPaesi, ' class="textbox" ');
					
					$form->addElement('header', 'Applicant_document', LABEL_RICHIEDENTE);
					
					$form->addElement('select', 'id_tipo_documento', LABEL_TIPO_DOCUMENTO, $optionDocumenti, ' class="textbox" ');
					$form->addRule('id_tipo_documento', LABEL_TIPO_DOCUMENTO, 'required', FALSE,'client');
					
					$form->addElement('text', 'codice_documento', LABEL_CODICE_DOCUMENTO,' class="textbox" size="30"');
					$form->addRule('codice_documento', LABEL_CODICE_DOCUMENTO, 'required', FALSE,'client');
					
					/*$form->addElement('date', 'datarilasciodocumento', LABEL_RILASCIATO_IL, $optionDate2,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_rilascio_documento\');"');
					$form->addElement('hidden', 'data_rilascio_documento', '');
					$form->addRule('datarilasciodocumento', LABEL_RILASCIATO_IL, 'required', FALSE,'client');
					$form->addRule('data_rilascio_documento', LABEL_RILASCIATO_IL, 'required', FALSE,'client');*/
					
					$form->addElement('html', '<div class="labelForm">'.LABEL_RILASCIATO_IL.'</div><div class="fieldForm">'.(!empty($record['data_rilascio_documento'])?$func->formatData($record['data_rilascio_documento'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');
					
					$form->addElement('text', 'documento_rilasciato_da', LABEL_DA_RILASCIATO,' class="textbox" size="50"');
					$form->addRule('documento_rilasciato_da', LABEL_DA_RILASCIATO, 'required', FALSE,'client');
					
					/*$form->addElement('date', 'datascadenzadocumento', LABEL_SCADENZA, $optionDate2,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_scadenza_documento\');"');
					$form->addElement('hidden', 'data_scadenza_documento', '');
					$form->addRule('datascadenzadocumento', LABEL_SCADENZA, 'required', FALSE,'client');
					$form->addRule('data_scadenza_documento', LABEL_SCADENZA, 'required', FALSE,'client');*/
					$form->addElement('html', '<div class="labelForm">'.LABEL_SCADENZA.'</div><div class="fieldForm">'.(!empty($record['data_scadenza_documento'])?$func->formatData($record['data_scadenza_documento'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');
					
					
					//$form->addElement('select', 'codice_paese_cittadinanza', LABEL_NAZIONALITA, $optionPaesi, ' class="textbox" ');
					
					
					//$form->addElement('text', 'skype', LABEL_SKYPE,' class="textbox" size="60"');
					
					//$form->addRule('codice_paese_cittadinanza', LABEL_NAZIONALITA, 'required', FALSE,'client');
					
					//$form->addElement('header', 'PhysicalAddress', LABEL_INDIRIZZO_FISICO); // id_tipo_indirizzo = 1
					
					//$form->addElement('text', 'nome_physical', LABEL_NOME2,' class="textbox" size="30"');
					//$form->addElement('text', 'telefono_physical', LABEL_TELEFONO,' class="textbox" size="30"');
					
					$form->addElement('textarea', 'note', LABEL_NOTE,' class="textbox" cols="50" rows="5"');
				


				break;  
				
				case "corporate":
				case "merchant":
				case "ca":
				case "fda":
				
				
					$form->addElement('header', 'Company', LABEL_COMPANY);
					
					$form->addElement('text', 'ragione_sociale', LABEL_COMPANY_NAME,' class="textbox" size="50"');
					$form->addRule('ragione_sociale', LABEL_COMPANY_NAME, 'required', FALSE,'client');
					
					$form->addElement('text', 'company_telefono', LABEL_TELEFONO,' class="textbox" size="30"');
					$form->addRule('company_telefono', LABEL_COMPANY_TELEFONO, 'required', FALSE,'client');
					
					$form->addElement('text', 'company_fax', LABEL_FAX,' class="textbox" size="30"');
					
					$form->addElement('text', 'company_email', LABEL_EMAIL,' class="textbox" size="30"');
					//$form->addRule('company_email', LABEL_EMAIL, 'required', FALSE,'client');
					$form->addRule('company_email', LABEL_COMPANY_EMAIL, 'email', FALSE,'client');
				
					$form->addElement('text', 'company_codice', LABEL_CODICE_COMPANY,' class="textbox" size="30"');
					$form->addRule('company_codice', LABEL_CODICE_COMPANY, 'required', FALSE,'client');
					
					$form->addElement('select', 'company_codice_paese', LABEL_PAESE, $optionPaesi, ' class="textbox" ');
					$form->addRule('company_codice_paese', LABEL_COMPANY_PAESE, 'required', FALSE,'client');
				
					$form->addElement('text', 'company_provincia_stato', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="50"');
					$form->addRule('company_provincia_stato', LABEL_COMPANY_PROVINCIA_STATO_REGIONE, 'required', FALSE,'client');
					
					$form->addElement('text', 'company_cap', LABEL_CAP,' class="textbox" size="15"');
					$form->addRule('company_cap', LABEL_COMPANY_CAP, 'required', FALSE,'client');
					
					$form->addElement('text', 'company_citta', LABEL_CITTA,' class="textbox" size="50"');
					$form->addRule('company_citta', LABEL_COMPANY_CITTA, 'required', FALSE,'client');
					
					$form->addElement('text', 'company_indirizzo', LABEL_INDIRIZZO,' class="textbox" size="50"');
					$form->addRule('company_indirizzo', LABEL_COMPANY_INDIRIZZO, 'required', FALSE,'client');
				
					$form->addElement('header', 'Applicant', LABEL_AUTHORIZED_PERSON);
				
					$form->addElement('select', 'id_tipo_titolo', LABEL_TITOLO, $optionTitoli, ' class="textbox" ');
					
					$form->addElement('text', 'cognome', LABEL_COGNOME,' class="textbox" size="50"');
					$form->addRule('cognome', LABEL_COGNOME, 'required', FALSE,'client');
					
					$form->addElement('text', 'nome', LABEL_NOME,' class="textbox" size="50"');
					$form->addRule('nome', LABEL_NOME, 'required', FALSE,'client');
				
					$form->addElement('select', 'codice_paese_residenza', LABEL_RESIDENZA, $optionPaesi, ' class="textbox" ');
					$form->addRule('codice_paese_residenza', LABEL_RESIDENZA, 'required', FALSE,'client');
				
					$form->addElement('text', 'provincia_stato_physical', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="50"');
					$form->addRule('provincia_stato_physical', LABEL_PROVINCIA_STATO_REGIONE, 'required', FALSE,'client');
					
					$form->addElement('text', 'cap_physical', LABEL_CAP,' class="textbox" size="15"');
					$form->addRule('cap_physical', LABEL_CAP, 'required', FALSE,'client');
					
					$form->addElement('text', 'citta_physical', LABEL_CITTA,' class="textbox" size="50"');
					$form->addRule('citta_physical', LABEL_CITTA, 'required', FALSE,'client');
					
					$form->addElement('text', 'indirizzo_physical', LABEL_INDIRIZZO,' class="textbox" size="50"');
					$form->addRule('indirizzo_physical', LABEL_INDIRIZZO, 'required', FALSE,'client');
					
					//$form->addElement('text', 'telefono_casa', LABEL_TEL_CASA,' class="textbox" size="30"');
					$form->addElement('text', 'telefono_physical', LABEL_TELEFONO,' class="textbox" size="30"');
					$form->addRule('telefono_physical', LABEL_TELEFONO, 'required', FALSE,'client');
					
					$form->addElement('text', 'fax', LABEL_FAX,' class="textbox" size="30"');
					
					$form->addElement('text', 'telefono_cellulare', LABEL_TEL_MOBILE,' class="textbox" size="50"');
					$form->addRule('telefono_cellulare', LABEL_TEL_MOBILE, 'required', FALSE,'client');
					
					$form->addElement('text', 'email', LABEL_EMAIL,' class="textbox" size="30"');
					$form->addRule('email', LABEL_EMAIL, 'required', FALSE,'client');
					$form->addRule('email', LABEL_EMAIL, 'email', FALSE,'client');
					
					$form->addElement('text', 'professione', LABEL_PROFESSIONE,' class="textbox" size="50"');
					
					/*$form->addElement('date', 'datadinascita', LABEL_DATA_NASCITA, $optionDate,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_di_nascita\');"');
					$form->addElement('hidden', 'data_di_nascita', '');
					$form->addRule('datadinascita', LABEL_DATA_NASCITA, 'required', FALSE,'client');
					$form->addRule('data_di_nascita', LABEL_DATA_NASCITA, 'required', FALSE,'client');*/
					$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_NASCITA.'</div><div class="fieldForm">'.(!empty($record['data_di_nascita'])?$func->formatData($record['data_di_nascita'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');
					
					$form->addElement('text', 'citta_nascita', LABEL_CITTA_NASCITA,' class="textbox" size="50"');
					$form->addRule('citta_nascita', LABEL_CITTA_NASCITA, 'required', FALSE,'client');
					
					$form->addElement('select', 'codice_paese_cittadinanza', LABEL_PAESE_NAZIONALITA, $optionPaesi, ' class="textbox" ');
					$form->addRule('codice_paese_cittadinanza', LABEL_PAESE_NAZIONALITA, 'required', FALSE,'client');
					
					$form->addElement('text', 'cognome_da_nubile', LABEL_COGNOME_NUBILE,' class="textbox" size="50"');
					//$form->addRule('cognome_da_nubile', LABEL_COGNOME_NUBILE, 'required', FALSE,'client');
					
					$form->addElement('text', 'nazionalita', LABEL_NAZIONALITA,' class="textbox" size="50"');
					$form->addRule('nazionalita', LABEL_NAZIONALITA, 'required', FALSE,'client');
					
					$sex[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_MASCHIO,'M', ' onclick="this.form.elements[\'sesso\'].value = this.value;"');
					$sex[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_FEMMINA,'F', ' onclick="this.form.elements[\'sesso\'].value = this.value;"');
					$form->addGroup($sex,'sex',LABEL_SEX,'&nbsp;');
					$form->addElement('hidden', 'sesso', 'M');
					$form->addRule('sesso', LABEL_SEX, 'required', FALSE,'client');
					$form->addRule('sex', LABEL_SEX, 'required', FALSE,'client');
					
					$statocivile[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_SINGLE,'single', ' onclick="this.form.elements[\'stato_civile\'].value = this.value;"');
					$statocivile[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_MARRIED,'married', ' onclick="this.form.elements[\'stato_civile\'].value = this.value;" ');
					$form->addGroup($statocivile,'statocivile',LABEL_STATO_CIVILE,'&nbsp;');
					$form->addElement('hidden', 'stato_civile', 'single');
					$form->addRule('stato_civile', LABEL_STATO_CIVILE, 'required', FALSE,'client');
					$form->addRule('statocivile', LABEL_STATO_CIVILE, 'required', FALSE,'client');
					
					
					$form->addElement('header', 'MailingAddress', LABEL_INDIRIZZO_POSTA.' '.LABEL_SE_DIVERSO); // id_tipo_indirizzo = 2
					
					//$form->addElement('checkbox', 'mail_same_physical', '', MAIL_SAME_PHYSICAL,' onclick="popolaMailAddress();"');
					//$form->addElement('text', 'nome_mail', LABEL_NOME2,' class="textbox" size="30"');
					$form->addElement('text', 'indirizzo_mail', LABEL_INDIRIZZO_POBOX,' class="textbox" size="50"');
					$form->addElement('text', 'cap_mail', LABEL_CAP,' class="textbox" size="15"');
					$form->addElement('text', 'citta_mail', LABEL_CITTA,' class="textbox" size="50"');
					$form->addElement('text', 'provincia_stato_mail', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="50"');
					//$form->addElement('text', 'telefono_mail', LABEL_TELEFONO,' class="textbox" size="30"');
					$form->addElement('select', 'codice_paese_mail', LABEL_PAESE, $optionPaesi, ' class="textbox" ');
					
					$form->addElement('header', 'Applicant_document', LABEL_AUTHORIZED_PERSON);
					
					$form->addElement('select', 'id_tipo_documento', LABEL_TIPO_DOCUMENTO, $optionDocumenti, ' class="textbox" ');
					$form->addRule('id_tipo_documento', LABEL_TIPO_DOCUMENTO, 'required', FALSE,'client');
					
					$form->addElement('text', 'codice_documento', LABEL_CODICE_DOCUMENTO,' class="textbox" size="30"');
					$form->addRule('codice_documento', LABEL_CODICE_DOCUMENTO, 'required', FALSE,'client');
					
					/*$form->addElement('date', 'datarilasciodocumento', LABEL_RILASCIATO_IL, $optionDate2,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_rilascio_documento\');"');
					$form->addElement('hidden', 'data_rilascio_documento', '');
					$form->addRule('datarilasciodocumento', LABEL_RILASCIATO_IL, 'required', FALSE,'client');
					$form->addRule('data_rilascio_documento', LABEL_RILASCIATO_IL, 'required', FALSE,'client');*/
					
					$form->addElement('html', '<div class="labelForm">'.LABEL_RILASCIATO_IL.'</div><div class="fieldForm">'.(!empty($record['data_rilascio_documento'])?$func->formatData($record['data_rilascio_documento'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');
					
					$form->addElement('text', 'documento_rilasciato_da', LABEL_DA_RILASCIATO,' class="textbox" size="50"');
					$form->addRule('documento_rilasciato_da', LABEL_DA_RILASCIATO, 'required', FALSE,'client');
					
					/*$form->addElement('date', 'datascadenzadocumento', LABEL_SCADENZA, $optionDate2,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_scadenza_documento\');"');
					$form->addElement('hidden', 'data_scadenza_documento', '');
					$form->addRule('datascadenzadocumento', LABEL_SCADENZA, 'required', FALSE,'client');
					$form->addRule('data_scadenza_documento', LABEL_SCADENZA, 'required', FALSE,'client');*/
					$form->addElement('html', '<div class="labelForm">'.LABEL_SCADENZA.'</div><div class="fieldForm">'.(!empty($record['data_scadenza_documento'])?$func->formatData($record['data_scadenza_documento'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');
					
					
					//$form->addElement('select', 'codice_paese_cittadinanza', LABEL_NAZIONALITA, $optionPaesi, ' class="textbox" ');
					
					
					//$form->addElement('text', 'skype', LABEL_SKYPE,' class="textbox" size="60"');
					
					//$form->addRule('codice_paese_cittadinanza', LABEL_NAZIONALITA, 'required', FALSE,'client');
					
					//$form->addElement('header', 'PhysicalAddress', LABEL_INDIRIZZO_FISICO); // id_tipo_indirizzo = 1
					
					//$form->addElement('text', 'nome_physical', LABEL_NOME2,' class="textbox" size="30"');
					//$form->addElement('text', 'telefono_physical', LABEL_TELEFONO,' class="textbox" size="30"');
					
					$form->addElement('textarea', 'note', LABEL_NOTE,' class="textbox" cols="50" rows="5"');
					
					
				break; // end corporate
				
			} // end switch $record['tipo_cliente']
			
	/*		
			$form->addElement('header', 'UserInformation', LABEL_INFO_UTENTE);
			//$form->addElement('date', 'data_richiesta', LABEL_DATA_RICHIESTA, $optionDate,' class="textbox" style="margin-left:2px;"');
			//$form->addElement('date', 'data_attivazione', LABEL_DATA_ATTIVAZIONE, $optionDate,' class="textbox" style="margin-left:2px;"');
	
			$form->addElement('hidden', 'data_attivazione', $record['data_attivazione']);
			$form->addElement('hidden', 'data_richiesta', $record['data_richiesta']);
			
			$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
			
			$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_ATTIVAZIONE.'</div><div class="fieldForm">'.(!empty($record['data_attivazione'])?$func->formatData($record['data_attivazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
			
			$form->addElement('hidden', 'attivo', $record['attivo']);
		//	$form->addElement('html', '<div class="labelForm">'.ATTIVO_CLIENTE.'</div><div class="fieldForm">'.($record['attivo']==1?LABEL_SI:LABEL_NO).'</div><div class="clearBoth"></div>');
			
			//$form->addElement('select', 'attivo', ATTIVO_CLIENTE, $optionBool, ' class="textbox"');
			
			$form->addElement('select', 'sigla_lingua', LABEL_LINGUA, $optionLingue, ' class="textbox" ');
			$form->addElement('select', 'id_tipo_titolo', LABEL_TITOLO, $optionTitoli, ' class="textbox" ');
			$form->addElement('text', 'cognome', LABEL_COGNOME,' class="textbox" size="60"');
			$form->addElement('text', 'nome', LABEL_NOME,' class="textbox" size="60"');
			$form->addElement('text', 'ragione_sociale', LABEL_RAGIONE_SOCIALE.ONLY_RAG_SOC,' class="textbox" size="60"');
			
			$form->addElement('date', 'data_di_nascita', LABEL_DATA_NASCITA, $optionDate,' class="textbox" style="margin-left:2px;"');
	
			$form->addElement('select', 'id_tipo_documento', LABEL_TIPO_DOCUMENTO, $optionDocumenti, ' class="textbox" ');
			$form->addElement('text', 'codice_documento', LABEL_CODICE_DOCUMENTO,' class="textbox" size="60"');
	
			$form->addElement('select', 'codice_paese_cittadinanza', LABEL_NAZIONALITA, $optionPaesi, ' class="textbox" ');
			$form->addElement('select', 'codice_paese_residenza', LABEL_RESIDENZA, $optionPaesi, ' class="textbox" ');
	
			$form->addElement('text', 'email', LABEL_EMAIL,' class="textbox" size="60"');
			$form->addElement('text', 'telefono_casa', LABEL_TEL_CASA,' class="textbox" size="60"');
			$form->addElement('text', 'telefono_ufficio', LABEL_TEL_UFFICIO,' class="textbox" size="60"');
			$form->addElement('text', 'telefono_cellulare', LABEL_TEL_MOBILE,' class="textbox" size="60"');
			
			$form->addElement('text', 'skype', LABEL_SKYPE,' class="textbox" size="60"');
			$form->addElement('text', 'fax', LABEL_FAX,' class="textbox" size="60"');
	
			$form->addRule('cognome', LABEL_COGNOME, 'required', FALSE,'client');
			$form->addRule('nome', LABEL_NOME, 'required', FALSE,'client');
			$form->addRule('data_di_nascita', LABEL_DATA_NASCITA, 'required', FALSE,'client');
			$form->addRule('id_tipo_documento', LABEL_TIPO_DOCUMENTO, 'required', FALSE,'client');
			$form->addRule('codice_documento', LABEL_CODICE_DOCUMENTO, 'required', FALSE,'client');
			$form->addRule('email', LABEL_EMAIL, 'required', FALSE,'client');
			$form->addRule('email', LABEL_EMAIL, 'email', FALSE,'client');
			$form->addRule('codice_paese_cittadinanza', LABEL_NAZIONALITA, 'required', FALSE,'client');
			$form->addRule('codice_paese_residenza', LABEL_RESIDENZA, 'required', FALSE,'client');
			$form->addElement('textarea', 'note', LABEL_NOTE,' class="textbox" cols="60" rows="5"');
	
			$form->addElement('header', 'PhysicalAddress', LABEL_INDIRIZZO_FISICO); // id_tipo_indirizzo = 1
			
			$form->addElement('text', 'nome_physical', LABEL_NOME2,' class="textbox" size="60"');
			$form->addElement('text', 'indirizzo_physical', LABEL_INDIRIZZO,' class="textbox" size="60"');
			$form->addElement('text', 'cap_physical', LABEL_CAP,' class="textbox" size="60"');
			$form->addElement('text', 'citta_physical', LABEL_CITTA,' class="textbox" size="60"');
			$form->addElement('text', 'provincia_stato_physical', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="60"');
			$form->addElement('text', 'telefono_physical', LABEL_TELEFONO,' class="textbox" size="60"');
			$form->addElement('select', 'codice_paese_physical', LABEL_PAESE, $optionPaesi, ' class="textbox" ');
	
			$form->addElement('header', 'MailingAddress', LABEL_INDIRIZZO_POSTA); // id_tipo_indirizzo = 2
			
			$form->addElement('checkbox', 'mail_same_physical', '', MAIL_SAME_PHYSICAL,' onclick="popolaMailAddress();"');
			$form->addElement('text', 'nome_mail', LABEL_NOME2,' class="textbox" size="60"');
			$form->addElement('text', 'indirizzo_mail', LABEL_INDIRIZZO,' class="textbox" size="60"');
			$form->addElement('text', 'cap_mail', LABEL_CAP,' class="textbox" size="60"');
			$form->addElement('text', 'citta_mail', LABEL_CITTA,' class="textbox" size="60"');
			$form->addElement('text', 'provincia_stato_mail', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="60"');
			$form->addElement('text', 'telefono_mail', LABEL_TELEFONO,' class="textbox" size="60"');
			$form->addElement('select', 'codice_paese_mail', LABEL_PAESE, $optionPaesi, ' class="textbox" ');
	*/
			$form->setDefaults($record); // da tabella
		
			$form->freeze();
			// mostro il form
			$form->display();
			
		} else {
			$goPage->alertback(ACCESSO_NEGATO);
		}
	
}
	
?>
