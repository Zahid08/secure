<?php
# transactions.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

// verifico se la trasnazione � permessa agli utenti

### transazioni accessibli ai clienti
$qry="select * from tipi_transazioni as tt, tipi_transazioni_lingue as ttl where tt.fra_utenti=1 and tt.id_tipo_transazione=ttl.id_tipo_transazione and tt.attivo=1 and ttl.sigla_lingua='".$_SESSION['lingua']."' order by tt.ordinamento";
$res=$db->query($qry);

while ($record=& $res->fetchRow()) $transazioniUtente[$record['id_tipo_transazione']]=$record;

if ($_REQUEST['id_tipo_transazione'] && !array_key_exists($_REQUEST['id_tipo_transazione'],$transazioniUtente)) {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}


echo '<div class="titoloAdmin">'.LINK_MENU_SN_TRANSACTIONS.'</div>';

$optionClientiConti=$zealandCredit->getClientiConti(true);
$optionValute = $zealandCredit->getValute($stato_opzioni, false);

$optionClienti=$zealandCredit->getClienti(true);
$optionConti=$zealandCredit->getContiDati(true,$_SESSION['utente']['id_cliente']);
$optionContiNum=array();
$optionContiNum['']='';
foreach ($optionConti as $id => $dati) {
	$optionContiNum[$id]=$dati['numero_conto'].' ('.$optionValute[$dati['id_valuta']].')';
}

$optionPaesi=$zealandCredit->getPaesi();

$optionInvestimenti = $zealandCredit->getTipiInvestimenti('tipi_investimenti', $_SESSION['lingua'], $stato_opzioni, false);

$optionTipiComm=$zealandCredit->getTypes('tipi_commissioni', $_SESSION['lingua'], true, true);

$optionBuySell['B']=BUY;
$optionBuySell['S']=SELL;

$optionTransazioni = $zealandCredit->getTypes('tipi_transazioni', $_SESSION['lingua'], $stato_opzioni);

$optionNormalExpress['N']=NORMAL;
$optionNormalExpress['E']=EXPRESS;
foreach ($optionInvestimenti as $id => $dati) $selectInvestimenti[$id]=$dati['nome_tipo'];

switch ($_REQUEST['act']) {
	
	case "form":
		
		
		if (!empty($_REQUEST['id_transazione']) && !empty($_REQUEST['id_tipo_transazione'])) {
			
			$qry="select * from transazioni where id_transazione=".$_REQUEST['id_transazione']." and id_tipo_transazione=".$_REQUEST['id_tipo_transazione'];
			
			// solo le transazioni del cliente
			/*
			# Transfer Between Accounts
			$qry .=($_REQUEST['id_tipo_transazione']=='1'?" and id_cliente_da = ".$_SESSION['utente']['id_cliente']." and id_cliente_a = ".$_SESSION['utente']['id_cliente']:'');
		
			# Transfer to Other User
			$qry .=($_REQUEST['id_tipo_transazione']=='2'?" and id_cliente_da = ".$_SESSION['utente']['id_cliente']:'');
		
			# Outgoing Wire Transfer
			$qry .=($_REQUEST['id_tipo_transazione']=='4'?" and id_cliente_da = ".$_SESSION['utente']['id_cliente']:'');
		
			# Trade Marker
			$qry .=($_REQUEST['id_tipo_transazione']=='6'?" and id_cliente_da = ".$_SESSION['utente']['id_cliente']:'');
		
			# Fees
			$qry .=($_REQUEST['id_tipo_transazione']=='7'?" and id_cliente_da = ".$_SESSION['utente']['id_cliente']:'');
		
			# Incoming Wire Transfer
			$qry .=($_REQUEST['id_tipo_transazione']=='5'?" and id_cliente_a = ".$_SESSION['utente']['id_cliente']:'');
		
			# Accredito Interessi
			$qry .=($_REQUEST['id_tipo_transazione']=='8'?" and id_cliente_a = ".$_SESSION['utente']['id_cliente']:'');
			*/

			// solo le transazioni RICHIESTE dal cliente
			$qry .=" and id_richiedente = ".$_SESSION['utente']['id_cliente'];
			
			$res=$db->query($qry);
			
			$rows=$res->numRows();

			if ($rows==0) { 
				$goPage->alertback(ACCESSO_NEGATO, false);
				exit();
			}
			
			$record = $res->fetchRow();
			
			$record['importo_transazione']=number_format($record['importo_transazione'],2,',','.');
			echo '<p>'.PRINT_FAX.'</p>';
		} else {
			if (empty($_REQUEST['id_tipo_transazione'])) $goPage->alertback(NO_RECORD, false);
			else echo '<h1>'.LABEL_RICHIEDI_NUOVA_TRANSAZIONE.'</h1>';
			$record['data_richiesta']=date("Y-m-d H:i:s");
			echo '<p>'.$_SESSION['title'].'</p>';
		
		}

			$form = new HTML_QuickForm('FormUpdate', 'post', $_SERVER['SCRIPT_NAME'], '', '', false);
			$form->addElement('hidden', 'page', $_REQUEST['page']);
			$form->addElement('hidden', 'act', 'upd');
			$form->addElement('hidden', 'data_richiesta', $record['data_richiesta']);
			$form->addElement('hidden', 'stato', 'P');
			$form->addElement('hidden', 'id_richiedente', $_SESSION['utente']['id_cliente']);
		
			if ($_REQUEST['id_transazione']) $form->addElement('select', 'id_tipo_transazione', LABEL_TIPO_TRANSAZIONE, $optionTransazioni, ' class="textbox"');
			else $form->addElement('hidden', 'id_tipo_transazione', $_REQUEST['id_tipo_transazione']);
			
			$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
			$form->addElement('select', 'normal_express', NORMAL.'/'.EXPRESS, $optionNormalExpress, ' class="textbox" ');
			
		//	$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_ATTIVAZIONE.'</div><div class="fieldForm">'.(!empty($record['data_attivazione'])?$func->formatData($record['data_attivazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
			
			//$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_TRANSAZIONE.'</div><div class="fieldForm">'.(!empty($record['  	data_transazione'])?$func->formatData($record['  	data_transazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
	
			//$form->addElement('select', 'stato', LABEL_STATO_TRANSAZIONE, $arrayStatiOperazioni, ' class="textbox" ');
			
			if ($_REQUEST['id_tipo_transazione']==7) $form->addElement('select', 'id_tipo_commissione', TIPO_COMMISSIONE, $optionTipiComm, ' class="textbox" ');
			$form->addElement('select', 'id_valuta', LABEL_VALUTA, $optionValute, ' class="textbox" ');
			

			$form->addElement('text', 'importo_transazione', LABEL_IMPORTO, ' class="textbox" size="25" ');
			$form->addElement('textarea', 'descrizione', CAUSALE, ' class="textbox" rows="5" cols="60"');
  	    	    	 
			switch ($_REQUEST['id_tipo_transazione']) {
				
				case "1": # Transfer Between Accounts
					
					$form->addElement('hidden', 'id_cliente_da', $_SESSION['utente']['id_cliente']);
					$form->addElement('hidden', 'id_cliente_a', $_SESSION['utente']['id_cliente']);
					
				//	$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
					
				//	$form->addElement('select', 'id_cliente_a', CLIENTE_TO, $optionClienti, ' class="textbox"');
					$form->addElement('select', 'id_conto_a', CONTO_TO, $optionContiNum, ' class="textbox"');
				
					
				break;
				
				case "2": # Transfer to Other User
					$form->addElement('hidden', 'id_cliente_da', $_SESSION['utente']['id_cliente']);
			
					//$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
					
				//	$form->addElement('select', 'id_cliente_a', CLIENTE_TO, $optionClienti, ' class="textbox"');
				//	$form->addElement('select', 'id_conto_a', CONTO_TO, $optionContiNum, ' class="textbox"');
					$form->addElement('text', 'cliente_a', CLIENTE_TO, ' class="textbox" size="60"');
					$form->addElement('text', 'conto_a', CONTO_TO, ' class="textbox" size="60"');
				
				break;
				
				case "4": # Outgoing Wire Transfer
				case "6": # Trade Marker
				//case "7": # Fees
					
					$form->addElement('hidden', 'id_cliente_da', $_SESSION['utente']['id_cliente']);
					//$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
				break;

				case "5": # Incoming Wire Transfer
				//case "8": # Accredito Interessi
					$form->addElement('hidden', 'id_cliente_a', $_SESSION['utente']['id_cliente']);
					//$form->addElement('select', 'id_cliente_a', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					$form->addElement('select', 'id_conto_a', CONTO_TO, $optionContiNum, ' class="textbox"');
				break;
								
				
			}


			if ($_REQUEST['id_tipo_transazione']==6) {
				$form->addElement('select', 'id_tipo_investimento', LABEL_TIPO_INVESTIMENTO, $selectInvestimenti, ' class="textbox" ');
			
				$form->addElement('text', 'valore_quota', LABEL_VALORE_QUOTA, ' class="textbox" ');
				$form->addElement('text', 'numero_quote', LABEL_NUMERO_QUOTE, ' class="textbox" ');
	
				$form->addElement('select', 'buy_sell', BUY.'/'.SELL, $optionBuySell, ' class="textbox" ');
			}
			
			   	    	    	    	 
		//	 id_gestione_offshore 
			if ($record['id_transazione']){ 
				$qry2="select * from gestione_offshore where id_transazione=".$_REQUEST['id_transazione'];
				$res2=$db->query($qry2);
				$record2 = $res2->fetchRow();
		 	}
			
			if ($_REQUEST['id_tipo_transazione']==5) {
				// in
				$form->addElement('html', '<div class="labelForm"><i>'.DATI_BONIFICO_IN.'</i></div><div class="clearBoth"></div>');
				$form->addElement('text', 'nome_ba_in', LABEL_BANCA, ' class="textbox" size="60"');
				$form->addElement('text', 'swift_ba_in', LABEL_SWIFT, ' class="textbox" size="60"');
				$form->addElement('text', 'indirizzo_ba_in', LABEL_INDIRIZZO, ' class="textbox" size="60"');
				$form->addElement('text', 'cap_ba_in', LABEL_CAP, ' class="textbox" size="60"');
				$form->addElement('text', 'citta_ba_in', LABEL_CITTA, ' class="textbox" size="60"');
				$form->addElement('select', 'codice_paese_ba_in', LABEL_PAESE, $optionPaesi, ' class="textbox"');
				$form->addElement('text', 'ncn_ba_in', LABEL_NCN, ' class="textbox" size="60"');
				$form->addElement('text', 'aba_ba_in', LABEL_ABA, ' class="textbox" size="60"');
				$form->addElement('text', 'intestatario_ba_in', LABEL_INTESTATARIO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'numero_conto_ba_in', LABEL_NUMERO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'iban_ba_in', LABEL_IBAN, ' class="textbox" size="60"');
			}
			
			
			if ($_REQUEST['id_tipo_transazione']==4) {
				// out
				$form->addElement('html', '<div class="labelForm"><i>'.DATI_BONIFICO_OUT.'</i></div><div class="clearBoth"></div>');
				//$form->addElement('html', '<i>'.DATI_BONIFICO_OUT.'</i>');
				$form->addElement('text', 'nome_ba_out', LABEL_BANCA, ' class="textbox" size="60"');
				$form->addElement('text', 'swift_ba_out', LABEL_SWIFT, ' class="textbox" size="60"');
				$form->addElement('text', 'indirizzo_ba_out', LABEL_INDIRIZZO, ' class="textbox" size="60"');
				$form->addElement('text', 'cap_ba_out', LABEL_CAP, ' class="textbox" size="60"');
				$form->addElement('text', 'citta_ba_out', LABEL_CITTA, ' class="textbox" size="60"');
				$form->addElement('select', 'codice_paese_ba_out', LABEL_PAESE, $optionPaesi, ' class="textbox"');
				$form->addElement('text', 'ncn_ba_out', LABEL_NCN, ' class="textbox" size="60"');
				$form->addElement('text', 'aba_ba_out', LABEL_ABA, ' class="textbox" size="60"');
				$form->addElement('text', 'intestatario_ba_out', LABEL_INTESTATARIO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'numero_conto_ba_out', LABEL_NUMERO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'iban_ba_out', LABEL_IBAN, ' class="textbox" size="60"');
			}
			
			$form->setDefaults($record); // da tabella
			$form->setDefaults($record2); // da tabella


			if (!empty($_REQUEST['id_transazione'])) {
		
				if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) {
					// CREO I PULSANTI
					$buttons[]=&HTML_QuickForm::createElement('button', 'btnPrint', LABEL_PRINT, 'class="button" onClick="window.open(\'print.php?page='.$_REQUEST['page'].'&act=form&id_tipo_transazione='.$_REQUEST['id_tipo_transazione'].'&id_transazione='.$_REQUEST['id_transazione'].'\', \'_blank\', \'width=650, height=500, scrollbars=yes, resizable=yes\'); return false;"');
					$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
					$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
				}
				
				$form->freeze(); 
			} else {
			
				$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', LABEL_INVIA,'class="button"');
				$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
				$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
				$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
			}
			// mostro il form
			$form->display();
			
	
	break; // end view
	
	
	
	case "upd": // insert / update
		echo '<h1>'.$_SESSION['title'].'</h1>';
		
		// inserimento dati
		$_REQUEST['data_transazione']=date("Y-m-d H:i:s");
		# insert
		$query=$sql->prepareQuery ('transazioni', $_REQUEST, 'insert');
		//echo '<br />'.$query;
		$res=$db->query($query);
		$lastid=mysql_insert_id();

		# gestione_offshore
		if ($_REQUEST['id_tipo_transazione'] == 4 || $_REQUEST['id_tipo_transazione'] == 5) {
		
			$_REQUEST['id_transazione']=$lastid;
			$query=$sql->prepareQuery ('gestione_offshore', $_REQUEST, 'insert');
			//echo '<br />'.$query;
			$res=$db->query($query);
		
		}
		
		# invio form richiesta
		### mail di avviso all'amministratore
		//require_once ($path_www."class/mail.class.php");
		
		//$titolo = TITOLO_MAIL_NUOVA_TRANSAZIONE;
	
		//$testa = '<div style="font-family: Verdana, Helvetica, sans-serif; font-size: 12px; font-weight:bold;">'.$titolo.'</div>';
		$dati='In date '.date("d-m-Y").' the customer asked for a new transaction.<br />Here some data he wrote:';
		
		$dati .='<br />Cliente: '.$_SESSION['utente']['cognome'].' '.$_SESSION['utente']['nome'].' '.$_SESSION['utente']['ragione_sociale'];
		$dati .=(!empty($_REQUEST['id_tipo_transazione'])?'<br />Transaction type: '.$optionTransazioni[$_REQUEST['id_tipo_transazione']]:'');
		$dati .=(!empty($_REQUEST['sigla_lingua'])?'<br />Language: '.$_REQUEST['sigla_lingua']:'');
		
		//$dati .='<br />Collegandoti al pannello di amministrazione puoi vedere tutti i dati della transaizone ed attivarla.';
		
		 
		
		$headers = array();

		$headers['to_name']=$name_admin;
		$headers['to_email']=$mail_admin;
		
		$headers['from_name']=$_SESSION['utente']['nome'].(!empty($_SESSION['utente']['cognome'])?' '.$_SESSION['utente']['cognome']:'').(!empty($_SESSION['utente']['ragione_sociale'])?' - '.$_SESSION['utente']['ragione_sociale']:'');
		
		$headers['from_email']=$_SESSION['utente']['email'];
		
		$headers['return_email']=$_SESSION['utente']['email'];
		$headers['reply_email']=$_SESSION['utente']['email'];
		$headers['reply_name']=$_SESSION['utente']['nome'].(!empty($_SESSION['utente']['cognome'])?' '.$_SESSION['utente']['cognome']:'').(!empty($_SESSION['utente']['ragione_sociale'])?' - '.$_SESSION['utente']['ragione_sociale']:'');
		
		$template_email = file_get_contents($path_www.'templates//email_post_richiesta_generica.html');
		$messaggio = str_replace('[CONTENUTO]',$dati, $template_email);
		
		$inviata = $func->sendMail($headers,TITOLO_MAIL_NUOVA_TRANSAZIONE,$messaggio,'');	
		
		// messaggi
		// salvo la comunicazione in db
		$data=array();
		$data['id_cliente_mittente']=$_SESSION['utente']['id_cliente'];
		$data['id_cliente_destinatario']=1; // admin
		$data['email_mittente']=$param['from'];
		$data['email_destinatario']=$param['to'];
		$data['oggetto_messaggio']=$param['subject'];
		$data['testo_messaggio']=$dati;
		$data['stato_messaggio_mittente']='I';
		$data['stato_messaggio_destinatario']='N';
		$data['sigla_lingua']=$_SESSION['lingua'];
		$data['data_creazione_messaggio']=$_REQUEST['data_richiesta'];
		$data['data_invio_messaggio']=date("Y-m-d H:i:s");
		$data['notifica_automatica']=1;
		$data['notifica_email']=1;
		$query=$sql->prepareQuery ('messaggi', $data, 'insert');
		//echo '<br />'.$query;
		$res=$db->query($query);
		
		$goPage->alertgo(RICHIESTA_INOLTRATA, 'index.php?page='.$_REQUEST['page'].'&act=form&id_transazione='.$lastid."&id_tipo_transazione=".$_REQUEST['id_tipo_transazione']);

	break;
/*	
	case "del": // delete
		echo '<h1>'.$_SESSION['title'].'</h1>';
		
		if (!empty($_REQUEST['id_transazione'])) {

			$query="delete from transazioni where id_transazione='".$_REQUEST['id_transazione']."'";
			$res=$db->query($query);

			$goPage->alertgo(DELETE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list&id_tipo_transazione='.$_REQUEST['id_tipo_transazione']);
			
		} else $goPage->alertback(NO_RECORD, false);
		
	break;
*/	
	case "list": // list
	# elenco transazioni pendenti
		
			echo '<h1>All requests';
			echo '</h1>';
			
			$qry="select * from transazioni where id_richiedente=".$_SESSION['utente']['id_cliente']; //." and stato='P'";
			
			# filtri
	
			$qry .=(!empty($_REQUEST['filtra_tipo']) && $_REQUEST['filtra_tipo']!='*'?" and id_tipo_transazione = ".$_REQUEST['filtra_tipo']."":'');
	
	
			# order
			$qry .="  order by data_richiesta desc";
	
			$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list';
			$link_extra_param .=(!empty($_REQUEST['filtra_tipo']) && $_REQUEST['filtra_tipo']!=''?"&amp;filtra_tipo=".$_REQUEST['filtra_tipo']:'');
			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
				
			echo $view_links;
		?><br /><br />
		<form name="fFast_upd" method="post" action="index.php" onsubmit="return controllaForm('fFast_upd');">
			<input type="hidden" name="act" value="fast_upd" />
			<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
			<input type="hidden" name="filter" value="0" />
            
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>
				<th><?php echo LABEL_TIPO_TRANSAZIONE; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
				<th><?php echo LABEL_DATA; ?></th>
				<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			</tr>
			
			<!-- filtri -->
			<tr>
				<td colspan="2" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
				<td><?php
				// id_cliente_da            
				?><select name="filtra_tipo" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionTransazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo']) && $_REQUEST['filtra_tipo']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
				//$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATT_TRANSAZIONE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATT_TRANSAZIONE.'" title="'.PENDING.'-'.ATT_TRANSAZIONE.'" /></a>';
			//	print_r($record);
				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW.'"><img src="'.$path_web.'img/icone/file_find.png" width="24" height="24" style="border: 0px;" alt="'.VIEW.'" /></a></td>
					<td class="'.$class.'">';
			
				echo "</td>";
				echo "\n".'<td class="'.$class.'">'.$optionTransazioni[$record['id_tipo_transazione']].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
					<td class="'.$class.'">'.number_format($record['importo_transazione'],2,',','.').'</td>
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
					</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>
            </form>
		<?php	
				echo $view_links;
	
			} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);

			
	break;
	
	default: // elenco tipi transazioni
		echo '<h1>'.LABEL_RICHIEDI_NUOVA_TRANSAZIONE.'</h1>';
		?>
        <ul>
        <?php
		//$qry="select * from tipi_transazioni as tt, tipi_transazioni_lingue as ttl where tt.fra_utenti=1 and tt.id_tipo_transazione=ttl.id_tipo_transazione and tt.attivo=1 and ttl.sigla_lingua='".$_SESSION['lingua']."' order by tt.ordinamento";
		//$res=$db->query($qry);
		
		//while ($record=& $res->fetchRow()) 
		foreach ($transazioniUtente as $id_tipo => $dati) echo "\n\t".'<li><a href="'.$path_web.'index.php?page='.$_REQUEST['page'].'&amp;act=form&amp;id_tipo_transazione='.$dati['id_tipo_transazione'].'&amp;title='.$dati['nome_tipo'].'" title="'.$dati['nome_tipo'].'">'.$dati['nome_tipo'].'</a></li>';

		?>
		</ul>
        
		<?php
	
		// tutte le richieste di transazioni pendenti
		echo '<p><a href="'.$path_web.'index.php?page='.$_REQUEST['page'].'&amp;act=list" title="'.ALL_PENDING_REQUESTS.'">'.ALL_PENDING_REQUESTS.'</a></p>';
}
	
?>
