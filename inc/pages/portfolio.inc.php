<?php
# portfolio.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

//$optionTipiInvestimenti = $zealandCredit->getTypes('tipi_investimenti', $_SESSION['lingua'], $stato_opzioni, false);
$optionClienti = $zealandCredit->getClientiConti($stato_opzioni); // solo clienti con conti aperti
$optionConti =$zealandCredit->getContiClientiPortfolio(); // solo conti con abilitazione prelievo/deposito
$optionValute = $zealandCredit->getValute($stato_opzioni, false);
$optionInvestimenti = $zealandCredit->getTipiInvestimenti('tipi_investimenti', $_SESSION['lingua'], $stato_opzioni, false);
$optionTipiInvestimenti = array();
$optionTipiInvestimenti['']='';
$arrayJs ="\n".'arrayInvValQuote = new Array();';
$arrayJs .="\n".'arrayInvValuta = new Array();';
foreach ($optionInvestimenti as $id => $dati) {
	$optionTipiInvestimenti[$id]=$dati['nome_tipo'].' ('.$optionValute[$dati['id_valuta']].')';
	$arrayJs .="\n".'arrayInvValQuote['.$id.']='.$dati['valore'].';';
	$arrayJs .="\n".'arrayInvValuta['.$id.']='.$dati['id_valuta'].';';
}

$optionContiDati =$zealandCredit->getContiDati($stato_opzioni); // solo conti con abilitazione prelievo/deposito

$arrayJs .="\n".'arrayContiValute = new Array();';
$arrayJs .="\n".'arrayContiGiacenza = new Array();';
foreach ($optionContiDati as $id_conto => $dati) {
	if ($dati['saldo_attuale']>0 && $dati['id_valuta']>0) {
		$arrayJs .="\n".'arrayContiValute['.$id_conto.']='.$dati['id_valuta'].';';
		$arrayJs .="\n".'arrayContiGiacenza['.$id_conto.']='.$dati['saldo_attuale'].';';
	}
}

?>
<script type="text/javascript">
<?php echo $arrayJs; ?>
</script>
<?php
/*
$optionValuteConti['']['']['']='';
foreach ($optionConti as $id_cliente => $dati) {
	$optionValuteConti[$id_cliente]['']='';
	if (is_array($dati)) {
		foreach ($dati as $id_conto => $dati1) {
			$optionValuteConti[$id_cliente][$id_conto]='';
			$qry="select id_valuta from conti where id_conto=".$id_conto;
			$res=$db->query($qry);
			$record = $res->fetchRow();
			//$optionValuteConti[$id_cliente][$id_conto]['']='';
			$optionValuteConti[$id_cliente][$id_conto][$record['id_valuta']]=$optionValute[$record['id_valuta']];
		}
	}
}
*/
echo '<div class="titoloAdmin">'.LINK_MENU_SN_PORTFOLIO.'</div>';

switch ($_REQUEST['act']) {

	case "form": // form new / mod
		$record=array();
		if (!empty($_REQUEST['id_portfolio'])) {
			echo '<h1>'.VIEW_MOD.'</h1>';
			# dati PORTFOLIO
			$qry="select * from portfolio where id_portfolio=".$_REQUEST['id_portfolio'];
			$res=$db->query($qry);
			$record = $res->fetchRow();
			$record['id_valuta_conto']=$optionContiDati[$record['id_conto']]['id_valuta'];
			$record['conto'][0]=$record['id_cliente'];
			$record['conto'][1]=$record['id_conto'];
			//$record['conto'][2]=$record['id_valuta'];
		} else {
			echo '<h1>'.INSERT_NEW.'</h1>';
		}


		$form = new HTML_QuickForm('FormUpdate', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
		$form->addElement('hidden', 'page', $_REQUEST['page']);
		$form->addElement('hidden', 'act', 'upd');
		$form->addElement('hidden', 'id_portfolio', $_REQUEST['id_portfolio']);
		$form->addElement('hidden', 'data_richiesta', $record['data_richiesta']);
		$form->addElement('hidden', 'data_attivazione', $record['data_attivazione']);
		$form->addElement('hidden', 'old_stato', $record['stato']);
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_ATTIVAZIONE.'</div><div class="fieldForm">'.(!empty($record['data_attivazione'])?$func->formatData($record['data_attivazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		

		$form->addElement('select', 'stato', LABEL_STATO_PORTFOLIO, $arrayStatiOperazioni, ' class="textbox" ');

		$form->addElement('hidden', 'id_valuta_conto', $record['id_valuta_conto']);
		$form->addElement('hidden', 'id_valuta', $record['id_valuta']);
		$form->addElement('select', 'id_tipo_investimento', LABEL_TIPO_INVESTIMENTO, $optionTipiInvestimenti, ' class="textbox" onchange="inserisciValoreQuota();"');
		
		//$form->addElement('select', 'id_cliente', LABEL_CLIENTE, $optionClienti, ' class="textbox" ');
		//$form->addElement('select', 'id_tipo_conto', LABEL_TIPO_CONTO, $optionTipiConti, ' class="textbox" ');
		$sel =& $form->addElement('hierselect', 'conto', LABEL_CONTO,'class="textbox" onchange="inserisciValutaConto();"');
		$sel->setOptions(array($optionClienti,$optionConti));
		

		$form->addElement('text', 'numero_quote', LABEL_NUMERO_QUOTE,' class="textbox" size="25" onChange="verificaDeposito();" ');

		$form->addElement('text', 'valore_quota_iniziale', LABEL_VALORE_INIZIALE, ' class="textbox" size="25" readonly');
				
		$form->addRule('stato', LABEL_STATO_CONTO, 'required', FALSE,'client');
		$form->addRule('id_tipo_investimento', LABEL_TIPO_INVESTIMENTO, 'required', FALSE,'client');
		$form->addRule('conto', LABEL_CONTO, 'required', FALSE,'client');

		$form->addRule(array('id_valuta', 'id_valuta_conto'), VALUTE_MATCH, 'compare', null, 'client');
		//$form->addRule('id_valuta', LABEL_VALUTA, 'required', FALSE,'client');
		$form->addRule('numero_quote', LABEL_NUMERO_QUOTE, 'required', FALSE,'client');
		$form->addRule('numero_quote', LABEL_NUMERO_QUOTE.' '.NOT_NUM, 'numeric', FALSE,'client');
		$form->addRule('valore_quota_iniziale', LABEL_VALORE_INIZIALE, 'required', FALSE,'client');
		$form->addRule('valore_quota_iniziale', LABEL_VALORE_INIZIALE.' '.NOT_NUM, 'numeric', FALSE,'client');
		
		$form->setDefaults($record); // da tabella
	
		$form->setRequiredNote(REQUIRED_FIELDS);
		$form->setJsWarnings(ERRORE_JS,'');
		
		// CREO I PULSANTI
		$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', SAVE,'class="button"');
		$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
		$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
		
		$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
	
		// mostro il form
		$form->display();
	
	break;
	
	case "upd": // insert / update
		echo '<h1>'.UPDATE.'</h1>';
		
		if (empty($_REQUEST['data_attivazione']) && $_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $_REQUEST['data_attivazione']=date("Y-m-d H:i:s");
		
		
		$_REQUEST['id_cliente']=$_REQUEST['conto'][0];
		$_REQUEST['id_conto']=$_REQUEST['conto'][1];
		//$_REQUEST['id_valuta']=$optionInvestimenti[$_REQUEST['id_tipo_investimento']]['id_valuta'];
		
/* elimino ogni controllo */		
		/*
		// verifico di nuovo:
		// id_valuta
		if ($_REQUEST['id_valuta']!=$_REQUEST['id_valuta_conto']) {
			$goPage->alertgo(VALUTE_MATCH, 'index.php?page='.$_REQUEST['page'].'&act=list&id_portfolio='.$_REQUEST['id_portfolio']);
			exit();
		}
		// giacenza conto
		if ($optionContiDati[$_REQUEST['id_conto']]['saldo_attuale']<($_REQUEST['numero_quote']*$_REQUEST['valore_quota_iniziale'])) {
			$goPage->alertgo(GIACENZA_INSUFF, 'index.php?page='.$_REQUEST['page'].'&act=list&id_portfolio='.$_REQUEST['id_portfolio']);
			exit();
		}
		
		// abilitazione conto
		if ($optionContiDati[$_REQUEST['id_conto']]['abilita_prelievo']==0 || $optionContiDati[$_REQUEST['id_conto']]['abilita_deposito']==0) {
			$goPage->alertgo(CONTO_ABILITATO, 'index.php?page='.$_REQUEST['page'].'&act=list&id_portfolio='.$_REQUEST['id_portfolio']);
			exit();
		}
*/	
		
		if (!empty($_REQUEST['id_portfolio'])) {
			# update
			$query=$sql->prepareQuery ('portfolio', $_REQUEST, 'update', "id_portfolio='".$_REQUEST['id_portfolio']."'");
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=$_REQUEST['id_portfolio'];
		} else {
			# insert
			$query=$sql->prepareQuery ('portfolio', $_REQUEST, 'insert');
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=mysql_insert_id();
		}	
			
		// attivazione portfolio dal form
		$_REQUEST['id_portfolio']=$lastid;
		if ($_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $zealandCredit->attivaPortfolio();			

		$goPage->alertgo(UPDATE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');
			
			
	break;
	
	case "del": // delete
		echo '<h1>'.DELETE.'</h1>';

		if (!empty($_REQUEST['id_portfolio'])) {
			
			$query="delete from portfolio where id_portfolio='".$_REQUEST['id_portfolio']."'";
			$res=$db->query($query);

			$query="delete from transazioni where id_portfolio='".$_REQUEST['id_portfolio']."'";
			$res=$db->query($query);
			
			$goPage->alertgo(DELETE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');
			
		} else $goPage->alertback(NO_RECORD, false);

	break;
	
	case "attiva": // attiva
		echo '<h1>'.ABILITA_PORTFOLIO.'</h1>';
		if (!empty($_REQUEST['id_portfolio'])) {
			
			$zealandCredit->attivaPortfolio();			
			
		} else $goPage->alertback(NO_RECORD, false);
	break; // end attiva
	
	default: // list
		
		// elenco tutti i conti filtrati per cliente / stato / tipo / deposito iniziale / attuale
		// attivabile da elenco con invio email all'utente
		echo '<h1>'.LIST_TABLE;
		echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form" title="'.INSERT_NEW.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.INSERT_NEW.'" /></a>';
		echo '</h1>';
		
		$qry="select por.*, cli.*, con.numero_conto, con.id_valuta from conti as con, portfolio as por, clienti as cli where por.id_cliente=cli.id_cliente and cli.id_cliente=con.id_cliente and por.id_conto=con.id_conto";
		
		# filtri
		$qry .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?" and (cli.cognome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.nome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.ragione_sociale like '%".$_REQUEST['filtra_nome_cognome_soc']."%')":'');

		$qry .=(!empty($_REQUEST['filtra_tipo_investimento']) && $_REQUEST['filtra_tipo_investimento']!='*'?" and por.id_tipo_portfolio = ".$_REQUEST['filtra_tipo_investimento']."":'');

		$qry .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?" and por.stato '= ".$_REQUEST['filtra_valuta']."'":'');


		# order
		$qry .="  order by cli.cognome, cli.nome, cli.ragione_sociale, por.id_portfolio";

		$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list';
		$link_extra_param .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?"&amp;filtra_nome_cognome_soc=".$_REQUEST['filtra_nome_cognome_soc']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_investimento']) && $_REQUEST['filtra_tipo_investimento']!='*'?"&amp;filtra_tipo_investimento=".$_REQUEST['filtra_tipo_investimento']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?"&amp;filtra_valuta=".$_REQUEST['filtra_valuta']:'');
		

			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
			
			echo $view_links;
		?><br /><br />
		<form name="fFast_upd" method="post" action="index.php" onsubmit="return controllaForm('fFast_upd');">
		<input type="hidden" name="act" value="fast_upd" />
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
		<input type="hidden" name="filter" value="0" />
		<table class="tblAdmin">
		<tr>
			<th style="width:30px;"></th>
			<th style="width:30px;"></th>
			<th><?php echo LABEL_NOME_SOCIETA; ?></th>
			<th><?php echo LABEL_NOME_INVESTIMENTO; ?></th>
			<th><?php echo LABEL_DESCRIZIONE; ?></th>
			<th><?php echo LABEL_VALUTA; ?></th>
			<th><?php echo LABEL_VALORE_QUOTA; ?></th>
			<th><?php echo LABEL_NUMERO_QUOTE; ?></th>
			<th><?php echo LABEL_TOT_INVESTIMENTO; ?></th>
			<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			<th style="width:30px;"><?php echo LABEL_TRADE; ?></th>
		</tr>
        
        <!-- filtri -->
        <tr>
            <td colspan="2" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
            <td><?php
            // cognome / nome / ragione_sociale
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_nome_cognome_soc']?>" name="filtra_nome_cognome_soc" class="textbox" size="15" /></td>
            <td><?php
            // id_tipo_investimento           
            ?><select name="filtra_tipo_investimento" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionTipiInvestimenti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo_investimento']) && $_REQUEST['filtra_tipo_investimento']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
            <td></td>
            <td><?php
            // stato            
            ?><select name="filtra_valuta" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionValute as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
	<?php    
		$class="tdRow1";
		while ($record =& $res->fetchRow()) {
			$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_portfolio='.$record['id_portfolio'].'" onclick="if (confirm(confirm_attiva_portfolio)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATTIVA_PORTFOLIO.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATTIVA_PORTFOLIO.'" title="'.PENDING.'-'.ATTIVA_PORTFOLIO.'" /></a>';
		
		//	print_r($record);
		// il portfolio non � modificabile se attivo, perch� ho gi� scalato il saldo del conto
		
		### lascio modificabile, modificano saldo a mano
			echo '
			<tr>';
			
/*			echo '
				<td class="'.$class.'">'.($record['stato']!='A'?'<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_portfolio='.$record['id_portfolio'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a>':'').'</td>
				<td class="'.$class.'">';
			if ($record['stato']!='A') echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_portfolio=".$record['id_portfolio']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			*/
			echo '
				<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_portfolio='.$record['id_portfolio'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
				<td class="'.$class.'">';
			echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_portfolio=".$record['id_portfolio']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			
			echo "</td>";
			echo "\n".'<td class="'.$class.'">'.$record['ragione_sociale'].' '.$record['cognome'].' '.$record['nome'].'</td>
				<td class="'.$class.'">'.$optionTipiInvestimenti[$record['id_tipo_investimento']].'</td>
				<td class="'.$class.'">'.$optionInvestimenti[$record['id_tipo_investimento']]['descrizione'].'</td>
				<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
				<td class="'.$class.'">'.number_format($record['valore_quota_iniziale'],2,',','.').'</td>
				<td class="'.$class.'">'.$record['numero_quote'].'</td>
				<td class="'.$class.'">'.number_format(($record['valore_quota_iniziale']*$record['numero_quote']),2,',','.').'</td>
				<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
				<td class="'.$class.'">'.($record['stato']=='A'?'<a href="'.$path_web.'?page=transactions&act=form&id_tipo_transazione=6&title='.TRADE_MARKER.'&amp;id_portfolio='.$record['id_portfolio'].'" title="'.LABEL_TRADE.'"><img src="'.$path_web.'img/icone/refresh2.png" alt="'.LABEL_TRADE.'" width="24" height="24" style="border: 0px;" /></a>':'').'</td>
			</tr>';
			
			$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
			
		} // end while
	?>
		</table>
		</form>
	
	<?php	
			echo $view_links;

		} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
		
}
	
?>
