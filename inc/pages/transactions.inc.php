<?php
# transactions.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}


echo '<div class="titoloAdmin">'.LINK_MENU_SN_TRANSACTIONS.'</div>';

$optionClientiConti=$zealandCredit->getClientiConti(true);
$optionValute = $zealandCredit->getValute($stato_opzioni, false);

$optionClienti=$zealandCredit->getClienti(true);
$optionConti=$zealandCredit->getContiDati(true);

$optionCarte = $zealandCredit->getCarte();
$optionContiDaA =$zealandCredit->getContiClienti();

$optionCarteDaA =$zealandCredit->getCarteClienti();
$optionCarteDebitoDaA =$zealandCredit->getCarteClienti(true,'D');

/*echo '<pre>';
print_r($optionCarteDaA);
echo '</pre>';*/

$optionContiNum=array();
$optionContiNum['']='';
foreach ($optionConti as $id => $dati) {
	$optionContiNum[$id]=$dati['numero_conto'].' ('.$optionValute[$dati['id_valuta']].')';
}

$optionPaesi=$zealandCredit->getPaesi();

$optionInvestimenti = $zealandCredit->getTipiInvestimenti('tipi_investimenti', $_SESSION['lingua'], $stato_opzioni, false);

$optionTipiComm=$zealandCredit->getTypes('tipi_commissioni', $_SESSION['lingua'], true, true);

$optionBuySell['B']=BUY;
$optionBuySell['S']=SELL;

$optionTransazioni = $zealandCredit->getTypes('tipi_transazioni', $_SESSION['lingua'], $stato_opzioni);

$optionNormalExpress['N']=NORMAL;
$optionNormalExpress['E']=EXPRESS;


foreach ($optionInvestimenti as $id => $dati) $selectInvestimenti[$id]=$dati['nome_tipo'];

switch ($_REQUEST['act']) {
	
	case "view":
	
		if (!empty($_REQUEST['id_transazione']) && !empty($_REQUEST['id_tipo_transazione'])) {
			
			$qry="select * from transazioni where id_transazione=".$_REQUEST['id_transazione']." and id_tipo_transazione=".$_REQUEST['id_tipo_transazione'];
			$res=$db->query($qry);
			$record = $res->fetchRow();
			
			$record['importo_transazione']=number_format($record['importo_transazione'],2,',','.');
			
			$form = new HTML_QuickForm('FormUpdate', 'post', $_SERVER['SCRIPT_NAME'], '', '', false);
			$form->addElement('select', 'id_richiedente', LABEL_RICHIEDENTE, $optionClienti, ' class="textbox" ');

			$form->addElement('select', 'id_tipo_transazione', LABEL_TIPO_TRANSAZIONE, $optionTransazioni, ' class="textbox"');

			$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
			$form->addElement('select', 'normal_express', NORMAL.'/'.EXPRESS, $optionNormalExpress, ' class="textbox" ');
			
			$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_ATTIVAZIONE.'</div><div class="fieldForm">'.(!empty($record['data_attivazione'])?$func->formatData($record['data_attivazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
			
			//$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_TRANSAZIONE.'</div><div class="fieldForm">'.(!empty($record['  	data_transazione'])?$func->formatData($record['  	data_transazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
	
			$form->addElement('select', 'stato', LABEL_STATO_TRANSAZIONE, $arrayStatiOperazioni, ' class="textbox" ');
			
			if ($record['id_tipo_transazione']==7) $form->addElement('select', 'id_tipo_commissione', TIPO_COMMISSIONE, $optionTipiComm, ' class="textbox" ');
			$form->addElement('select', 'id_valuta', LABEL_VALUTA, $optionValute, ' class="textbox" ');
			

			$form->addElement('text', 'importo_transazione', LABEL_IMPORTO, ' class="textbox" ');
			$form->addElement('textarea', 'descrizione', CAUSALE, ' class="textbox" ');
  	    	    	 
			switch ($record['id_tipo_transazione']) {
				
				case "1": # Transfer Between Accounts
				case "2": # Transfer to Other User
				
					$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
					
					$form->addElement('select', 'id_cliente_a', CLIENTE_TO, $optionClienti, ' class="textbox"');
					$form->addElement('select', 'id_conto_a', CONTO_TO, $optionContiNum, ' class="textbox"');
			
				break;
				
				case "4": # Outgoing Wire Transfer
				case "6": # Trade Marker
				case "7": # Fees
					$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
				break;

				case "5": # Incoming Wire Transfer
					$form->addElement('select', 'id_cliente_a', CLIENTE_TO, $optionClienti, ' class="textbox"');
					$form->addElement('select', 'id_conto_a', CONTO_TO, $optionContiNum, ' class="textbox"');
				break;
								
				case "8": # accredito interessi
					$form->addElement('select', 'id_cliente_a', CLIENTE_TO, $optionClienti, ' class="textbox"');
					$form->addElement('select', 'id_conto_a', CONTO_TO, $optionContiNum, ' class="textbox"');
				break;

				case "9": # Visa Debit Card fee
					//$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					//$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
					//$form->addElement('select', 'id_carta_da', CARTA_FROM, $optionCarte, ' class="textbox"');
					
					$record['cliente_conto_carta_da'][0]=$record['id_cliente_da'];
					$record['cliente_conto_carta_da'][1]=$record['id_conto_da'];
					$record['cliente_conto_carta_da'][2]=$record['id_carta_da'];
		
					
					$sel =& $form->addElement('hierselect', 'cliente_conto_carta_da', CLIENTE_FROM,'class="textbox"');
					//print_r($optionContiDaA);
					/*echo '<pre>';
					foreach ($optionContiDaA as $id_cli => $conto) {
						foreach ($conto as $id_con => $dati) $optionContiDaA[$id_cli][$id_con]['']=''; 
					}
					print_r($optionContiDaA);*/
					$sel->setOptions(array($optionClientiConti,$optionContiDaA,$optionCarteDaA));
				break;

				case "10": # Trasferimento alla Carta di Debito
					//$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					//$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
					//$form->addElement('select', 'id_carta_a', CARTA_TO, $optionCarte, ' class="textbox"');
					$record['cliente_conto_carta_a'][0]=$record['id_cliente_da'];
					$record['cliente_conto_carta_a'][1]=$record['id_conto_da'];
					$record['cliente_conto_carta_a'][2]=$record['id_carta_a'];
		
					$sel =& $form->addElement('hierselect', 'cliente_conto_carta_a', CLIENTE_FROM,'class="textbox"');
					$sel->setOptions(array($optionClientiConti,$optionContiDaA,$optionCarteDebitoDaA));
				break;
				
			}
					 
//			$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
//			$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
			
//			$form->addElement('select', 'id_cliente_a', CLIENTE_TO, $optionClienti, ' class="textbox"');
//			$form->addElement('select', 'id_conto_a', CONTO_TO, $optionContiNum, ' class="textbox"');

			if ($record['id_tipo_transazione']==6) {
				$form->addElement('select', 'id_tipo_investimento', LABEL_TIPO_INVESTIMENTO, $selectInvestimenti, ' class="textbox" ');
			
				$form->addElement('text', 'valore_quota', LABEL_VALORE_QUOTA, ' class="textbox" ');
				$form->addElement('text', 'numero_quote', LABEL_NUMERO_QUOTE, ' class="textbox" ');
	
				$form->addElement('select', 'buy_sell', BUY.'/'.SELL, $optionBuySell, ' class="textbox" ');
			}
			
			   	    	    	    	 
		//	 id_gestione_offshore  
			$qry2="select * from gestione_offshore where id_transazione=".$_REQUEST['id_transazione'];
			$res2=$db->query($qry2);
			$record2 = $res2->fetchRow();
		 	
			
			if ($record['id_tipo_transazione']==5) {
				// in
				$form->addElement('html', '<div class="labelForm"><i>'.DATI_BONIFICO_IN.'</i></div><div class="clearBoth"></div>');
				$form->addElement('text', 'nome_ba_in', LABEL_BANCA, ' class="textbox" size="60"');
				$form->addElement('text', 'swift_ba_in', LABEL_SWIFT, ' class="textbox" size="60"');
				$form->addElement('text', 'indirizzo_ba_in', LABEL_INDIRIZZO, ' class="textbox" size="60"');
				$form->addElement('text', 'cap_ba_in', LABEL_CAP, ' class="textbox" size="60"');
				$form->addElement('text', 'citta_ba_in', LABEL_CITTA, ' class="textbox" size="60"');
				$form->addElement('select', 'codice_paese_ba_in', LABEL_PAESE, $optionPaesi, ' class="textbox"');
				$form->addElement('text', 'ncn_ba_in', LABEL_NCN, ' class="textbox" size="60"');
				$form->addElement('text', 'aba_ba_in', LABEL_ABA, ' class="textbox" size="60"');
				$form->addElement('text', 'intestatario_ba_in', LABEL_INTESTATARIO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'numero_conto_ba_in', LABEL_NUMERO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'iban_ba_in', LABEL_IBAN, ' class="textbox" size="60"');
			}
			
			
			if ($record['id_tipo_transazione']==4) {
				// out
				$form->addElement('html', '<div class="labelForm"><i>'.DATI_BONIFICO_OUT.'</i></div><div class="clearBoth"></div>');
				//$form->addElement('html', '<i>'.DATI_BONIFICO_OUT.'</i>');
				$form->addElement('text', 'nome_ba_out', LABEL_BANCA, ' class="textbox" size="60"');
				$form->addElement('text', 'swift_ba_out', LABEL_SWIFT, ' class="textbox" size="60"');
				$form->addElement('text', 'indirizzo_ba_out', LABEL_INDIRIZZO, ' class="textbox" size="60"');
				$form->addElement('text', 'cap_ba_out', LABEL_CAP, ' class="textbox" size="60"');
				$form->addElement('text', 'citta_ba_out', LABEL_CITTA, ' class="textbox" size="60"');
				$form->addElement('select', 'codice_paese_ba_out', LABEL_PAESE, $optionPaesi, ' class="textbox"');
				$form->addElement('text', 'ncn_ba_out', LABEL_NCN, ' class="textbox" size="60"');
				$form->addElement('text', 'aba_ba_out', LABEL_ABA, ' class="textbox" size="60"');
				$form->addElement('text', 'intestatario_ba_out', LABEL_INTESTATARIO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'numero_conto_ba_out', LABEL_NUMERO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'iban_ba_out', LABEL_IBAN, ' class="textbox" size="60"');
			}
			
			$form->setDefaults($record); // da tabella
			$form->setDefaults($record2); // da tabella
			
			if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) {
				// CREO I PULSANTI
				$buttons[]=&HTML_QuickForm::createElement('button', 'btnPrint', LABEL_PRINT, 'class="button" onClick="window.open(\'print.php?page='.$_REQUEST['page'].'&act=view&id_tipo_transazione='.$_REQUEST['id_tipo_transazione'].'&id_transazione='.$_REQUEST['id_transazione'].'\', \'_blank\', \'width=650, height=500, scrollbars=yes, resizable=yes\'); return false;"');
				
				$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST, 'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list&id_tipo_transazione='.$_REQUEST['id_tipo_transazione'].'\';"');
			
				$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
			}
			
			$form->freeze(); 
			// mostro il form
			$form->display();
			
		} else $goPage->alertback(NO_RECORD, false);
	
	break; // end view
	
	case "form":
	
		if (!empty($_REQUEST['id_tipo_transazione'])) {
			if (!empty($_REQUEST['id_transazione'])) {
			
				echo '<h1>'.VIEW_MOD.' '.$optionTransazioni[$_REQUEST['id_tipo_transazione']].'</h1>';
				$qry="select * from transazioni where id_transazione=".$_REQUEST['id_transazione']." and id_tipo_transazione=".$_REQUEST['id_tipo_transazione'];
				$res=$db->query($qry);
				$record = $res->fetchRow();
				$record['cliente_conto_da'][0]=$record['id_cliente_da'];
				$record['cliente_conto_da'][1]=$record['id_conto_da'];
				$record['cliente_conto_a'][0]=$record['id_cliente_a'];
				$record['cliente_conto_a'][1]=$record['id_conto_a'];
			} else {
				echo '<h1>'.INSERT_NEW.' '.$optionTransazioni[$_REQUEST['id_tipo_transazione']].'</h1>';
				$record['data_richiesta'] = date("Y-m-d H:i:s");
				$record['id_richiedente'] = $_SESSION['utente']['id_cliente'];
				$record['id_tipo_transazione']=$_REQUEST['id_tipo_transazione'];
			}
			
			$form = new HTML_QuickForm('FormUpdate', 'post', $_SERVER['SCRIPT_NAME'], '', '', false);
			$form->addElement('hidden', 'page', $_REQUEST['page']);
			$form->addElement('hidden', 'act', 'upd');
			$form->addElement('hidden', 'data_richiesta', $record['data_richiesta']);
			$form->addElement('hidden', 'data_attivazione', $record['data_attivazione']);
			$form->addElement('hidden', 'data_transazione', $record['data_transazione']);
			$form->addElement('hidden', 'old_stato', $record['stato']);
			$form->addElement('hidden', 'id_tipo_transazione', $_REQUEST['id_tipo_transazione']);
			$form->addElement('hidden', 'id_transazione', $_REQUEST['id_transazione']);

			$form->addElement('select', 'id_tipo_transazione', LABEL_TIPO_TRANSAZIONE, $optionTransazioni, ' class="textbox" disabled="disabled"');
			
			$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');

			$form->addElement('select', 'id_richiedente', LABEL_RICHIEDENTE, $optionClienti, ' class="textbox" ');
			
			$form->addElement('select', 'normal_express', NORMAL.'/'.EXPRESS, $optionNormalExpress, ' class="textbox" ');

			$form->addElement('select', 'stato', LABEL_STATO_TRANSAZIONE, $arrayStatiOperazioni, ' class="textbox" ');
			
			$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_ATTIVAZIONE.'</div><div class="fieldForm">'.(!empty($record['data_attivazione'])?$func->formatData($record['data_attivazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
			
			if ($_REQUEST['id_tipo_transazione']==7) $form->addElement('select', 'id_tipo_commissione', TIPO_COMMISSIONE, $optionTipiComm, ' class="textbox" ');
			$form->addElement('select', 'id_valuta', LABEL_VALUTA, $optionValute, ' class="textbox" ');
			

			$form->addElement('text', 'importo_transazione', LABEL_IMPORTO, ' class="textbox" size="25" ');
			$form->addElement('textarea', 'descrizione', CAUSALE, ' class="textbox" rows="5" cols="60"');
  	    	    
				 
			switch ($_REQUEST['id_tipo_transazione']) {
				
				case "1": # Transfer Between Accounts
					
				//	$form->addElement('hidden', 'id_cliente_da', $_SESSION['utente']['id_cliente']);
				//	$form->addElement('hidden', 'id_cliente_a', $_SESSION['utente']['id_cliente']);
					
				//	$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
				//	$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
					
				//	$form->addElement('select', 'id_cliente_a', CLIENTE_TO, $optionClienti, ' class="textbox"');
				//	$form->addElement('select', 'id_conto_a', CONTO_TO, $optionContiNum, ' class="textbox"');
				
					$sel =& $form->addElement('hierselect', 'cliente_conto_da', CLIENTE_FROM,'class="textbox"');
					$sel->setOptions(array($optionClientiConti,$optionContiDaA));
					
					$sel =& $form->addElement('hierselect', 'cliente_conto_a', CLIENTE_TO,'class="textbox"');
					$sel->setOptions(array($optionClientiConti,$optionContiDaA));
					
				break;
				
				case "2": # Transfer to Other User
					//$form->addElement('hidden', 'id_cliente_da', $_SESSION['utente']['id_cliente']);
			
					//$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					//$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
					
					//$form->addElement('select', 'id_cliente_a', CLIENTE_TO, $optionClienti, ' class="textbox"');
					//$form->addElement('select', 'id_conto_a', CONTO_TO, $optionContiNum, ' class="textbox"');
					
					$sel =& $form->addElement('hierselect', 'cliente_conto_da', CLIENTE_FROM,'class="textbox"');
					$sel->setOptions(array($optionClientiConti,$optionContiDaA));
					
					$form->addElement('text', 'cliente_a', CLIENTE_TO, ' class="textbox" size="60"');
					$form->addElement('text', 'conto_a', CONTO_TO, ' class="textbox" size="60"');
				
					$sel =& $form->addElement('hierselect', 'cliente_conto_a', CLIENTE_TO,'class="textbox"');
					$sel->setOptions(array($optionClientiConti,$optionContiDaA));
					
				break;
				
				case "4": # Outgoing Wire Transfer
				case "6": # Trade Marker
				case "7": # Fees
					
					//$form->addElement('hidden', 'id_cliente_da', $_SESSION['utente']['id_cliente']);
					//$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					//$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
				
					//$sel =& $form->addElement('select', 'cliente_conto_da', CLIENTE_FROM,'class="textbox"');
					$sel =& $form->addElement('hierselect', 'cliente_conto_da', CLIENTE_FROM,'class="textbox"');
					$sel->setOptions(array($optionClientiConti,$optionContiDaA));
				break;
				

				case "5": # Incoming Wire Transfer
				case "8": # accredito interessi
					//$form->addElement('hidden', 'id_cliente_a', $_SESSION['utente']['id_cliente']);
					//$form->addElement('select', 'id_cliente_a', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					//$form->addElement('select', 'id_conto_a', CONTO_TO, $optionContiNum, ' class="textbox"');
					//$sel =$form->addElement('select', 'cliente_conto_a', CLIENTE_TO,'class="textbox"');					 
					//$sel =& $form->addElement('hierselect', 'cliente_conto_a', CLIENTE_TO,$optionClientiConti,'class="textbox"');
					//$sel->setOptions(array($optionClientiConti,$optionContiDaA));
				break;
								

				case "9": # Visa Debit Card fee
					//$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					//$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
					//$form->addElement('select', 'id_carta_da', CARTA_FROM, $optionCarte, ' class="textbox"');
					
					$record['cliente_conto_carta_da'][0]=$record['id_cliente_da'];
					$record['cliente_conto_carta_da'][1]=$record['id_conto_da'];
					$record['cliente_conto_carta_da'][2]=$record['id_carta_da'];
		
					
					$sel =& $form->addElement('hierselect', 'cliente_conto_carta_da', CLIENTE_FROM,'class="textbox"');
					//print_r($optionContiDaA);
					/*echo '<pre>';
					foreach ($optionContiDaA as $id_cli => $conto) {
						foreach ($conto as $id_con => $dati) $optionContiDaA[$id_cli][$id_con]['']=''; 
					}
					print_r($optionContiDaA);*/
					$sel->setOptions(array($optionClientiConti,$optionContiDaA,$optionCarteDaA));
				break;

				case "10": # Trasferimento alla Carta di Debito
					//$form->addElement('select', 'id_cliente_da', CLIENTE_FROM, $optionClienti, ' class="textbox"');
					//$form->addElement('select', 'id_conto_da', CONTO_FROM, $optionContiNum, ' class="textbox"');
					//$form->addElement('select', 'id_carta_a', CARTA_TO, $optionCarte, ' class="textbox"');
					$record['cliente_conto_carta_a'][0]=$record['id_cliente_da'];
					$record['cliente_conto_carta_a'][1]=$record['id_conto_da'];
					$record['cliente_conto_carta_a'][2]=$record['id_carta_a'];
		
					$sel =& $form->addElement('hierselect', 'cliente_conto_carta_a', CLIENTE_FROM,'class="textbox"');
					$sel->setOptions(array($optionClientiConti,$optionContiDaA,$optionCarteDebitoDaA));
				break;
				
			}
			
				


			if ($_REQUEST['id_tipo_transazione']==6) {
				$form->addElement('select', 'id_tipo_investimento', LABEL_TIPO_INVESTIMENTO, $selectInvestimenti, ' class="textbox" ');
			
				$form->addElement('text', 'valore_quota', LABEL_VALORE_QUOTA, ' class="textbox" ');
				$form->addElement('text', 'numero_quote', LABEL_NUMERO_QUOTE, ' class="textbox" ');
	
				$form->addElement('select', 'buy_sell', BUY.'/'.SELL, $optionBuySell, ' class="textbox" ');
			}
			
		//	 id_gestione_offshore 
			if ($record['id_transazione']){ 
				$qry2="select * from gestione_offshore where id_transazione=".$_REQUEST['id_transazione'];
				$res2=$db->query($qry2);
				$record2 = $res2->fetchRow();
		 	}
			
			if ($_REQUEST['id_tipo_transazione']==5) {
				// in
				$form->addElement('html', '<div class="labelForm"><i>'.DATI_BONIFICO_IN.'</i></div><div class="clearBoth"></div>');
				$form->addElement('text', 'nome_ba_in', LABEL_BANCA, ' class="textbox" size="60"');
				$form->addElement('text', 'swift_ba_in', LABEL_SWIFT, ' class="textbox" size="60"');
				$form->addElement('text', 'indirizzo_ba_in', LABEL_INDIRIZZO, ' class="textbox" size="60"');
				$form->addElement('text', 'cap_ba_in', LABEL_CAP, ' class="textbox" size="60"');
				$form->addElement('text', 'citta_ba_in', LABEL_CITTA, ' class="textbox" size="60"');
				$form->addElement('select', 'codice_paese_ba_in', LABEL_PAESE, $optionPaesi, ' class="textbox"');
				$form->addElement('text', 'ncn_ba_in', LABEL_NCN, ' class="textbox" size="60"');
				$form->addElement('text', 'aba_ba_in', LABEL_ABA, ' class="textbox" size="60"');
				$form->addElement('text', 'intestatario_ba_in', LABEL_INTESTATARIO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'numero_conto_ba_in', LABEL_NUMERO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'iban_ba_in', LABEL_IBAN, ' class="textbox" size="60"');
			}
			
			
			
			
			
			if ($_REQUEST['id_tipo_transazione']==4) {
				// out
				$form->addElement('html', '<div class="labelForm"><i>'.DATI_BONIFICO_OUT.'</i></div><div class="clearBoth"></div>');
				//$form->addElement('html', '<i>'.DATI_BONIFICO_OUT.'</i>');
				$form->addElement('text', 'nome_ba_out', LABEL_BANCA, ' class="textbox" size="60"');
				$form->addElement('text', 'swift_ba_out', LABEL_SWIFT, ' class="textbox" size="60"');
				$form->addElement('text', 'indirizzo_ba_out', LABEL_INDIRIZZO, ' class="textbox" size="60"');
				$form->addElement('text', 'cap_ba_out', LABEL_CAP, ' class="textbox" size="60"');
				$form->addElement('text', 'citta_ba_out', LABEL_CITTA, ' class="textbox" size="60"');
				$form->addElement('select', 'codice_paese_ba_out', LABEL_PAESE, $optionPaesi, ' class="textbox"');
				$form->addElement('text', 'ncn_ba_out', LABEL_NCN, ' class="textbox" size="60"');
				$form->addElement('text', 'aba_ba_out', LABEL_ABA, ' class="textbox" size="60"');
				$form->addElement('text', 'intestatario_ba_out', LABEL_INTESTATARIO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'numero_conto_ba_out', LABEL_NUMERO_CONTO, ' class="textbox" size="60"');
				$form->addElement('text', 'iban_ba_out', LABEL_IBAN, ' class="textbox" size="60"');
			}
			
			$form->setDefaults($record); // da tabella
			$form->setDefaults($record2); // da tabella
			
			
			$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', SAVE,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST, 'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list&id_tipo_transazione='.$_REQUEST['id_tipo_transazione'].'\';"');
			$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
		
			// mostro il form
			$form->display();
			
		} else $goPage->alertback(NO_RECORD, false);
	
	break; // end view

	case "upd": // insert / update
		echo '<h1>'.$_SESSION['title'].'</h1>';

		$_REQUEST['id_cliente_da']=$_REQUEST['cliente_conto_da'][0];
		$_REQUEST['id_conto_da']=$_REQUEST['cliente_conto_da'][1];
		$_REQUEST['id_cliente_a']=$_REQUEST['cliente_conto_a'][0];
		$_REQUEST['id_conto_a']=$_REQUEST['cliente_conto_a'][1];
		
		// 9
		if ($_REQUEST['id_tipo_transazione']==9) {
		
			$_REQUEST['id_cliente_da']=$_REQUEST['cliente_conto_carta_da'][0];
			$_REQUEST['id_conto_da']=$_REQUEST['cliente_conto_carta_da'][1];
			$_REQUEST['id_carta_da']=$_REQUEST['cliente_conto_carta_da'][2];
		
		}
		
		// 10 
		if ($_REQUEST['id_tipo_transazione']==10) {
		
			$_REQUEST['id_cliente_da']=$_REQUEST['cliente_conto_carta_a'][0];
			$_REQUEST['id_conto_da']=$_REQUEST['cliente_conto_carta_a'][1];
			$_REQUEST['id_carta_a']=$_REQUEST['cliente_conto_carta_a'][2];
		
		}
		
		if (empty($_REQUEST['data_attivazione']) && $_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $_REQUEST['data_attivazione']=date("Y-m-d H:i:s");
		
		
		if (!empty($_REQUEST['id_transazione'])) {
			# update
			$query=$sql->prepareQuery ('transazioni', $_REQUEST, 'update', "id_transazione='".$_REQUEST['id_transazione']."'");
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=$_REQUEST['id_transazione'];
			//$_REQUEST['id_transazione']=$lastid;
			# gestione_offshore
			if ($_REQUEST['id_tipo_transazione'] == 4 || $_REQUEST['id_tipo_transazione'] == 5) {
			
				$_REQUEST['id_transazione']=$lastid;
				$query=$sql->prepareQuery ('gestione_offshore', $_REQUEST, 'update', "id_transazione='".$_REQUEST['id_transazione']."'");
				//echo '<br />'.$query;
				$res=$db->query($query);
			
			}
			
		} else {
			# insert
			$query=$sql->prepareQuery ('transazioni', $_REQUEST, 'insert');
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=mysql_insert_id();
			$_REQUEST['id_transazione']=$lastid;
			
			# gestione_offshore
			if ($_REQUEST['id_tipo_transazione'] == 4 || $_REQUEST['id_tipo_transazione'] == 5) {
			
				$_REQUEST['id_transazione']=$lastid;
				$query=$sql->prepareQuery ('gestione_offshore', $_REQUEST, 'insert');
				//echo '<br />'.$query;
				$res=$db->query($query);
			
			}
		}	
			
		// attivazione da form
		if ($_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $zealandCredit->attivaTransazione();
	
		$goPage->alertgo(UPDATE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list&id_tipo_transazione='.$_REQUEST['id_tipo_transazione']);


	break;
	
	case "del": // delete
		echo '<h1>'.$_SESSION['title'].'</h1>';
		
		if (!empty($_REQUEST['id_transazione'])) {
			
			$query="delete from transazioni where id_transazione='".$_REQUEST['id_transazione']."'";
			$res=$db->query($query);

			$goPage->alertgo(DELETE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list&id_tipo_transazione='.$_REQUEST['id_tipo_transazione']);
			
		} else $goPage->alertback(NO_RECORD, false);
		
	break;
	
	case "attiva": // attiva
		echo '<h1>'.ATT_TRANSAZIONE.'</h1>';
		if (!empty($_REQUEST['id_transazione'])) {
			
			$zealandCredit->attivaTransazione();			
			
		} else $goPage->alertback(NO_RECORD, false);
	break; // end attiva
	
	
	case "list": // list
		echo '<h1>'.$_SESSION['title'].'</h1>';
		
		if (!empty($_REQUEST['id_tipo_transazione'])) {
			// elenco tutte le transazioni del tipo scelto, con filtro per cliente / conto / stato
			// attivabile da elenco con invio email all'utente
			echo '<h1>'.LIST_TABLE;
			if ($_REQUEST['id_tipo_transazione']!=6) echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&id_tipo_transazione='.$_REQUEST['id_tipo_transazione'].'" title="'.INSERT_NEW.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.INSERT_NEW.'" /></a>';
			echo '</h1>';
			
			$qry="select * from transazioni where id_tipo_transazione=".$_REQUEST['id_tipo_transazione'];
			
			# filtri
	
			$qry .=(!empty($_REQUEST['filtra_cliente_da']) && $_REQUEST['filtra_cliente_da']!='*'?" and id_cliente_da = ".$_REQUEST['filtra_cliente_da']."":'');
	
			$qry .=(!empty($_REQUEST['filtra_cliente_a']) && $_REQUEST['filtra_cliente_a']!='*'?" and id_cliente_a = ".$_REQUEST['filtra_cliente_a']."":'');
	
			$qry .=(!empty($_REQUEST['filtra_conto_da']) && $_REQUEST['filtra_conto_da']!='*'?" and id_conto_da = ".$_REQUEST['filtra_conto_da']."":'');
	
			$qry .=(!empty($_REQUEST['filtra_conto_a']) && $_REQUEST['filtra_conto_a']!='*'?" and id_conto_a = ".$_REQUEST['filtra_conto_a']."":'');

// carta
			$qry .=(!empty($_REQUEST['filtra_carta_da']) && $_REQUEST['filtra_carta_da']!='*'?" and id_carta_da = ".$_REQUEST['filtra_carta_da']."":'');
	
			$qry .=(!empty($_REQUEST['filtra_carta_a']) && $_REQUEST['filtra_carta_a']!='*'?" and id_carta_a = ".$_REQUEST['filtra_carta_a']."":'');
//////////////////////

			
			$qry .=(!empty($_REQUEST['filtra_stato']) && $_REQUEST['filtra_stato']!='*'?" and stato = '".$_REQUEST['filtra_stato']."'":'');
	
			# order
			$qry .="  order by data_transazione desc";
			//echo '<!-- qry: '.$qry.' -->';
			
			$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list&id_tipo_transazione='.$_REQUEST['id_tipo_transazione'];
			$link_extra_param .=(!empty($_REQUEST['filtra_cliente_da']) && $_REQUEST['filtra_cliente_da']!=''?"&amp;filtra_cliente_da=".$_REQUEST['filtra_cliente_da']:'');
			$link_extra_param .=(!empty($_REQUEST['filtra_cliente_a']) && $_REQUEST['filtra_cliente_a']!=''?"&amp;filtra_cliente_a=".$_REQUEST['filtra_cliente_a']:'');
			$link_extra_param .=(!empty($_REQUEST['filtra_conto_da']) && $_REQUEST['filtra_conto_da']!=''?"&amp;filtra_conto_da=".$_REQUEST['filtra_conto_da']:'');
			$link_extra_param .=(!empty($_REQUEST['filtra_conto_a']) && $_REQUEST['filtra_conto_a']!=''?"&amp;filtra_conto_a=".$_REQUEST['filtra_conto_a']:'');
			$link_extra_param .=(!empty($_REQUEST['filtra_stato']) && $_REQUEST['filtra_stato']!=''?"&amp;filtra_stato=".$_REQUEST['filtra_stato']:'');

  			$link_extra_param .=(!empty($_REQUEST['filtra_carta_da']) && $_REQUEST['filtra_carta_da']!=''?"&amp;filtra_carta_da=".$_REQUEST['filtra_carta_da']:'');
			$link_extra_param .=(!empty($_REQUEST['filtra_carta_a']) && $_REQUEST['filtra_carta_a']!=''?"&amp;filtra_carta_a=".$_REQUEST['filtra_carta_a']:'');


	
			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			//echo $qry;
		/*	$tot_record=0;
			
			$resTest=$db->query($qry);
			$rows = $resTest->numRows();
				echo '<!--';
				echo $rows;
				echo ' -->';
			while ($recTest =& $resTest->fetchRow()) {
				echo '<!--';
				print_r($recTest);
				echo ' -->';
			}*/
			
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			//
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
				
				// solo i conti attivi
				$selectConti_da='';
				foreach ($optionConti as $id => $dati) {
					if ($dati['stato']=='A') $selectConti_da .= '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_conto_da']) && $_REQUEST['filtra_conto_da']==$id?' selected':'').'>'.$dati['numero_conto'].'</option>';
				}
				
				$selectConti_a='';
				foreach ($optionConti as $id => $dati) {
					if ($dati['stato']=='A') $selectConti_a .= '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_conto_a']) && $_REQUEST['filtra_conto_a']==$id?' selected':'').'>'.$dati['numero_conto'].'</option>';
				}
				
	/*			// solo carte attive
				$selectCarte_da='';
				foreach ($optionCarte as $id => $dati) {
					if ($dati['stato']=='A') $selectCarte_da .= '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_carta_da']) && $_REQUEST['filtra_carta_da']==$id?' selected':'').'>'.$dati['numero_carta'].'</option>';
				}
				
				$selectCarte_a='';
				foreach ($optionCarte as $id => $dati) {
					if ($dati['stato']=='A') $selectCarte_a .= '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_carta_a']) && $_REQUEST['filtra_carta_a']==$id?' selected':'').'>'.$dati['numero_carta'].'</option>';
				}*/
				
			echo $view_links;
		?><br /><br />
		<form name="fFast_upd" method="post" action="index.php" onsubmit="return controllaForm('fFast_upd');">
			<input type="hidden" name="act" value="fast_upd" />
			<input type="hidden" name="id_tipo_transazione" value="<?php echo $_REQUEST['id_tipo_transazione']; ?>" />
			<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
			<input type="hidden" name="filter" value="0" />
            
            <?php
			
			switch ($_REQUEST['id_tipo_transazione']) {
				
				case "1": // Transfer Between Accounts

?>
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>		
				<th style="width:30px;"></th>
                <th><?php echo CLIENTE_FROM; ?></th>
				<th><?php echo CONTO_FROM; ?></th>
				<th><?php echo CONTO_TO; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
				<th><?php echo LABEL_DATA; ?></th>
				<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			</tr>
			
			<!-- filtri -->
			<tr>
				<td colspan="3" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
				<td><?php
				// id_cliente_da            
				?><select name="filtra_cliente_da" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionClienti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_cliente_da']) && $_REQUEST['filtra_cliente_da']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td><?php
				// id_conto_da            
				?><select name="filtra_conto_da" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectConti_da;
					?>
				</select></td>
                
				<td><?php
				// id_conto_a            
				?><select name="filtra_conto_a" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectConti_a;
					?>
				</select></td>

				<td></td>
				<td></td>
				<td></td>
				<td><?php
				// stato            
				?><select name="filtra_stato" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($arrayStatiOperazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_stato']) && $_REQUEST['filtra_stato']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
				$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATT_TRANSAZIONE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATT_TRANSAZIONE.'" title="'.PENDING.'-'.ATT_TRANSAZIONE.'" /></a>';
			//	print_r($record);
				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=view&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW.'"><img src="'.$path_web.'img/icone/file_find.png" width="24" height="24" style="border: 0px;" alt="'.VIEW.'" /></a></td>
<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
					<td class="'.$class.'">';
				
				echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_tipo_transazione=".$_REQUEST['id_tipo_transazione']."&amp;id_transazione=".$record['id_transazione']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
				
				echo "</td>";
				echo "\n".'<td class="'.$class.'">'.$optionClienti[$record['id_cliente_da']].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_conto_da']]['numero_conto'].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_conto_a']]['numero_conto'].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
					<td class="'.$class.'">'.number_format($record['importo_transazione'],2,',','.').'</td>
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
				</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>
			
<?php

					
				break; // end 1 // Transfer Between Accounts
				
				case "2": // Transfer to Other User


?>
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>		
				<th style="width:30px;"></th>
                <th><?php echo CLIENTE_FROM; ?></th>
				<th><?php echo CONTO_FROM; ?></th>
				<th><?php echo CLIENTE_TO; ?></th>
				<th><?php echo CONTO_TO; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
				<th><?php echo LABEL_DATA; ?></th>
				<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			</tr>
			
			<!-- filtri -->
			<tr>
				<td colspan="3" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
				<td><?php
				// id_cliente_da            
				?><select name="filtra_cliente_da" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionClienti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_cliente_da']) && $_REQUEST['filtra_cliente_da']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td><?php
				// id_conto_da            
				?><select name="filtra_conto_da" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectConti_da;
					?>
				</select></td>
                
				<td><?php
				// id_cliente_a            
				?><select name="filtra_cliente_a" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionClienti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_cliente_a']) && $_REQUEST['filtra_cliente_a']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td><?php
				// id_conto_a            
				?><select name="filtra_conto_a" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectConti_a;
					?>
				</select></td>

				<td></td>
				<td></td>
				<td></td>
				<td><?php
				// stato            
				?><select name="filtra_stato" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($arrayStatiOperazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_stato']) && $_REQUEST['filtra_stato']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
				$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATT_TRANSAZIONE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATT_TRANSAZIONE.'" title="'.PENDING.'-'.ATT_TRANSAZIONE.'" /></a>';
			//	print_r($record);
				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=view&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW.'"><img src="'.$path_web.'img/icone/file_find.png" width="24" height="24" style="border: 0px;" alt="'.VIEW.'" /></a></td>
<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
					<td class="'.$class.'">';
				
				echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_tipo_transazione=".$_REQUEST['id_tipo_transazione']."&amp;id_transazione=".$record['id_transazione']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
				
				echo "</td>";
				echo "\n".'<td class="'.$class.'">'.$optionClienti[$record['id_cliente_da']].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_conto_da']]['numero_conto'].'</td>
					<td class="'.$class.'">'.$optionClienti[$record['id_cliente_a']].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_conto_a']]['numero_conto'].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
					<td class="'.$class.'">'.number_format($record['importo_transazione'],2,',','.').'</td>
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
				</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>

<?php
				
				
				break; // end 2 // Transfer to Other User
				
				case "7": // Fees

?>
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>		
				<th style="width:30px;"></th>
                <th><?php echo LABEL_CLIENTE; ?></th>
				<th><?php echo CONTO_FROM; ?></th>
				<th><?php echo TIPO_COMMISSIONE; ?></th>
				<th><?php echo CAUSALE; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
				<th><?php echo LABEL_DATA; ?></th>
				<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			</tr>
			
			<!-- filtri -->
			<tr>
				<td colspan="3" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
				<td><?php
				// id_cliente_da            
				?><select name="filtra_cliente_da" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionClienti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_cliente_da']) && $_REQUEST['filtra_cliente_da']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td><?php
				// id_conto_da            
				?><select name="filtra_conto_da" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectConti_da;
					?>
				</select></td>
                
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?php
				// stato            
				?><select name="filtra_stato" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($arrayStatiOperazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_stato']) && $_REQUEST['filtra_stato']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
				$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATT_TRANSAZIONE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATT_TRANSAZIONE.'" title="'.PENDING.'-'.ATT_TRANSAZIONE.'" /></a>';
			//	print_r($record);
				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=view&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW.'"><img src="'.$path_web.'img/icone/file_find.png" width="24" height="24" style="border: 0px;" alt="'.VIEW.'" /></a></td>
<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
					<td class="'.$class.'">';
				
				
				echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_tipo_transazione=".$_REQUEST['id_tipo_transazione']."&amp;id_transazione=".$record['id_transazione']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
				
				echo "</td>";
				echo "\n".'<td class="'.$class.'">'.$optionClienti[$record['id_cliente_da']].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_conto_da']]['numero_conto'].'</td>
					<td class="'.$class.'">'.$optionTipiComm[$record['id_tipo_commissione']].'</td>
					<td class="'.$class.'">'.$record['descrizione'].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
					<td class="'.$class.'">'.number_format($record['importo_transazione'],2,',','.').'</td>
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
				</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>

<?php
				break; // end 7 // Fees
				
				case "4": // Outgoing Wire Transfer

?>
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>		
				<th style="width:30px;"></th>
                <th><?php echo CLIENTE_FROM; ?></th>
				<th><?php echo CONTO_FROM; ?></th>
				<th><?php echo LABEL_BANCA; ?></th>
				<th><?php echo LABEL_INTESTATARIO_CONTO; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
				<th><?php echo LABEL_DATA; ?></th>
				<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			</tr>
			
			<!-- filtri -->
			<tr>
				<td colspan="3" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
				<td><?php
				// id_cliente_da            
				?><select name="filtra_cliente_da" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionClienti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_cliente_da']) && $_REQUEST['filtra_cliente_da']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td><?php
				// id_conto_da            
				?><select name="filtra_conto_da" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectConti_da;
					?>
				</select></td>
                
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?php
				// stato            
				?><select name="filtra_stato" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($arrayStatiOperazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_stato']) && $_REQUEST['filtra_stato']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
			
				$qry2="select * from gestione_offshore where id_transazione=".$record['id_transazione'];
				$res2=$db->query($qry2);
				$record2 = $res2->fetchRow();
			
				$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATT_TRANSAZIONE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATT_TRANSAZIONE.'" title="'.PENDING.'-'.ATT_TRANSAZIONE.'" /></a>';
			//	print_r($record);
				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=view&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW.'"><img src="'.$path_web.'img/icone/file_find.png" width="24" height="24" style="border: 0px;" alt="'.VIEW.'" /></a></td>
<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
					<td class="'.$class.'">';
				
				
				echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_tipo_transazione=".$_REQUEST['id_tipo_transazione']."&amp;id_transazione=".$record['id_transazione']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
				
				echo "</td>";
				echo "\n".'<td class="'.$class.'">'.$optionClienti[$record['id_cliente_da']].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_conto_da']]['numero_conto'].'</td>
					<td class="'.$class.'">'.$record2['nome_ba_out'].'</td>
					<td class="'.$class.'">'.$record2['intestatario_ba_out'].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
					<td class="'.$class.'">'.number_format($record['importo_transazione'],2,',','.').'</td>
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
				</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>

<?php
				break; // end 4 // Outgoing Wire Transfer
				
				case "5": // Incoming Wire Transfer

?>
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>		
				<th style="width:30px;"></th>
                <th><?php echo CLIENTE_TO; ?></th>
				<th><?php echo CONTO_TO; ?></th>
				<th><?php echo LABEL_BANCA; ?></th>
				<th><?php echo LABEL_INTESTATARIO_CONTO; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
				<th><?php echo LABEL_DATA; ?></th>
				<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			</tr>
			
			<!-- filtri -->
			<tr>
				<td colspan="3" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
				<td><?php
				// id_cliente_a            
				?><select name="filtra_cliente_a" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionClienti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_cliente_a']) && $_REQUEST['filtra_cliente_a']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td><?php
				// id_conto_a            
				?><select name="filtra_conto_a" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectConti_a;
					?>
				</select></td>

				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?php
				// stato            
				?><select name="filtra_stato" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($arrayStatiOperazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_stato']) && $_REQUEST['filtra_stato']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
				$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATT_TRANSAZIONE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATT_TRANSAZIONE.'" title="'.PENDING.'-'.ATT_TRANSAZIONE.'" /></a>';
				
				$qry2="select * from gestione_offshore where id_transazione=".$record['id_transazione'];
				$res2=$db->query($qry2);
				$record2 = $res2->fetchRow();
				
			//	print_r($record);
				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=view&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW.'"><img src="'.$path_web.'img/icone/file_find.png" width="24" height="24" style="border: 0px;" alt="'.VIEW.'" /></a></td>
<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
					<td class="'.$class.'">';
				
				
				echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_tipo_transazione=".$_REQUEST['id_tipo_transazione']."&amp;id_transazione=".$record['id_transazione']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
				
				echo "</td>";
				echo "\n".'<td class="'.$class.'">'.$optionClienti[$record['id_cliente_a']].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_conto_a']]['numero_conto'].'</td>
					<td class="'.$class.'">'.$record2['nome_ba_out'].'</td>
					<td class="'.$class.'">'.$record2['intestatario_ba_out'].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
					<td class="'.$class.'">'.number_format($record['importo_transazione'],2,',','.').'</td>
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
				</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>

<?php
				break; // end 5 // Incoming Wire Transfer


			case "6": // Trade Marker

?>
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>		
				<th style="width:30px;"></th>
                <th><?php echo CLIENTE_FROM; ?></th>
				<th><?php echo CONTO_FROM; ?></th>
				<th><?php echo LABEL_TIPO_INVESTIMENTO; ?></th>
				<th><?php echo BUY.'/'.SELL; ?></th>
				<th><?php echo LABEL_NUMERO_QUOTE; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
				<th><?php echo LABEL_DATA; ?></th>
				<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			</tr>
			
			<!-- filtri -->
			<tr>
				<td colspan="3" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
				<td><?php
				// id_cliente_da            
				?><select name="filtra_cliente_da" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionClienti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_cliente_da']) && $_REQUEST['filtra_cliente_da']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td><?php
				// id_conto_da            
				?><select name="filtra_conto_da" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectConti_da;
					?>
				</select></td>
                
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?php
				// stato            
				?><select name="filtra_stato" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($arrayStatiOperazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_stato']) && $_REQUEST['filtra_stato']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
				$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATT_TRANSAZIONE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATT_TRANSAZIONE.'" title="'.PENDING.'-'.ATT_TRANSAZIONE.'" /></a>';
			//	print_r($record);
				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=view&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW.'"><img src="'.$path_web.'img/icone/file_find.png" width="24" height="24" style="border: 0px;" alt="'.VIEW.'" /></a></td>
<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
					<td class="'.$class.'">';
				
				echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_tipo_transazione=".$_REQUEST['id_tipo_transazione']."&amp;id_transazione=".$record['id_transazione']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
				
				echo "</td>";
				echo "\n".'<td class="'.$class.'">'.$optionClienti[$record['id_cliente_da']].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_conto_da']]['numero_conto'].'</td>
					<td class="'.$class.'">'.$optionInvestimenti[$record['id_tipo_investimento']]['nome_tipo'].'</td>
					<td class="'.$class.'">'.$optionBuySell[$record['buy_sell']].'</td>
					<td class="'.$class.'">'.($record['buy_sell']=='B'?'+':'-').$record['numero_quote'].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
					<td class="'.$class.'">'.($record['buy_sell']=='B'?'-':'+').number_format($record['importo_transazione'],2,',','.').'</td>
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
				</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>

<?php
				break; // end 6 // Trade Marker



				case "8": // Accredito Interessi

?>
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>		
				<th style="width:30px;"></th>
                <th><?php echo CLIENTE_TO; ?></th>
				<th><?php echo CONTO_TO; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
				<th><?php echo LABEL_DATA; ?></th>
				<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			</tr>
			
			<!-- filtri -->
			<tr>
				<td colspan="3" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
				<td><?php
				// id_cliente_a            
				?><select name="filtra_cliente_a" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionClienti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_cliente_a']) && $_REQUEST['filtra_cliente_a']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td><?php
				// id_conto_a            
				?><select name="filtra_conto_a" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectConti_a;
					?>
				</select></td>

				<td></td>
				<td></td>
				<td></td>
				<td><?php
				// stato            
				?><select name="filtra_stato" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($arrayStatiOperazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_stato']) && $_REQUEST['filtra_stato']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
				$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATT_TRANSAZIONE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATT_TRANSAZIONE.'" title="'.PENDING.'-'.ATT_TRANSAZIONE.'" /></a>';
				
				$qry2="select * from gestione_offshore where id_transazione=".$record['id_transazione'];
				$res2=$db->query($qry2);
				$record2 = $res2->fetchRow();
				
			//	print_r($record);
				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=view&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW.'"><img src="'.$path_web.'img/icone/file_find.png" width="24" height="24" style="border: 0px;" alt="'.VIEW.'" /></a></td>
<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
					<td class="'.$class.'">';
				
				
				echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_tipo_transazione=".$_REQUEST['id_tipo_transazione']."&amp;id_transazione=".$record['id_transazione']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
				
				echo "</td>";
				echo "\n".'<td class="'.$class.'">'.$optionClienti[$record['id_cliente_a']].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_conto_a']]['numero_conto'].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
					<td class="'.$class.'">'.number_format($record['importo_transazione'],2,',','.').'</td>
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
				</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>

<?php
				break; // end 8 // Accredito Interessi



				case "9": # Visa Debit Card fee
				
?>				
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>		
				<th style="width:30px;"></th>
                <th><?php echo LABEL_CLIENTE; ?></th>
				<th><?php echo CONTO_FROM; ?></th>
				<th><?php echo CARTA_FROM; ?></th>
				<th><?php echo CAUSALE; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
				<th><?php echo LABEL_DATA; ?></th>
				<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			</tr>
			
			<!-- filtri -->
			<tr>
				<td colspan="3" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
				<td><?php
				// id_cliente_da            
				?><select name="filtra_cliente_da" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionClienti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_cliente_da']) && $_REQUEST['filtra_cliente_da']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td><?php
				// id_conto_da            
				?><select name="filtra_conto_da" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectConti_da;
					?>
				</select></td>
                
				<td><?php
				// id_conto_da            
				?><select name="filtra_carta_da" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionCarte as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_carta_da']) && $_REQUEST['filtra_carta_da']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?php
				// stato            
				?><select name="filtra_stato" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($arrayStatiOperazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_stato']) && $_REQUEST['filtra_stato']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
				$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATT_TRANSAZIONE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATT_TRANSAZIONE.'" title="'.PENDING.'-'.ATT_TRANSAZIONE.'" /></a>';
			//	print_r($record);
				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=view&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW.'"><img src="'.$path_web.'img/icone/file_find.png" width="24" height="24" style="border: 0px;" alt="'.VIEW.'" /></a></td>
<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
					<td class="'.$class.'">';
				
				
				echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_tipo_transazione=".$_REQUEST['id_tipo_transazione']."&amp;id_transazione=".$record['id_transazione']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
				
				echo "</td>";
				echo "\n".'<td class="'.$class.'">'.$optionClienti[$record['id_cliente_da']].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_conto_da']]['numero_conto'].'</td>
					<td class="'.$class.'">'.$optionCarte[$record['id_carta_da']].'</td>
					<td class="'.$class.'">'.$record['descrizione'].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
					<td class="'.$class.'">'.number_format($record['importo_transazione'],2,',','.').'</td>
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
				</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>
<?php



				break; // end 9 // Visa Debit Card fee 

				case "10": # Trasferimento alla Carta di Debito
?>
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>		
				<th style="width:30px;"></th>
                <th><?php echo CLIENTE_FROM; ?></th>
				<th><?php echo CONTO_FROM; ?></th>
				<th><?php echo CARTA_TO; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
				<th><?php echo LABEL_DATA; ?></th>
				<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			</tr>
			
			<!-- filtri -->
			<tr>
				<td colspan="3" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
				<td><?php
				// id_cliente_a            
				?><select name="filtra_cliente_da" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionClienti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_cliente_da']) && $_REQUEST['filtra_cliente_a']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td><?php
				// id_conto_a            
				?><select name="filtra_conto_da" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectConti_da;
					?>
				</select></td>
				<td><?php
				// id_conto_a            
				?><select name="filtra_carta_a" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					echo $selectCarte_a;
					?>
				</select></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?php
				// stato            
				?><select name="filtra_stato" class="textbox">
					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($arrayStatiOperazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_stato']) && $_REQUEST['filtra_stato']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
				$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATT_TRANSAZIONE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATT_TRANSAZIONE.'" title="'.PENDING.'-'.ATT_TRANSAZIONE.'" /></a>';
				
				$qry2="select * from gestione_offshore where id_transazione=".$record['id_transazione'];
				$res2=$db->query($qry2);
				$record2 = $res2->fetchRow();
				
			//	print_r($record);
				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=view&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW.'"><img src="'.$path_web.'img/icone/file_find.png" width="24" height="24" style="border: 0px;" alt="'.VIEW.'" /></a></td>
<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
					<td class="'.$class.'">';
				
				
				echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_tipo_transazione=".$_REQUEST['id_tipo_transazione']."&amp;id_transazione=".$record['id_transazione']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
				
				echo "</td>";
				echo "\n".'<td class="'.$class.'">'.$optionClienti[$record['id_cliente_da']].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_conto_da']]['numero_conto'].'</td>
					<td class="'.$class.'">'.$optionConti[$record['id_carta_a']]['numero_carta'].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
					<td class="'.$class.'">'.number_format($record['importo_transazione'],2,',','.').'</td>
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
				</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>

<?php


				break; // end 10 // Trasferimento alla Carta di Debito
				
				default:
			 
			//	while ($record =& $res->fetchRow()) print_r($record);
				
				
				

				
			} // end switch id_tipo_transazione
            
			?>
            
			</form>
		
		<?php	
				echo $view_links;
	
			} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
	

		} else $goPage->alertgo(SELEZIONA_TIPO_TRANS, 'index.php?page='.$_REQUEST['page']);

			
	break;



	
	default: // elenco tipi transazioni
	
/*
		$qry="select * from tipi_transazioni as tt, tipi_transazioni_lingue as ttl where tt.id_tipo_transazione=ttl.id_tipo_transazione and tt.attivo=1 and ttl.sigla_lingua='".$_SESSION['lingua']."'";
		$res=$db->query($qry);
			
		// ... se si verifica un errore, lo scriviamo
		if( DB::isError($res) ) { 
			print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
		}			
			
		$qry2="select * from tipi_commissioni as tc, tipi_commissioni_lingue as tcl where tc.id_tipo_commissione=tcl.id_tipo_commissione and tc.attivo=1 and tcl.sigla_lingua='".$_SESSION['lingua']."'";
		$res2=$db->query($qry2);
			
		// ... se si verifica un errore, lo scriviamo
		if( DB::isError($res2) ) { 
			print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry2."."; die($res2->getMessage()); 
		}			
*/
		?>
        <ul>
        <?php
		// transazioni
		//while ($record=& $res->fetchRow()) echo '<li><a href="'.$path_web.'index.php?page='.$_REQUEST['page'].'&amp;act=list&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'&amp;title='.$record['nome_tipo'].'" title="'.$record['nome_tipo'].'">'.$record['nome_tipo'].'</a></li>';
		foreach ($optionTransazioni as $id=>$nome) echo '<li><a href="'.$path_web.'index.php?page='.$_REQUEST['page'].'&amp;act=list&amp;id_tipo_transazione='.$id.'&amp;title='.$nome.'" title="'.$nome.'">'.$nome.'</a></li>';
		?>
    	</ul>
    <?php
	

}
	
?>
