<?php
# settings.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}
$investimento = $zealandCredit->getInvestimentoNome($_REQUEST['id_invest'],$_SESSION['lingua']);
$durate  = $zealandCredit->getDurate();

echo '<div class="titoloAdmin">'.LINK_MENU_SN_SETTINGS.' '.$investimento.'</div>';
// chiave primaria
switch ($_REQUEST['tbl']) {
	case "lingue": $PryKey='sigla_lingua'; break;
	case "": $PryKey=''; break;
	default: $PryKey=$sql->PrimaryKey($_REQUEST['tbl']);
}

switch ($_REQUEST['act']) {

	
	case "del":
		if (!empty($_REQUEST['id']) && !empty($_REQUEST['tbl'])) {
			
			$query="delete from ".$_REQUEST['tbl']." where ".$PryKey."='".$_REQUEST['id']."'";
			$res=$db->query($query);
			
			$goPage->alertgo(DELETE_OK, 'index.php?page='.$_REQUEST['page'].'&id_invest='.$_REQUEST['id_invest'].'&tbl='.$_REQUEST['tbl'].'&act=list');
			
		} else $goPage->alertback(NO_RECORD, false);
		
	break;
	

	case "upd": // insert / update
	
	echo '<h1>'.$investimento.'</h1>';
	

			
			echo '<h1>'.$_SESSION['title'].'</h1>';

			//echo '<pre>';
			//print_r($_REQUEST);
			//echo '</pre>';
			
			# dati generali
			$data=array();
			$data=$_REQUEST;
			//die(print_r($data));
			if (!empty($_REQUEST['id'])) {
				
				
				# update
				$query=$sql->prepareQuery ($_REQUEST['tbl'], $data, 'update', $PryKey."='".$_REQUEST['id']."'");
				//echo $query;
				$res=$db->query($query);
				$lastid=$_REQUEST['id'];
				
			} else {
				# insert
				$query=$sql->prepareQuery ($_REQUEST['tbl'], $data, 'insert');
				//echo $query;
				$res=$db->query($query);
				$lastid=(!empty($_REQUEST[$PryKey])?$_REQUEST[$PryKey]:mysql_insert_id());
			}

			
			# dati per le lingue
			
			$goPage->alertgo(UPDATE_OK, 'index.php?page='.$_REQUEST['page'].'&tbl='.$_REQUEST['tbl'].'&act=list&id_invest='.$_REQUEST['id_tipo_investimento']);


	
	
	break;
	
	case "form": // form new / mod
	
			echo '<h1>'.$investimento.'</h1>';
			
			$record=array();
			if (!empty($_REQUEST['id'])) {
			
				
						$qry= "select * from ".$_REQUEST['tbl']." where id = ".$_REQUEST['id'];
						$res=$db->query($qry);
						$record=array();
						while ($rec =& $res->fetchRow()) {
							$record[$PryKey]=$rec[$PryKey];
							$record['id_tipo_investimento']=$rec['id_tipo_investimento'];
							$record['minimo_deposito']=$rec['minimo_deposito'];
							$record['id_durata_minima']=$rec['id_durata_minima'];
							$record['tasso_nzd']=$rec['tasso_nzd'];
							$record['tasso_usd']=$rec['tasso_usd'];
							$record['tasso_chf']=$rec['tasso_chf'];
							$record['tasso_eur']=$rec['tasso_eur'];
							$record['tasso_gbp']=$rec['tasso_gbp'];
						}

				
			} // if !empty id
			
			$form = new HTML_QuickForm('FormTipi', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
			$form->addElement('hidden', 'page', $_REQUEST['page']);
			$form->addElement('hidden', 'act', 'upd');
			$form->addElement('hidden', 'id_tipo_investimento', $_REQUEST['id_invest']);
			$form->addElement('hidden', 'id_invest', $_REQUEST['id_invest']);
			$form->addElement('hidden', 'tbl', $_REQUEST['tbl']);
			$form->addElement('hidden', 'id', $record[$PryKey]);
			
			$form->addElement('text', 'minimo_deposito', MIN_DEP, ' class="textbox" size="10" ');
			$form->addElement('select', 'id_durata_minima', DURATA_INV, $durate, ' class="textbox"');
			$form->addElement('text', 'tasso_nzd', T_NZD, ' class="textbox" size="10" ');
			$form->addElement('text', 'tasso_usd', T_USD, ' class="textbox" size="10" ');
			$form->addElement('text', 'tasso_chf', T_CHF, ' class="textbox" size="10" ');
			$form->addElement('text', 'tasso_eur', T_EUR, ' class="textbox" size="10" ');
			$form->addElement('text', 'tasso_gbp', T_GBP, ' class="textbox" size="10" ');			
			
			
			$form->setDefaults($record); // da tabella
		
			
			$form->setRequiredNote(REQUIRED_FIELDS);
			$form->setJsWarnings(ERRORE_JS,'');
			
			// CREO I PULSANTI
			$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', SAVE,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?id_invest='.$_REQUEST['id_invest'].'&page='.$_REQUEST['page'].'&act=list&tbl='.$_REQUEST['tbl'].'\';"');
			
			$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
		
			// mostro il form
			$form->display();
	
	break;
	
	case "list": // list
	
			echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_invest='.$_REQUEST['id_invest'].'&amp;tbl='.$_REQUEST['tbl'].'" title="'.INSERT_NEW.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.INSERT_NEW.'" /></a>';
				
			
			
			$query = "select * from ".$_REQUEST['tbl']." where id_tipo_investimento = ".$_REQUEST['id_invest'];
			$res = $db->query($query);
			?>
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>
                <th>minimo</th>
                <th>durata</th>
                <th>rate NZD</th>
                <th>rate USD</th>
                <th>rate CHF</th>
                <th>rate EUR</th>
                <th>rate GBP</th>
                </tr>			
			<?php
			while($row = $res->fetchRow()){

				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;tbl='.$_REQUEST['tbl'].'&amp;id='.$row['id'].'&amp;id_invest='.$row['id_tipo_investimento'].'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
					
					
					<td class="'.$class.'">'.'<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;id_invest='.$row['id_tipo_investimento'].'&amp;act=del&amp;tbl='.$_REQUEST['tbl'].'&amp;id='.$row['id'].'" title="'.DELETE.'"  onclick="if (confirm(\'Confirm?\')) window.open(this.href, \'_self\', \'\'); return false;" ><img src="'.$path_web.'img/icone/file_del.png" width="24" height="24" style="border: 0px;" alt="'.DELETE.'"/></a></td>
				<td>'.$row['minimo_deposito'].'</td>
				<td>'.($row['id_durata_minima']/2592000).' mesi</td>
				<td>'.$row['tasso_nzd'].'</td>				
				<td>'.$row['tasso_usd'].'</td>
				<td>'.$row['tasso_chf'].'</td>
				<td>'.$row['tasso_eur'].'</td>				
				<td>'.$row['tasso_gbp'].'</td>
				</tr>';

			}
			?>
            
			</table>
			<?php
		
	break;

}
	
?>
