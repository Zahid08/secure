

<?php

# accounts.inc.php

if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {

	$goPage->alertback(ACCESSO_NEGATO, false);

	//header("Location:index.php");

	exit();

}



$optionTipiConti = $zealandCredit->getTypes('tipi_conti', $_SESSION['lingua'], $stato_opzioni, false);

$optionValute = $zealandCredit->getValute($stato_opzioni, false);

$optionClienti = $zealandCredit->getClienti($stato_opzioni);



echo '<div class="titoloAdmin">'.LINK_MENU_SN_MY_ACCOUNTS.'</div>';



switch ($_REQUEST['act']) {



	case "form": // form new  modulo richiesta nuovo conto



		

		if (!empty($_REQUEST['id_conto'])) {

			

			# dati conto

			$qry="select * from conti where id_conto=".$_REQUEST['id_conto']." and id_cliente=".$_SESSION['utente']['id_cliente'];

			$res=$db->query($qry);

			$rows=$res->numRows();

			if ($rows==0) { 

				$goPage->alertback(ACCESSO_NEGATO, false);

				exit();

			} else {

				$record = $res->fetchRow();

				

				echo '<p>'.PRINT_FAX.'</p>';

			}

		} else {

			echo '<h1>'.RICHIEDI_NUOVO_CONTO.'</h1>';

			$record['data_richiesta']=date("Y-m-d H:i:s");

		}

		

		$form = new HTML_QuickForm('FormUpdate', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);

		$form->addElement('hidden', 'page', $_REQUEST['page']);

		$form->addElement('hidden', 'act', 'upd');

		$form->addElement('hidden', 'stato', 'P');

		$form->addElement('hidden', 'attivo', '1');

		$form->addElement('hidden', 'data_richiesta', date("Y-m-d H:i:s"));

		$form->addElement('hidden', 'id_cliente', $_SESSION['utente']['id_cliente']);

		$form->addElement('hidden', 'id_conto', $_REQUEST['id_conto']);

		$form->addElement('hidden', 'minimo_deposito', '', 'id="minimo_deposito"');

		



		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):date("d-m-Y")).'</div><div class="clearBoth"></div>');

		

		$form->addElement('text', 'intestatario', LABEL_INTESTATARIO, ' class="textbox" size="60"');

		$form->addRule('intestatario', LABEL_INTESTATARIO, 'required', FALSE,'client');



		$form->addElement('select', 'id_tipo_conto', LABEL_TIPO_CONTO, $optionTipiConti, ' id="id_tipo_conto" class="textbox" onchange="controllaMinimo()"');



		if (!empty($record['numero_conto'])) 

		$form->addElement('text', 'numero_conto', LABEL_NUMERO_CONTO,' class="textbox" size="25"');

	//	$form->addElement('html', '<div class="labelForm">'.LABEL_NUMERO_CONTO.'</div><div class="fieldForm"> '.$record['numero_conto'].'</div><div class="clearBoth"></div>');

		

		

		$form->addElement('select', 'id_valuta', LABEL_VALUTA, $optionValute, ' id="id_valuta"  class="textbox" onchange="controllaMinimo()"');



		$form->addElement('text', 'saldo_iniziale', LABEL_SALDO_INIZIALE, ' class="textbox" size="25" id="saldo_iniziale" onblur="controllaMinimo()"');

		$form->addElement('html', '<div class="labelForm"></div><div class="fieldForm" id="checkMinimo"></div><div class="clearBoth"></div>');



		$form->addElement('select', 'abilita_prelievo', LABEL_ABILITA_PRELIEVO, $optionBool, ' class="textbox"');

		$form->addElement('select', 'abilita_deposito', LABEL_ABILITA_DEPOSITO, $optionBool, ' class="textbox"');



		$form->addElement('textarea', 'descrizione', LABEL_DESCRIZIONE,' class="textbox" cols="60" rows="5"');

				

		$form->addRule('id_tipo_conto', LABEL_TIPO_CONTO, 'required', FALSE,'client');

		$form->addRule('id_valuta', LABEL_VALUTA, 'required', FALSE,'client');

		$form->addRule('abilita_prelievo', LABEL_ABILITA_PRELIEVO, 'required', FALSE,'client');

		$form->addRule('abilita_deposito', LABEL_ABILITA_DEPOSITO, 'required', FALSE,'client');

		$form->addRule('minimo_deposito', LABEL_MINIMO_DEPOSITO, 'required', FALSE,'client');		

		



		$form->setRequiredNote(REQUIRED_FIELDS);

		$form->setJsWarnings(ERRORE_JS,'');

		

		// CREO I PULSANTI

		if (empty($_REQUEST['id_conto'])) {

		

			$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', LABEL_INVIA,'class="button"');

			$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');

			$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');

			

		} else {

		

			$form->setDefaults($record);

			if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) {



				// stampa

				$buttons[]=&HTML_QuickForm::createElement('button', 'btnPrint', LABEL_PRINT, 'class="button" onClick="window.open(\'print.php?page='.$_REQUEST['page'].'&act=form&id_conto='.$record['id_conto'].'\', \'_blank\', \'width=450, height=500, scrollbars=yes, resizable=yes\'); return false;"');

				$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');

			

			}

		

		}

		$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');

	

		if (!empty($_REQUEST['id_conto'])) $form->freeze(); 

		

		// mostro il form

		$form->display();

	

	break;

	

	case "upd": // insert / update

		echo '<h1>'.UPDATE.'</h1>';

		

		# insert

		$query=$sql->prepareQuery ('conti', $_REQUEST, 'insert');

		//echo '<br />'.$query;

		$res=$db->query($query);

		$lastid=mysql_insert_id();



		# invio form richiesta

		### mail di avviso all'amministratore

		//require_once ($path_www."class/mail.class.php");

		

		

		//$titolo = TITOLO_MAIL_NUOVO_CONTO;

	

		//$testa = '<div style="font-family: Verdana, Helvetica, sans-serif; font-size: 12px; font-weight:bold;">'.$titolo.'</div>';

		$dati='In data '.date("d-m-Y").' the owner asked for attivation of a new account.<br />Here the data he wrote:';

		

		$dati .='<br />Person: '.$_SESSION['utente']['cognome'].' '.$_SESSION['utente']['nome'].' '.$_SESSION['utente']['ragione_sociale'];

		$dati .='<br />Owner: '.$_REQUEST['intestatario'];

		$dati .=(!empty($_REQUEST['id_tipo_conto'])?'<br />Account type: '.$optionTipiConti[$_REQUEST['id_tipo_conto']]:'');

		$dati .=(!empty($_REQUEST['id_valuta'])?'<br />Currency: '.$optionValute[$_REQUEST['id_valuta']]:'');

		$dati .=(!empty($_REQUEST['saldo_iniziale'])?'<br />Opening balance: '.$_REQUEST['saldo_iniziale']:'');

		$dati .=(!empty($_REQUEST['sigla_lingua'])?'<br />Language: '.$_REQUEST['sigla_lingua']:'');

		

		$dati .='<br />You can connect to admin panel to activate it.';

		

		$headers = array();



		$headers['to_name']=$name_admin;

		$headers['to_email']=$mail_admin;

		

		$headers['from_name']=$_SESSION['utente']['nome'].(!empty($_SESSION['utente']['cognome'])?' '.$_SESSION['utente']['cognome']:'').(!empty($_SESSION['utente']['ragione_sociale'])?' - '.$_SESSION['utente']['ragione_sociale']:'');

		

		$headers['from_email']=$_SESSION['utente']['email'];

		

		$headers['return_email']=$_SESSION['utente']['email'];

		$headers['reply_email']=$_SESSION['utente']['email'];

		$headers['reply_name']=$_SESSION['utente']['nome'].(!empty($_SESSION['utente']['cognome'])?' '.$_SESSION['utente']['cognome']:'').(!empty($_SESSION['utente']['ragione_sociale'])?' - '.$_SESSION['utente']['ragione_sociale']:'');

		

		$template_email = file_get_contents($path_www.'templates//email_post_richiesta_generica.html');

		$messaggio = str_replace('[CONTENUTO]',$dati, $template_email);

		

		$inviata = $func->sendMail($headers,TITOLO_MAIL_NUOVO_CONTO,$messaggio,'');

		

		

		

		// messaggi

		// salvo la comunicazione in db

		$data=array();

		$data['id_cliente_mittente']=$_SESSION['utente']['id_cliente'];

		$data['id_cliente_destinatario']=1; // admin

		$data['email_mittente']=$param['from'];

		$data['email_destinatario']=$param['to'];

		$data['oggetto_messaggio']=$param['subject'];

		$data['testo_messaggio']=$dati;

		$data['stato_messaggio_mittente']='I';

		$data['stato_messaggio_destinatario']='N';

		$data['sigla_lingua']=$_SESSION['lingua'];

		$data['data_creazione_messaggio']=$_REQUEST['data_richiesta'];

		$data['data_invio_messaggio']=date("Y-m-d H:i:s");

		$data['notifica_automatica']=1;

		$data['notifica_email']=1;

		$query=$sql->prepareQuery ('messaggi', $data, 'insert');

		//echo '<br />'.$query;

		$res=$db->query($query);



		$goPage->alertgo(RICHIESTA_INOLTRATA, 'index.php?page='.$_REQUEST['page'].'&act=form&id_conto='.$lastid);

			

			

	break;

	

	default: // list

		

		// elenco tutti i conti del cliente

		// form richiesta nuovo conto

		//echo '<h1>'.LIST_TABLE;

		echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form" title="'.RICHIEDI_NUOVO_CONTO.'"><h1>New account<img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.RICHIEDI_NUOVO_CONTO.'" /></h1></a>';

		

	//	echo '</h1>';

		

		$qry="select * from conti as con, clienti as cli where con.attivo=1 and con.id_cliente=cli.id_cliente and cli.id_cliente = ".$_SESSION['utente']['id_cliente']." ";

		

		$tot_record=0;

		$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);

		if ($tot_record>0) 

		{

			//echo 'Accounts : '.$tot_record.'<br />';

		} else

			echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);

		



}

?>



<div style="margin-top:5px;margin-bottom:15px;">

<?php

  

  echo $zealandCredit->reportInteressi($_SESSION['utente']['id_cliente']);

?>

 

</div>



 





<? if (!$_REQUEST['act']) { ?>

    <!--<div style="width:90%;margin:0px auto;position:relative; text-align:center; font-size:14px;">

    <br /><br />

    Exclusive offer for our clients.  <a href="/?page=myaccounts&act=form"><U>Order this card</U></a> from your home banking or by email: <a href="mailto:jaylee.bodden@bmercantil.com">jaylee.bodden@bmercantil.com</a>

    <DIV STYLE="margin-top:20px; text-align:center"><a href="/?page=myaccounts&act=form"><img src="/img/claim.jpg" /></a>

    

    </DIV>

    </div>-->

<? } ?>



  <div style="width:98%; margin-top:50px; margin-bottom:20px;">

<div id=""  style="float:left; width:50%;height:400px;">

	<DIV STYLE="MARGIN-BOTTOM:15PX; text-align:center">Investing in Gold</DIV>



    <iframe  src="/inc/grafico_oro.php"  style="width:100%;height:375px;border:0px;overflow:hidden" scrolling="no" ></iframe>

</div>



<div id=""  style="float:right; width:50%;height:400px;">

	<DIV STYLE="MARGIN-BOTTOM:15PX; text-align:center">Investing in Silver</DIV>

	<iframe  src="/inc/grafico_argento.php"  style="width:100%;height:375px;border:0px;" scrolling="no" ></iframe>

</div>

<div style="clear:both;"></div>

</div> 

 

<?php break; ?>



