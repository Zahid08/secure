<?php
# portfolio.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

//$optionTipiInvestimenti = $zealandCredit->getTypes('tipi_investimenti', $_SESSION['lingua'], $stato_opzioni, false);
$optionValute = $zealandCredit->getValute($stato_opzioni, false);


$optionInvestimenti = $zealandCredit->getTipiInvestimenti('tipi_investimenti', $_SESSION['lingua'], $stato_opzioni, false);

$optionTipiInvestimenti = array();
$optionTipiInvestimenti['']='';
$arrayJs ="\n".'arrayInvValQuote = new Array();';
$arrayJs .="\n".'arrayInvValuta = new Array();';
foreach ($optionInvestimenti as $id => $dati) {
	$dati['id_valuta'] = 1; //sempre EUR
	$optionTipiInvestimenti[$id]=$dati['nome_tipo'].' ('.$optionValute[$dati['id_valuta']].')';
	$arrayJs .="\n".'arrayInvValQuote['.$id.']='.$dati['valore'].';';
	$arrayJs .="\n".'arrayInvValuta['.$id.']='.$dati['id_valuta'].';';
}

$optionContiDati =$zealandCredit->getContiDati($stato_opzioni); // solo conti con abilitazione prelievo/deposito

$arrayJs .="\n".'arrayContiValute = new Array();';
$arrayJs .="\n".'arrayContiGiacenza = new Array();';
foreach ($optionContiDati as $id_conto => $dati) {
	if ($dati['saldo_attuale']>0 && $dati['id_valuta']>0) {
		$arrayJs .="\n".'arrayContiValute['.$id_conto.']='.$dati['id_valuta'].';';
		$arrayJs .="\n".'arrayContiGiacenza['.$id_conto.']='.$dati['saldo_attuale'].';';
	}
}

?>
<script type="text/javascript">
<?php echo $arrayJs; ?>
</script>
<?php
echo '<div class="titoloAdmin">'.LINK_MENU_SN_PORTFOLIO.'</div>';

switch ($_REQUEST['act']) {

	case "form": // form new / mod
		$record=array();
		if (!empty($_REQUEST['id_portfolio'])) {
			# dati PORTFOLIO
			$qry="select * from portfolio where id_portfolio=".$_REQUEST['id_portfolio']." and id_cliente=".$_SESSION['utente']['id_cliente'];
			$res=$db->query($qry);
			$rows=$res->numRows();
			if ($rows==0) { 
				$goPage->alertback(ACCESSO_NEGATO, false);
				exit();
			} else {
				$record = $res->fetchRow();
				$record['id_valuta_conto']=$optionContiDati[$record['id_conto']]['id_valuta'];
				$record['importo']=$record['valore_quota_iniziale']*$record['numero_quote'];

				echo '<p>'.PRINT_FAX.'</p>';
			}
		} else {
			echo '<h1>'.INSERT_NEW.'</h1>';
			$record['data_richiesta']=date("Y-m-d H:i:s");
		}

		$form = new HTML_QuickForm('FormUpdate', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
		$form->addElement('hidden', 'page', $_REQUEST['page']);
		$form->addElement('hidden', 'act', 'upd');
		$form->addElement('hidden', 'id_portfolio', $_REQUEST['id_portfolio']);
		$form->addElement('hidden', 'id_cliente', $_SESSION['utente']['id_cliente']);
		$form->addElement('hidden', 'data_richiesta', $record['data_richiesta']);
		$form->addElement('hidden', 'stato', 'P');
		$form->addElement('hidden', 'attivo', '1');

		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');

		$form->addElement('hidden', 'id_valuta_conto', $record['id_valuta_conto']);
		$form->addElement('hidden', 'id_valuta', $record['id_valuta']);

		
		// seleziono i conti del cliente attivi con prelievo abilitato
		$qry2="select * from conti where id_cliente=".$_SESSION['utente']['id_cliente']." and attivo=1 and stato='A' and abilita_prelievo=1";
		$res2=$db->query($qry2);
		$optionContiCliente=array();
		$optionContiCliente['']='';

		
		while ($record2 =& $res2->fetchRow())
		{
		
 		 	$optionContiCliente[$record2['id_conto']]=$record2['numero_conto'].' ('.$optionValute[$record2['id_valuta']].')';
		}
				$form->addElement('select', 'id_tipo_investimento', LABEL_TIPO_INVESTIMENTO, $optionTipiInvestimenti, ' class="textbox" onchange="inserisciValoreQuota();"');
		$form->addElement('select', 'id_conto', LABEL_CONTO, $optionContiCliente, ' class="textbox" onChange="inserisciValutaConto2();"');
		$form->addRule('id_conto', LABEL_CONTO, 'required', FALSE,'client');
		

		$form->addElement('text', 'valore_quota_iniziale', LABEL_VALORE_INIZIALE, ' class="textbox" size="25" readonly');
		
		$form->addElement('text', 'importo', LABEL_AMOUNT,' class="textbox" size="25" onChange="inserisciQuote();"');
		
		$form->addElement('text', 'numero_quote', LABEL_NUMERO_QUOTE,' class="textbox" size="25" readonly');
				
		$form->addRule('id_tipo_investimento', LABEL_TIPO_INVESTIMENTO, 'required', FALSE,'client');
		$form->addRule('id_conto', LABEL_CONTO, 'required', FALSE,'client');

		$form->addRule(array('id_valuta', 'id_valuta_conto'), VALUTE_MATCH, 'compare', null, 'client');
		//$form->addRule('id_valuta', LABEL_VALUTA, 'required', FALSE,'client');
		$form->addRule('importo', LABEL_AMOUNT, 'required', FALSE,'client');
		$form->addRule('importo', LABEL_AMOUNT.' '.NOT_NUM, 'numeric', FALSE,'client');
		$form->addRule('numero_quote', LABEL_NUMERO_QUOTE, 'required', FALSE,'client');
		$form->addRule('numero_quote', LABEL_NUMERO_QUOTE.' '.NOT_NUM, 'numeric', FALSE,'client');
		$form->addRule('valore_quota_iniziale', LABEL_VALORE_INIZIALE, 'required', FALSE,'client');
		$form->addRule('valore_quota_iniziale', LABEL_VALORE_INIZIALE.' '.NOT_NUM, 'numeric', FALSE,'client');
		
		$form->setRequiredNote(REQUIRED_FIELDS);
		$form->setJsWarnings(ERRORE_JS,'');
		
		// CREO I PULSANTI
		if (empty($_REQUEST['id_portfolio'])) {
		
			$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', LABEL_INVIA,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
			
		} else {
		
			$form->setDefaults($record);
			
			if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) {
				// stampa
				$buttons[]=&HTML_QuickForm::createElement('button', 'btnPrint', LABEL_PRINT, 'class="button" onClick="window.open(\'print.php?page='.$_REQUEST['page'].'&act=form&id_portfolio='.$record['id_portfolio'].'\', \'_blank\', \'width=450, height=500, scrollbars=yes, resizable=yes\'); return false;"');
				$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
			}
		
		}
		
		$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
		
		if (!empty($_REQUEST['id_portfolio'])) $form->freeze(); 
		
		// mostro il form
		$form->display();
	
	break;
	
	case "upd": // insert / update
		echo '<h1>'.UPDATE.'</h1>';
		
		if (!empty($_REQUEST['id_portfolio'])) {
			$qry="select * from portfolio where id_portfolio=".$_REQUEST['id_portfolio']." and id_cliente=".$_SESSION['utente']['id_cliente'];
			$res=$db->query($qry);
			$rows=$res->numRows();
			if ($rows==0) { 
				$goPage->alertback(ACCESSO_NEGATO, false);
				exit();
			} 
			# update
			$query=$sql->prepareQuery ('portfolio', $_REQUEST, 'update', "id_portfolio='".$_REQUEST['id_portfolio']."'");
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=$_REQUEST['id_portfolio'];
		} else {
			# insert
			$query=$sql->prepareQuery ('portfolio', $_REQUEST, 'insert');
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=mysql_insert_id();
		}	
			
		# invio form richiesta
		### mail di avviso all'amministratore
		//require_once ($path_www."class/mail.class.php");
		
		//$titolo = TITOLO_MAIL_NUOVO_PORTFOLIO;
	
		//$testa = '<div style="font-family: Verdana, Helvetica, sans-serif; font-size: 12px; font-weight:bold;">'.$titolo.'</div>';
		$dati='In date '.date("d-m-Y").' the customer asked for buyng new title.<br />Here some data:';
		
		$dati .='<br />Customer: '.$_SESSION['utente']['cognome'].' '.$_SESSION['utente']['nome'].' '.$_SESSION['utente']['ragione_sociale'];
		$dati .=(!empty($_REQUEST['id_tipo_investimento'])?'<br />Investiment name: '.$optionTipiInvestimenti[$_REQUEST['id_tipo_investimento']]:'');
		$dati .=(!empty($_REQUEST['id_conto'])?'<br />Account n�: '.$optionContiDati[$_REQUEST['id_conto']]['numero_conto']:'');
		$dati .=(!empty($_REQUEST['sigla_lingua'])?'<br />Language: '.$_REQUEST['sigla_lingua']:'');
		
//		$dati .='<br />Collegandoti al pannello di amministrazione puoi vedere tutti i dati del portfolio ed attivarlo.';
		
		 
		
		$headers = array();

		$headers['to_name']=$name_admin;
		$headers['to_email']=$mail_admin;
		
		$headers['from_name']=$_SESSION['utente']['nome'].(!empty($_SESSION['utente']['cognome'])?' '.$_SESSION['utente']['cognome']:'').(!empty($_SESSION['utente']['ragione_sociale'])?' - '.$_SESSION['utente']['ragione_sociale']:'');
		
		$headers['from_email']=$_SESSION['utente']['email'];
		
		$headers['return_email']=$_SESSION['utente']['email'];
		$headers['reply_email']=$_SESSION['utente']['email'];
		$headers['reply_name']=$_SESSION['utente']['nome'].(!empty($_SESSION['utente']['cognome'])?' '.$_SESSION['utente']['cognome']:'').(!empty($_SESSION['utente']['ragione_sociale'])?' - '.$_SESSION['utente']['ragione_sociale']:'');
		
		$template_email = file_get_contents($path_www.'templates//email_post_richiesta_generica.html');
		$messaggio = str_replace('[CONTENUTO]',$dati, $template_email);
		
		$inviata = $func->sendMail($headers,TITOLO_MAIL_NUOVO_PORTFOLIO,$messaggio,'');	
		
		// messaggi
		// salvo la comunicazione in db
		$data=array();
		$data['id_cliente_mittente']=$_SESSION['utente']['id_cliente'];
		$data['id_cliente_destinatario']=1; // admin
		$data['email_mittente']=$param['from'];
		$data['email_destinatario']=$param['to'];
		$data['oggetto_messaggio']=$param['subject'];
		$data['testo_messaggio']=$dati;
		$data['stato_messaggio_mittente']='I';
		$data['stato_messaggio_destinatario']='N';
		$data['sigla_lingua']=$_SESSION['lingua'];
		$data['data_creazione_messaggio']=$_REQUEST['data_richiesta'];
		$data['data_invio_messaggio']=date("Y-m-d H:i:s");
		$data['notifica_automatica']=1;
		$data['notifica_email']=1;
		$query=$sql->prepareQuery ('messaggi', $data, 'insert');
		//echo '<br />'.$query;
		$res=$db->query($query);
		
		$goPage->alertgo(RICHIESTA_INOLTRATA, 'index.php?page='.$_REQUEST['page'].'&act=form&id_portfolio='.$lastid);
			
			
	break;
	
	case "del": // delete
		echo '<h1>'.DELETE.'</h1>';
		// non lo cancello per non perdere le transazioni
		// setto attivo a 0
		// il cliente non lo vede pi�
		// admin lo vede ma non pu� modificarlo o non lo vede pi� ?
		if (!empty($_REQUEST['id_portfolio'])) {
			$qry="select * from portfolio where id_portfolio=".$_REQUEST['id_portfolio']." and id_cliente=".$_SESSION['utente']['id_cliente'];
			$res=$db->query($qry);
			$rows=$res->numRows();
			if ($rows==0) { 
				$goPage->alertback(ACCESSO_NEGATO, false);
				exit();
			}			
			$query="delete from portfolio where id_portfolio='".$_REQUEST['id_portfolio']."'";
			$res=$db->query($query);

			$goPage->alertgo(DELETE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');
			
		} else $goPage->alertback(NO_RECORD, false);

	break;
/*	
	case "attiva": // attiva
		echo '<h1>'.ABILITA_PORTFOLIO.'</h1>';
		if (!empty($_REQUEST['id_portfolio'])) {
			
			$zealandCredit->attivaPortfolio();			
			
		} else $goPage->alertback(NO_RECORD, false);
	break; // end attiva
*/	
	default: // list
		
		// elenco tutti i conti filtrati per cliente / stato / tipo / deposito iniziale / attuale
		// attivabile da elenco con invio email all'utente
		echo '<h1>'.LIST_TABLE;
		echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form" title="'.INSERT_NEW.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.INSERT_NEW.'" /></a>';
		echo '</h1>';
		
		$qry="select por.*, cli.*, con.numero_conto, con.id_valuta from conti as con, portfolio as por, clienti as cli where por.id_cliente=cli.id_cliente and cli.id_cliente=".$_SESSION['utente']['id_cliente']." and cli.id_cliente=con.id_cliente and por.id_conto=con.id_conto";
		
		# filtri
		$qry .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?" and (cli.cognome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.nome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.ragione_sociale like '%".$_REQUEST['filtra_nome_cognome_soc']."%')":'');

		$qry .=(!empty($_REQUEST['filtra_tipo_investimento']) && $_REQUEST['filtra_tipo_investimento']!='*'?" and por.id_tipo_portfolio = ".$_REQUEST['filtra_tipo_investimento']."":'');

		$qry .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?" and por.stato '= ".$_REQUEST['filtra_valuta']."'":'');


		# order
		$qry .="  order by cli.cognome, cli.nome, cli.ragione_sociale, por.id_portfolio";

		$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list';
		$link_extra_param .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?"&amp;filtra_nome_cognome_soc=".$_REQUEST['filtra_nome_cognome_soc']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_investimento']) && $_REQUEST['filtra_tipo_investimento']!='*'?"&amp;filtra_tipo_investimento=".$_REQUEST['filtra_tipo_investimento']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?"&amp;filtra_valuta=".$_REQUEST['filtra_valuta']:'');
		
			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
			
			echo $view_links;
		?><br /><br />
		<form name="fFast_upd" method="post" action="index.php" onsubmit="return controllaForm('fFast_upd');">
		<input type="hidden" name="act" value="fast_upd" />
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
		<input type="hidden" name="filter" value="0" />
		<table class="tblAdmin">
		<tr>
			<th style="width:30px;"></th>
			<th style="width:30px;"></th>
			<th><?php echo LABEL_NOME_INVESTIMENTO; ?></th>
			<th><?php echo LABEL_DESCRIZIONE; ?></th>
			<th><?php echo LABEL_VALUTA; ?></th>
			<th><?php echo LABEL_VALORE_QUOTA; ?></th>
			<th><?php echo LABEL_NUMERO_QUOTE; ?></th>
			<th><?php echo LABEL_TOT_INVESTIMENTO; ?></th>
			<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			<th style="width:30px;"><?php echo LABEL_TRADE; ?></th>
		</tr>
        
        <!-- filtri -->
        <tr>
            <td colspan="2" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
            <td><?php
            // id_tipo_investimento           
            ?><select name="filtra_tipo_investimento" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionTipiInvestimenti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo_investimento']) && $_REQUEST['filtra_tipo_investimento']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
            <td></td>
            <td><?php
            // stato            
            ?><select name="filtra_valuta" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionValute as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
	<?php    
		$class="tdRow1";
		while ($record =& $res->fetchRow()) {
			//$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_portfolio='.$record['id_portfolio'].'" onclick="if (confirm(confirm_attiva_portfolio)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATTIVA_PORTFOLIO.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATTIVA_PORTFOLIO.'" title="'.PENDING.'-'.ATTIVA_PORTFOLIO.'" /></a>';
		
		//	print_r($record);
		// il portfolio non � modificabile se attivo, perch� ho gi� scalato il saldo del conto
			echo '
			<tr>';
			
			echo '
				<td class="'.$class.'">'.($record['stato']!='A'?'<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_portfolio='.$record['id_portfolio'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a>':'').'</td>
				<td class="'.$class.'">';
			if ($record['stato']!='A') echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_portfolio=".$record['id_portfolio']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			
			echo "</td>";
			echo "\n".'
				<td class="'.$class.'">'.$optionTipiInvestimenti[$record['id_tipo_investimento']].'</td>
				<td class="'.$class.'">'.$optionInvestimenti[$record['id_tipo_investimento']]['descrizione'].'</td>
				<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
				<td class="'.$class.'">'.number_format($record['valore_quota_iniziale'],2,',','.').'</td>
				<td class="'.$class.'">'.$record['numero_quote'].'</td>
				<td class="'.$class.'">'.number_format(($record['valore_quota_iniziale']*$record['numero_quote']),2,',','.').'</td>
				<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
				<td class="'.$class.'">'.($record['stato']=='A'?'<a href="'.$path_web.'?page=mytransactions&act=form&id_tipo_transazione=6&title='.TRADE_MARKER.'&amp;id_portfolio='.$record['id_portfolio'].'" title="'.LABEL_TRADE.'"><img src="'.$path_web.'img/icone/refresh2.png" alt="'.LABEL_TRADE.'" width="24" height="24" style="border: 0px;" /></a>':'').'</td>
			</tr>';
			
			$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
			
		} // end while
	?>
		</table>
		</form>
	
	<?php	
			echo $view_links;

		} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
		
}
	
?>
