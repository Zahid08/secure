<?php
# accounts.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

$optionTipiInvestimenti = $zealandCredit->getInvestimentiDurate($_SESSION['lingua'], true, true);
$optionValute = $zealandCredit->getValute($stato_opzioni, true);
$optionConti = $zealandCredit->getConti($stato_opzioni);

echo '<div class="titoloAdmin">'.LINK_MENU_SN_INVEST.'</div>';

switch ($_REQUEST['act']) {

	case "form": // form new / mod
		$record=array();
		if (!empty($_REQUEST['id_investimento'])) {
			echo '<h1>'.VIEW_MOD.'</h1>';
			# dati conto
			$qry="select * from investimenti where attivo = 1 and id_investimento=".$_REQUEST['id_investimento'];
			$res=$db->query($qry);
			$record = $res->fetchRow();
			
	
		} else {
			echo '<h1>'.INSERT_NEW.'</h1>';
			$record['data_creazione'] = date("Y-m-d H:i:s");
			$record['attivo']=1;
		}
		
		$form = new HTML_QuickForm('FormUpdate', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
		$form->addElement('hidden', 'page', $_REQUEST['page']);
		$form->addElement('hidden', 'act', 'upd');
		$form->addElement('hidden', 'id_investimento', $_REQUEST['id_investimento']);
		if (!empty($_REQUEST['id_investimento'])) {
			$form->addElement('hidden', 'minimo_deposito', '0', 'id="minimo_deposito"');
			$form->addElement('hidden', 'saldo_precedente', $record['saldo_attuale'], 'id="saldo_precedente"'); //campo ammontare
			$form->addElement('hidden', 'id_contoH', $record['id_conto'],'');			
		}else{
			$form->addElement('hidden', 'minimo_deposito', '', 'id="minimo_deposito"');
		}
		
		$form->addElement('hidden', 'minimo_conto', '', 'id="minimo_conto"');
		$form->addElement('hidden', 'id_cliente', '', 'id="id_cliente"');		
		$form->addElement('hidden', 'data_richiesta', $record['data_richiesta']);
		$form->addElement('hidden', 'data_attivazione', $record['data_attivazione']);
		$form->addElement('hidden', 'saldo_attuale_conto', '', 'id="saldo_attuale_conto"');
		
		$form->addElement('hidden', 'id_valuta', '', 'id="id_valuta"');
		$form->addElement('hidden', 'attivo', $record['attivo']);
		$form->addElement('hidden', 'old_stato', $record['stato']);
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_ATTIVAZIONE.'</div><div class="fieldForm">'.(!empty($record['data_attivazione'])?$func->formatData($record['data_attivazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_CREAZIONE.'</div><div class="fieldForm">'.$func->formatData($record['data_creazione'], 'd-m-Y H:i').'</div><div class="clearBoth"></div>');

		$form->addElement('select', 'stato', LABEL_STATO_INVESTIMENTO, $arrayStatiOperazioni, ' class="textbox" ');

		if (!empty($_REQUEST['id_investimento'])) {
			$form->addElement('select', 'id_conto', LABEL_CONTO, $optionConti, ' class="textbox" id="id_conto" disabled="disabled" onchange="setValuta(); setSaldoDeposito();" ');
		}else{
			$form->addElement('select', 'id_conto', LABEL_CONTO, $optionConti, ' class="textbox" id="id_conto"  onchange="setValuta(); setSaldoDeposito();" ');
		}
		$form->addElement('select', 'id_tipo_investimento', LABEL_TIPO_INVESTIMENTO, $optionTipiInvestimenti, ' id="id_tipo_investimento" class="textbox" onchange="controllaMinimoInvestimenti()"');

		
		if (!empty($_REQUEST['id_investimento'])) {
		$form->addElement('text', 'intestatario', LABEL_INTESTATARIO, ' class="textbox" disabled="disabled" size="60"');
		}else{
		$form->addElement('text', 'intestatario', LABEL_INTESTATARIO, ' class="textbox" size="60"');			
		}
		
		
		
		$form->addRule('intestatario', LABEL_INTESTATARIO, 'required', FALSE,'client');

		if (!empty($record['numero_investimento'])) $form->addElement('text', 'numero_investimento', LABEL_NUMERO_INVESTIMENTO,' class="textbox" disabled="disabled" size="25"');
		else $form->addElement('html', '<div class="labelForm">'.LABEL_NUMERO_INVESTIMENTO.'</div><div class="fieldForm">'.GENERAZIONE_AUTOMATICA.'</div><div class="clearBoth"></div>');
		if (!empty($_REQUEST['id_investimento'])) {
			$form->addElement('text', 'saldo_iniziale', 'Ammontare:', ' class="textbox" size="25" id="saldo_iniziale" onblur="controllaMinimoInvestimenti(); controllaRestante(\'upd\');"');			
		}else{
			$form->addElement('text', 'saldo_iniziale', 'Ammontare:', ' class="textbox" size="25" id="saldo_iniziale" onblur="controllaMinimoInvestimenti(); controllaRestante(\'ins\');"');			
		}
		
		$form->addElement('html', '<div class="labelForm"></div><div class="fieldForm" id="checkMinimoInvestimenti"></div><div class="clearBoth"></div>');

		$form->addElement('textarea', 'descrizione', LABEL_DESCRIZIONE,' class="textbox" cols="60" rows="5"');
				
		$form->addRule('minimo_deposito', LABEL_MINIMO_DEPOSITO, 'required', FALSE,'client');
		$form->addRule('stato', LABEL_STATO_INVESTIMENTO, 'required', FALSE,'client');
		$form->addRule('id_tipo_investimento', LABEL_TIPO_INVESTIMENTO, 'required', FALSE,'client');
		$form->addRule('id_conto', LABEL_CONTO, 'required', FALSE,'client');
		$form->addRule('saldo_iniziale', LABEL_SALDO_INIZIALE, 'required', FALSE,'client');
		$form->addRule('saldo_iniziale', LABEL_SALDO_INIZIALE.' '.NOT_NUM, 'numeric', FALSE,'client');
		

		$form->setDefaults($record); // da tabella
	
		
		$form->setRequiredNote(REQUIRED_FIELDS);
		$form->setJsWarnings(ERRORE_JS,'');
		
		// CREO I PULSANTI
		$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', SAVE,'class="button"');
		$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
		$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
		
		$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
	
		// mostro il form
		$form->display();
		if (!empty($_REQUEST['id_investimento'])) {
		echo '<script language="javascript"> 
				setValuta(); 
				setSaldoDeposito();
				</script>';
		}
	break;
	
	case "upd": // insert / update
	
		echo '<h1>'.UPDATE.'</h1>';
		//echo '<!--'.print_r($_REQUEST).' -->';
		$_REQUEST['saldo_attuale']=$_REQUEST['saldo_iniziale'];
		
		if (empty($_REQUEST['data_attivazione']) && $_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $_REQUEST['data_attivazione']=date("Y-m-d H:i:s");
		
		if (!empty($_REQUEST['id_investimento'])) {
			# reimposto il totale del conto##############
			$res=$db->query("update conti set saldo_attuale = saldo_attuale + ".$_REQUEST['saldo_precedente']." where id_conto = ".$_REQUEST['id_contoH']);
			
			# update
			$query=$sql->prepareQuery ('investimenti', $_REQUEST, 'update', "id_investimento='".$_REQUEST['id_investimento']."'");
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=$_REQUEST['id_investimento'];
		} else {
			# insert
			$query=$sql->prepareQuery ('investimenti', $_REQUEST, 'insert');
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=mysql_insert_id();
		}
		
		// numero conto
		if (empty($_REQUEST['numero_investimento'])) $_REQUEST['numero_investimento']=$zealandCredit->generaNumeroInvestimento($lastid, $_REQUEST['data_creazione']);
		$query="update investimenti set numero_investimento = '".$_REQUEST['numero_investimento']."' where id_investimento='".$lastid."'";
		//echo '<br />'.$query;
		$res=$db->query($query);
	
		// registrazione su interessi correnti per procedura batch
		//$dataAttiva = $zealandCredit->getAttivazione($lastid); //da fare
		$intStart = array();		
		
		if (!empty($_REQUEST['id_investimento'])) {
				$intStart['id_conto_investimento']=$lastid;
				$intStart['tipo']='D';
				//$intStart['data_attivazione'] = $dataAttiva;
				$intStart['tasso_applicato'] = $zealandCredit->getTasso($_REQUEST['id_tipo_investimento'],$_REQUEST['id_valuta'],$_REQUEST['saldo_iniziale'],$_SESSION['lingua']);
				$query=$sql->prepareQuery ('interessi_correnti', $intStart, 'update', "id_conto_investimento='".$_REQUEST['id_investimento']."'");
				$res=$db->query($query);
				
				//aggiornamento del saldo effettivo sul conto  corrente.
					$db->query("update conti set saldo_attuale = saldo_attuale - ".$_REQUEST['saldo_iniziale']." where id_conto = ".$_REQUEST['id_contoH']);				
				
		}else{
				//die(print_r($_REQUEST));
				$intStart['id_conto_investimento']=$lastid;
				$intStart['tipo']='D';
				//$intStart['data_attivazione'] = $dataAttiva;
				$intStart['tasso_applicato'] = $zealandCredit->getTasso($_REQUEST['id_tipo_investimento'],$_REQUEST['id_valuta'],$_REQUEST['saldo_iniziale'],$_SESSION['lingua']);
				$query=$sql->prepareQuery ('interessi_correnti', $intStart, 'insert');
				$res=$db->query($query);
				
				//aggiornamento del saldo effettivo sul conto  corrente.
					$db->query("update conti set saldo_attuale = saldo_attuale - ".$_REQUEST['saldo_iniziale']." where id_conto = ".$_REQUEST['id_contoH']);										
				
		}
		//


		
		
		
		
		// attivazione da form
		$_REQUEST['id_investimento']=$lastid;
		if ($_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $zealandCredit->attivaDeposito();
		
		
		$goPage->alertgo(UPDATE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');
			
			
	break;
	
	case "del": // delete
		echo '<h1>'.DELETE.'</h1>';
		// non lo cancello per non perdere le transazioni
		// setto attivo a 0
		// il cliente non lo vede più
		// admin lo vede ma non può modificarlo o non lo vede più ?
		if (!empty($_REQUEST['id_investimento'])) {
			//cancello virtualmente il deposito
			$query="update investimenti set attivo=0 where id_investimento='".$_REQUEST['id_investimento']."'";
			$res=$db->query($query);
			//cancello virtualmente il conteggio degli interessi
			$query="update interessi_correnti set attivo=0 where id_conto_investimento='".$_REQUEST['id_investimento']."' and tipo = 'D'";
			$res=$db->query($query);
			
			//riaccredito il saldo iniziale del deposito sul conto senza tenere conto di eventuali interessi
			$zealandCredit->aggiornoContoNoInteressi($_REQUEST['id_investimento']);
			
			
			$goPage->alertgo(DELETE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');
			
		} else $goPage->alertback(NO_RECORD, false);

	break;
	
	case "attiva": // delete
		echo '<h1>'.ABILITA_CONTO.'</h1>';

		if (!empty($_REQUEST['id_investimento'])) {
			$zealandCredit->attivaDeposito();
		} else $goPage->alertback(NO_RECORD, false);

	break;
	
	default: // list
		
		$qry = "select * from investimenti where attivo <> 0";
		
		if ($_SESSION['utente']['id_tipo_utente']!=1)
			$qry .=	" and id_conto in (select id_conto from conti where id_cliente = " . $_SESSION['utente']['id_cliente']  ." ) ";
		
		//echo $qry;
		$res = $db->query($qry);



		?><br /><br />

		<table class="tblAdmin">
		<tr>
        <?php	if ($_SESSION['utente']['id_tipo_utente']==1)	{ ?>
			<th style="width:30px;"></th>
			<th style="width:30px;"></th>
        <?php } ?>
			<th><?php echo LABEL_INTESTATARIO; ?></th>
			<th><?php echo LABEL_IDCONTO; ?></th>
   			<th><?php echo LABEL_NUMERODEPOSITO; ?></th>            
			<th><?php echo LABEL_TIPOINVESTIMENTO; ?></th>
			<th><?php echo LABEL_DURATA; ?></th>
			<th><?php echo LABEL_AMMONTARE; ?></th>                                    
			<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
		</tr>
        
        <!-- filtri -->
	<?php    
		$class="tdRow1";
		while ($record =& $res->fetchRow()) {
			
			$dati = explode('|', $record['id_tipo_investimento']);
			
			/*Fixed term deposit account|31104000|7*/
			
			$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_investimento='.$record['id_investimento'].'" onclick="if (confirm(confirm_attiva_conto)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATTIVA_CONTO.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATTIVA_CONTO.'" title="'.PENDING.'-'.ATTIVA_CONTO.'" /></a>';
		//	print_r($record);
			echo '
			<tr>';

			if ($_SESSION['utente']['id_tipo_utente']==1)
			{

			
			echo '<td class="'.$class.'">';
				echo '<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_investimento='.$record['id_investimento'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
				<td class="'.$class.'">';
			
			echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_investimento=".$record['id_investimento']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			
			echo "</td>";
			}
			
			echo "\n".'<td class="'.$class.'">'.$record['intestatario'].'</td>
				<td class="'.$class.'">'.$record['id_conto'].'</td>
				<td class="'.$class.'">'.$record['numero_investimento'].'</td>
				<td class="'.$class.'">'.$dati[0].'</td>
				<td class="'.$class.'">'.($dati[1]/2592000).' monthes</td>				
				<td class="'.$class.'">'.number_format($record['saldo_iniziale'],2,',','.').'</td>
				<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
			</tr>';
			
			$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
			
		} // end while
	?>
		</table>
		</form>
	
	<?php	
	
}
	
?>
