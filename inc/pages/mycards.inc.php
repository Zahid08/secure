<?php
# cards.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

$optionTipiCarte = $zealandCredit->getTypes('tipi_carte', $_SESSION['lingua'], $stato_opzioni, false);
//$optionClienti = $zealandCredit->getClientiConti($stato_opzioni);
//$optionConti =$zealandCredit->getContiClienti($stato_opzioni);
$optionClienti = $zealandCredit->getClientiConti();
$optionConti =$zealandCredit->getContiDati(true, $_SESSION['utente']['id_cliente']);
$optionValute = $zealandCredit->getValute($stato_opzioni, false);

$arrayConti=array();
foreach($optionConti as $id => $dati) $arrayConti[$id]=$dati['numero_conto'].' ('.$optionValute[$dati['id_valuta']].')';

$arrayTipoCard['D']=DEBIT_CARD;
$arrayTipoCard['C']=CREDIT_CARD;

$arrayDB['D']='-';
$arrayDB['C']='+';

$optionContiDati =$zealandCredit->getContiDati(true); // solo conti con abilitazione prelievo/deposito



echo '<div class="titoloAdmin">'.LINK_MENU_SN_CARDS.'</div>';

switch ($_REQUEST['act']) {

	case "form": // form richiesta nuova carta

		if (!empty($_REQUEST['id_carta'])) {
			# dati carta
			$qry="select * from carte where id_carta=".$_REQUEST['id_carta']." and id_cliente=".$_SESSION['utente']['id_cliente'];
			$res=$db->query($qry);
			$rows=$res->numRows();
			if ($rows==0) { 
				$goPage->alertback(ACCESSO_NEGATO, false);
				exit();
			} else {
				$record = $res->fetchRow();
				echo '<p>'.PRINT_FAX.'</p>';
			}
		} else {
			echo '<h1>'.RICHIEDI_NUOVA_CARTA.'</h1>';
			$record['data_richiesta']=date("Y-m-d H:i:s");
		}
		
		$form = new HTML_QuickForm('FormUpdate', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
		$form->addElement('hidden', 'page', $_REQUEST['page']);
		$form->addElement('hidden', 'act', 'upd');
		$form->addElement('hidden', 'data_richiesta', date("Y-m-d H:i:s"));
		$form->addElement('hidden', 'stato', 'P');
		$form->addElement('hidden', 'id_cliente', $_SESSION['utente']['id_cliente']);
		$form->addElement('hidden', 'attivo', '1');

		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'. date("d-m-Y").'</div><div class="clearBoth"></div>');

		
		$form->addElement('select', 'id_conto', LABEL_CONTO, $arrayConti, ' class="textbox" ');
		
		$form->addElement('select', 'id_tipo_carta', LABEL_TIPO_CARTA, $optionTipiCarte, ' class="textbox" ');

		$form->addElement('select', 'debito_credito', LABEL_DEBITO_CREDITO, $arrayTipoCard, ' class="textbox" ');
		
		$form->addRule('id_conto', LABEL_CONTO, 'required', FALSE,'client');
		$form->addRule('debito_credito', LABEL_DEBITO_CREDITO, 'required', FALSE,'client');
		$form->addRule('id_tipo_carta', LABEL_TIPO_CARTA, 'required', FALSE,'client');
		
		$form->setRequiredNote(REQUIRED_FIELDS);
		$form->setJsWarnings(ERRORE_JS,'');
		// CREO I PULSANTI
		if (empty($_REQUEST['id_carta'])) {
		
			$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', LABEL_INVIA,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
			
		} else {
		
			$form->setDefaults($record);
			if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) {
			
				// stampa
				$buttons[]=&HTML_QuickForm::createElement('button', 'btnPrint', LABEL_PRINT, 'class="button" onClick="window.open(\'print.php?page='.$_REQUEST['page'].'&act=form&id_carta='.$record['id_carta'].'\', \'_blank\', \'width=450, height=500, scrollbars=yes, resizable=yes\'); return false;"');
				$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
			}
			
		}
		
		$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
	
		if (!empty($_REQUEST['id_carta'])) $form->freeze(); 
	
		// mostro il form
		$form->display();
	
	break; // end form
	
	case "upd": // insert / update
		echo '<h1>'.RICHIEDI_NUOVA_CARTA.'</h1>';
		
		// valuta del conto
		$_REQUEST['id_valuta']=$optionContiDati[$_REQUEST['id_conto']]['id_valuta'];
		
		# insert
		$query=$sql->prepareQuery ('carte', $_REQUEST, 'insert');
		//echo '<br />'.$query;
		$res=$db->query($query);
		$lastid=mysql_insert_id();

		# invio form richiesta
		### mail di avviso all'amministratore
	//	require_once ($path_www."class/mail.class.php");
		
		
	//	$titolo = TITOLO_MAIL_NUOVA_CARTA;
	
	//	$testa = '<div style="font-family: Verdana, Helvetica, sans-serif; font-size: 12px; font-weight:bold;">'.$titolo.'</div>';
		$dati='In date '.date("d-m-Y").' the customer asked for a new card activation:<br />';
		
		$dati .='<br />Customer: '.$_SESSION['utente']['cognome'].' '.$_SESSION['utente']['nome'].' '.$_SESSION['utente']['ragione_sociale'];
		$dati .=(!empty($_REQUEST['id_tipo_carta'])?'<br />Card type: '.$optionTipiCarte[$_REQUEST['id_tipo_carta']]:'');
		$dati .=(!empty($_REQUEST['debito_credito'])?'<br />Debit/Credit: '.$arrayTipoCard[$_REQUEST['debito_credito']]:'');
		$dati .=(!empty($_REQUEST['id_conto'])?'<br />Account n�: '.$arrayConti[$_REQUEST['id_conto']]:'');
		$dati .=(!empty($_REQUEST['sigla_lingua'])?'<br />Language: '.$_REQUEST['sigla_lingua']:'');
		
//		$dati .='<br />Collegandoti al pannello di amministrazione puoi vedere tutti i dati della carta ed attivarla.';
		
		 
		$headers = array();

		$headers['to_name']=$name_admin;
		$headers['to_email']=$mail_admin;
		
		$headers['from_name']=$_SESSION['utente']['nome'].(!empty($_SESSION['utente']['cognome'])?' '.$_SESSION['utente']['cognome']:'').(!empty($_SESSION['utente']['ragione_sociale'])?' - '.$_SESSION['utente']['ragione_sociale']:'');
		
		$headers['from_email']=$_SESSION['utente']['email'];
		
		$headers['return_email']=$_SESSION['utente']['email'];
		$headers['reply_email']=$_SESSION['utente']['email'];
		$headers['reply_name']=$_SESSION['utente']['nome'].(!empty($_SESSION['utente']['cognome'])?' '.$_SESSION['utente']['cognome']:'').(!empty($_SESSION['utente']['ragione_sociale'])?' - '.$_SESSION['utente']['ragione_sociale']:'');
		
		$template_email = file_get_contents($path_www.'templates/email_post_richiesta_generica.html');
		$messaggio = str_replace('[CONTENUTO]',$dati, $template_email);
		 
		$inviata = $func->sendMail($headers,TITOLO_MAIL_NUOVA_CARTA,$messaggio,'');	
		
		// messaggi
		// salvo la comunicazione in db
		$data=array();
		$data['id_cliente_mittente']=$_SESSION['utente']['id_cliente'];
		$data['id_cliente_destinatario']=1; // admin
		$data['email_mittente']=$param['from'];
		$data['email_destinatario']=$param['to'];
		$data['oggetto_messaggio']=$param['subject'];
		$data['testo_messaggio']=$dati;
		$data['stato_messaggio_mittente']='I';
		$data['stato_messaggio_destinatario']='N';
		$data['sigla_lingua']=$_SESSION['lingua'];
		$data['data_creazione_messaggio']=$_REQUEST['data_richiesta'];
		$data['data_invio_messaggio']=date("Y-m-d H:i:s");
		$data['notifica_automatica']=1;
		$data['notifica_email']=1;
		$query=$sql->prepareQuery ('messaggi', $data, 'insert');
		//echo '<br />'.$query;
		$res=$db->query($query);
		
		$goPage->alertgo(RICHIESTA_INOLTRATA, 'index.php?page='.$_REQUEST['page'].'&act=form&id_carta='.$lastid);
			
	break; // end upd
	
	default: // list
		
		// elenco tutti i conti filtrati per cliente / stato / tipo / deposito iniziale / attuale
		// attivabile da elenco con invio email all'utente
		echo '<h1>'.LIST_TABLE;
		echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form" title="'.RICHIEDI_NUOVA_CARTA.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.RICHIEDI_NUOVA_CARTA.'" /></a>';
		echo '</h1>';
		
		$qry="select car.*, cli.*, con.numero_conto from carte as car, conti as con, clienti as cli where cli.id_cliente=".$_SESSION['utente']['id_cliente']." and car.attivo=1 and con.id_cliente=cli.id_cliente and car.id_cliente=cli.id_cliente and car.id_conto=con.id_conto";
		
		# filtri
		$qry .=(!empty($_REQUEST['filtra_tipo_carta']) && $_REQUEST['filtra_tipo_carta']!='*'?" and cli.id_tipo_carta = ".$_REQUEST['filtra_tipo_carta']."":'');

		$qry .=(!empty($_REQUEST['filtra_stato_carta']) && $_REQUEST['filtra_stato_carta']!='*'?" and ctg.stato '= ".$_REQUEST['filtra_stato_carta']."'":'');


		# order
		$qry .="  order by cli.cognome, cli.nome, cli.ragione_sociale, car.numero_carta";

		$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list';
		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_carta']) && $_REQUEST['filtra_tipo_carta']!='*'?"&amp;filtra_tipo_carta=".$_REQUEST['filtra_tipo_carta']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_stato_carta']) && $_REQUEST['filtra_stato_carta']!='*'?"&amp;filtra_stato_carta=".$_REQUEST['filtra_stato_carta']:'');
		

			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
			
			echo $view_links;
		?><br /><br />
		<form name="fFast_upd" method="post" action="index.php" onsubmit="return controllaForm('fFast_upd');">
		<input type="hidden" name="act" value="fast_upd" />
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
		<input type="hidden" name="filter" value="0" />
		<table class="tblAdmin">
		<tr>
 			<th><?php echo LABEL_DEBITO_CREDITO; ?></th>
			<th><?php echo LABEL_TIPO_CARTA; ?></th>
			<th><?php echo LABEL_NUMERO_CARTA; ?></th>
			<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
		</tr>

	<?php    
		$class="tdRow1";
		while ($record =& $res->fetchRow()) {
			$arrayImgStati['P']='<img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATTIVA_CARTA.'" title="'.PENDING.'" />';
		
		//	print_r($record);
			echo '
			<tr>
				<!--<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_carta='.$record['id_carta'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
				<td class="'.$class.'">';
			
			echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_carta=".$record['id_carta']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			
			echo "</td>-->";
			echo "\n".'
				<td class="'.$class.'">'.$arrayTipoCard[$record['debito_credito']].'</td>
				<td class="'.$class.'">'.$optionTipiCarte[$record['id_tipo_carta']].'</td>
				<td class="'.$class.'">'.$record['numero_carta'].'</td>
				<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
			</tr>';
			
			$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
			
		} // end while
	?>
		</table>
		</form>
	
	<?php	
			echo $view_links;

		} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
		
}
	
?>
