<?php
# myreports.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}


if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) {
	echo '<div class="titoloAdmin">'.LINK_MENU_SN_MY_REPORTS.'</div>';
	
	echo '<div style="text-align:right;">'.VIEW_REPORTS.': ';
	echo '<a href="'.$path_web.'?page=myreports&amp;report_type=transactions" title="'.LINK_MENU_SN_TRANSACTIONS.'">'.LINK_MENU_SN_TRANSACTIONS.'</a> | ';
	echo '<a href="'.$path_web.'?page=myreports&amp;report_type=cards" title="'.LINK_MENU_SN_CARDS.'">'.LINK_MENU_SN_CARDS.'</a>';
	
	echo '</div>';
}
// estratto conto e movimenti carte
// non modificabili - solo elenco
// solo quelli attivi


// data dell'operazione - cliente - tipo operazione - valuta - importo
// data dell'operazione - cliente - carta - valuta - importo

// filtri / ordinamenti / stampabile


switch ($_REQUEST['report_type']) {
	
	case "cards": // carte
	# 3 - carte
	
		$optionTipiCarte = $zealandCredit->getTypes('tipi_carte', $_SESSION['lingua'], $stato_opzioni, false);
		$optionValute = $zealandCredit->getValute($stato_opzioni, false);
		
		//$optionClienti = $zealandCredit->getClientiConti($stato_opzioni);
		//$optionConti =$zealandCredit->getContiClienti($stato_opzioni);
		$optionClienti = $zealandCredit->getClientiConti();
		$optionCarte =$zealandCredit->getCarte($_SESSION['utente']['id_cliente']);
		
		$arrayTipoCard['D']=DEBIT_CARD;
		$arrayTipoCard['C']=CREDIT_CARD;
		$arrayDB['D']='-';
		$arrayDB['C']='+';
				
		if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) echo '<div class="titoloAdmin">'.MOVIMENTI_CARDS.'</div>';

			$qry="select mov.*, car.id_tipo_carta, car.id_cliente, car.id_conto, car.id_valuta, car.numero_carta, car.debito_credito  as d_b_carta from carte as car, movimenti_carte as mov where mov.id_carta=car.id_carta and mov.stato='A' and car.id_cliente = ".$_SESSION['utente']['id_cliente']."";
			
			
			// eventuali filtri
			$qry .=(!empty($_REQUEST['filtra_carta']) && $_REQUEST['filtra_carta']!='*'?" and mov.id_carta = ".$_REQUEST['filtra_carta']."":'');
	
		//	$qry .=(!empty($_REQUEST['filtra_cliente']) && $_REQUEST['filtra_cliente']!='*'?" and car.id_cliente = ".$_REQUEST['filtra_cliente']."":'');

			$qry .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?" and mov.id_valuta = ".$_REQUEST['filtra_valuta']."":'');
			
			$qry .=(!empty($_REQUEST['filtra_data']) && $_REQUEST['filtra_data']!='*'?" and DATE_FORMAT(mov.data_attivazione, '%d-%m-%Y') ='".$_REQUEST['filtra_data']."'":'');

			// intervallo di tempo
			$qry .=(!empty($_REQUEST['from_date']) && $_REQUEST['from_date']!='*'?" and DATE_FORMAT(mov.data_attivazione, '%d-%m-%Y')>='".$_REQUEST['from_date']."'":'');
			
			$qry .=(!empty($_REQUEST['since_date']) && $_REQUEST['since_date']!='*'?" and DATE_FORMAT(mov.data_attivazione, '%d-%m-%Y') <='".$_REQUEST['since_date']."'":'');
		
			
			$qry .=" order by mov.data_attivazione asc";
			
			//echo $qry;
			$link_extra_param ='page='.$_REQUEST['page'].'&amp;report_type='.$_REQUEST['report_type'];
			
			$link_extra_param .=(!empty($_REQUEST['filtra_carta']) && $_REQUEST['filtra_carta']!=''?"&amp;filtra_carta=".$_REQUEST['filtra_carta']:'');
			
		//	$link_extra_param .=(!empty($_REQUEST['filtra_cliente']) && $_REQUEST['filtra_cliente']!=''?"&amp;filtra_cliente=".$_REQUEST['filtra_cliente']:'');
			
			$link_extra_param .=(!empty($_REQUEST['filtra_conto']) && $_REQUEST['filtra_conto']!=''?"&amp;filtra_conto=".$_REQUEST['filtra_conto']:'');

			$link_extra_param .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?"&amp;filtra_valuta=".$_REQUEST['filtra_valuta']:'');
			
			$link_extra_param .=(!empty($_REQUEST['since_date']) && $_REQUEST['since_date']!='*'?"&amp;since_date=".$_REQUEST['since_date']:'');

			$link_extra_param .=(!empty($_REQUEST['from_date']) && $_REQUEST['from_date']!='*'?"&amp;from_date=".$_REQUEST['from_date']:'');
			
			$res=$db->query($qry);
				
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}			
				
			$rows = $res->numRows();
				
			if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) $res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
				
			if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) {
?>			<fieldset style="padding:5px">
				<legend style="font-weight:bold"><?php echo LABEL_SEARCH; ?></legend>
                	<p><?php echo WRITE_DATE; ?></p>
			<form name="formCerca" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" onsubmit="return ValidateForm();">
			<input type="hidden" name="act" value="list" />
			<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
			<input type="hidden" name="report_type" value="<?php echo $_REQUEST['report_type']; ?>" />
			<input type="hidden" name="filter" value="1" />
            
            <?php echo FROM; ?> <input type="text" name="from_date" value="<?php echo $_REQUEST['from_date']; ?>" class="textbox" size="15" maxlength="10" />
			<?php echo FINO_AL; ?> <input type="text" name="since_date" value="<?php echo $_REQUEST['since_date']; ?>" class="textbox" size="15" maxlength="10" /> 
                 
           <?php echo LABEL_NUMERO_CARTA; ?> <select name="filtra_carta" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionCarte as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_carta']) && $_REQUEST['filtra_carta']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select>
           
            &nbsp;&nbsp;&nbsp;
            <input type="submit" value="<?php echo FILTRA; ?>" class="button" />
			</form></fieldset><br />
<?php			
            } // stampo ricerca solo in index
				
			if ($rows>0) {
			
				if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) {
			
				echo NUM_RECORDS.': '.$rows.' <a href="print.php?'.$link_extra_param.'" title="'.LABEL_PRINT.'" onClick="window.open(this.href, \'_blank\', \'width=650, height=600, scrollbars=yes, resizable=yes\'); return false;"><img src="'.$path_web.'img/icone/print.png" alt="'.LABEL_PRINT.'" width="24" height="24" border="0" /></a><br /><br />';
				} else {
					
					echo '<div class="titoloAdmin">'.MOVIMENTI_CARDS;
					
					echo '</div><p>';
					if (!empty($_REQUEST['filtra_carta']) && $_REQUEST['filtra_carta']!='*') echo '<br />'.$optionTipiCarte[$_REQUEST['filtra_carta']].' '.$optionCarte[$_REQUEST['filtra_carta']];
					
					if (!empty($_REQUEST['from_date'])) echo '<br />'.FROM.' '.$_REQUEST['from_date'];
					
					if (!empty($_REQUEST['since_date'])) echo '<br />'.FINO_AL.' '.$_REQUEST['since_date'];
					
					echo '</p>';
				}
	
					
				?>
				<table class="tblAdmin">
				<tr>
					<th><?php echo LABEL_DATA; ?></th>
                    <th><?php echo LABEL_CLIENTE; ?></th>
                    <th><?php echo LABEL_TIPO_CARTA; ?></th>
                    <th><?php echo LABEL_NUMERO_CARTA; ?></th>
					<th><?php echo LABEL_VALUTA; ?></th>
					<th><?php echo LABEL_IMPORTO; ?></th>
					<th><?php echo LABEL_CAUSALE; ?></th>
				</tr>
				
				<!-- filtri -->

			<?php    
				$class="tdRow1";
				while ($record =& $res->fetchRow()) {
				
				//	print_r($record);
					echo '
					<tr>';
					echo "\n".'<td class="'.$class.'">'.$func->formatData($record['data_attivazione'], "d-m-Y H:i").'</td>
						<td class="'.$class.'">'.$optionClienti[$record['id_cliente']].'</td>
						<td class="'.$class.'">'.$optionTipiCarte[$record['id_carta']].'</td>
						<td class="'.$class.'">'.$optionCarte[$record['id_carta']].'</td>
						<td class="'.$class.'">'.$record['causale'].'</td>
						<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
						<td class="'.$class.'">'.$arrayDB[$record['debito_credito']].' '.number_format($record['importo'],2,',','.').'</td>
					</tr>';
					
					$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
					
				} // end while
			?>
				</table>
			
			<?php	
					echo $view_links;

				} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
		
	break; # 3 - carte
	
	default:
	//case "transazioni": // transazioni
	# 4 - transazioni
			$optionClientiConti=$zealandCredit->getClientiConti(true);
			$optionValute = $zealandCredit->getValute($stato_opzioni, false);
			$optionTransazioni = $zealandCredit->getTypes('tipi_transazioni', $_SESSION['lingua'], $stato_opzioni);
			$optionContiDaA =$zealandCredit->getContiDati(true, $_SESSION['utente']['id_cliente']);
			//print_r($optionContiDaA);
			if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) {
				echo '<h1>'.LINK_MENU_SN_TRANSACTIONS;
				echo '</h1>';
			}
			
			$qry="select * from transazioni where stato='A' and id_richiedente = ".$_SESSION['utente']['id_cliente'];
			
			# filtri
	
			$qry .=(!empty($_REQUEST['filtra_tipo']) && $_REQUEST['filtra_tipo']!='*'?" and id_tipo_transazione = ".$_REQUEST['filtra_tipo']."":'');
	
		//	$qry .=(!empty($_REQUEST['filtra_cliente']) && $_REQUEST['filtra_cliente']!='*'?" and id_richiedente = ".$_REQUEST['filtra_cliente']."":'');

			$qry .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?" and id_valuta = ".$_REQUEST['filtra_valuta']."":'');
			
			$qry .=(!empty($_REQUEST['filtra_conto']) && $_REQUEST['filtra_conto']!='*'?" and (id_conto_da = ".$_REQUEST['filtra_conto']." or id_conto_a = ".$_REQUEST['filtra_conto'].")":'');

			//$qry .=(!empty($_REQUEST['filtra_data']) && $_REQUEST['filtra_data']!='*'?" and DATE_FORMAT(data_transazione, '%d-%m-%Y') ='".$_REQUEST['filtra_data']."'":'');

			// intervallo di tempo
			$qry .=(!empty($_REQUEST['from_date']) && $_REQUEST['from_date']!='*'?" and DATE_FORMAT(data_transazione, '%d-%m-%Y')>='".$_REQUEST['from_date']."'":'');
			
			$qry .=(!empty($_REQUEST['since_date']) && $_REQUEST['since_date']!='*'?" and DATE_FORMAT(data_transazione, '%d-%m-%Y') <='".$_REQUEST['since_date']."'":'');
			
			# order
			$qry .="  order by data_transazione desc";
			//echo $qry;
			$link_extra_param ='page='.$_REQUEST['page'].'&amp;report_type='.$_REQUEST['report_type'];
			
			$link_extra_param .=(!empty($_REQUEST['filtra_tipo']) && $_REQUEST['filtra_tipo']!=''?"&amp;filtra_tipo=".$_REQUEST['filtra_tipo']:'');
			
	//		$link_extra_param .=(!empty($_REQUEST['filtra_cliente']) && $_REQUEST['filtra_cliente']!=''?"&amp;filtra_cliente=".$_REQUEST['filtra_cliente']:'');
			
			$link_extra_param .=(!empty($_REQUEST['filtra_conto']) && $_REQUEST['filtra_conto']!=''?"&amp;filtra_conto=".$_REQUEST['filtra_conto']:'');

			$link_extra_param .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?"&amp;filtra_valuta=".$_REQUEST['filtra_valuta']:'');
			
			$link_extra_param .=(!empty($_REQUEST['filtra_data']) && $_REQUEST['filtra_data']!='*'?"&amp;filtra_data=".$_REQUEST['filtra_data']:'');

			$link_extra_param .=(!empty($_REQUEST['from_date']) && $_REQUEST['from_date']!='*'?"&amp;from_date=".$_REQUEST['from_date']:'');

			$link_extra_param .=(!empty($_REQUEST['since_date']) && $_REQUEST['since_date']!='*'?"&amp;since_date=".$_REQUEST['since_date']:'');
			
			$res=$db->query($qry);
				
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();
				
			if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) $res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
				
			if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) {
?>			<fieldset>
				<legend style="font-weight:bold"><?php echo LABEL_SEARCH; ?></legend>
                	<p><?php echo WRITE_DATE; ?></p>
			<form name="formCerca" method="post" action="index.php" onsubmit="return ValidateForm();">
			<input type="hidden" name="act" value="list" />
			<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
			<input type="hidden" name="report_type" value="<?php echo $_REQUEST['report_type']; ?>" />
			<input type="hidden" name="filter" value="1" />
            
            <?php echo FROM; ?> <input type="text" name="from_date" value="<?php echo $_REQUEST['from_date']; ?>" class="textbox" size="15" maxlength="10" />
			<?php echo FINO_AL; ?> <input type="text" name="since_date" value="<?php echo $_REQUEST['since_date']; ?>" class="textbox" size="15" maxlength="10" /> 
           <?php echo LABEL_TIPO_TRANSAZIONE; ?> <select name="filtra_tipo" class="textbox" style="width:160px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionTransazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo']) && $_REQUEST['filtra_tipo']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select>
           
           <?php echo LABEL_CONTO; ?> <select name="filtra_conto" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionContiDaA as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_conto']) && $_REQUEST['filtra_conto']==$id?' selected':'').'>'.$nome['numero_conto'].'</option>';
					?>
				</select>
            &nbsp;&nbsp;&nbsp;
            <input type="submit" value="<?php echo FILTRA; ?>" class="button" />
			</form></fieldset><br />
<?php			
            } // stampo ricerca solo in index
			
			if ($rows>0) {
				if (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) {
			
				echo NUM_RECORDS.': '.$rows.' <a href="print.php?'.$link_extra_param.'" title="'.LABEL_PRINT.'" onClick="window.open(this.href, \'_blank\', \'width=650, height=600, scrollbars=yes, resizable=yes\'); return false;"><img src="'.$path_web.'img/icone/print.png" alt="'.LABEL_PRINT.'" width="24" height="24" border="0" /></a><br /><br />';
				} else {
					
					echo '<div class="titoloAdmin">'.LINK_MENU_SN_TRANSACTIONS;
					echo '</div><p>';
					
					if (!empty($_REQUEST['filtra_conto']) && $_REQUEST['filtra_conto']!='*') echo '<br />'.LABEL_CONTO.' '.$optionContiDaA[$_REQUEST['filtra_conto']]['numero_conto'];
					
					if (!empty($_REQUEST['from_date'])) echo '<br />'.FROM.' '.$_REQUEST['from_date'];
					
					if (!empty($_REQUEST['since_date'])) echo '<br />'.FINO_AL.' '.$_REQUEST['since_date'];
					
					echo '</p>';
				}
				
			?>
			<table class="tblAdmin">
			<tr>
				<th><?php echo LABEL_DATA; ?></th>
				<th><?php echo LABEL_CLIENTE; ?></th>
				<!--<th><?php echo CONTO_DA; ?></th>-->
				<th><?php echo LABEL_TIPO_TRANSAZIONE; ?></th>
				<th><?php echo LABEL_DESCRIZIONE; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
			//	print_r($record);
				echo '
				<tr>
					<!--<td class="'.$class.'"</td>
					<td class="'.$class.'"></td>-->';
				echo "\n".'
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$optionClientiConti[$record['id_richiedente']].'</td>
					<!--<td class="'.$class.'">'.$optionContiDaA[$record['id_conto_da']]['numero_conto'].'</td>-->
					<td class="'.$class.'">'.$optionTransazioni[$record['id_tipo_transazione']].'</td>
					<td class="'.$class.'">'.$record['descrizione'].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>';
				
				$importo=number_format($record['importo_transazione'],2,',','.');
				
				switch ($record['id_tipo_transazione']) {
					
					case "1": // Transfer Between Accounts
					
						if (!empty($_REQUEST['filtra_conto']) && $_REQUEST['filtra_conto']!='*') {
							
							if ($_REQUEST['filtra_conto']==$record['id_conto_da']) $importo = '-'.$importo;
							if ($_REQUEST['filtra_conto']==$record['id_conto_a']) $importo = '+'.$importo;
							
						} else $importo = '+/-'.$importo;
					break;

					case "2": // Transfer to Other User
					
						if (!empty($_REQUEST['filtra_conto']) && $_REQUEST['filtra_conto']!='*') {
							
							if ($_REQUEST['filtra_conto']==$record['id_conto_da']) $importo = '-'.$importo;
							if ($_REQUEST['filtra_conto']==$record['id_conto_a']) $importo = '+'.$importo;
							
						} else $importo = '+/-'.$importo;

					break;

					case "4": // Outgoing Wire Transfer
						$importo = '-'.$importo;
					break;

					case "5": // Incoming Wire Transfer
						$importo = '+'.$importo;
					break;

					case "6": // Trade Marker
						$importo = ($record['buy_sell']=='B'?'-':'+').$importo;
					break;

					case "7": // Fees sempre negativo
						$importo = '-'.$importo;
					break;
					
				}
				
				
				echo '	
					<td class="'.$class.'">'.$importo.'</td>
					</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>
		<?php	
				echo $view_links;
				// se ho scelto un conto metto il saldo attuale
				
				if (!empty($_REQUEST['filtra_conto']) && $_REQUEST['filtra_conto']!='*') {
					$qryC="select * from conti where id_conto = ".$_REQUEST['filtra_conto'];
					$resC=$db->query($qryC);
					$recordC = $resC->fetchRow();
					
					echo '<div style="text-align:right; font-weight:bold; margin:5px;">'.LABEL_SALDO_ATTUALE.' '.$optionValute[$recordC['id_valuta']].' '.number_format($recordC['saldo_attuale'],2,',','.').'</div>';
					

				}
				
			
			} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
	
	//break; # 4 - transazioni
	
} // end switch report_type
?>
