<?php
# cards.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

$optionTipiCarte = $zealandCredit->getTypes('tipi_carte', $_SESSION['lingua'], $stato_opzioni, false);

$optionValute = $zealandCredit->getValute($stato_opzioni, false);

//$optionClienti = $zealandCredit->getClientiConti($stato_opzioni);
//$optionConti =$zealandCredit->getContiClienti($stato_opzioni);
$optionClienti = $zealandCredit->getClientiConti();
$optionConti =$zealandCredit->getContiClienti();

$arrayTipoCard['D']=DEBIT_CARD;
$arrayTipoCard['C']=CREDIT_CARD;

$arrayDB['D']='-';
$arrayDB['C']='+';

$optionContiDati =$zealandCredit->getContiDati(true); // solo conti con abilitazione prelievo/deposito

echo '<div class="titoloAdmin">'.LINK_MENU_SN_CARDS.'</div>';

switch ($_REQUEST['act']) {

	case "form": // form new / mod
		$record=array();
		if (!empty($_REQUEST['id_carta'])) {
			echo '<h1>'.VIEW_MOD.'</h1>';
			# dati conto
			$qry="select * from carte where attivo = 1 and id_carta=".$_REQUEST['id_carta'];
			$res=$db->query($qry);
			$record = $res->fetchRow();
		} else {
			echo '<h1>'.INSERT_NEW.'</h1>';
			$record['data_creazione'] = date("Y-m-d H:i:s");
			$record['attivo']=1;
		}

		$record['conto'][0]=$record['id_cliente'];
		$record['conto'][1]=$record['id_conto'];
		
		$form = new HTML_QuickForm('FormUpdate', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
		$form->addElement('hidden', 'page', $_REQUEST['page']);
		$form->addElement('hidden', 'act', 'upd');
		$form->addElement('hidden', 'id_carta', $_REQUEST['id_carta']);
		$form->addElement('hidden', 'data_richiesta', $record['data_richiesta']);
		$form->addElement('hidden', 'data_attivazione', $record['data_attivazione']);
		$form->addElement('hidden', 'data_creazione', $record['data_creazione']);
		$form->addElement('hidden', 'attivo', $record['attivo']);
		$form->addElement('hidden', 'old_stato', $record['stato']);

		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_ATTIVAZIONE.'</div><div class="fieldForm">'.(!empty($record['data_attivazione'])?$func->formatData($record['data_attivazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_CREAZIONE.'</div><div class="fieldForm">'.$func->formatData($record['data_creazione'], 'd-m-Y H:i').'</div><div class="clearBoth"></div>');

		$form->addElement('select', 'stato', LABEL_STATO_CARTA, $arrayStatiOperazioni, ' class="textbox" ');

		//$form->addElement('hidden', 'attivo', $record['attivo']);
		//$form->addElement('html', '<div class="labelForm">'.ATTIVO_CLIENTE.'</div><div class="fieldForm">'.($record['attivo']==1?LABEL_SI:LABEL_NO).'</div><div class="clearBoth"></div>');
		
		//$form->addElement('select', 'id_cliente', LABEL_CLIENTE, $optionClienti, ' class="textbox" ');
		//$form->addElement('select', 'id_conto', LABEL_CONTO, $optionConti, ' class="textbox" ');
		$sel =& $form->addElement('hierselect', 'conto', LABEL_CONTO,'class="textbox"');
		$sel->setOptions(array($optionClienti,$optionConti));
		
		$form->addElement('select', 'id_tipo_carta', LABEL_TIPO_CARTA, $optionTipiCarte, ' class="textbox" ');

		$form->addElement('select', 'debito_credito', LABEL_DEBITO_CREDITO, $arrayTipoCard, ' class="textbox" ');
		


		//if (!empty($record['numero_carta'])) 
		$form->addElement('text', 'numero_carta', LABEL_NUMERO_CARTA,' class="textbox" size="25"');
		//else $form->addElement('html', '<div class="labelForm">'.LABEL_NUMERO_CARTA.'</div><div class="fieldForm">'.GENERAZIONE_AUTOMATICA.'</div><div class="clearBoth"></div>');
		
		$form->addRule('stato', LABEL_STATO_CARTA, 'required', FALSE,'client');
		
		//$form->addRule('id_cliente', LABEL_CLIENTE, 'required', FALSE,'client');
		//$form->addRule('id_conto', LABEL_CONTO, 'required', FALSE,'client');
		$form->addRule('conto', LABEL_CONTO, 'required', FALSE,'client');
		$form->addRule('debito_credito', LABEL_DEBITO_CREDITO, 'required', FALSE,'client');
		$form->addRule('id_tipo_carta', LABEL_TIPO_CARTA, 'required', FALSE,'client');
		$form->addRule('numero_carta', LABEL_NUMERO_CARTA, 'required', FALSE,'client');
		
		$form->setDefaults($record); // da tabella
	
		$form->setRequiredNote(REQUIRED_FIELDS);
		$form->setJsWarnings(ERRORE_JS,'');
		
		// CREO I PULSANTI
		$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', SAVE,'class="button"');
		$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
		$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
		
		$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
	
		// mostro il form
		$form->display();
	
	break; // end form
	
	case "upd": // insert / update
		echo '<h1>'.UPDATE.'</h1>';
		if (empty($_REQUEST['data_attivazione']) && $_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $_REQUEST['data_attivazione']=date("Y-m-d H:i:s");
		
		$_REQUEST['id_cliente']=$_REQUEST['conto'][0];
		$_REQUEST['id_conto']=$_REQUEST['conto'][1];
		
		$_REQUEST['id_valuta']=$optionContiDati[$_REQUEST['id_conto']]['id_valuta'];
		if (!empty($_REQUEST['id_carta'])) {
			# update
			$query=$sql->prepareQuery ('carte', $_REQUEST, 'update', "id_carta='".$_REQUEST['id_carta']."'");
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=$_REQUEST['id_carta'];
		} else {
			# insert
			$query=$sql->prepareQuery ('carte', $_REQUEST, 'insert');
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=mysql_insert_id();
		}	
			
		// attivazione da form
		$_REQUEST['id_carta']=$lastid;
		if ($_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $zealandCredit->attivaCarta();
	
		$goPage->alertgo(UPDATE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');
			
			
	break; // end upd
	
	case "del": // delete
		echo '<h1>'.DELETE.'</h1>';

		if (!empty($_REQUEST['id_carta'])) {
			
			$query="delete from carte where id_carta='".$_REQUEST['id_carta']."'";
			$res=$db->query($query);

			$goPage->alertgo(DELETE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');
			
		} else $goPage->alertback(NO_RECORD, false);

	break; // end del
	
	case "attiva": // attiva
		echo '<h1>'.ABILITA_CARTA.'</h1>';
		if (!empty($_REQUEST['id_carta'])) {
			$zealandCredit->attivaCarta();
		} else $goPage->alertback(NO_RECORD, false);
	break; // and attiva
	
	case "form_mov":
		$record=array();
		if (!empty($_REQUEST['id_movimento'])) {
			echo '<h1>'.VIEW_MOD.'</h1>';
			# dati conto
			$qry="select * from movimenti_carte where id_movimento=".$_REQUEST['id_movimento'];
			$res=$db->query($qry);
			$record = $res->fetchRow();
			$record['importo_db']['importo']=$record['importo'];
			$record['importo_db']['debito_credito']=$record['debito_credito'];
			
		} else {
			if (empty($_REQUEST['id_carta'])) $goPage->alertback(NO_RECORD, false);
			else echo '<h1>'.INSERT_NEW.'</h1>';
			$record['data_richiesta']=date("Y-m-d H:i:s");
		}

		$form = new HTML_QuickForm('FormUpdate', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
		$form->addElement('hidden', 'page', $_REQUEST['page']);
		$form->addElement('hidden', 'act', 'upd_mov');
		$form->addElement('hidden', 'id_movimento', $_REQUEST['id_movimento']);
		$form->addElement('hidden', 'id_carta', $_REQUEST['id_carta']);
		$form->addElement('hidden', 'data_richiesta', $record['data_richiesta']);
		$form->addElement('hidden', 'data_attivazione', $record['data_attivazione']);
		$form->addElement('hidden', 'old_stato', $record['stato']);

		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_ATTIVAZIONE.'</div><div class="fieldForm">'.(!empty($record['data_attivazione'])?$func->formatData($record['data_attivazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		
		$form->addElement('select', 'stato', LABEL_STATUS, $arrayStatiOperazioni, ' class="textbox" ');

	//	$form->addElement('select', 'debito_credito', LABEL_DEBITO_CREDITO, $arrayDB, ' class="textbox" ');
	//	$form->addElement('text', 'importo', LABEL_IMPORTO,' class="textbox" size="25"');
		
		$form->addElement('select', 'id_valuta', LABEL_VALUTA, $optionValute, ' class="textbox" ');
	
		$importo_db['debito_credito'] = &HTML_QuickForm::createElement('select', 'debito_credito', 'debito_credito', $arrayDB, ' class="textbox"');
		$importo_db['importo'] = &HTML_QuickForm::createElement('text', 'importo', 'importo',' class="textbox" size="25"');
		$form->addGroup($importo_db, 'importo_db', LABEL_IMPORTO, '&nbsp;');	
		$form->addGroupRule('importo_db', array(
			'importo' => array(
				array(LABEL_IMPORTO, 'required', FALSE,'client'),
				array(LABEL_IMPORTO.' '.NOT_NUM, 'numeric', FALSE,'client')
			),
			'debito_credito' => array(
				array(ucwords($_SESSION['dizionario']['prefisso']).' '.$_SESSION['dizionario']['richiesto'], 'required', FALSE,'client')
			)
		));	
			
		$form->addElement('text', 'causale', LABEL_CAUSALE,' class="textbox" size="60"');
		
		$form->addRule('causale', LABEL_CAUSALE, 'required', FALSE,'client');
		$form->addRule('stato', LABEL_STATUS, 'required', FALSE,'client');
		
		$form->setDefaults($record); // da tabella
	
		$form->setRequiredNote(REQUIRED_FIELDS);
		$form->setJsWarnings(ERRORE_JS,'');
		
		// CREO I PULSANTI
		$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', SAVE,'class="button"');
		$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
		$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
		
		$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
	
		// mostro il form
		$form->display();
	
	break; // end form_mov
	
	case "upd_mov": // insert / update
		echo '<h1>'.UPDATE.'</h1>';
		if (empty($_REQUEST['data_attivazione']) && $_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $_REQUEST['data_attivazione']=date("Y-m-d H:i:s");
		
		if (!empty($_REQUEST['data_richiesta'])) $_REQUEST['data_richiesta']= $_REQUEST['data_richiesta']['Y'].'-'.$_REQUEST['data_richiesta']['m'].'-'.$_REQUEST['data_richiesta']['d'].' ';

		$_REQUEST['importo']=$_REQUEST['importo_db']['importo'];
		$_REQUEST['debito_credito']=$_REQUEST['importo_db']['debito_credito'];

		if (!empty($_REQUEST['id_movimento']) && !empty($_REQUEST['id_carta'])) {
			# update
			$query=$sql->prepareQuery ('movimenti_carte', $_REQUEST, 'update', "id_movimento='".$_REQUEST['id_movimento']."'");
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=$_REQUEST['id_movimento'];
		} else {
			# insert
			$query=$sql->prepareQuery ('movimenti_carte', $_REQUEST, 'insert');
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=mysql_insert_id();
		}	
			
		// attivazione da form
		$_REQUEST['id_movimento']=$lastid;
		if ($_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $zealandCredit->attivaMovimentoCarta();
	
		$goPage->alertgo(UPDATE_OK, 'index.php?page='.$_REQUEST['page'].'&act=movimenti&id_carta='.$_REQUEST['id_carta']);
			
			
	break; // end upd_mov
	
	case "attiva_mov": // attiva
		echo '<h1>'.ABILITA_CARTA.'</h1>';
		if (!empty($_REQUEST['id_carta'])) {
			$zealandCredit->attivaCarta();
		} else $goPage->alertback(NO_RECORD, false);
		
	break; // and attiva
	
	case "del_mov": // delete
		echo '<h1>'.DELETE.'</h1>';

		if (!empty($_REQUEST['id_movimento']) && !empty($_REQUEST['id_carta'])) {
			
			$query="delete from movimenti_carte where id_movimento='".$_REQUEST['id_movimento']."'";
			$res=$db->query($query);

			$goPage->alertgo(DELETE_OK, 'index.php?page='.$_REQUEST['page'].'&act=movimenti&id_carta='.$_REQUEST['id_carta']);
			
		} else $goPage->alertback(NO_RECORD, false);
	break; // end del_mov
	
	case "attiva_mov": // attiva_mov
		echo '<h1>'.ABILITA_MOVIMENTO_CARTA.'</h1>';
		if (!empty($_REQUEST['id_movimento'])) {
			$zealandCredit->attivaMovimentoCarta();
		} else $goPage->alertback(NO_RECORD, false);
	break; // end attiva_mov
	
	
	case "movimenti": // movimenti della carta selezionata
	
		if (!empty($_REQUEST['id_carta'])) {

			echo '<h1>'.LIST_TABLE.' '.MOVIMENTI_CARD;
			echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form_mov&amp;id_carta='.$_REQUEST['id_carta'].'" title="'.INSERT_NEW.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.INSERT_NEW.'" /></a>';
			echo '</h1>';
			
			$qry="select mov.*, car.id_tipo_carta, car.id_cliente, car.id_conto, car.id_valuta, car.numero_carta, car.debito_credito  as d_b_carta from carte as car, movimenti_carte as mov where mov.id_carta=car.id_carta and car.id_carta=".$_REQUEST['id_carta'];
			
			
			// eventuali filtri
			
			
			$qry .=" order by data_richiesta desc";
			
			$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=movimenti&amp;id_carta='.$_REQUEST['id_carta'];
			
			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
	
					
				?>
				<form name="fFast_upd" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" onsubmit="return controllaForm('fFast_upd');">
				<input type="hidden" name="act" value="fast_upd" />
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
				<input type="hidden" name="id_carta" value="<?php echo $_REQUEST['id_carta']; ?>" />
				<input type="hidden" name="filter" value="0" />
				<table class="tblAdmin">
				<tr>
					<th style="width:30px;"></th>
					<th style="width:30px;"></th>
					<th><?php echo LABEL_DATA; ?></th>
					<th><?php echo LABEL_VALUTA; ?></th>
					<th><?php echo LABEL_IMPORTO; ?></th>
					<th><?php echo LABEL_CAUSALE; ?></th>
					<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
				</tr>
				
				<!-- filtri -->

			<?php    
				$class="tdRow1";
				while ($record =& $res->fetchRow()) {
				//	$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_carta='.$record['id_carta'].'&amp;id_movimento='.$record['id_movimento'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATTIVA_MOVIMENTO.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATTIVA_MOVIMENTO.'" title="'.PENDING.'-'.ATTIVA_MOVIMENTO.'" /></a>';
				
				//	print_r($record);
					echo '
					<tr>
						<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form_mov&amp;id_carta='.$record['id_carta'].'&amp;id_movimento='.$record['id_movimento'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
						<td class="'.$class.'">';
					
					echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del_mov&amp;id_movimento=".$record['id_movimento']."&amp;id_carta=".$record['id_carta']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
					
					echo "</td>";
					$data=(($record['data_attivazione']!='0000-00-00 00:00:00')?$record['data_attivazione']:$record['data_richiesta']);
					echo "\n".'<td class="'.$class.'">'.$func->formatData($data, "d-m-Y H:i").'</td>
						<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
						<td class="'.$class.'">'.$arrayDB[$record['debito_credito']].' '.number_format($record['importo'],2,',','.').'</td>
						<td class="'.$class.'">'.$record['causale'].'</td>
						<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
					</tr>';
					
					$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
					
				} // end while
			?>
				</table>
				</form>
			
			<?php	
					echo $view_links;
		
				} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
		
		} else $goPage->alertback(NO_RECORD, false);
	
	break; // end movimenti (elenco)
	
	default: // list
		
		// elenco tutti i conti filtrati per cliente / stato / tipo / deposito iniziale / attuale
		// attivabile da elenco con invio email all'utente
		echo '<h1>'.LIST_TABLE;
		echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form" title="'.INSERT_NEW.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.INSERT_NEW.'" /></a>';
		echo '</h1>';
		
		$qry="select car.*, cli.*, con.numero_conto from carte as car, conti as con, clienti as cli where car.attivo=1 and con.id_cliente=cli.id_cliente and car.id_cliente=cli.id_cliente and car.id_conto=con.id_conto";
		
		# filtri
		$qry .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?" and (cli.cognome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.nome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.ragione_sociale like '%".$_REQUEST['filtra_nome_cognome_soc']."%')":'');

		$qry .=(!empty($_REQUEST['filtra_tipo_carta']) && $_REQUEST['filtra_tipo_carta']!='*'?" and car.id_tipo_carta = ".$_REQUEST['filtra_tipo_carta']."":'');

		$qry .=(!empty($_REQUEST['filtra_stato_carta']) && $_REQUEST['filtra_stato_carta']!='*'?" and car.stato '= ".$_REQUEST['filtra_stato_carta']."'":'');


		# order
		$qry .="  order by cli.cognome, cli.nome, cli.ragione_sociale, car.numero_carta";

		$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list';
		$link_extra_param .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?"&amp;filtra_nome_cognome_soc=".$_REQUEST['filtra_nome_cognome_soc']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_carta']) && $_REQUEST['filtra_tipo_carta']!='*'?"&amp;filtra_tipo_carta=".$_REQUEST['filtra_tipo_carta']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_stato_carta']) && $_REQUEST['filtra_stato_carta']!='*'?"&amp;filtra_stato_carta=".$_REQUEST['filtra_stato_carta']:'');
		

		/*$res=$db->query($qry);
		//	echo $qry;
		// ... se si verifica un errore, lo scriviamo
		if( DB::isError($res) ) { 
			print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
		}
			
		$rows = $res->numRows();*/
		$tot_record=0;
		$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
		
			
		if ($tot_record>0) {
		
			echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
			
			echo $view_links;
		?><br /><br />
		<form name="fFast_upd" method="post" action="index.php" onsubmit="return controllaForm('fFast_upd');">
		<input type="hidden" name="act" value="fast_upd" />
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
		<input type="hidden" name="filter" value="0" />
		<table class="tblAdmin">
		<tr>
			<th style="width:30px;"></th>
			<th style="width:30px;"></th>
			<th style="width:30px;"></th>
			<th><?php echo LABEL_NOME_SOCIETA; ?></th>
 			<th><?php echo LABEL_DEBITO_CREDITO; ?></th>
			<th><?php echo LABEL_TIPO_CARTA; ?></th>
			<th><?php echo LABEL_NUMERO_CARTA; ?></th>
			<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
		</tr>
        
        <!-- filtri -->
        <tr>
            <td colspan="3" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
            <td><?php
            // cognome / nome / ragione_sociale
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_nome_cognome_soc']?>" name="filtra_nome_cognome_soc" class="textbox" size="15" /></td>
            <td></td>
            <td><?php
            // id_tipo_carta            
            ?><select name="filtra_tipo_carta" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionTipiCarte as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo_carta']) && $_REQUEST['filtra_tipo_carta']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
            <td></td>
            <td><?php
            // stato            
            ?><select name="filtra_stato_carta" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($arrayStatiOperazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_stato_carta']) && $_REQUEST['filtra_stato_carta']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
            
        </tr>
	<?php    
		$class="tdRow1";
		while ($record =& $res->fetchRow()) {
			$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_carta='.$record['id_carta'].'" onclick="if (confirm(confirm_attiva_carta)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATTIVA_CARTA.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATTIVA_CARTA.'" title="'.PENDING.'-'.ATTIVA_CARTA.'" /></a>';
		
		//	print_r($record);
			echo '
			<tr>
				<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_carta='.$record['id_carta'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
				<td class="'.$class.'">';
			
			echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_carta=".$record['id_carta']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			
			echo "</td>";
			echo '	<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=movimenti&amp;id_carta='.$record['id_carta'].'" title="'.MOVIMENTI_CARD.'"><img src="'.$path_web.'img/icone/note.png" width="24" height="24" style="border: 0px;" alt="'.MOVIMENTI_CARD.'" /></a></td>';
			echo "\n".'<td class="'.$class.'">'.$record['ragione_sociale'].' '.$record['cognome'].' '.$record['nome'].'</td>
				<td class="'.$class.'">'.$arrayTipoCard[$record['debito_credito']].'</td>
				<td class="'.$class.'">'.$optionTipiCarte[$record['id_tipo_carta']].'</td>
				<td class="'.$class.'">'.$record['numero_carta'].'</td>
				<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
			</tr>';
			
			$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
			
		} // end while
	?>
		</table>
		</form>
	
	<?php	
			echo $view_links;

		} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
		
}
	
?>
