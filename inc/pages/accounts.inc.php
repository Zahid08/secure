<?php
# accounts.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

$optionTipiConti = $zealandCredit->getTypes('tipi_conti', $_SESSION['lingua'], true, true);
$optionValute = $zealandCredit->getValute($stato_opzioni, true);
$optionClienti = $zealandCredit->getClienti($stato_opzioni);

echo '<div class="titoloAdmin">'.LINK_MENU_SN_ACCOUNTS.'</div>';

switch ($_REQUEST['act']) {

	case "form": // form new / mod
		$record=array();
		if (!empty($_REQUEST['id_conto'])) {
			echo '<h1>'.VIEW_MOD.'</h1>';
			# dati conto
			$qry="select * from conti where attivo = 1 and id_conto=".$_REQUEST['id_conto'];
			$res=$db->query($qry);
			$record = $res->fetchRow();
			$tipoContoTestuale = $zealandCredit->getContoNome($record['id_tipo_conto'],'en');
			$valuta = $zealandCredit->getValuta($record['id_valuta']);
		
		} else {
			echo '<h1>'.INSERT_NEW.'</h1>';
			$record['data_creazione'] = date("Y-m-d H:i:s");
			$record['attivo']=1;
			
		}
		
		
		$form = new HTML_QuickForm('FormUpdate', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
		$form->addElement('hidden', 'page', $_REQUEST['page']);
		$form->addElement('hidden', 'act', 'upd');
		$form->addElement('hidden', 'id_conto', $_REQUEST['id_conto']);
		
		$form->addElement('hidden', 'minimo_deposito', !empty($record['numero_conto'])?'0':'', 'id="minimo_deposito"');
		$form->addElement('hidden', 'data_richiesta', $record['data_richiesta']);
		$form->addElement('hidden', 'data_attivazione', $record['data_attivazione']);
		$form->addElement('hidden', 'data_creazione', $record['data_creazione']);
		$form->addElement('hidden', 'attivo', $record['attivo']);
		$form->addElement('hidden', 'old_stato', $record['stato']);
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_ATTIVAZIONE.'</div><div class="fieldForm">'.(!empty($record['data_attivazione'])?$func->formatData($record['data_attivazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_CREAZIONE.'</div><div class="fieldForm">'.$func->formatData($record['data_creazione'], 'd-m-Y H:i').'</div><div class="clearBoth"></div>');

		$form->addElement('select', 'stato', LABEL_STATO_CONTO, $arrayStatiOperazioni, ' class="textbox" ');


		//$form->addElement('hidden', 'attivo', $record['attivo']);
		//$form->addElement('html', '<div class="labelForm">'.ATTIVO_CLIENTE.'</div><div class="fieldForm">'.($record['attivo']==1?LABEL_SI:LABEL_NO).'</div><div class="clearBoth"></div>');
		
		$form->addElement('select', 'id_cliente', LABEL_CLIENTE, $optionClienti, ' class="textbox" ');
		
		$form->addElement('html', '<div class="labelForm"></div><div class="fieldForm" id=tipoContotesto">'.$tipoContoTestuale.'</div><div class="clearBoth"></div>');
		$form->addElement('hidden', 'id_tipo_conto', '11', 'id="id_tipo_conto"');
		
		//$form->addElement('select', 'id_tipo_conto', LABEL_TIPO_CONTO, $optionTipiConti, ' id="id_tipo_conto" class="textbox" onchange="controllaMinimo()"');

		$form->addElement('text', 'intestatario', LABEL_INTESTATARIO, ' class="textbox" size="60"');
		$form->addRule('intestatario', LABEL_INTESTATARIO, 'required', FALSE,'client');

		if (!empty($record['numero_conto'])){
			$form->addElement('html', '<div class="labelForm">'.LABEL_NUMERO_CONTO.'</div><div class="fieldForm">'.$record['numero_conto'].'</div><div class="clearBoth"></div>');
			$form->addElement('hidden', 'numero_conto', $record['numero_conto'], 'id="numero_conto"');
		}else $form->addElement('html', '<div class="labelForm">'.LABEL_NUMERO_CONTO.'</div><div class="fieldForm">'.GENERAZIONE_AUTOMATICA.'</div><div class="clearBoth"></div>');
		
		if (!empty($record['id_valuta'])){
			$form->addElement('html', '<div class="labelForm">Currency:</div><div class="fieldForm">'.$valuta.'</div><div class="clearBoth"></div>');
			$form->addElement('hidden', 'id_valuta', $record['id_valuta'], 'id="id_valuta"');			
		}else $form->addElement('select', 'id_valuta', LABEL_VALUTA, $optionValute, ' id="id_valuta"  class="textbox" onchange="controllaMinimo()"');


		
		$form->addElement('text', 'saldo_iniziale', LABEL_SALDO_INIZIALE, ' value="3000" class="textbox" size="25" id="saldo_iniziale" onblur="controllaMinimo()"');
		$form->addElement('text', 'saldo_attuale', LABEL_SALDO_ATTUALE, ' class="textbox" size="25"');
		
		
		$form->addElement('html', '<div class="labelForm"></div><div class="fieldForm" id="checkMinimo"></div><div class="clearBoth"></div>');
		
		$form->addElement('select', 'abilita_prelievo', LABEL_ABILITA_PRELIEVO, $optionBool, ' class="textbox"');
		$form->addElement('select', 'abilita_deposito', LABEL_ABILITA_DEPOSITO, $optionBool, ' class="textbox"');


		$form->addElement('textarea', 'descrizione', LABEL_DESCRIZIONE,' class="textbox" cols="60" rows="5"');
				
		$form->addRule('minimo_deposito', LABEL_MINIMO_DEPOSITO, 'required', FALSE,'client');
		$form->addRule('stato', LABEL_STATO_CONTO, 'required', FALSE,'client');
		$form->addRule('id_cliente', LABEL_CLIENTE, 'required', FALSE,'client');
		$form->addRule('id_tipo_conto', LABEL_TIPO_CONTO, 'required', FALSE,'client');
		$form->addRule('id_valuta', LABEL_VALUTA, 'required', FALSE,'client');
		$form->addRule('saldo_iniziale', LABEL_SALDO_INIZIALE, 'required', FALSE,'client');
		$form->addRule('saldo_iniziale', LABEL_SALDO_INIZIALE.' '.NOT_NUM, 'numeric', FALSE,'client');
		$form->addRule('saldo_attuale', LABEL_SALDO_ATTUALE.' '.NOT_NUM, 'numeric', FALSE,'client');
		
		$form->addRule('abilita_prelievo', LABEL_ABILITA_PRELIEVO, 'required', FALSE,'client');
		$form->addRule('abilita_deposito', LABEL_ABILITA_DEPOSITO, 'required', FALSE,'client');


		$form->setDefaults($record); // da tabella
	
		
		$form->setRequiredNote(REQUIRED_FIELDS);
		$form->setJsWarnings(ERRORE_JS,'');
		
		// CREO I PULSANTI
		$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', SAVE,'class="button"');
		$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
		$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
		
		$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
	
		// mostro il form
		$form->display();
	
	break;
	
	case "upd": // insert / update
		echo '<h1>'.UPDATE.'</h1>';
		//echo '<!--'.print_r($_REQUEST).' -->';
		//if (empty($_REQUEST['saldo_attuale'])) $_REQUEST['saldo_attuale']=$_REQUEST['saldo_iniziale'];
		
		if (empty($_REQUEST['data_attivazione']) && $_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $_REQUEST['data_attivazione']=date("Y-m-d H:i:s");
		
		if (!empty($_REQUEST['id_conto'])) {
			# update
			$query=$sql->prepareQuery ('conti', $_REQUEST, 'update', "id_conto='".$_REQUEST['id_conto']."'");
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=$_REQUEST['id_conto'];
			$azione = 'mod'; //serve alla completa conto
		} else {
			# insert
			$query=$sql->prepareQuery ('conti', $_REQUEST, 'insert');
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=mysql_insert_id();
			
			$azione = 'ins';
			
		}		
			
		// numero conto
		if (empty($_REQUEST['numero_conto']))
		{
			// MOD. ALE BALDUCCI 28/04/2012 NON METTE IL NUMERO CONTO CON L'ANNO ????!!!! $_REQUEST['numero_conto']=$zealandCredit->generaNumeroConto($lastid, $_REQUEST['data_creazione']);
			 $_REQUEST['numero_conto']=$zealandCredit->generaNumeroConto($lastid, date("Y-m-d H:i:s"));
			 
		}
		$query="update conti set numero_conto = '".$_REQUEST['numero_conto']."' where id_conto='".$lastid."'";
		//echo '<br />'.$query;
		$res=$db->query($query);
	
	

		// registrazione su interessi correnti per procedura batch
	/*	$dataAttiva = $zealandCredit->getAttivazioneConto($lastid);
		$intStart = array();		
		
	
	ale balducci 30/04/2012 - otttimizzo il processo e faccio in modo che sia ok zio prete
	if (!empty($_REQUEST['id_conto'])) {
				
				$intStart['id_conto_investimento']=$lastid;
				$intStart['tipo']='C';
				$intStart['data_attivazione'] = $dataAttiva;
				$intStart['tasso_applicato'] = $zealandCredit->getTassoConto($_REQUEST['id_tipo_conto']);  
					
				$query=$sql->prepareQuery ('interessi_correnti', $intStart, 'update', "id_conto_investimento='".$_REQUEST['id_conto']."'");
				$res=$db->query($query);
		}else{
				
				 $intStart['id_conto_investimento']=$lastid;
				$intStart['tipo']='C';
				$intStart['data_attivazione'] = $dataAttiva;
				$intStart['tasso_applicato'] = $zealandCredit->getTassoConto($_REQUEST['id_tipo_conto']);  

				$query=$sql->prepareQuery ('interessi_correnti', $intStart, 'insert');
				//die($query);				
				$res=$db->query($query); 
				
		}*/
		
		// interessi correnti e numero conto (se non presente)
		$zealandCredit->completa_conto($lastid,$azione);
		//

		// attivazione da form
		$_REQUEST['id_conto']=$lastid;
		if ($_REQUEST['stato']=='A' && $_REQUEST['old_stato']!='A') $zealandCredit->attivaConto();		
		
		
		$goPage->alertgo(UPDATE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');
			
			
	break;
	
	case "del": // delete
		echo '<h1>'.DELETE.'</h1>';
		// non lo cancello per non perdere le transazioni
		// setto attivo a 0
		// il cliente non lo vede pi�
		// admin lo vede ma non pu� modificarlo o non lo vede pi� ?
		if (!empty($_REQUEST['id_conto'])) {
			
			$query="update conti set attivo=0 where id_conto='".$_REQUEST['id_conto']."'";
			$res=$db->query($query);
			
			$query="update interessi_correnti set attivo=0 where id_conto_investimento='".$_REQUEST['id_investimento']."' and tipo = 'C'";
			$res=$db->query($query);
			

			$goPage->alertgo(DELETE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');
			
		} else $goPage->alertback(NO_RECORD, false);

	break;
	
	case "attiva": // delete
		echo '<h1>'.ABILITA_CONTO.'</h1>';
 
		if (!empty($_REQUEST['id_conto'])) {
			$zealandCredit->completa_conto($_REQUEST['id_conto'],'ins'); //ale balducci 30/04/2012 senno la procedura sembrava incompleta (ins si riferisce agli interessi correnti...) 
			$zealandCredit->attivaConto();
		} else $goPage->alertback(NO_RECORD, false);

	break;
	
	default: // list
		
		// elenco tutti i conti filtrati per cliente / stato / tipo / deposito iniziale / attuale
		// attivabile da elenco con invio email all'utente
		echo '<h1>'.LIST_TABLE;
		echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form" title="'.INSERT_NEW.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.INSERT_NEW.'" /></a>';
		echo '</h1>';
		
		$qry="select * from conti as con, clienti as cli where con.attivo=1 and con.id_cliente=cli.id_cliente ";
		
		# filtri
		$qry .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?" and (cli.cognome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.nome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.ragione_sociale like '%".$_REQUEST['filtra_nome_cognome_soc']."%')":'');

		$qry .=(!empty($_REQUEST['filtra_tipo_conto']) && $_REQUEST['filtra_tipo_conto']!='*'?" and con.id_tipo_conto = ".$_REQUEST['filtra_tipo_conto']."":'');

		$qry .=(!empty($_REQUEST['filtra_stato_conto']) && $_REQUEST['filtra_stato_conto']!='*'?" and con.stato '= ".$_REQUEST['filtra_stato_conto']."'":'');

		$qry .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?" and con.id_valuta = ".$_REQUEST['filtra_valuta']."":'');


		$qry .=(!empty($_REQUEST['filtra_intestatario']) && $_REQUEST['filtra_intestatario']!='*'?" and con.intestatario like '%".$_REQUEST['filtra_intestatario']."%'":'');

		$qry .=(!empty($_REQUEST['filtra_numero_conto']) && $_REQUEST['filtra_numero_conto']!='*'?" and con.numero_conto = '".$_REQUEST['filtra_numero_conto']."'":'');

		# order
		$orderby=(!empty($_REQUEST['orderby'])?$_REQUEST['orderby']:'cli.cognome, cli.nome, cli.ragione_sociale, con.numero_conto');
		$qry .="  order by ".$orderby;

		$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list';
		$link_extra_param .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?"&amp;filtra_nome_cognome_soc=".$_REQUEST['filtra_nome_cognome_soc']:'');

		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_conto']) && $_REQUEST['filtra_tipo_conto']!='*'?"&amp;filtra_tipo_conto=".$_REQUEST['filtra_tipo_conto']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_stato_conto']) && $_REQUEST['filtra_stato_conto']!='*'?"&amp;filtra_stato_conto=".$_REQUEST['filtra_stato_conto']:'');
		
		$link_extra_param .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?"&amp;filtra_valuta=".$_REQUEST['filtra_valuta']:'');

		$link_extra_param .=(!empty($_REQUEST['filtra_intestatario']) && $_REQUEST['filtra_intestatario']!='*'?"&amp;filtra_intestatario=".$_REQUEST['filtra_intestatario']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_numero_conto']) && $_REQUEST['filtra_numero_conto']!='*'?"&amp;filtra_numero_conto=".$_REQUEST['filtra_numero_conto']:'');


		/*$res=$db->query($qry);
		//	echo $qry;
		// ... se si verifica un errore, lo scriviamo
		if( DB::isError($res) ) { 
			print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
		}
			
		$rows = $res->numRows();*/
		$tot_record=0;
		$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
		
			
		if ($tot_record>0) {
		
			echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
			
			echo $view_links;
		?><br /><br />
		<form name="fFast_upd" method="post" action="index.php" onsubmit="return controllaForm('fFast_upd');">
		<input type="hidden" name="act" value="fast_upd" />
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
		<input type="hidden" name="filter" value="0" />
		<table class="tblAdmin">
		<tr>
			<th style="width:30px;"></th>
			<th style="width:30px;"></th>
			<th><?php echo LABEL_NOME_SOCIETA; ?></th>
			<th><a href="index.php?<?php echo $link_extra_param; ?>&amp;orderby=con.intestatario" title="order by <?php echo LABEL_INTESTATARIO_CONTO; ?>" class="linkBold"><?php echo LABEL_INTESTATARIO; ?></a></th>
			<th><?php echo LABEL_TIPO_CONTO; ?></th>
			<th><a href="index.php?<?php echo $link_extra_param; ?>&amp;orderby=con.numero_conto" title="order by <?php echo LABEL_NUMERO_CONTO; ?>" class="linkBold"><?php echo LABEL_NUMERO_CONTO; ?></a></th>
			<th><?php echo LABEL_VALUTA; ?></th>
			<th><?php echo LABEL_SALDO_INIZIALE; ?></th>
			<th><?php echo LABEL_SALDO_ATTUALE; ?></th>
			<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
		</tr>
        
        <!-- filtri -->
        <tr>
            <td colspan="2" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
            <td><?php
            // cognome / nome / ragione_sociale
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_nome_cognome_soc']?>" name="filtra_nome_cognome_soc" class="textbox" size="15" /></td>
			<td><?php
            // intestatario
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_intestatario']?>" name="filtra_intestatario" class="textbox" size="15" /></td>
            <td><?php
            // id_tipo_conto            
            ?><select name="filtra_tipo_conto" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionTipiConti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo_conto']) && $_REQUEST['filtra_tipo_conto']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
			<td><?php
            // numero_conto
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_numero_conto']?>" name="filtra_numero_conto" class="textbox" size="15" /></td>
            <td><?php
            // id_valuta            
            ?><select name="filtra_valuta" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionValute as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
			<td></td>
			<td></td>
            <td><?php
            // stato            
           /* <select name="filtra_stato_conto" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($arrayStatiOperazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_stato_conto']) && $_REQUEST['filtra_stato_conto']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select>*/ ?></td>
            
        </tr>
	<?php    
		$class="tdRow1";
		while ($record =& $res->fetchRow()) {
			$arrayImgStati['P']='<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_conto='.$record['id_conto'].'" onclick="if (confirm(confirm_attiva_conto)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATTIVA_CONTO.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATTIVA_CONTO.'" title="'.PENDING.'-'.ATTIVA_CONTO.'" /></a>';
		//	print_r($record);
			echo '
			<tr>
				<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_conto='.$record['id_conto'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
				<td class="'.$class.'">';
			
			echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_conto=".$record['id_conto']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			
			echo "</td>";
			echo "\n".'<td class="'.$class.'">'.$record['ragione_sociale'].' '.$record['cognome'].' '.$record['nome'].'</td>
				<td class="'.$class.'">'.$record['intestatario'].'</td>
				<td class="'.$class.'">'.$optionTipiConti[$record['id_tipo_conto']].'</td>
				<td class="'.$class.'">'.$record['numero_conto'].'</td>
				<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
				<td class="'.$class.'">'.number_format($record['saldo_iniziale'],2,',','.').'</td>
				<td class="'.$class.'">'.number_format($record['saldo_attuale'],2,',','.').'</td>
				<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
			</tr>';
			
			$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
			
		} // end while
	?>
		</table>
		</form>
	
	<?php	
			echo $view_links;

		} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
		
}
	
?>
