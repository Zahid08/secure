<?php
# login.inc.php
function PasswordCasuale($lunghezza=6){
	$caratteri_disponibili ="ABCDEFGHJKLMNPQRSTUVWXYZ23456789";
	$password = "";
		for($i = 0; $i<$lunghezza; $i++){
			$password = $password.substr($caratteri_disponibili,rand(0,strlen($caratteri_disponibili)-1),1);
		}
	return $password;
}
	
$_SESSION['code'] = PasswordCasuale();


$form = new HTML_QuickForm('FormLogin', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);

$form->addElement('text', 'username', USERNAME, ' class="textbox"  size="25"');
$form->addElement('password', 'password', PASSWORD, ' class="textbox"  size="25"');
$form->addElement('hidden', 'da_login', '', 'value="1"');

$form->addRule('username', USERNAME, 'required', FALSE,'client');
$form->addRule('password', PASSWORD, 'required', FALSE,'client');


$form->addElement('html', '<div class="labelForm">'.LABEL_CODICE_SICUREZZA.'</div><div class="fieldForm"><img src="/captcha/captcha.php?ck='.md5(base64_encode($_SESSION['code'])).'&sessid='.urlencode(strrev(base64_encode($_SESSION['code']))).'" /><input name="codice" type="hidden" id="codice" value="'.md5($_SESSION['code']).'" /></div><div class="clearBoth"></div>');

$form->addElement('text', 'captcha', '<span class="red">*</span>'.LABEL_RICOPIA_CODICE_SICUREZZA, ' class="textbox"  size="25"');


$form->setRequiredNote('');
//$form->setRequiredNote('<span class="red">'.REQUIRED_FIELDS.'</span>');
$form->setJsWarnings(ERRORE_JS,'');

// CREO I PULSANTI
$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', LABEL_ENTRA,'class="button"');
$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', LABEL_ANNULLA,'class="button"');

$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');

// mostro il form
$form->display();

?>
<br /><div style="text-align:right">Your Ip Address <?php echo $_SERVER['REMOTE_ADDR']; ?></div>