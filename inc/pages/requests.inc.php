<?php
# reports.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

 

echo '<div class="titoloAdmin">'.LINK_MENU_SN_REQUESTS.'</div>';
echo '<div style="text-align:right;">'.VIEW_REQUESTS.': ';
echo '<a href="'.$path_web.'?page=requests&amp;request_type=profiles" title="'.LINK_MENU_SN_PROFILES.'">'.LINK_MENU_SN_PROFILES.'</a> | ';
echo '<a href="'.$path_web.'?page=requests&amp;request_type=accounts" title="'.LINK_MENU_SN_ACCOUNTS.'">'.LINK_MENU_SN_ACCOUNTS.'</a> | ';
echo '<a href="'.$path_web.'?page=requests&amp;request_type=cards" title="'.LINK_MENU_SN_CARDS.'">'.LINK_MENU_SN_CARDS.'</a> | ';
echo '<a href="'.$path_web.'?page=requests&amp;request_type=transactions" title="'.LINK_MENU_SN_TRANSACTIONS.'">'.LINK_MENU_SN_TRANSACTIONS.'</a> | ';
echo '<a href="'.$path_web.'?page=requests&amp;request_type=portfolio" title="'.LINK_MENU_SN_PORTFOLIO.'">'.LINK_MENU_SN_PORTFOLIO.'</a>';

echo '</div>';

// elenco richieste pendenti
switch ($_REQUEST['request_type']) {
	
	case "accounts": // conti
	# 2 - conti
		// elenco tutti i conti filtrati per cliente / stato / tipo / deposito iniziale / attuale
		// attivabile da elenco con invio email all'utente
		echo '<h1>'.LINK_MENU_SN_ACCOUNTS.'</h1>';
		$optionTipiConti = $zealandCredit->getTypes('tipi_conti', $_SESSION['lingua'], $stato_opzioni, false);
		$optionValute = $zealandCredit->getValute($stato_opzioni, false);
		$optionClienti = $zealandCredit->getClienti($stato_opzioni);
		$qry="select * from conti as con, clienti as cli where con.attivo=1 and con.stato='P' and con.id_cliente=cli.id_cliente ";
		
		# filtri
		$qry .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?" and (cli.cognome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.nome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.ragione_sociale like '%".$_REQUEST['filtra_nome_cognome_soc']."%')":'');

		$qry .=(!empty($_REQUEST['filtra_tipo_conto']) && $_REQUEST['filtra_tipo_conto']!='*'?" and con.id_tipo_conto = ".$_REQUEST['filtra_tipo_conto']."":'');

		$qry .=(!empty($_REQUEST['filtra_stato_conto']) && $_REQUEST['filtra_stato_conto']!='*'?" and con.stato '= ".$_REQUEST['filtra_stato_conto']."'":'');

		$qry .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?" and con.id_valuta = ".$_REQUEST['filtra_valuta']."":'');

		# order
		$qry .=" order by con.data_richiesta desc, cli.cognome asc, cli.nome asc, cli.ragione_sociale asc, con.numero_conto asc";

		$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list&amp;request_type='.$_REQUEST['request_type'];
		$link_extra_param .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?"&amp;filtra_nome_cognome_soc=".$_REQUEST['filtra_nome_cognome_soc']:'');

		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_conto']) && $_REQUEST['filtra_tipo_conto']!='*'?"&amp;filtra_tipo_conto=".$_REQUEST['filtra_tipo_conto']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_stato_conto']) && $_REQUEST['filtra_stato_conto']!='*'?"&amp;filtra_stato_conto=".$_REQUEST['filtra_stato_conto']:'');
		
		$link_extra_param .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?"&amp;filtra_valuta=".$_REQUEST['filtra_valuta']:'');
			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
			
		?>
		<form name="fFast_upd" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" onsubmit="return controllaForm('fFast_upd');">
		<input type="hidden" name="act" value="fast_upd" />
		<input type="hidden" name="request_type" value="<?php echo $_REQUEST['request_type']; ?>" />
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
		<input type="hidden" name="filter" value="0" />
		<table class="tblAdmin">
		<tr>
			<th style="width:30px;"></th>
			<th style="width:30px;"></th>
			<th><?php echo LABEL_NOME_SOCIETA; ?></th>
			<th><?php echo LABEL_INTESTATARIO; ?></th>
			<th><?php echo LABEL_TIPO_CONTO; ?></th>
			<th><?php echo LABEL_VALUTA; ?></th>
			<th><?php echo LABEL_SALDO_INIZIALE; ?></th>
			<th><?php echo LABEL_DATA_RICHIESTA; ?></th>
			<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
		</tr>
        
        <!-- filtri -->
        <tr>
            <td colspan="2" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
            <td><?php
            // cognome / nome / ragione_sociale
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_nome_cognome_soc']?>" name="filtra_nome_cognome_soc" class="textbox" size="15" /></td>
			<td></td>
            <td><?php
            // id_tipo_conto            
            ?><select name="filtra_tipo_conto" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionTipiConti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo_conto']) && $_REQUEST['filtra_tipo_conto']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
            <td><?php
            // id_valuta            
            ?><select name="filtra_valuta" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionValute as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
			<td></td>
			<td></td>
            <td></td>
            
        </tr>
	<?php    
		$class="tdRow1";
		while ($record =& $res->fetchRow()) {
			$arrayImgStati['P']='<a href="'.$path_web.'?page=accounts&amp;act=attiva&amp;id_conto='.$record['id_conto'].'&amp;pageR='.$_REQUEST['page'].'&amp;request_type='.$_REQUEST['request_type'].'" onclick="if (confirm(confirm_attiva_conto)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATTIVA_CONTO.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATTIVA_CONTO.'" title="'.PENDING.'-'.ATTIVA_CONTO.'" /></a>';
		//	print_r($record);
			echo '
			<tr>
				<td class="'.$class.'"><a href="'.$path_web.'?page=accounts&amp;act=form&amp;id_conto='.$record['id_conto'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
				<td class="'.$class.'">';
			
			
			echo "<a href=\"".$path_web."?page=accounts&amp;act=del&amp;id_conto=".$record['id_conto']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			
			echo "</td>";
			
			echo "\n".'<td class="'.$class.'">'.$record['ragione_sociale'].' '.$record['cognome'].' '.$record['nome'].'</td>
				<td class="'.$class.'">'.$record['intestatario'].'</td>
				<td class="'.$class.'">'.$optionTipiConti[$record['id_tipo_conto']].'</td>
				<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
				<td class="'.$class.'">'.number_format($record['saldo_iniziale'],2,',','.').'</td>
				<td class="'.$class.'">'.$func->formatData($record['data_richiesta'], "d-m-Y H:i").'</td>
				<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
			</tr>';
			
			$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
			
		} // end while
	?>
		</table>
		</form>
	
	<?php	
			echo $view_links;

		} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);

	break; # 2 - conti

	case "cards": // carte
	# 3 - carte
		$optionTipiCarte = $zealandCredit->getTypes('tipi_carte', $_SESSION['lingua'], $stato_opzioni, false);
		//$optionClienti = $zealandCredit->getClientiConti($stato_opzioni);
		//$optionConti =$zealandCredit->getContiClienti($stato_opzioni);
		$optionClienti = $zealandCredit->getClientiConti();
		$optionConti =$zealandCredit->getContiClienti();
		
		$arrayTipoCard['D']=DEBIT_CARD;
		$arrayTipoCard['C']=CREDIT_CARD;
		
		echo '<div class="titoloAdmin">'.LINK_MENU_SN_CARDS.'</div>';

		$qry="select car.*, cli.*, con.numero_conto from carte as car, conti as con, clienti as cli where car.attivo=1 and con.id_cliente=cli.id_cliente and car.id_cliente=cli.id_cliente and car.id_conto=con.id_conto and car.stato='P'";
		
		# filtri
		$qry .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?" and (cli.cognome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.nome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.ragione_sociale like '%".$_REQUEST['filtra_nome_cognome_soc']."%')":'');

		$qry .=(!empty($_REQUEST['filtra_tipo_carta']) && $_REQUEST['filtra_tipo_carta']!='*'?" and car.id_tipo_carta = ".$_REQUEST['filtra_tipo_carta']."":'');

		$qry .=(!empty($_REQUEST['filtra_stato_carta']) && $_REQUEST['filtra_stato_carta']!='*'?" and car.stato '= ".$_REQUEST['filtra_stato_carta']."'":'');


		# order
		$qry .="  order by car.data_richiesta desc, cli.cognome, cli.nome, cli.ragione_sociale, car.numero_carta";

		$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list&amp;request_type='.$_REQUEST['request_type'];
		$link_extra_param .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?"&amp;filtra_nome_cognome_soc=".$_REQUEST['filtra_nome_cognome_soc']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_carta']) && $_REQUEST['filtra_tipo_carta']!='*'?"&amp;filtra_tipo_carta=".$_REQUEST['filtra_tipo_carta']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_stato_carta']) && $_REQUEST['filtra_stato_carta']!='*'?"&amp;filtra_stato_carta=".$_REQUEST['filtra_stato_carta']:'');
		
			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
			
		?>
		<form name="fFast_upd" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" onsubmit="return controllaForm('fFast_upd');">
		<input type="hidden" name="act" value="fast_upd" />
		<input type="hidden" name="request_type" value="<?php echo $_REQUEST['request_type']; ?>" />
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
		<input type="hidden" name="filter" value="0" />
		<table class="tblAdmin">
		<tr>
			<th style="width:30px;"></th>
			<th style="width:30px;"></th>
			<th><?php echo LABEL_NOME_SOCIETA; ?></th>
 			<th><?php echo LABEL_DEBITO_CREDITO; ?></th>
			<th><?php echo LABEL_TIPO_CARTA; ?></th>
			<th><?php echo LABEL_DATA_RICHIESTA; ?></th>
            
           
			<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
		</tr>
        
        <!-- filtri -->
        <tr>
            <td colspan="2" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
            <td><?php
            // cognome / nome / ragione_sociale
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_nome_cognome_soc']?>" name="filtra_nome_cognome_soc" class="textbox" size="15" /></td>
            <td></td>
            <td><?php
            // id_tipo_carta            
            ?><select name="filtra_tipo_carta" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionTipiCarte as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo_carta']) && $_REQUEST['filtra_tipo_carta']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
            <td></td>
            <td></td>
            
        </tr>
	<?php    
		$class="tdRow1";
		while ($record =& $res->fetchRow()) {
			$arrayImgStati['P']='<a href="'.$path_web.'?page=cards&amp;act=attiva&amp;id_carta='.$record['id_carta'].'&amp;pageR='.$_REQUEST['page'].'&amp;request_type='.$_REQUEST['request_type'].'" onclick="if (confirm(confirm_attiva_carta)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATTIVA_CARTA.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATTIVA_CARTA.'" title="'.PENDING.'-'.ATTIVA_CARTA.'" /></a>';
		
		//	print_r($record);
			echo '
			<tr>
				<td class="'.$class.'"><a href="'.$path_web.'?page=cards&amp;act=form&amp;id_carta='.$record['id_carta'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
				<td class="'.$class.'">';
			
			echo "<a href=\"".$path_web."?page=cards&amp;act=del&amp;id_carta=".$record['id_carta']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			
			echo "</td>";
			echo "\n".'<td class="'.$class.'">'.$record['ragione_sociale'].' '.$record['cognome'].' '.$record['nome'].'</td>
				<td class="'.$class.'">'.$arrayTipoCard[$record['debito_credito']].'</td>
				<td class="'.$class.'">'.$optionTipiCarte[$record['id_tipo_carta']].'</td>
				<td class="'.$class.'">'.$func->formatData($record['data_richiesta'], "d-m-Y H:i").'</td>
			 
				<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
			</tr>';
			
			$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
			
		} // end while
	?>
		</table>
		</form>
	
	<?php	
			echo $view_links;

		} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);

	break; # 3 - carte

	case "transactions": // transazioni
	# 4 - transazioni
	
	# elenco transazioni pendenti
			$optionClientiConti=$zealandCredit->getClientiConti(true);
			$optionValute = $zealandCredit->getValute($stato_opzioni, false);
			$optionTransazioni = $zealandCredit->getTypes('tipi_transazioni', $_SESSION['lingua'], $stato_opzioni);
			echo '<h1>'.ALL_PENDING_REQUESTS;
			echo '</h1>';
			
			$qry="select * from transazioni where stato='P'";
			
			# filtri
	
			$qry .=(!empty($_REQUEST['filtra_tipo']) && $_REQUEST['filtra_tipo']!='*'?" and id_tipo_transazione = ".$_REQUEST['filtra_tipo']."":'');
	
			$qry .=(!empty($_REQUEST['filtra_cliente']) && $_REQUEST['filtra_cliente']!='*'?" and id_richiedente = ".$_REQUEST['filtra_cliente']."":'');

		$qry .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?" and id_valuta = ".$_REQUEST['filtra_valuta']."":'');

			# order
			$qry .="  order by data_richiesta desc";
	
			$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list&amp;request_type='.$_REQUEST['request_type'];

			$link_extra_param .=(!empty($_REQUEST['filtra_tipo']) && $_REQUEST['filtra_tipo']!=''?"&amp;filtra_tipo=".$_REQUEST['filtra_tipo']:'');
			$link_extra_param .=(!empty($_REQUEST['filtra_cliente']) && $_REQUEST['filtra_cliente']!=''?"&amp;filtra_cliente=".$_REQUEST['filtra_cliente']:'');
			$link_extra_param .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?"&amp;filtra_valuta=".$_REQUEST['filtra_valuta']:'');

			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
				
			?>
			<form name="fFast_upd" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" onsubmit="return controllaForm('fFast_upd');">
			<input type="hidden" name="act" value="fast_upd" />
			<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
			<input type="hidden" name="filter" value="0" />
            
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>
				<th><?php echo LABEL_TIPO_TRANSAZIONE; ?></th>
				<th><?php echo LABEL_CLIENTE; ?></th>
				<th><?php echo LABEL_VALUTA; ?></th>
				<th><?php echo LABEL_IMPORTO; ?></th>
				<th><?php echo LABEL_DATA; ?></th>
				<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
			</tr>
			
			<!-- filtri -->
            
            
            
			<tr>
				<td colspan="2" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
				<td><?php
				// id_tipo_transazione            
				?><select name="filtra_tipo" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionTransazioni as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo']) && $_REQUEST['filtra_tipo']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
				<td><?php
				// id_richiedente            
				?><select name="filtra_cliente" class="textbox" style="width:130px;">

					<option value="*"><?php echo LABEL_TUTTI; ?></option>
					<?php
					foreach ($optionClientiConti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_cliente']) && $_REQUEST['filtra_cliente']==$id?' selected':'').'>'.$nome.'</option>';
					?>
				</select></td>
                <td><?php
                // id_valuta            
                ?><select name="filtra_valuta" class="textbox">
                    <option value="*"><?php echo LABEL_TUTTI; ?></option>
                    <?php
                    foreach ($optionValute as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']==$id?' selected':'').'>'.$nome.'</option>';
                    ?>
                </select></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		<?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
				$arrayImgStati['P']='<a href="'.$path_web.'?page=transactions&amp;act=attiva&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'&amp;pageR='.$_REQUEST['page'].'&amp;request_type='.$_REQUEST['request_type'].'" onclick="if (confirm(confirm_attiva_transazione)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATT_TRANSAZIONE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATT_TRANSAZIONE.'" title="'.PENDING.'-'.ATT_TRANSAZIONE.'" /></a>';
			//	print_r($record);
				echo '
				<tr>
					<td class="'.$class.'"><a href="'.$path_web.'?page=transactions&amp;act=form&amp;id_transazione='.$record['id_transazione'].'&amp;id_tipo_transazione='.$record['id_tipo_transazione'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
					<td class="'.$class.'">';
				echo "<a href=\"".$path_web."?page=transactions&amp;act=del&amp;id_tipo_transazione=".$_REQUEST['id_tipo_transazione']."&amp;id_transazione=".$record['id_transazione']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
				echo "</td>";
				echo "\n".'<td class="'.$class.'">'.$optionTransazioni[$record['id_tipo_transazione']].'</td>
					<td class="'.$class.'">'.$optionClientiConti[$record['id_richiedente']].'</td>
					<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
					<td class="'.$class.'">'.number_format($record['importo_transazione'],2,',','.').'</td>
					<td class="'.$class.'">'.$func->formatData($record['data_transazione'], "d-m-Y").'</td>
					<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
					</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			</table>
			</form>            
		<?php	
				echo $view_links;
	
			} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
	
	
	break; # 4 - transazioni

	case "portfolio": // portfolio
	# 5 - portfolio

	$optionClienti = $zealandCredit->getClientiConti($stato_opzioni); // solo clienti con conti aperti
	$optionConti =$zealandCredit->getContiClientiPortfolio(); // solo conti con abilitazione prelievo/deposito
	$optionValute = $zealandCredit->getValute($stato_opzioni, false);
	$optionInvestimenti = $zealandCredit->getTipiInvestimenti('tipi_investimenti', $_SESSION['lingua'], $stato_opzioni, false);
	$optionTipiInvestimenti = array();
	$optionTipiInvestimenti['']='';
	foreach ($optionInvestimenti as $id => $dati) {
		$optionTipiInvestimenti[$id]=$dati['nome_tipo'].' ('.$optionValute[$dati['id_valuta']].')';
	}
	
	$optionContiDati =$zealandCredit->getContiDati($stato_opzioni); // solo conti con abilitazione prelievo/deposito

		$qry="select por.*, cli.*, con.numero_conto, con.id_valuta from conti as con, portfolio as por, clienti as cli where por.id_cliente=cli.id_cliente and cli.id_cliente=con.id_cliente and por.id_conto=con.id_conto and por.stato='P'";
		
		# filtri
		$qry .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?" and (cli.cognome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.nome like '%".$_REQUEST['filtra_nome_cognome_soc']."%' or cli.ragione_sociale like '%".$_REQUEST['filtra_nome_cognome_soc']."%')":'');

		$qry .=(!empty($_REQUEST['filtra_tipo_investimento']) && $_REQUEST['filtra_tipo_investimento']!='*'?" and por.id_tipo_portfolio = ".$_REQUEST['filtra_tipo_investimento']."":'');

		$qry .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?" and por.stato '= ".$_REQUEST['filtra_valuta']."'":'');


		# order
		$qry .="  order by por.data_richiesta desc, cli.cognome, cli.nome, cli.ragione_sociale, por.id_portfolio";

		$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list&amp;request_type='.$_REQUEST['request_type'];
		$link_extra_param .=(!empty($_REQUEST['filtra_nome_cognome_soc']) && $_REQUEST['filtra_nome_cognome_soc']!=''?"&amp;filtra_nome_cognome_soc=".$_REQUEST['filtra_nome_cognome_soc']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_investimento']) && $_REQUEST['filtra_tipo_investimento']!='*'?"&amp;filtra_tipo_investimento=".$_REQUEST['filtra_tipo_investimento']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']!='*'?"&amp;filtra_valuta=".$_REQUEST['filtra_valuta']:'');
			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
			
		?>
		<form name="fFast_upd" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" onsubmit="return controllaForm('fFast_upd');">
		<input type="hidden" name="act" value="fast_upd" />
		<input type="hidden" name="request_type" value="<?php echo $_REQUEST['request_type']; ?>" />
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
		<input type="hidden" name="filter" value="0" />
		<table class="tblAdmin">
		<tr>
			<th style="width:30px;"></th>
			<th style="width:30px;"></th>
			<th><?php echo LABEL_NOME_SOCIETA; ?></th>
			<th><?php echo LABEL_NOME_INVESTIMENTO; ?></th>
			<th><?php echo LABEL_DESCRIZIONE; ?></th>
			<th><?php echo LABEL_VALUTA; ?></th>
			<th><?php echo LABEL_VALORE_QUOTA; ?></th>
			<th><?php echo LABEL_NUMERO_QUOTE; ?></th>
			<th><?php echo LABEL_TOT_INVESTIMENTO; ?></th>
			<th><?php echo LABEL_DATA_RICHIESTA; ?></th>
			<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
		</tr>
        
        <!-- filtri -->
        <tr>
            <td colspan="2" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
            <td><?php
            // cognome / nome / ragione_sociale
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_nome_cognome_soc']?>" name="filtra_nome_cognome_soc" class="textbox" size="15" /></td>
            <td><?php
            // id_tipo_investimento           
            ?><select name="filtra_tipo_investimento" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionTipiInvestimenti as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo_investimento']) && $_REQUEST['filtra_tipo_investimento']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
            <td></td>
            <td><?php
            // stato            
            ?><select name="filtra_valuta" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionValute as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_valuta']) && $_REQUEST['filtra_valuta']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
	<?php    
		$class="tdRow1";
		while ($record =& $res->fetchRow()) {
			$arrayImgStati['P']='<a href="'.$path_web.'?page=portfolio&amp;act=attiva&amp;id_portfolio='.$record['id_portfolio'].'&amp;pageR='.$_REQUEST['page'].'&amp;request_type='.$_REQUEST['request_type'].'" onclick="if (confirm(confirm_attiva_portfolio)) window.open(this.href, \'_self\', \'\'); return false;" title="'.PENDING.'-'.ATTIVA_PORTFOLIO.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.PENDING.'-'.ATTIVA_PORTFOLIO.'" title="'.PENDING.'-'.ATTIVA_PORTFOLIO.'" /></a>';
		
		//	print_r($record);
		// il portfolio non � modificabile se attivo, perch� ho gi� scalato il saldo del conto
			echo '
			<tr>';
			
			echo '
				<td class="'.$class.'">'.($record['stato']!='A'?'<a href="'.$path_web.'?page=portfolio&amp;act=form&amp;id_portfolio='.$record['id_portfolio'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a>':'').'</td>
				<td class="'.$class.'">';
			if ($record['stato']!='A') echo "<a href=\"".$path_web."?page=portfolio&amp;act=del&amp;id_portfolio=".$record['id_portfolio']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			
			echo "</td>";
			echo "\n".'<td class="'.$class.'">'.$record['ragione_sociale'].' '.$record['cognome'].' '.$record['nome'].'</td>
				<td class="'.$class.'">'.$optionTipiInvestimenti[$record['id_tipo_investimento']].'</td>
				<td class="'.$class.'">'.$optionInvestimenti[$record['id_tipo_investimento']]['descrizione'].'</td>
				<td class="'.$class.'">'.$optionValute[$record['id_valuta']].'</td>
				<td class="'.$class.'">'.number_format($record['valore_quota_iniziale'],2,',','.').'</td>
				<td class="'.$class.'">'.$record['numero_quote'].'</td>
				<td class="'.$class.'">'.number_format(($record['valore_quota_iniziale']*$record['numero_quote']),2,',','.').'</td>
				<td class="'.$class.'">'.$func->formatData($record['data_richiesta'], "d-m-Y H:i").'</td>
				<td class="'.$class.'">'.$arrayImgStati[$record['stato']].'</td>
			</tr>';
			
			$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
			
		} // end while
	?>
		</table>
		</form>
	
	<?php	
			echo $view_links;

		} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);

	break; # 5 - portfolio
	
	default: // utenti
	# 1 - utenti
		if ($_REQUEST['act']=='del_multiple') {
	
			if (is_array($_REQUEST['elimina']) && @count($_REQUEST['elimina'])>0) {
			
				foreach ($_REQUEST['elimina'] as $id => $ok) {
				
					$qry="delete from clienti where id_cliente =".$id;
					$res=$db->query($qry);
					echo '<h1>'.DELETE.'</h1>';

					$goPage->go_to('index.php?page='.$_REQUEST['page'].'&request_type='.$_REQUEST['request_type']);
				
				}
			} else $goPage->alertback(NO_RECORD, false);
		
			
		}
		echo '<h1>'.LINK_MENU_SN_PROFILES.'</h1>';
		
		$optionTipoUtente = $zealandCredit->getTypes('tipi_utenti', $_SESSION['lingua'], $stato_opzioni, false);
		$optionGruppiUtente = $zealandCredit->getTypes('tipi_gruppi', $_SESSION['lingua'], $stato_opzioni, false);


		$qry="select * from clienti as cli".(!empty($_REQUEST['filtra_tipo_gruppo']) && $_REQUEST['filtra_tipo_gruppo']!='*'?" left outer join clienti_tipi_gruppi as ctg on cli.id_cliente=ctg.id_cliente ":'')." where (attivo=0)";
		
		# filtri
		$qry .=(!empty($_REQUEST['filtra_nome_cognome']) && $_REQUEST['filtra_nome_cognome']!='*'?" and (cli.cognome like '%".$_REQUEST['filtra_nome_cognome']."%' or cli.nome like '%".$_REQUEST['filtra_nome_cognome']."%')":'');

		$qry .=(!empty($_REQUEST['filtra_ragione_sociale']) && $_REQUEST['filtra_ragione_sociale']!='*'?" and cli.ragione_sociale like '%".$_REQUEST['filtra_ragione_sociale']."%'":'');

		$qry .=(!empty($_REQUEST['filtra_tipo_utente']) && $_REQUEST['filtra_tipo_utente']!='*'?" and cli.id_tipo_utente = ".$_REQUEST['filtra_tipo_utente']."":'');

		$qry .=(!empty($_REQUEST['filtra_tipo_gruppo']) && $_REQUEST['filtra_tipo_gruppo']!='*'?" and ctg.id_tipo_gruppo = ".$_REQUEST['filtra_tipo_gruppo']."":'');

		# order
		$qry .="  order by cli.data_richiesta desc, cli.cognome, cli.nome, cli.ragione_sociale";

		$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list&amp;request_type='.$_REQUEST['request_type'];
		$link_extra_param .=(!empty($_REQUEST['filtra_nome_cognome']) && $_REQUEST['filtra_nome_cognome']!='*'?"&amp;filtra_nome_cognome=".$_REQUEST['filtra_nome_cognome']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_ragione_sociale']) && $_REQUEST['filtra_ragione_sociale']!='*'?"&amp;filtra_ragione_sociale=".$_REQUEST['filtra_ragione_sociale']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_utente']) && $_REQUEST['filtra_tipo_utente']!='*'?"&amp;filtra_tipo_utente=".$_REQUEST['filtra_tipo_utente']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_gruppo']) && $_REQUEST['filtra_tipo_gruppo']!='*'?"&amp;filtra_tipo_gruppo=".$_REQUEST['filtra_tipo_gruppo']:'');

			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
			
			echo $view_links;
		?><br /><br />
		<form name="fFast_upd" method="post" action="index.php" onsubmit="return controllaForm('fFast_upd');">
		<input type="hidden" name="act" value="del_multiple" /><!--fast_upd -->
		<input type="hidden" name="request_type" value="<?php echo $_REQUEST['request_type']; ?>" />
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
		<input type="hidden" name="filter" value="0" />
		<table class="tblAdmin">
		<tr>
			<th style="width:30px;"></th>
			<th style="width:30px;"></th>
			<th style="width:8px;"></th>
			<th><?php echo LABEL_FULL_NAME; ?></th>
			<th><?php echo LABEL_RAGIONE_SOCIALE; ?></th>
			<th><?php echo LABEL_DATA_RICHIESTA; ?></th>
            <th>Type of Request</th>
            <th><?php echo LABEL_VALUTA; ?></th>

			<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
		</tr>
        
        <!-- filtri -->
 <!--       <tr>
            <td colspan="3" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
            <td><?php
            // cognome / nome
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_nome_cognome']?>" name="filtra_nome_cognome" class="textbox" size="15" /></td>
            <td><?php
            // ragione_sociale
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_ragione_sociale']?>" name="filtra_ragione_sociale" class="textbox" size="15" /></td>
			<td></td>
			<td></td>
            
        </tr>-->
	<?php    
		$class="tdRow1";
		while ($record =& $res->fetchRow()) {
		//	print_r($record);
			echo '
			<tr>
				<td class="'.$class.'"><a href="'.$path_web.'?page=profiles&amp;act=form&amp;id_cliente='.$record['id_cliente'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
				<td class="'.$class.'">';
			
			echo "<a href=\"".$path_web."?page=profiles&amp;act=del&amp;id_cliente=".$record['id_cliente']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			
			echo "</td>";
			echo "\n".'<td class="'.$class.'"><input type="checkbox" name="elimina['.$record['id_cliente'].']" value="1" /></td>';
			
			// gruppi utente
			$qry2="select id_tipo_gruppo from clienti_tipi_gruppi where id_cliente=".$record['id_cliente'];
			$res2=$db->query($qry2);
				
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res2) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry2."."; die($res2->getMessage()); 
			}			
			
			$gruppi_utente='';
			$v='';
			while ($record2 =& $res2->fetchRow()) {
				$gruppi_utente .=$v.$optionGruppiUtente[$record2['id_tipo_gruppo']];
				$v=', ';
			}
			
			echo "\n".'<td class="'.$class.'">'.$record['cognome'].' '.$record['nome'].'</td>';
			echo '
				<td class="'.$class.'">'.$record['ragione_sociale'].'</td>
				<td class="'.$class.'">'.$func->formatData($record['data_richiesta'], "d-m-Y H:i").'</td>
				<td class="'.$class.'">'.$arrayConti[$record['tipo_cliente']]['titolo'].'</td>
				<td class="'.$class.'">'. $arrayValute[$record['id_valuta_richiesta']].'</TD>
				<td class="'.$class.'">'.($record['attivo']==1?'<img src="'.$path_web.'img/icone/ball_green.png" width="24" height="24" style="border: 0px;" alt="'.ATTIVO_CLIENTE.'" title="'.ATTIVO_CLIENTE.'" />':'<a href="'.$path_web.'?page=profiles&amp;act=attiva&amp;id_cliente='.$record['id_cliente'].'&amp;pageR='.$_REQUEST['page'].'&amp;request_type='.$_REQUEST['request_type'].'" onclick="if (confirm(confirm_attiva_cliente)) window.open(this.href, \'_self\', \'\'); return false;" title="'.NON_ATTIVO_CLIENTE.'-'.ATTIVA_CLIENTE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.NON_ATTIVO_CLIENTE.'-'.ATTIVA_CLIENTE.'" title="'.NON_ATTIVO_CLIENTE.'-'.ATTIVA_CLIENTE.'" /></a>').'</td>
			</tr>';
			
			$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
			
		} // end while
	?>
		</table>
        <br /><input type="submit" name="btElimina" class="button" value="elimina selezionati" />
		</form>
	
	<?php	
			echo $view_links;

		} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
	
	### end utenti
	
} // end switch report_type
?>
