<?php



# settings.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

echo '<div class="titoloAdmin">'.LINK_MENU_SN_SETTINGS.'</div>';
// chiave primaria
switch ($_REQUEST['tbl']) {
	case "lingue": $PryKey='sigla_lingua'; break;
	case "": $PryKey=''; break;
	default: $PryKey=$sql->PrimaryKey($_REQUEST['tbl']);
}

switch ($_REQUEST['act']) {

	
	case "del":
		if (!empty($_REQUEST['id']) && !empty($_REQUEST['tbl'])) {
			
			$query="delete from ".$_REQUEST['tbl']." where ".$PryKey."='".$_REQUEST['id']."'";
			$res=$db->query($query);
			
			if ($_REQUEST['tbl']!='lingue' && $_REQUEST['tbl']!='valute') {

				$query="delete from ".$_REQUEST['tbl']."_lingue where ".$PryKey."='".$_REQUEST['id']."'";
				//echo $query;
				$res=$db->query($query);
			
			}
			
			$goPage->alertgo(DELETE_OK, 'index.php?page='.$_REQUEST['page'].'&tbl='.$_REQUEST['tbl'].'&act=list');
			
		} else $goPage->alertback(NO_RECORD, false);
		
	break;
	
	case "fast_upd": // fast update (attivo, ordinamento)

		if (is_array($_REQUEST['ordinamento']) && is_array($_REQUEST['attivo']) && !empty($_REQUEST['tbl'])) {

			foreach ($_REQUEST['ordinamento'] as $id_tip => $dati) {
				
				$query="update ".$_REQUEST['tbl']." set ordinamento=".$dati.", attivo=".$_REQUEST['attivo'][$id_tip]."".(is_array($_REQUEST['valore_quota'])?", valore='".$_REQUEST['valore_quota'][$id_tip]."'":'')."".(is_array($_REQUEST['fra_utenti'])?", fra_utenti='".$_REQUEST['fra_utenti'][$id_tip]."'":'')." where ".$PryKey."='".$id_tip."'";
				//echo '<br />'.$query;
				$res=$db->query($query);
			}
			
			$goPage->alertgo(UPDATES_OK, 'index.php?page='.$_REQUEST['page'].'&tbl='.$_REQUEST['tbl'].'&act=list');
		} else $goPage->alertback(PAGE_NOT_FOUND, false);
		
	break;
	
	case "upd": // insert / update
	
		if (!empty($_REQUEST['tbl'])) {
			
			echo '<h1>'.$_SESSION['title'].'</h1>';

			//echo '<pre>';
			//print_r($_REQUEST);
			//echo '</pre>';
			
			# dati generali
			$data=array();
			$data=$_REQUEST;
			
			if (!empty($_REQUEST['id'])) {
				
				
				# update
				$query=$sql->prepareQuery ($_REQUEST['tbl'], $data, 'update', $PryKey."='".$_REQUEST['id']."'");
				//echo $query;
				$res=$db->query($query);
				$lastid=$_REQUEST['id'];
				
				if ($_REQUEST['tbl']!='lingue' && $_REQUEST['tbl']!='valute') {
					# cancello i dati lingua
					$query="delete from ".$_REQUEST['tbl']."_lingue where ".$PryKey."='".$_REQUEST['id']."'";
					//echo $query;
					$res=$db->query($query);
				}
				
			} else {
				# insert
				$query=$sql->prepareQuery ($_REQUEST['tbl'], $data, 'insert');
				//echo $query;
				$res=$db->query($query);
				$lastid=(!empty($_REQUEST[$PryKey])?$_REQUEST[$PryKey]:mysql_insert_id());
			}

			
			# dati per le lingue

			switch ($_REQUEST['tbl']) {
				
				case 'valute':
				break;
				case 'lingue':
					// creo il file dizionario
					if (!file_exists($path_www.'inc/dizionario/'.$_REQUEST[$PryKey].'.inc.php')) copy($path_www.'inc/dizionario/en.inc.php', $path_www.'inc/dizionario/'.$_REQUEST[$PryKey].'.inc.php');
				break;
				
				case 'tipi_investimenti':
					$data=array();
						
					# dati per lingue
					foreach ($_REQUEST['nome_tipo'] as $sigla => $dati) {
						$data[$PryKey]=$lastid;
						$data['sigla_lingua']=$sigla;
						$data['nome_tipo']=$_REQUEST['nome_tipo'][$sigla];
						$data['descrizione']=$_REQUEST['descrizione'][$sigla];
						$query=$sql->prepareQuery ($_REQUEST['tbl'].'_lingue', $data, 'insert');
						//echo '<br />'.$query;
						$res=$db->query($query);
					}
			
				break;
				default:
				
					$data=array();
						
					# dati per lingue
					foreach ($_REQUEST['nome_tipo'] as $sigla => $dati) {
						$data[$PryKey]=$lastid;
						$data['sigla_lingua']=$sigla;
						$data['nome_tipo']=$_REQUEST['nome_tipo'][$sigla];
						$query=$sql->prepareQuery ($_REQUEST['tbl'].'_lingue', $data, 'insert');
						//echo '<br />'.$query;
						$res=$db->query($query);
					}
			
			} // end switch tbl
			
			$goPage->alertgo(UPDATE_OK, 'index.php?page='.$_REQUEST['page'].'&tbl='.$_REQUEST['tbl'].'&act=list');

			
		} else $goPage->alertback(PAGE_NOT_FOUND, false);
	
	
	break;
	
	case "form": // form new / mod
	
		if (!empty($_REQUEST['tbl'])) {
			
			echo '<h1>'.$_SESSION['title'].'</h1>';
			
			$record=array();
			if (!empty($_REQUEST['id'])) {
			
				switch ($_REQUEST['tbl']) {
					
					case 'valute':
					case 'lingue':
					
						$qry="select * from ".$_REQUEST['tbl']." where ".$PryKey."='".$_REQUEST['id']."'";
						$res=$db->query($qry);
						$record = $res->fetchRow();
						
					break;
					
					default:
						
						$qry="select * from ".$_REQUEST['tbl']." as tab, ".$_REQUEST['tbl']."_lingue as tab_l where tab.".$PryKey." = tab_l.".$PryKey." and tab.".$PryKey." =".$_REQUEST['id'];
						$res=$db->query($qry);
						$record=array();
						while ($rec =& $res->fetchRow()) {
							$record[$PryKey]=$rec[$PryKey];
							$record['attivo']=$rec['attivo'];
							$record['ordinamento']=$rec['ordinamento'];
							$record['id_valuta']=$rec['id_valuta'];
							$record['valore']=$rec['valore'];
							$record['nome_tipo'][$rec['sigla_lingua']]=$rec['nome_tipo'];
							$record['descrizione'][$rec['sigla_lingua']]=$rec['descrizione'];
							$record['fra_utenti']=$rec['fra_utenti'];
							if ($_REQUEST['tbl'] == 'tipi_conti'){
								$record['min_nzd']=$rec['min_nzd'];
								$record['min_usd']=$rec['min_usd'];
								$record['min_chf']=$rec['min_chf'];							
								$record['min_eur']=$rec['min_eur'];							
								$record['min_gbp']=$rec['min_gbp'];														
								$record['int_annuo']=$rec['int_annuo'];														
								$record['int_3_mesi']=$rec['int_3_mesi'];																					
							}
							
					//		$record['sigla_lingua']=$rec['sigla_lingua'];
							
						}
				
				} // end switch tbl
				
			} // if !empty id
			
			$form = new HTML_QuickForm('FormTipi', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
			$form->addElement('hidden', 'page', $_REQUEST['page']);
			$form->addElement('hidden', 'act', 'upd');
			$form->addElement('hidden', 'tbl', $_REQUEST['tbl']);
			$form->addElement('hidden', 'id', $record[$PryKey]);
			
			$form->addElement('text', 'ordinamento', ORDINAMENTO, ' class="textbox" size="10" ');
			
			$form->addElement('select', 'attivo', ATTIVO, $optionBool, ' class="textbox"');
			
			
			switch ($_REQUEST['tbl']) {
				
				case 'tipi_transazioni':
					
					// campo in pi�
					$form->addElement('select', 'fra_utenti', FRA_UTENTI, $optionBool, ' class="textbox"');
					// campi standard
					foreach ($lingue as $sigla => $dati) {
				
						$form->addElement('text', 'nome_tipo['.$sigla.']', '<img src="'.$path_web.'img/icone/'.$sigla.'.gif" style="border:0px; display:inline; vertical-align:middle;" title="'.$dati['nome_lingua'].'" alt="'.$dati['nome_lingua'].'" /> '.NOME, ' class="textbox"  size="60"');
						$form->addRule('nome_tipo['.$sigla.']', NOME.' '.$sigla, 'required', FALSE,'client');
					
					}
				
				break;

				case 'valute':
				
					$form->addElement('text', 'nome_valuta', NOME, ' class="textbox"  size="60"');
					$form->addRule('nome_valuta', NOME, 'required', FALSE,'client');
				
				break;
				
				case 'lingue':

					$form->addElement('text', 'sigla_lingua', SIGLA_LINGUA, ' class="textbox"  size="4" maxlength="2"');
					$form->addRule('sigla_lingua', SIGLA_LINGUA, 'required', FALSE,'client');

					$form->addElement('text', 'nome_lingua', NOME, ' class="textbox"  size="60"');
					$form->addRule('nome_lingua', NOME, 'required', FALSE,'client');

					$form->addElement('text', 'charset', CHARSET, ' class="textbox"  size="60"');
					//$form->addRule('charset', CHARSET, 'required', FALSE,'client');
					
				break;
				
				case 'tipi_investimenti':
					
					#############COMMENTO ALE
					/*$optionCurrency = $zealandCredit->getValute();
					$form->addElement('select', 'id_valuta', VALUTE, $optionCurrency, ' class="textbox"');
					$form->addRule('id_valuta', VALUTE, 'required', FALSE,'client');*/

					$form->addElement('text', 'valore', VALORE, ' class="textbox"  size="15" maxlength="10"');
					$form->addRule('valore', VALORE, 'required', FALSE,'client');
					
					foreach ($lingue as $sigla => $dati) {
					
						$form->addElement('text', 'nome_tipo['.$sigla.']', '<img src="'.$path_web.'img/icone/'.$sigla.'.gif" style="border:0px; display:inline; vertical-align:middle;" title="'.$dati['nome_lingua'].'" alt="'.$dati['nome_lingua'].'" /> '.NOME, ' class="textbox"  size="60"');
						$form->addRule('nome_tipo['.$sigla.']', NOME.' '.$sigla, 'required', FALSE,'client');
					
						$form->addElement('textarea', 'descrizione['.$sigla.']', '<img src="'.$path_web.'img/icone/'.$sigla.'.gif" style="border:0px; display:inline; vertical-align:middle;" title="'.$dati['nome_lingua'].'" alt="'.$dati['nome_lingua'].'" /> '.DESCRIZIONE, ' class="textbox"  rows="5" cols="60"');
						
					}
				
				break;
				
				case 'tipi_conti':

					foreach ($lingue as $sigla => $dati) {
				
						$form->addElement('text', 'nome_tipo['.$sigla.']', '<img src="'.$path_web.'img/icone/'.$sigla.'.gif" style="border:0px; display:inline; vertical-align:middle;" title="'.$dati['nome_lingua'].'" alt="'.$dati['nome_lingua'].'" /> '.NOME, ' class="textbox"  size="60"');
						$form->addRule('nome_tipo['.$sigla.']', NOME.' '.$sigla, 'required', FALSE,'client');
					
					}

					$form->addElement('text', 'min_nzd', 'min dep NZD', ' class="textbox"  size="15" maxlength="10"');
					$form->addElement('text', 'min_usd', 'min dep USD', ' class="textbox"  size="15" maxlength="10"');
					$form->addElement('text', 'min_chf', 'min dep CHF', ' class="textbox"  size="15" maxlength="10"');
					$form->addElement('text', 'min_eur', 'min dep EUR', ' class="textbox"  size="15" maxlength="10"');					
					$form->addElement('text', 'min_gbp', 'min dep GBP', ' class="textbox"  size="15" maxlength="10"');										


					$form->addElement('text', 'int_3_mesi', INT_PRIMI_3_MESI, ' class="textbox"  size="15" maxlength="10"');					
					$form->addElement('text', 'int_annuo', INT_ANNUO, ' class="textbox"  size="15" maxlength="10"');										

					
					
					
				
				break;				
				
				default:
				
					foreach ($lingue as $sigla => $dati) {
				
						$form->addElement('text', 'nome_tipo['.$sigla.']', '<img src="'.$path_web.'img/icone/'.$sigla.'.gif" style="border:0px; display:inline; vertical-align:middle;" title="'.$dati['nome_lingua'].'" alt="'.$dati['nome_lingua'].'" /> '.NOME, ' class="textbox"  size="60"');
						$form->addRule('nome_tipo['.$sigla.']', NOME.' '.$sigla, 'required', FALSE,'client');
					
					}
					
			} // end switch tbl
			
			$form->setDefaults($record); // da tabella
		
			
			$form->setRequiredNote(REQUIRED_FIELDS);
			$form->setJsWarnings(ERRORE_JS,'');
			
			// CREO I PULSANTI
			$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', SAVE,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list&tbl='.$_REQUEST['tbl'].'\';"');
			
			$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
		
			// mostro il form
			$form->display();
	
	
		} else $goPage->alertback(PAGE_NOT_FOUND, false);
	
	break;
	
	case "list": // list
		
		if (!empty($_REQUEST['tbl'])) {
			
			echo '<h1>'.$_SESSION['title'];
			echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;tbl='.$_REQUEST['tbl'].'" title="'.INSERT_NEW.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.INSERT_NEW.'" /></a>';
			echo '</h1>';
			
			// eccezione per valute e lingue
			
			switch ($_REQUEST['tbl']) {
				
				case 'valute':
				
					$qry="select nome_valuta as nome_tipo, ordinamento, attivo, id_valuta, valore from ".$_REQUEST['tbl']." order by ordinamento";

				break;
				case 'lingue':
					
					$qry="select nome_lingua as nome_tipo, charset, ordinamento, attivo, sigla_lingua from ".$_REQUEST['tbl']." order by ordinamento";

				break;
				
				default:
					
					$qry="select * from ".$_REQUEST['tbl']." as tab, ".$_REQUEST['tbl']."_lingue as tab_l where tab.".$PryKey." = tab_l.".$PryKey." and tab_l.sigla_lingua='".$_SESSION['lingua']."'";
			
			}
				
			$res=$db->query($qry);
		
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}			
			
			$rows = $res->numRows();
			
			if ($rows>0) {
			
				echo NUM_RECORDS.': '.$rows.'<br /><br />';
				
			?>
        	<form name="fFast_upd" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" onsubmit="return controllaForm('fFast_upd');">
            <input type="hidden" name="act" value="fast_upd" />
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
            <input type="hidden" name="tbl" value="<?php echo $_REQUEST['tbl']; ?>" />
			<table class="tblAdmin">
			<tr>
				<th style="width:30px;"></th>
				<th style="width:30px;"></th>
				<th><?php echo NOME; ?></th>
            <?php
            if ($_REQUEST['tbl']=='tipi_investimenti') {
				$optionValute = $zealandCredit->getValute($stato_opzioni, false);

			?>
				<th><?php echo DESCRIZIONE; ?></th>
				<th><?php echo LABEL_VALORE_QUOTA; ?></th><?php			
			}
			?>
            <?php
            if ($_REQUEST['tbl']=='tipi_transazioni') {
			?>
				<th><?php echo FRA_UTENTI; ?></th><?php			
			}
			?>
            
				<th style="width:60px;"><?php echo ORDINAMENTO; ?></th>
				<th style="width:60px;"><?php echo ATTIVO; ?></th>
            <?php
            if ($_REQUEST['tbl']=='tipi_investimenti') {
			?>
				<th><?php echo GEST_TASSI; ?></th><?php 
			}
			?>                
			</tr>
        <?php    
			$class="tdRow1";
			while ($record =& $res->fetchRow()) {
			//	print_r($record);
			// i tipi di transazioni non sono cancellabili perch� gli id servono per le transazioni
				echo '
				<tr>
					<td class="'.$class.'">'.($_REQUEST['tbl']=='tipi_transazioni'?'<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;tbl='.$_REQUEST['tbl'].'&amp;id='.$record[$PryKey].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a>':'<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;tbl='.$_REQUEST['tbl'].'&amp;id='.$record[$PryKey].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a>').'</td>
					<td class="'.$class.'">';
				if ($_REQUEST['tbl']!='tipi_transazioni') echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;tbl=".$_REQUEST['tbl']."&amp;id=".$record[$PryKey]."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a></td>";
			
			
           	echo "\n".'<td class="'.$class.'">'.$record['nome_tipo'].'</td>';
			
            if ($_REQUEST['tbl']=='tipi_investimenti') echo '
				<td class="'.$class.'">'.$record['descrizione'].'</td>
				<td class="'.$class.'"><input type="text"class="textbox" name="valore_quota['.$record[$PryKey].']" value="'.$record['valore'].'" size="10" /></td>';


            if ($_REQUEST['tbl']=='tipi_transazioni') echo '<td class="'.$class.'">
					<select name="fra_utenti['.$record[$PryKey].']" class="textbox">
						<option value="0">'.LABEL_NO.'</option>
						<option value="1"'.($record['fra_utenti']==1?' selected="selected"':'').'>'.LABEL_SI.'</option>
					</select>	
				</td>';

			echo '
				<td class="'.$class.'"><input type="text" class="textbox" name="ordinamento['.$record[$PryKey].']" value="'.$record['ordinamento'].'" size="3" /></td>
				<td class="'.$class.'">
					<select name="attivo['.$record[$PryKey].']" class="textbox">
						<option value="0">'.LABEL_NO.'</option>
						<option value="1"'.($record['attivo']==1?' selected="selected"':'').'>'.LABEL_SI.'</option>
					</select>	
				</td>';
				
            if ($_REQUEST['tbl']=='tipi_investimenti') echo '<td class="'.$class.'"><input type="button" value="manage" class="button" onclick="javascript:document.location.href=\''.$path_web.'?page=sett_int&amp;act=list&amp;tbl=rel_interessi_investimenti&amp;id_invest='.$record[$PryKey].'\';"/></td>';

				
			echo '</tr>';
				
				$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
				
			} // end while
		?>
			
			<tr>
				<th></th>
				<th></th>
			<?php
            if ($_REQUEST['tbl']=='tipi_investimenti') {?>
                <th></th>
                <th></th>
                <th></th><?php			
            }
            ?>
            <?php
            if ($_REQUEST['tbl']=='tipi_transazioni') {
			?>
				<th></th><?php			
			}
			?>
				<th></th>
				<th colspan="2" style="text-align:center;"><input type="submit" value="<?php echo UPDATE; ?>" class="button" /></th>
			</tr>
			</table>
           	</form>
        
		<?php	
				echo $view_links;

			} else echo TABELLA_VUOTA;
			
		} else $goPage->alertback(PAGE_NOT_FOUND, false);
		
	break;
	
	default: // list of settings
	
		?>
        <p style="font-weight:bold"><?php echo LINK_MENU_SN_PROFILES; ?></p>
        <ul>
        	<li><a href="<?php echo $path_web; ?>index.php?page=settings&amp;act=list&amp;tbl=tipi_gruppi&amp;title=<?php echo GRUPPI_UTENTI; ?>" title="<?php echo GRUPPI_UTENTI; ?>"><?php echo GRUPPI_UTENTI; ?></a></li>
        	<li><a href="<?php echo $path_web; ?>index.php?page=settings&amp;act=list&amp;tbl=tipi_documenti&amp;title=<?php echo TIPI_DOCUMENTI; ?>" title="<?php echo TIPI_DOCUMENTI; ?>"><?php echo TIPI_DOCUMENTI; ?></a></li>
         	<li><a href="<?php echo $path_web; ?>index.php?page=settings&amp;act=list&amp;tbl=tipi_titoli&amp;title=<?php echo TITOLI; ?>" title="<?php echo TITOLI; ?>"><?php echo TITOLI; ?></a></li>
        </ul>  


        <p style="font-weight:bold"><?php echo LINK_MENU_SN_ACCOUNTS; ?></p>
        <ul>
        	<li><a href="<?php echo $path_web; ?>index.php?page=settings&amp;act=list&amp;tbl=tipi_conti&amp;title=<?php echo TIPI_CONTI; ?>" title="<?php echo TIPI_CONTI; ?>"><?php echo TIPI_CONTI; ?></a></li>
        	<li><a href="<?php echo $path_web; ?>index.php?page=settings&amp;act=list&amp;tbl=tipi_carte&amp;title=<?php echo TIPI_CARTE; ?>" title="<?php echo TIPI_CARTE; ?>"><?php echo TIPI_CARTE; ?></a></li>
        </ul>        
        
              
        <p style="font-weight:bold"><?php echo LINK_MENU_SN_TRANSACTIONS; ?></p>
        <ul>
        	<li><a href="<?php echo $path_web; ?>index.php?page=settings&amp;act=list&amp;tbl=tipi_transazioni&amp;title=<?php echo TIPI_TRANSAZIONI; ?>" title="<?php echo TIPI_TRANSAZIONI; ?>"><?php echo TIPI_TRANSAZIONI; ?></a></li>
         	<li><a href="<?php echo $path_web; ?>index.php?page=settings&amp;act=list&amp;tbl=tipi_commissioni&amp;title=<?php echo TIPI_COMMISSIONI; ?>" title="<?php echo TIPI_COMMISSIONI; ?>"><?php echo TIPI_COMMISSIONI; ?></a></li>
        </ul>        

        <p style="font-weight:bold"><?php echo LINK_MENU_SN_PORTFOLIO; ?></p>
        <ul>
        	<li><a href="<?php echo $path_web; ?>index.php?page=settings&amp;act=list&amp;tbl=tipi_investimenti&amp;title=<?php echo TIPI_INVESTIMENTI; ?>" title="<?php echo TIPI_INVESTIMENTI; ?>"><?php echo TIPI_INVESTIMENTI; ?></a></li>
            
        </ul>        

        <p style="font-weight:bold"><?php echo ALTRI_SETTAGGI; ?></p>
        <ul>
        	<li><a href="<?php echo $path_web; ?>index.php?page=settings&amp;act=list&amp;tbl=valute&amp;title=<?php echo VALUTE; ?>" title="<?php echo VALUTE; ?>"><?php echo VALUTE; ?></a></li>
         	<li><a href="<?php echo $path_web; ?>index.php?page=settings&amp;act=list&amp;tbl=lingue&amp;title=<?php echo LINGUE; ?>" title="<?php echo LINGUE; ?>"><?php echo LINGUE; ?></a></li>
       </ul>        

        <?php		

}
	
?>
