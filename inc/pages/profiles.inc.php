<?php
# profiles.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

$optionTitoli = $zealandCredit->getTypes('tipi_titoli', $_SESSION['lingua'], $stato_opzioni);
$optionDocumenti = $zealandCredit->getTypes('tipi_documenti', $_SESSION['lingua'], $stato_opzioni);
$optionPaesi = $zealandCredit->getPaesi();
$optionTipoUtente = $zealandCredit->getTypes('tipi_utenti', $_SESSION['lingua'], $stato_opzioni, false);
$optionGruppiUtente = $zealandCredit->getTypes('tipi_gruppi', $_SESSION['lingua'], $stato_opzioni, false);

$optionLingue['']='';
foreach ($lingue as $sigla => $dati) $optionLingue[$sigla] = $dati['nome_lingua'];

echo '<div class="titoloAdmin">'.LINK_MENU_SN_PROFILES.'</div>';
switch ($_REQUEST['act']) {

	case "form": // form new / mod
		
			
		$record=array();
		if (!empty($_REQUEST['id_cliente'])) {
			
			 
			echo '<h1>'.VIEW_MOD.'</h1>';
			# dati cliente
			$qry="select * from clienti as cli where cli.id_cliente='".$_REQUEST['id_cliente']."'";
			$res=$db->query($qry);
			$record = $res->fetchRow();
			
			$record['psw']['password']=$record['password'];
			$record['user']=$record['username'];
			
		 
			
			# gruppi
			$qry="select * from clienti_tipi_gruppi where id_cliente='".$_REQUEST['id_cliente']."'";
			$res=$db->query($qry);
			while ($record2 =& $res->fetchRow()) {
				$record['clienti_tipi_gruppi'][$record2['id_tipo_gruppo']]=$record2['id_tipo_gruppo'];
			}
			
			
			$record['datadinascita'] = $record['data_di_nascita'];
			$record['datascadenzadocumento'] = $record['data_scadenza_documento'];
			$record['datarilasciodocumento'] = $record['data_rilascio_documento'];
			$record['statocivile'] = $record['stato_civile'];
			$record['sex'] = $record['sesso'];
			
		} else {
			$record['tipo_cliente'] = $_REQUEST['tipo_cliente'];
			echo '<h1>'.($record['tipo_cliente']=='single-personal'?INSERT_NEW_PERSONAL:INSERT_NEW_CORPORATE).'</h1>';
		}
			
		$form = new HTML_QuickForm('FormApply', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
		$form->addElement('hidden', 'page', $_REQUEST['page']);
		$form->addElement('hidden', 'act', 'upd');
		$form->addElement('hidden', 'id_cliente', $_REQUEST['id_cliente']);
		$form->addElement('hidden', 'old_psw', $record['password']);
		$form->addElement('hidden', 'old_username', $record['username']);
		$form->addElement('hidden', 'tipo_cliente', $record['tipo_cliente']);
		
		$form->addElement('header', 'UserInformation', LABEL_INFO_UTENTE);
		//$form->addElement('date', 'data_richiesta', LABEL_DATA_RICHIESTA, $optionDate,' class="textbox" style="margin-left:2px;"');
		//$form->addElement('date', 'data_attivazione', LABEL_DATA_ATTIVAZIONE, $optionDate,' class="textbox" style="margin-left:2px;"');

		$form->addElement('hidden', 'data_attivazione', $record['data_attivazione']);
		$form->addElement('hidden', 'data_richiesta', $record['data_richiesta']);
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_RICHIESTA.'</div><div class="fieldForm">'.(!empty($record['data_richiesta'])?$func->formatData($record['data_richiesta'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		
		$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_ATTIVAZIONE.'</div><div class="fieldForm">'.(!empty($record['data_attivazione'])?$func->formatData($record['data_attivazione'], 'd-m-Y H:i'):'').'</div><div class="clearBoth"></div>');
		
		//$form->addElement('hidden', 'attivo', $record['attivo']);
		//$form->addElement('html', '<div class="labelForm">'.ATTIVO_CLIENTE.'</div><div class="fieldForm">'.($record['attivo']==1?LABEL_SI:LABEL_NO).'</div><div class="clearBoth"></div>');
		
		$form->addElement('select', 'attivo', ATTIVO_CLIENTE, $optionBool, ' class="textbox"');
		$form->addElement('select', 'id_valuta_richiesta', LABEL_VALUTA, $arrayValute, ' class="textbox" ');
		
		$form->addElement('select', 'sigla_lingua', LABEL_LINGUA, $optionLingue, ' class="textbox" ');
		
		
		
		
	
		
			switch ($record['tipo_cliente']) {
				case "":
				case "single-personal":
				case "chequing":
				case "children":
				case "bsa":
				case "sa":
				case "cda":
				case "gia":
				case "sia":

					$form->addElement('select', 'id_tipo_titolo', LABEL_TITOLO, $optionTitoli, ' class="textbox" ');
					
					$form->addElement('text', 'cognome', LABEL_COGNOME,' class="textbox" size="50"');
					$form->addRule('cognome', LABEL_COGNOME, 'required', FALSE,'client');
					
					$form->addElement('text', 'nome', LABEL_NOME,' class="textbox" size="50"');
					$form->addRule('nome', LABEL_NOME, 'required', FALSE,'client');
					
					$form->addElement('select', 'codice_paese_residenza', LABEL_RESIDENZA, $optionPaesi, ' class="textbox" ');
					$form->addRule('codice_paese_residenza', LABEL_RESIDENZA, 'required', FALSE,'client');
					
					$form->addElement('text', 'provincia_stato_physical', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="50"');
					$form->addRule('provincia_stato_physical', LABEL_PROVINCIA_STATO_REGIONE, 'required', FALSE,'client');
					
					$form->addElement('text', 'cap_physical', LABEL_CAP,' class="textbox" size="15"');
					$form->addRule('cap_physical', LABEL_CAP, 'required', FALSE,'client');
					
					$form->addElement('text', 'citta_physical', LABEL_CITTA,' class="textbox" size="50"');
					$form->addRule('citta_physical', LABEL_CITTA, 'required', FALSE,'client');
					
					$form->addElement('text', 'indirizzo_physical', LABEL_INDIRIZZO,' class="textbox" size="50"');
					$form->addRule('indirizzo_physical', LABEL_INDIRIZZO, 'required', FALSE,'client');
					
					//$form->addElement('text', 'telefono_casa', LABEL_TEL_CASA,' class="textbox" size="30"');
					$form->addElement('text', 'telefono_physical', LABEL_TELEFONO,' class="textbox" size="30"');
					$form->addRule('telefono_physical', LABEL_TELEFONO, 'required', FALSE,'client');
					
					$form->addElement('text', 'fax', LABEL_FAX,' class="textbox" size="30"');
					
					$form->addElement('text', 'telefono_cellulare', LABEL_TEL_MOBILE,' class="textbox" size="50"');
					$form->addRule('telefono_cellulare', LABEL_TEL_MOBILE, 'required', FALSE,'client');
					
					$form->addElement('text', 'email', LABEL_EMAIL,' class="textbox" size="30"');
					$form->addRule('email', LABEL_EMAIL, 'required', FALSE,'client');
					$form->addRule('email', LABEL_EMAIL, 'email', FALSE,'client');
					
					$form->addElement('text', 'professione', LABEL_PROFESSIONE,' class="textbox" size="50"');
					
					$form->addElement('date', 'datadinascita', LABEL_DATA_NASCITA, $optionDate,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_di_nascita\');"');
					$form->addElement('hidden', 'data_di_nascita', '');
					$form->addRule('datadinascita', LABEL_DATA_NASCITA, 'required', FALSE,'client');
					$form->addRule('data_di_nascita', LABEL_DATA_NASCITA, 'required', FALSE,'client');

					//$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_NASCITA.'</div><div class="fieldForm">'.(!empty($record['data_di_nascita'])?$func->formatData($record['data_di_nascita'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');



					$form->addElement('text', 'citta_nascita', LABEL_CITTA_NASCITA,' class="textbox" size="50"');
					$form->addRule('citta_nascita', LABEL_CITTA_NASCITA, 'required', FALSE,'client');
					
					$form->addElement('select', 'codice_paese_cittadinanza', LABEL_PAESE_NAZIONALITA, $optionPaesi, ' class="textbox" ');
					$form->addRule('codice_paese_cittadinanza', LABEL_PAESE_NAZIONALITA, 'required', FALSE,'client');
					
					$form->addElement('text', 'cognome_da_nubile', LABEL_COGNOME_NUBILE,' class="textbox" size="50"');
					//$form->addRule('cognome_da_nubile', LABEL_COGNOME_NUBILE, 'required', FALSE,'client');
					
					$form->addElement('text', 'nazionalita', LABEL_NAZIONALITA,' class="textbox" size="50"');
					$form->addRule('nazionalita', LABEL_NAZIONALITA, 'required', FALSE,'client');
					
					$sex[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_MASCHIO,'M', ' onclick="this.form.elements[\'sesso\'].value = this.value;"');
					$sex[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_FEMMINA,'F', ' onclick="this.form.elements[\'sesso\'].value = this.value;"');
					$form->addGroup($sex,'sex',LABEL_SEX,'&nbsp;');
					$form->addElement('hidden', 'sesso', 'M');
					$form->addRule('sesso', LABEL_SEX, 'required', FALSE,'client');
					$form->addRule('sex', LABEL_SEX, 'required', FALSE,'client');
					
					$statocivile[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_SINGLE,'single', ' onclick="this.form.elements[\'stato_civile\'].value = this.value;"');
					$statocivile[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_MARRIED,'married', ' onclick="this.form.elements[\'stato_civile\'].value = this.value;" ');
					$form->addGroup($statocivile,'statocivile',LABEL_STATO_CIVILE,'&nbsp;');
					$form->addElement('hidden', 'stato_civile', 'single');
					$form->addRule('stato_civile', LABEL_STATO_CIVILE, 'required', FALSE,'client');
					$form->addRule('statocivile', LABEL_STATO_CIVILE, 'required', FALSE,'client');
					
					
					$form->addElement('header', 'MailingAddress', LABEL_INDIRIZZO_POSTA.' '.LABEL_SE_DIVERSO); // id_tipo_indirizzo = 2
					
					//$form->addElement('checkbox', 'mail_same_physical', '', MAIL_SAME_PHYSICAL,' onclick="popolaMailAddress();"');
					//$form->addElement('text', 'nome_mail', LABEL_NOME2,' class="textbox" size="30"');
					$form->addElement('text', 'indirizzo_mail', LABEL_INDIRIZZO_POBOX,' class="textbox" size="50"');
					$form->addElement('text', 'cap_mail', LABEL_CAP,' class="textbox" size="15"');
					$form->addElement('text', 'citta_mail', LABEL_CITTA,' class="textbox" size="50"');
					$form->addElement('text', 'provincia_stato_mail', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="50"');
					//$form->addElement('text', 'telefono_mail', LABEL_TELEFONO,' class="textbox" size="30"');
					$form->addElement('select', 'codice_paese_mail', LABEL_PAESE, $optionPaesi, ' class="textbox" ');
					
					$form->addElement('header', 'Applicant_document', LABEL_RICHIEDENTE);
					
					$form->addElement('select', 'id_tipo_documento', LABEL_TIPO_DOCUMENTO, $optionDocumenti, ' class="textbox" ');
					$form->addRule('id_tipo_documento', LABEL_TIPO_DOCUMENTO, 'required', FALSE,'client');
					
					$form->addElement('text', 'codice_documento', LABEL_CODICE_DOCUMENTO,' class="textbox" size="30"');
					$form->addRule('codice_documento', LABEL_CODICE_DOCUMENTO, 'required', FALSE,'client');
					
					$form->addElement('date', 'datarilasciodocumento', LABEL_RILASCIATO_IL, $optionDate2,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_rilascio_documento\');"');
					$form->addElement('hidden', 'data_rilascio_documento', '');
					$form->addRule('datarilasciodocumento', LABEL_RILASCIATO_IL, 'required', FALSE,'client');
					$form->addRule('data_rilascio_documento', LABEL_RILASCIATO_IL, 'required', FALSE,'client');
					
					//$form->addElement('html', '<div class="labelForm">'.LABEL_RILASCIATO_IL.'</div><div class="fieldForm">'.(!empty($record['data_rilascio_documento'])?$func->formatData($record['data_rilascio_documento'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');
					
					$form->addElement('text', 'documento_rilasciato_da', LABEL_DA_RILASCIATO,' class="textbox" size="50"');
					$form->addRule('documento_rilasciato_da', LABEL_DA_RILASCIATO, 'required', FALSE,'client');
					
					$form->addElement('date', 'datascadenzadocumento', LABEL_SCADENZA, $optionDate2,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_scadenza_documento\');"');
					$form->addElement('hidden', 'data_scadenza_documento', '');
					$form->addRule('datascadenzadocumento', LABEL_SCADENZA, 'required', FALSE,'client');
					$form->addRule('data_scadenza_documento', LABEL_SCADENZA, 'required', FALSE,'client');
					
					//$form->addElement('html', '<div class="labelForm">'.LABEL_SCADENZA.'</div><div class="fieldForm">'.(!empty($record['data_scadenza_documento'])?$func->formatData($record['data_scadenza_documento'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');
					
					
					//$form->addElement('select', 'codice_paese_cittadinanza', LABEL_NAZIONALITA, $optionPaesi, ' class="textbox" ');
					
					
					//$form->addElement('text', 'skype', LABEL_SKYPE,' class="textbox" size="60"');
					
					//$form->addRule('codice_paese_cittadinanza', LABEL_NAZIONALITA, 'required', FALSE,'client');
					
					//$form->addElement('header', 'PhysicalAddress', LABEL_INDIRIZZO_FISICO); // id_tipo_indirizzo = 1
					
					//$form->addElement('text', 'nome_physical', LABEL_NOME2,' class="textbox" size="30"');
					//$form->addElement('text', 'telefono_physical', LABEL_TELEFONO,' class="textbox" size="30"');
					
					$form->addElement('textarea', 'note', LABEL_NOTE,' class="textbox" cols="50" rows="5"');

				break; // end single-personal
				
				case "corporate":
				case "merchant":
				case "ca":
				case "fda":	
					$form->addElement('header', 'Company', LABEL_COMPANY);
					
					$form->addElement('text', 'ragione_sociale', LABEL_COMPANY_NAME,' class="textbox" size="50"');
					$form->addRule('ragione_sociale', LABEL_COMPANY_NAME, 'required', FALSE,'client');
					
					$form->addElement('text', 'company_telefono', LABEL_TELEFONO,' class="textbox" size="30"');
					$form->addRule('company_telefono', LABEL_COMPANY_TELEFONO, 'required', FALSE,'client');
					
					$form->addElement('text', 'company_fax', LABEL_FAX,' class="textbox" size="30"');
					
					$form->addElement('text', 'company_email', LABEL_EMAIL,' class="textbox" size="30"');
					//$form->addRule('company_email', LABEL_EMAIL, 'required', FALSE,'client');
					$form->addRule('company_email', LABEL_COMPANY_EMAIL, 'email', FALSE,'client');
				
					$form->addElement('text', 'company_codice', LABEL_CODICE_COMPANY,' class="textbox" size="30"');
					$form->addRule('company_codice', LABEL_CODICE_COMPANY, 'required', FALSE,'client');
					
					$form->addElement('select', 'company_codice_paese', LABEL_PAESE, $optionPaesi, ' class="textbox" ');
					$form->addRule('company_codice_paese', LABEL_COMPANY_PAESE, 'required', FALSE,'client');
				
					$form->addElement('text', 'company_provincia_stato', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="50"');
					$form->addRule('company_provincia_stato', LABEL_COMPANY_PROVINCIA_STATO_REGIONE, 'required', FALSE,'client');
					
					$form->addElement('text', 'company_cap', LABEL_CAP,' class="textbox" size="15"');
					$form->addRule('company_cap', LABEL_COMPANY_CAP, 'required', FALSE,'client');
					
					$form->addElement('text', 'company_citta', LABEL_CITTA,' class="textbox" size="50"');
					$form->addRule('company_citta', LABEL_COMPANY_CITTA, 'required', FALSE,'client');
					
					$form->addElement('text', 'company_indirizzo', LABEL_INDIRIZZO,' class="textbox" size="50"');
					$form->addRule('company_indirizzo', LABEL_COMPANY_INDIRIZZO, 'required', FALSE,'client');
				
					$form->addElement('header', 'Applicant', LABEL_AUTHORIZED_PERSON);
				
					$form->addElement('select', 'id_tipo_titolo', LABEL_TITOLO, $optionTitoli, ' class="textbox" ');
					
					$form->addElement('text', 'cognome', LABEL_COGNOME,' class="textbox" size="50"');
					$form->addRule('cognome', LABEL_COGNOME, 'required', FALSE,'client');
					
					$form->addElement('text', 'nome', LABEL_NOME,' class="textbox" size="50"');
					$form->addRule('nome', LABEL_NOME, 'required', FALSE,'client');
				
					$form->addElement('select', 'codice_paese_residenza', LABEL_RESIDENZA, $optionPaesi, ' class="textbox" ');
					$form->addRule('codice_paese_residenza', LABEL_RESIDENZA, 'required', FALSE,'client');
				
					$form->addElement('text', 'provincia_stato_physical', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="50"');
					$form->addRule('provincia_stato_physical', LABEL_PROVINCIA_STATO_REGIONE, 'required', FALSE,'client');
					
					$form->addElement('text', 'cap_physical', LABEL_CAP,' class="textbox" size="15"');
					$form->addRule('cap_physical', LABEL_CAP, 'required', FALSE,'client');
					
					$form->addElement('text', 'citta_physical', LABEL_CITTA,' class="textbox" size="50"');
					$form->addRule('citta_physical', LABEL_CITTA, 'required', FALSE,'client');
					
					$form->addElement('text', 'indirizzo_physical', LABEL_INDIRIZZO,' class="textbox" size="50"');
					$form->addRule('indirizzo_physical', LABEL_INDIRIZZO, 'required', FALSE,'client');
					
					//$form->addElement('text', 'telefono_casa', LABEL_TEL_CASA,' class="textbox" size="30"');
					$form->addElement('text', 'telefono_physical', LABEL_TELEFONO,' class="textbox" size="30"');
					$form->addRule('telefono_physical', LABEL_TELEFONO, 'required', FALSE,'client');
					
					$form->addElement('text', 'fax', LABEL_FAX,' class="textbox" size="30"');
					
					$form->addElement('text', 'telefono_cellulare', LABEL_TEL_MOBILE,' class="textbox" size="50"');
					$form->addRule('telefono_cellulare', LABEL_TEL_MOBILE, 'required', FALSE,'client');
					
					$form->addElement('text', 'email', LABEL_EMAIL,' class="textbox" size="30"');
					$form->addRule('email', LABEL_EMAIL, 'required', FALSE,'client');
					$form->addRule('email', LABEL_EMAIL, 'email', FALSE,'client');
					
					$form->addElement('text', 'professione', LABEL_PROFESSIONE,' class="textbox" size="50"');
					
					$form->addElement('date', 'datadinascita', LABEL_DATA_NASCITA, $optionDate,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_di_nascita\');"');
					$form->addElement('hidden', 'data_di_nascita', '');
					$form->addRule('datadinascita', LABEL_DATA_NASCITA, 'required', FALSE,'client');
					$form->addRule('data_di_nascita', LABEL_DATA_NASCITA, 'required', FALSE,'client');
					
					//$form->addElement('html', '<div class="labelForm">'.LABEL_DATA_NASCITA.'</div><div class="fieldForm">'.(!empty($record['data_di_nascita'])?$func->formatData($record['data_di_nascita'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');
					
					$form->addElement('text', 'citta_nascita', LABEL_CITTA_NASCITA,' class="textbox" size="50"');
					$form->addRule('citta_nascita', LABEL_CITTA_NASCITA, 'required', FALSE,'client');
					
					$form->addElement('select', 'codice_paese_cittadinanza', LABEL_PAESE_NAZIONALITA, $optionPaesi, ' class="textbox" ');
					$form->addRule('codice_paese_cittadinanza', LABEL_PAESE_NAZIONALITA, 'required', FALSE,'client');
					
					$form->addElement('text', 'cognome_da_nubile', LABEL_COGNOME_NUBILE,' class="textbox" size="50"');
					//$form->addRule('cognome_da_nubile', LABEL_COGNOME_NUBILE, 'required', FALSE,'client');
					
					$form->addElement('text', 'nazionalita', LABEL_NAZIONALITA,' class="textbox" size="50"');
					$form->addRule('nazionalita', LABEL_NAZIONALITA, 'required', FALSE,'client');
					
					$sex[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_MASCHIO,'M', ' onclick="this.form.elements[\'sesso\'].value = this.value;"');
					$sex[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_FEMMINA,'F', ' onclick="this.form.elements[\'sesso\'].value = this.value;"');
					$form->addGroup($sex,'sex',LABEL_SEX,'&nbsp;');
					$form->addElement('hidden', 'sesso', 'M');
					$form->addRule('sesso', LABEL_SEX, 'required', FALSE,'client');
					$form->addRule('sex', LABEL_SEX, 'required', FALSE,'client');
					
					$statocivile[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_SINGLE,'single', ' onclick="this.form.elements[\'stato_civile\'].value = this.value;"');
					$statocivile[] =& HTML_QuickForm::createElement('radio',null,null,LABEL_MARRIED,'married', ' onclick="this.form.elements[\'stato_civile\'].value = this.value;" ');
					$form->addGroup($statocivile,'statocivile',LABEL_STATO_CIVILE,'&nbsp;');
					$form->addElement('hidden', 'stato_civile', 'single');
					$form->addRule('stato_civile', LABEL_STATO_CIVILE, 'required', FALSE,'client');
					$form->addRule('statocivile', LABEL_STATO_CIVILE, 'required', FALSE,'client');
					
					
					$form->addElement('header', 'MailingAddress', LABEL_INDIRIZZO_POSTA.' '.LABEL_SE_DIVERSO); // id_tipo_indirizzo = 2
					
					//$form->addElement('checkbox', 'mail_same_physical', '', MAIL_SAME_PHYSICAL,' onclick="popolaMailAddress();"');
					//$form->addElement('text', 'nome_mail', LABEL_NOME2,' class="textbox" size="30"');
					$form->addElement('text', 'indirizzo_mail', LABEL_INDIRIZZO_POBOX,' class="textbox" size="50"');
					$form->addElement('text', 'cap_mail', LABEL_CAP,' class="textbox" size="15"');
					$form->addElement('text', 'citta_mail', LABEL_CITTA,' class="textbox" size="50"');
					$form->addElement('text', 'provincia_stato_mail', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="50"');
					//$form->addElement('text', 'telefono_mail', LABEL_TELEFONO,' class="textbox" size="30"');
					$form->addElement('select', 'codice_paese_mail', LABEL_PAESE, $optionPaesi, ' class="textbox" ');
					
					$form->addElement('header', 'Applicant_document', LABEL_AUTHORIZED_PERSON);
					
					$form->addElement('select', 'id_tipo_documento', LABEL_TIPO_DOCUMENTO, $optionDocumenti, ' class="textbox" ');
					$form->addRule('id_tipo_documento', LABEL_TIPO_DOCUMENTO, 'required', FALSE,'client');
					
					$form->addElement('text', 'codice_documento', LABEL_CODICE_DOCUMENTO,' class="textbox" size="30"');
					$form->addRule('codice_documento', LABEL_CODICE_DOCUMENTO, 'required', FALSE,'client');
					
					$form->addElement('date', 'datarilasciodocumento', LABEL_RILASCIATO_IL, $optionDate2,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_rilascio_documento\');"');
					$form->addElement('hidden', 'data_rilascio_documento', '');
					$form->addRule('datarilasciodocumento', LABEL_RILASCIATO_IL, 'required', FALSE,'client');
					$form->addRule('data_rilascio_documento', LABEL_RILASCIATO_IL, 'required', FALSE,'client');
					
					//$form->addElement('html', '<div class="labelForm">'.LABEL_RILASCIATO_IL.'</div><div class="fieldForm">'.(!empty($record['data_rilascio_documento'])?$func->formatData($record['data_rilascio_documento'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');
					
					$form->addElement('text', 'documento_rilasciato_da', LABEL_DA_RILASCIATO,' class="textbox" size="50"');
					$form->addRule('documento_rilasciato_da', LABEL_DA_RILASCIATO, 'required', FALSE,'client');
					
					$form->addElement('date', 'datascadenzadocumento', LABEL_SCADENZA, $optionDate2,' class="textbox" style="margin-left:2px;" onchange="insertData(\'data_scadenza_documento\');"');
					$form->addElement('hidden', 'data_scadenza_documento', '');
					$form->addRule('datascadenzadocumento', LABEL_SCADENZA, 'required', FALSE,'client');
					$form->addRule('data_scadenza_documento', LABEL_SCADENZA, 'required', FALSE,'client');
					//$form->addElement('html', '<div class="labelForm">'.LABEL_SCADENZA.'</div><div class="fieldForm">'.(!empty($record['data_scadenza_documento'])?$func->formatData($record['data_scadenza_documento'], 'd-m-Y'):'').'</div><div class="clearBoth"></div>');
					
					
					//$form->addElement('select', 'codice_paese_cittadinanza', LABEL_NAZIONALITA, $optionPaesi, ' class="textbox" ');
					
					
					//$form->addElement('text', 'skype', LABEL_SKYPE,' class="textbox" size="60"');
					
					//$form->addRule('codice_paese_cittadinanza', LABEL_NAZIONALITA, 'required', FALSE,'client');
					
					//$form->addElement('header', 'PhysicalAddress', LABEL_INDIRIZZO_FISICO); // id_tipo_indirizzo = 1
					
					//$form->addElement('text', 'nome_physical', LABEL_NOME2,' class="textbox" size="30"');
					//$form->addElement('text', 'telefono_physical', LABEL_TELEFONO,' class="textbox" size="30"');
					
					$form->addElement('textarea', 'note', LABEL_NOTE,' class="textbox" cols="50" rows="5"');
					
					
				break; // end corporate
				
			} // end switch $record['tipo_cliente']
		###########
		/*
		$form->addElement('select', 'id_tipo_titolo', LABEL_TITOLO, $optionTitoli, ' class="textbox" ');
		$form->addElement('text', 'cognome', LABEL_COGNOME,' class="textbox" size="60"');
		$form->addElement('text', 'nome', LABEL_NOME,' class="textbox" size="60"');
		$form->addElement('text', 'ragione_sociale', LABEL_RAGIONE_SOCIALE.ONLY_RAG_SOC,' class="textbox" size="60"');
		
		$form->addElement('date', 'data_di_nascita', LABEL_DATA_NASCITA, $optionDate,' class="textbox" style="margin-left:2px;"');

		$form->addElement('select', 'id_tipo_documento', LABEL_TIPO_DOCUMENTO, $optionDocumenti, ' class="textbox" ');
		$form->addElement('text', 'codice_documento', LABEL_CODICE_DOCUMENTO,' class="textbox" size="60"');

		$form->addElement('select', 'codice_paese_cittadinanza', LABEL_NAZIONALITA, $optionPaesi, ' class="textbox" ');
		$form->addElement('select', 'codice_paese_residenza', LABEL_RESIDENZA, $optionPaesi, ' class="textbox" ');

		$form->addElement('text', 'email', LABEL_EMAIL,' class="textbox" size="60"');
		$form->addElement('text', 'telefono_casa', LABEL_TEL_CASA,' class="textbox" size="60"');
		$form->addElement('text', 'telefono_ufficio', LABEL_TEL_UFFICIO,' class="textbox" size="60"');
		$form->addElement('text', 'telefono_cellulare', LABEL_TEL_MOBILE,' class="textbox" size="60"');
		
		$form->addElement('text', 'skype', LABEL_SKYPE,' class="textbox" size="60"');
		$form->addElement('text', 'fax', LABEL_FAX,' class="textbox" size="60"');

		$form->addRule('cognome', LABEL_COGNOME, 'required', FALSE,'client');
		$form->addRule('nome', LABEL_NOME, 'required', FALSE,'client');
		$form->addRule('data_di_nascita', LABEL_DATA_NASCITA, 'required', FALSE,'client');
		$form->addRule('id_tipo_documento', LABEL_TIPO_DOCUMENTO, 'required', FALSE,'client');
		$form->addRule('codice_documento', LABEL_CODICE_DOCUMENTO, 'required', FALSE,'client');
		$form->addRule('email', LABEL_EMAIL, 'required', FALSE,'client');
		$form->addRule('email', LABEL_EMAIL, 'email', FALSE,'client');
		$form->addRule('codice_paese_cittadinanza', LABEL_NAZIONALITA, 'required', FALSE,'client');
		$form->addRule('codice_paese_residenza', LABEL_RESIDENZA, 'required', FALSE,'client');
		$form->addElement('textarea', 'note', LABEL_NOTE,' class="textbox" cols="60" rows="5"');

		$form->addElement('header', 'PhysicalAddress', LABEL_INDIRIZZO_FISICO); // id_tipo_indirizzo = 1
		
		$form->addElement('text', 'nome_physical', LABEL_NOME2,' class="textbox" size="60"');
		$form->addElement('text', 'indirizzo_physical', LABEL_INDIRIZZO,' class="textbox" size="60"');
		$form->addElement('text', 'cap_physical', LABEL_CAP,' class="textbox" size="60"');
		$form->addElement('text', 'citta_physical', LABEL_CITTA,' class="textbox" size="60"');
		$form->addElement('text', 'provincia_stato_physical', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="60"');
		$form->addElement('text', 'telefono_physical', LABEL_TELEFONO,' class="textbox" size="60"');
		$form->addElement('select', 'codice_paese_physical', LABEL_PAESE, $optionPaesi, ' class="textbox" ');

		$form->addElement('header', 'MailingAddress', LABEL_INDIRIZZO_POSTA); // id_tipo_indirizzo = 2
		
		$form->addElement('checkbox', 'mail_same_physical', '', MAIL_SAME_PHYSICAL,' onclick="popolaMailAddress();"');
		$form->addElement('text', 'nome_mail', LABEL_NOME2,' class="textbox" size="60"');
		$form->addElement('text', 'indirizzo_mail', LABEL_INDIRIZZO,' class="textbox" size="60"');
		$form->addElement('text', 'cap_mail', LABEL_CAP,' class="textbox" size="60"');
		$form->addElement('text', 'citta_mail', LABEL_CITTA,' class="textbox" size="60"');
		$form->addElement('text', 'provincia_stato_mail', LABEL_PROVINCIA_STATO_REGIONE,' class="textbox" size="60"');
		$form->addElement('text', 'telefono_mail', LABEL_TELEFONO,' class="textbox" size="60"');
		$form->addElement('select', 'codice_paese_mail', LABEL_PAESE, $optionPaesi, ' class="textbox" ');
*/
		$form->addElement('header', 'ProfileSettings', LABEL_PROFILO_UTENTE);
		
		$form->addElement('text', 'user', USERNAME,' class="textbox" size="25" maxlength="15"');
	
		$form->addElement('text', 'segnalatore', LABEL_SEGNALATORE,' class="textbox" size="25""');
	
	
		$group_psw[]=&HTML_QuickForm::createElement('text', 'password', LABEL_PASSWORD,' class="textbox" size="25" maxlength="'.($record['id_tipo_utente']==1?'12':'6').'"');
		if (empty($record['psw']['password'])) $group_psw[]=&HTML_QuickForm::createElement('checkbox', 'genera', null, GENERA_ORA, ' onclick="generaPsw(this.form.name);"');
	
		$form->addGroup($group_psw, 'psw', LABEL_PASSWORD, '&nbsp;');
		$form->addGroupRule('psw', LABEL_PASSWORD, 'required', null, 1, 'client', true);	
		
		if (!empty($record['data_aggiornamento_psw'])) $form->addElement('text', 'data_aggiornamento_psw', LABEL_AGG_PSW,' class="textbox" size="25" disabled="disabled"');
		
		if (!empty($record['chiave'])) $form->addElement('text', 'chiave', LABEL_CHIAVE,' class="textbox" size="25" maxlength="6"');
		else $form->addElement('html', '<div class="labelForm">'.LABEL_CHIAVE.'</div><div class="fieldForm">'.GENERAZIONE_AUTOMATICA.'</div><div class="clearBoth"></div>');
		
		$form->addElement('select', 'id_tipo_utente', LABEL_TIPO_UTENTE, $optionTipoUtente, ' class="textbox" '.($record['id_cliente']==1?' disabled="disabled"':''));
		
		// a group of checkboxes
		//foreach ($optionGruppiUtente as $id => $nome) $checkbox[] = &HTML_QuickForm::createElement('checkbox', $id, null, $nome);
		//$form->addGroup($checkbox, 'clienti_tipi_gruppi', LABEL_GRUPPI_UTENTE, '<br />');
		//$form->addGroupRule('clienti_tipi_gruppi', LABEL_GRUPPI_UTENTE_JS, 'required', null, 1, 'client', true);	clienti_tipi_gruppi
		$form->addElement('hidden', 'clienti_tipi_gruppi', '3');
		
		$form->addRule('user', USERNAME, 'required', FALSE,'client');
		$form->addRule('psw', LABEL_PASSWORD, 'required', FALSE,'client');
		$form->addRule('chiave', LABEL_CHIAVE, 'required', FALSE,'client');
		$form->addRule('chiave', LABEL_CHIAVE_RANGE, 'rangelength',array(6,6), 'client');
		$form->addRule('chiave', LABEL_CHIAVE_CHARS, 'alphanumeric', 'client');
		$form->addRule('id_tipo_utente', LABEL_TIPO_UTENTE, 'required', FALSE,'client');
	
		$form->addElement('textarea', 'note_admin', LABEL_NOTE_AMMINISTRATORE,' class="textbox" cols="60" rows="5"');


		$form->setDefaults($record); // da tabella
	
		
		$form->setRequiredNote(REQUIRED_FIELDS);
		$form->setJsWarnings(ERRORE_JS,'');
		
		// CREO I PULSANTI
		$buttons[]=&HTML_QuickForm::createElement('submit', 'btnSubmit', SAVE,'class="button"');
		$buttons[]=&HTML_QuickForm::createElement('reset', 'btnClear', RESET,'class="button"');
		
		if ($_SESSION['utente']['id_tipo_utente']==1) $buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list\';"');
		
		$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
	
		// mostro il form
		$form->display();
	
	

	break;
	
	case "upd": // insert / update
			
			echo '<h1>'.UPDATE.'</h1>';

		//	echo '<pre>';
		//	print_r($_REQUEST);
		//	echo '</pre>';
		//	exit();
			
			$data = $_REQUEST;
			$data['username'] = $_REQUEST['user'];
			$data['password'] = $_REQUEST['psw']['password'];
			
			# date
			$data['data_di_nascita']=(!empty($_REQUEST['data_di_nascita']['Y']) && !empty($_REQUEST['data_di_nascita']['m']) && !empty($_REQUEST['data_di_nascita']['d'])?$_REQUEST['data_di_nascita']['Y'].'-'.$_REQUEST['data_di_nascita']['m'].'-'.$_REQUEST['data_di_nascita']['d']:null);
			
			if (empty($data['chiave'])) $data['chiave']=$zealandCredit->generaChiave(6);
			
			if (!empty($_REQUEST['id_cliente'])) {
				
				if ($_REQUEST['psw']['password']!=$_REQUEST['old_psw']) 
					$data['data_aggiornamento_psw']=date("Y-m-d H:i:s");
					
				// ***LOG CAMBIO DATI DI ACCESSO (ale balducci 08/03/2012) 
				//gestione personalizzata in caso di cambio username e pwd --> metto nel log i nuovi dati
				if ($data['username'] != $_REQUEST['old_username'] or $data['password'] != $_REQUEST['old_psw'])
				{
					$sql_account = "insert into log_modaccount(id_utente,new_username,new_pwd) values(".$_REQUEST['id_cliente'].",'".$data['username']."','".$data['password']."')";

					$db->query($sql_account);
				}
					
					
				 
				# update
				// clienti
			
				$query=$sql->prepareQuery ('clienti', $data, 'update', "id_cliente='".$_REQUEST['id_cliente']."'");
				//echo '<!--'.$query.'-->';
				$res=$db->query($query);
				$lastid=$_REQUEST['id_cliente'];
				
				// gruppi utenti
				// cancello
				$query="delete from clienti_tipi_gruppi where id_cliente='".$_REQUEST['id_cliente']."'";
				//echo '<!--'.$query.'-->';
				$res=$db->query($query);
				
			} else {
				# insert
				
				$data['data_aggiornamento_psw']=date("Y-m-d H:i:s");
				$query=$sql->prepareQuery ('clienti', $data, 'insert');
				echo '<!--'.$query.'-->';
				$res=$db->query($query);
				$lastid=mysql_insert_id();
			}
			
			# clienti_tipi_gruppi
			$data=array();
			$data['id_cliente']=$lastid;
			$data['id_tipo_gruppo']=$_REQUEST['clienti_tipi_gruppi'];
			$query=$sql->prepareQuery ('clienti_tipi_gruppi', $data, 'insert');
			

			# se ho modificato il profilo dell'utente, lo ricarico in sessione
			if ($_SESSION['utente']['id_cliente'] == $_REQUEST['id_cliente']) {
			
				$queryAut="select * from tipi_utenti_lingue as tul, tipi_utenti as tu, clienti as cli where cli.id_cliente='".$_SESSION['utente']['id_cliente']."' and tul.id_tipo_utente=tu.id_tipo_utente and tul.sigla_lingua=cli.sigla_lingua and tul.id_tipo_utente=cli.id_tipo_utente";
				$res=$db->query($queryAut);
				$record = $res->fetchRow();
				$_SESSION['utente']=$record;
				
			}
			
			$goPage->alertgo(UPDATE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');

	break;
	
	case "del": // delete
		echo '<h1>'.DELETE.'</h1>';
		if (!empty($_REQUEST['id_cliente'])) {
			
			$query="delete from clienti where id_cliente='".$_REQUEST['id_cliente']."'";
			$res=$db->query($query);
				
			$query="delete from clienti_tipi_gruppi where id_cliente='".$_REQUEST['id_cliente']."'";
			$res=$db->query($query);
			
			$goPage->alertgo(DELETE_OK, 'index.php?page='.$_REQUEST['page'].'&act=list');
			
		} else $goPage->alertback(NO_RECORD, false);
	
	break;
	
	case "attiva": // attiva utente
		
		echo '<h1>'.ATTIVA_CLIENTE.'</h1>';
		if (!empty($_REQUEST['id_cliente'])) {
	
			$data=array();
			
			// modifica tabella
			$data['attivo']=1;
			$data['data_attivazione']=date("Y-m-d H:i:s");
			$query=$sql->prepareQuery ('clienti', $data, 'update', "id_cliente='".$_REQUEST['id_cliente']."'");
			//echo '<br />'.$query;
			$res=$db->query($query);
			
			// invio email attivazione
			# dati cliente
			$qry="select * from clienti as cli where cli.id_cliente='".$_REQUEST['id_cliente']."'";
			$res=$db->query($qry);
			$record = $res->fetchRow();
			
			// salvo la comunicazione in db
			$data=array();
			$data['id_cliente_mittente']=1; // admin
			$data['id_cliente_destinatario']=$record['id_cliente'];
			$data['email_mittente']=$param['from'];
			$data['email_destinatario']=$param['to'];
			$data['oggetto_messaggio']=$param['subject'];
			$data['testo_messaggio']=$dati;
			$data['stato_messaggio_mittente']='I';
			$data['stato_messaggio_destinatario']='N';
			$data['sigla_lingua']=$_SESSION['lingua'];
			$data['data_creazione_messaggio']=date("Y-m-d H:i:s");
			$data['data_invio_messaggio']=date("Y-m-d H:i:s");
			$data['notifica_automatica']=1;
			$data['notifica_email']=1;
			
			$query=$sql->prepareQuery ('messaggi', $data, 'insert');
			//echo '<br />'.$query;
			//$res=$db->query($query);

	//		$goPage->alertgo(ATTIVAZIONE_OK_CLIENTE, 'index.php?page='.$_REQUEST['page'].'&act=list');
		
			// torno alle richieste pendenti
			$goPage->alertgo(ATTIVAZIONE_OK_CLIENTE, 'index.php?page='.(!empty($_REQUEST['pageR'])?$_REQUEST['pageR'].'&request_type='.$_REQUEST['request_type']:$_REQUEST['page']).'&act=list');
		
			
		} else $goPage->alertback(NO_RECORD, false);
	
	break;
	
	default: // list
	
			
		echo '<h1>'.LIST_TABLE;
		//echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form" title="'.INSERT_NEW.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.INSERT_NEW.'" /></a>';
		echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;tipo_cliente=single-personal" title="'.INSERT_NEW_PERSONAL.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.INSERT_NEW_PERSONAL.'" /></a> <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;tipo_cliente=single-personal" title="'.INSERT_NEW_PERSONAL.'">'.INSERT_NEW_PERSONAL.'</a>';
		
		echo ' <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;tipo_cliente=corporate" title="'.INSERT_NEW_CORPORATE.'"><img src="'.$path_web.'img/icone/file_new.png" width="24" height="24" style="border: 0px; margin:2px; display:inline; vertical-align:middle" alt="'.INSERT_NEW_CORPORATE.'" /></a> <a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;tipo_cliente=corporate" title="'.INSERT_NEW_CORPORATE.'">'.INSERT_NEW_CORPORATE.'</a>';
		echo '</h1>';
		
		
		
		// visualizzo solo quelli gi� attivi
		$qry="select * from clienti as cli".(!empty($_REQUEST['filtra_tipo_gruppo']) && $_REQUEST['filtra_tipo_gruppo']!='*'?" left outer join clienti_tipi_gruppi as ctg on cli.id_cliente=ctg.id_cliente ":'')." where (cli.attivo=1)";
		
		# filtri
		$qry .=(!empty($_REQUEST['filtra_nome_cognome']) && $_REQUEST['filtra_nome_cognome']!='*'?" and (cli.cognome like '%".$_REQUEST['filtra_nome_cognome']."%' or cli.nome like '%".$_REQUEST['filtra_nome_cognome']."%')":'');

		$qry .=(!empty($_REQUEST['filtra_ragione_sociale']) && $_REQUEST['filtra_ragione_sociale']!='*'?" and cli.ragione_sociale like '%".$_REQUEST['filtra_ragione_sociale']."%'":'');

		$qry .=(!empty($_REQUEST['filtra_tipo_utente']) && $_REQUEST['filtra_tipo_utente']!='*'?" and cli.id_tipo_utente = ".$_REQUEST['filtra_tipo_utente']."":'');

		$qry .=(!empty($_REQUEST['filtra_tipo_gruppo']) && $_REQUEST['filtra_tipo_gruppo']!='*'?" and ctg.id_tipo_gruppo = ".$_REQUEST['filtra_tipo_gruppo']."":'');

		# order
		$orderby=(!empty($_REQUEST['orderby'])?$_REQUEST['orderby']:'cli.cognome, cli.nome, cli.ragione_sociale');
		$qry .="  order by ".$orderby;

		$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list';
		$link_extra_param .=(!empty($_REQUEST['filtra_nome_cognome']) && $_REQUEST['filtra_nome_cognome']!='*'?"&amp;filtra_nome_cognome=".$_REQUEST['filtra_nome_cognome']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_ragione_sociale']) && $_REQUEST['filtra_ragione_sociale']!='*'?"&amp;filtra_ragione_sociale=".$_REQUEST['filtra_ragione_sociale']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_utente']) && $_REQUEST['filtra_tipo_utente']!='*'?"&amp;filtra_tipo_utente=".$_REQUEST['filtra_tipo_utente']:'');
		$link_extra_param .=(!empty($_REQUEST['filtra_tipo_gruppo']) && $_REQUEST['filtra_tipo_gruppo']!='*'?"&amp;filtra_tipo_gruppo=".$_REQUEST['filtra_tipo_gruppo']:'');

			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
			
			echo $view_links;
		?><br /><br />
		<form name="fFast_upd" method="post" action="index.php" onsubmit="return controllaForm('fFast_upd');">
		<input type="hidden" name="act" value="fast_upd" />
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
		<input type="hidden" name="filter" value="0" />
		<table class="tblAdmin">
		<tr>
			<th style="width:30px;"></th>
			<th style="width:30px;"></th>
			<th><a href="index.php?<?php echo $link_extra_param; ?>&amp;orderby=cli.cognome, cli.nome" title="order by lastname and name" class="linkBold"><?php echo LABEL_FULL_NAME; ?></a></th>
			<th><a href="index.php?<?php echo $link_extra_param; ?>&amp;orderby=cli.ragione_sociale" title="order by business name" class="linkBold"><?php echo LABEL_RAGIONE_SOCIALE; ?></a></th>
			<th><?php echo LABEL_TIPO_UTENTE; ?></th>
			<th><?php echo LABEL_GRUPPI_UTENTE; ?></th>
			<th style="width:30px;"><?php echo LABEL_VALUTA; ?></th>
            <th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
		</tr>
        
        <!-- filtri -->
        <tr>
            <td colspan="2" style="text-align:center; padding:4px;">
           
            
            <input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
            <td><?php
            // cognome / nome
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_nome_cognome']?>" name="filtra_nome_cognome" class="textbox" size="15" /></td>
            <td><?php
            // ragione_sociale
            ?>
            <input type="text" value="<?php echo $_REQUEST['filtra_ragione_sociale']?>" name="filtra_ragione_sociale" class="textbox" size="15" /></td>
            <td><?php
            // id_tipo_utente            
            ?><select name="filtra_tipo_utente" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionTipoUtente as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo_utente']) && $_REQUEST['filtra_tipo_utente']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
            <td><?php
            // id_tipo_gruppo            
            ?><select name="filtra_tipo_gruppo" class="textbox">
                <option value="*"><?php echo LABEL_TUTTI; ?></option>
                <?php
                foreach ($optionGruppiUtente as $id => $nome) echo '<option value="'.$id.'"'.(!empty($_REQUEST['filtra_tipo_gruppo']) && $_REQUEST['filtra_tipo_gruppo']==$id?' selected':'').'>'.$nome.'</option>';
                ?>
            </select></td>
			
            <td></td>
            
        </tr>
	<?php    
		$class="tdRow1";
		while ($record =& $res->fetchRow()) {
		//	print_r($record);
			echo '
			<tr>
				<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=form&amp;id_cliente='.$record['id_cliente'].'" title="'.VIEW_MOD.'"><img src="'.$path_web.'img/icone/file_edit.png" width="24" height="24" style="border: 0px;" alt="'.VIEW_MOD.'" /></a></td>
				<td class="'.$class.'">';
			
			// non posso eliminare l'admin
			if ($record['id_cliente']!=1) echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;act=del&amp;id_cliente=".$record['id_cliente']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/file_del.png\" width=\"24\" height=\"24\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
			
			echo "</td>";
			
			// gruppi utente
			$qry2="select id_tipo_gruppo from clienti_tipi_gruppi where id_cliente=".$record['id_cliente'];
			$res2=$db->query($qry2);
				
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res2) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry2."."; die($res2->getMessage()); 
			}			
			
			$gruppi_utente='';
			$v='';
			while ($record2 =& $res2->fetchRow()) {
				$gruppi_utente .=$v.$optionGruppiUtente[$record2['id_tipo_gruppo']];
				$v=', ';
			}
			
			echo "\n".'<td class="'.$class.'">'.$record['cognome'].' '.$record['nome'].'</td>';
			echo '
				<td class="'.$class.'">'.$record['ragione_sociale'].'</td>
				<td class="'.$class.'">'.$optionTipoUtente[$record['id_tipo_utente']].'</td>
				<td class="'.$class.'">'.$gruppi_utente.'</td>
				<td class="'.$class.'">'. $arrayValute[$record['id_valuta_richiesta']].'</td>
				<td class="'.$class.'">'.($record['attivo']==1?'<img src="'.$path_web.'img/icone/ball_green.png" width="24" height="24" style="border: 0px;" alt="'.ATTIVO_CLIENTE.'" title="'.ATTIVO_CLIENTE.'" />':'<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;act=attiva&amp;id_cliente='.$record['id_cliente'].'" onclick="if (confirm(confirm_attiva_cliente)) window.open(this.href, \'_self\', \'\'); return false;" title="'.NON_ATTIVO_CLIENTE.'-'.ATTIVA_CLIENTE.'"><img src="'.$path_web.'img/icone/ball_gray.png" width="24" height="24" style="border: 0px;" alt="'.NON_ATTIVO_CLIENTE.'-'.ATTIVA_CLIENTE.'" title="'.NON_ATTIVO_CLIENTE.'-'.ATTIVA_CLIENTE.'" /></a>').'</td>
			</tr>';
			
			$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
			
		} // end while
	?>
		</table>
		</form>
	
	<?php	
			echo $view_links;

		} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
			
}

?>
