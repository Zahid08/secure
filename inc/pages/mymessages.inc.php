<?php
# messages.inc.php
if ( !strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']],'('.$page.')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']]!='all') {
	$goPage->alertback(ACCESSO_NEGATO, false);
	//header("Location:index.php");
	exit();
}

echo '<div class="titoloAdmin">'.LINK_MENU_SN_MY_MESSAGES.'</div>';

echo '<div style="text-align:right;">';
echo '<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;msg_type=sent" title="'.SENT_MES.'">'.SENT_MES.'</a> | ';
echo '<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;msg_type=inbox" title="'.INBOX_MES.'">'.INBOX_MES.'</a> | ';
echo '<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;msg_type=draft" title="'.DRAFT_MES.'">'.DRAFT_MES.'</a> | ';
echo '<a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;msg_type=trash" title="'.TRASH_MES.'">'.TRASH_MES.'</a>';
echo '</div>';
echo '<div style="text-align:right;"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;msg_type=sent&amp;act=form" title="'.LABEL_SCRIVI.'"><img src="'.$path_web.'img/icone/file_document.png" width="24" height="24" border="0" alt="'.LABEL_SCRIVI.'" /></a></div>';


$optionClienti = $zealandCredit->getClienti($stato_opzioni);
switch ($_REQUEST['act']) {

	case "read": // form read
	
		if (!empty($_REQUEST['id_messaggio'])) {
			echo '<h1>'.LABEL_LEGGI_MSG.'</h1>';
			$qry="select * from messaggi where id_messaggio=".$_REQUEST['id_messaggio'];
			$qry .=($_REQUEST['msg_type']=='inbox'?" and id_cliente_destinatario = ".$_SESSION['utente']['id_cliente']:'');
			$qry .=($_REQUEST['msg_type']=='sent' || $_REQUEST['msg_type']=='draft'?" and id_cliente_mittente = ".$_SESSION['utente']['id_cliente']:'');
			$qry .=($_REQUEST['msg_type']=='trash'?" and (id_cliente_mittente = ".$_SESSION['utente']['id_cliente']." or id_cliente_destinatario = ".$_SESSION['utente']['id_cliente']:'');

			$res=$db->query($qry);
			$rows=$res->numRows();
			if ($rows==0) { 
				$goPage->alertback(ACCESSO_NEGATO, false);
				exit();
			} else {
				$record = $res->fetchRow();
				
				switch ($_REQUEST['msg_type']) {
					case "sent":
						$record['data_creazione_messaggio']=$func->formatData($record['data_creazione_messaggio'], "d-m-Y H:i");
						$record['data_invio_messaggio']=$func->formatData($record['data_invio_messaggio'], "d-m-Y H:i");
						$record['data_lettura_messaggio']=$func->formatData($record['data_lettura_messaggio'], "d-m-Y H:i");
					break;
					case "inbox":
						$record['data_invio_messaggio']=$func->formatData($record['data_invio_messaggio'], "d-m-Y H:i");
						$record['data_lettura_messaggio']=$func->formatData($record['data_lettura_messaggio'], "d-m-Y H:i");
						if ($record['stato_messaggio_destinatario']=='N') {
							$record['stato_messaggio_destinatario']='L';
							$record['data_lettura_messaggio']=date("d-m-Y H:i");
							
							$query2="update messaggi set data_lettura_messaggio='".date("Y-m-d H:i:s")."', stato_messaggio_destinatario='L' where id_messaggio=".$_REQUEST['id_messaggio'];
							$res2=$db->query($query2);
						}
					break;
					case "draft":
						$record['data_creazione_messaggio']=$func->formatData($record['data_creazione_messaggio'], "d-m-Y H:i");
					break;
					case "trash":
						$record['data_creazione_messaggio']=$func->formatData($record['data_creazione_messaggio'], "d-m-Y H:i");
						$record['data_invio_messaggio']=$func->formatData($record['data_invio_messaggio'], "d-m-Y H:i");
						$record['data_lettura_messaggio']=$func->formatData($record['data_lettura_messaggio'], "d-m-Y H:i");
					break;
				}
				$record['testo_messaggio']=str_replace('<br />', "\n", $record['testo_messaggio']);
				
				$form = new HTML_QuickForm('FormUpdate', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
				$form->addElement('hidden', 'id_messaggio', $_REQUEST['id_messaggio']);
				$form->addElement('hidden', 'act', 'form');
				$form->addElement('hidden', 'msg_type', $_REQUEST['msg_type']);
				$form->addElement('hidden', 'page', $_REQUEST['page']);
				$form->addElement('hidden', 'id_messaggio_risposta', $_REQUEST['id_messaggio']);
				$form->addElement('hidden', 'email_mittente', $record['email_mittente']);
				$form->addElement('hidden', 'email_destinatario', $record['email_destinatario']); // sempre ad admin
				//$form->addElement('hidden', 'id_cliente_mittente', $record['id_cliente_mittente']);
				//$form->addElement('hidden', 'id_cliente_destinatario', $record['id_cliente_destinatario']);
	
				$form->addElement('select', 'id_cliente_mittente', FROM, $optionClienti, ' class="textbox"');
				$form->addElement('select', 'id_cliente_destinatario', TO, $optionClienti, ' class="textbox"');
	
				$form->addElement('text', 'data_creazione_messaggio', LABEL_DATA_CREAZIONE, ' class="textbox"');
				$form->addElement('text', 'data_invio_messaggio', LABEL_DATA_INVIO, ' class="textbox"');
				$form->addElement('text', 'data_lettura_messaggio', LABEL_DATA_LETTURA, ' class="textbox"');
	
				$form->addElement('select', 'notifica_automatica', LABEL_NOTIFICA_AUTOMATICA, $optionBool, ' class="textbox"');
				$form->addElement('checkbox', 'notifica_email', '', LABEL_EMAIL);
	
	
				$form->addElement('text', 'oggetto_messaggio', OBJECT, ' class="textbox"');
				$form->addElement('textarea', 'testo_messaggio', TEXT, ' class="textbox" rows="10" cols="60"');
				$form->setDefaults($record); // da tabella
				
				// CREO I PULSANTI
				if ($record['id_cliente_mittente']!=$_SESSION['utente']['id_cliente']) $buttons[]=&HTML_QuickForm::createElement('button', 'btnAnswer', LABEL_RISPONDI, 'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=form&msg_type='.$_REQUEST['msg_type'].'&id_messaggio='.$_REQUEST['id_messaggio'].'&answer=1\';"');
				$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST, 'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list&msg_type='.$_REQUEST['msg_type'].'\';"');
				
				$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
			
				$form->freeze(); 
				// mostro il form
				$form->display();
			}
		} else $goPage->alertback(NO_RECORD, false);
	break;

	case "form": // form new / mod
	
		if (!empty($_REQUEST['id_messaggio'])) {
			$qry="select * from messaggi where id_messaggio=".$_REQUEST['id_messaggio'];

			$qry .=($_REQUEST['msg_type']=='inbox'?" and id_cliente_destinatario = ".$_SESSION['utente']['id_cliente']:'');
			$qry .=($_REQUEST['msg_type']=='sent' || $_REQUEST['msg_type']=='draft'?" and id_cliente_mittente = ".$_SESSION['utente']['id_cliente']:'');
			$qry .=($_REQUEST['msg_type']=='trash'?" and (id_cliente_mittente = ".$_SESSION['utente']['id_cliente']." or id_cliente_destinatario = ".$_SESSION['utente']['id_cliente']:'');

			$res=$db->query($qry);
			$record2 = $res->fetchRow();
			
			$rows=$res->numRows();
			if ($rows==0) { 
				$goPage->alertback(ACCESSO_NEGATO, false);
				exit();
			} else {
				$record2['testo_messaggio']=str_replace('<br />', "\n", $record2['testo_messaggio']);
				
				if ($_REQUEST['answer']==1) {
					echo '<h1>'.LABEL_RISPONDI.'</h1>';
					$record['id_messaggio_risposta'] = $_REQUEST['id_messaggio'];
					$record['id_cliente_mittente'] = $_SESSION['utente']['id_cliente'];
					$record['email_mittente'] = $_SESSION['utente']['email'];
					$record['id_cliente_destinatario'] = $record2['id_cliente_mittente'];
					$record['email_destinatario'] = $record2['email_mittente'];
					$record['id_messaggio']='';
					$record['testo_messaggio']="\n\n\n".'----------------'."\n".$record2['testo_messaggio'];
					$record['oggetto_messaggio']='RE: '.$record2['oggetto_messaggio'];
				} else {
					echo '<h1>'.MODIFY.'</h1>';
					$record=$record2;
				}
			}
		} else {
			echo '<h1>'.LABEL_SCRIVI.'</h1>';
		}
			
			
			$form = new HTML_QuickForm('FormUpdate', 'post',$_SERVER['SCRIPT_NAME'], '', '', false);
			$form->addElement('hidden', 'page', $_REQUEST['page']);
			$form->addElement('hidden', 'id_messaggio', $_REQUEST['id_messaggio']);
			$form->addElement('hidden', 'id_cliente_mittente', $_SESSION['utente']['id_cliente']);
			$form->addElement('hidden', 'id_messaggio_risposta', $_REQUEST['id_messaggio_risposta']);
			$form->addElement('hidden', 'id_cliente_destinatario', '1'); // sempre ad admin
			$form->addElement('hidden', 'email_mittente', $_SESSION['utente']['email']);
			$form->addElement('hidden', 'email_destinatario', $mail_admin); // sempre ad admin
			$form->addElement('hidden', 'act', 'upd');
			$form->addElement('hidden', 'msg_type', $_REQUEST['msg_type']);

			$form->addElement('text', 'oggetto_messaggio', OBJECT, ' class="textbox" size="60"');
			$form->addElement('textarea', 'testo_messaggio', TEXT, ' class="textbox" rows="10" cols="60"');
			$form->addElement('checkbox', 'notifica_email', '', LABEL_EMAIL);
			
			$form->addRule('oggetto_messaggio', OBJECT, 'required', FALSE,'client');
			$form->addRule('testo_messaggio', TEXT, 'required', FALSE,'client');

			$form->setDefaults($record); // da tabella
			
			// CREO I PULSANTI
			$buttons[]=&HTML_QuickForm::createElement('submit', 'btnInvia', LABEL_INVIA,'class="button"');
			$buttons[]=&HTML_QuickForm::createElement('button', 'btnDraft', SAVE_DRAFT,'class="button" onClick="document.FormUpdate.act.value=\'save_draft\'; document.FormUpdate.submit();"');
			$buttons[]=&HTML_QuickForm::createElement('button', 'btnBack', BACK_LIST,'class="button" onClick="window.location=\'index.php?page='.$_REQUEST['page'].'&act=list&msg_type='.$_REQUEST['msg_type'].'\';"');
			
			$form->addGroup($buttons,'bottoniera',null,'&nbsp;&nbsp;');
			
			$form->setRequiredNote(REQUIRED_FIELDS);
			$form->setJsWarnings(ERRORE_JS,'');
		
			// mostro il form
			$form->display();
	
	break;
	
	case "upd": // invio
	
		echo '<h1>'.UPDATE.'</h1>';
		
		$_REQUEST['data_creazione_messaggio'] = (!empty($_REQUEST['data_creazione_messaggio'])?$_REQUEST['data_creazione_messaggio']:date("Y-m-d H:i:s"));
		$_REQUEST['data_invio_messaggio'] = date("Y-m-d H:i:s");
		$_REQUEST['sigla_lingua'] = $_SESSION['lingua'];

		$_REQUEST['stato_messaggio_mittente'] = 'I';
		$_REQUEST['stato_messaggio_destinatario'] = 'N';
		
		if (!empty($_REQUEST['id_messaggio'])) {
		
		
			# prima verifico se l'utente ha i permessi di modifica
			$qry="select * from messaggi where id_messaggio=".$_REQUEST['id_messaggio']." and (id_cliente_destinatario = ".$_SESSION['utente']['id_cliente']." and id_cliente_mittente=1) or (id_cliente_mittente = ".$_SESSION['utente']['id_cliente']." and id_cliente_destinatario=1)";

			$res=$db->query($qry);
			$rows=$res->numRows();
			if ($rows==0) { 
				$goPage->alertback(ACCESSO_NEGATO, false);
				exit();
			}
			
		
			# update
			$query=$sql->prepareQuery ('messaggi', $_REQUEST, 'update', "id_messaggio='".$_REQUEST['id_messaggio']."'");
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=$_REQUEST['id_messaggio'];
		} else {
			# insert
			$query=$sql->prepareQuery ('messaggi', $_REQUEST, 'insert');
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=mysql_insert_id();
		}		
		
		if ($_REQUEST['notifica_email']) {
			//require_once ($path_www."class/mail.class.php");
			
			
			//$titolo = $_REQUEST['oggetto_messaggio'];
		
			$headers = array();
	
			$headers['to_name']=$name_admin;
			$headers['to_email']=$mail_admin;
			
			$headers['from_name']=$_SESSION['utente']['nome'].(!empty($_SESSION['utente']['cognome'])?' '.$_SESSION['utente']['cognome']:'').(!empty($_SESSION['utente']['ragione_sociale'])?' - '.$_SESSION['utente']['ragione_sociale']:'');
			
			$headers['from_email']=$_SESSION['utente']['email'];
			
			$headers['return_email']=$_SESSION['utente']['email'];
			$headers['reply_email']=$_SESSION['utente']['email'];
			$headers['reply_name']=$_SESSION['utente']['nome'].(!empty($_SESSION['utente']['cognome'])?' '.$_SESSION['utente']['cognome']:'').(!empty($_SESSION['utente']['ragione_sociale'])?' - '.$_SESSION['utente']['ragione_sociale']:'');
			
			$template_email = file_get_contents($path_www.'templates//email_post_richiesta_generica.html');
			$messaggio = str_replace('[CONTENUTO]',$dati, $template_email);
			
			$inviata = $func->sendMail($headers,$_REQUEST['oggetto_messaggio'],$messaggio,array($path_www.'allegati/'.$_REQUEST['allegato'] => $_REQUEST['allegato']));
			
		}
		
		$goPage->go_to('index.php?page='.$_REQUEST['page'].'&msg_type=inbox&act=list');
		
	break;
	
	case "save_draft": // save_draft

		$_REQUEST['stato_messaggio_mittente'] = 'D';
		$_REQUEST['data_creazione_messaggio'] = date("Y-m-d H:i:s");
		$_REQUEST['sigla_lingua'] = $_SESSION['lingua'];
		
		echo '<h1>'.UPDATE.'</h1>';
		if (!empty($_REQUEST['id_messaggio'])) {


			# prima verifico se l'utente ha i permessi di modifica
			$qry="select * from messaggi where id_messaggio=".$_REQUEST['id_messaggio']." and (id_cliente_destinatario = ".$_SESSION['utente']['id_cliente']." and id_cliente_mittente=1) or (id_cliente_mittente = ".$_SESSION['utente']['id_cliente']." and id_cliente_destinatario=1)";

			$res=$db->query($qry);
			$rows=$res->numRows();
			if ($rows==0) { 
				$goPage->alertback(ACCESSO_NEGATO, false);
				exit();
			}



			# update
			$query=$sql->prepareQuery ('messaggi', $_REQUEST, 'update', "id_messaggio='".$_REQUEST['id_messaggio']."'");
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=$_REQUEST['id_messaggio'];
		} else {
			# insert
			$query=$sql->prepareQuery ('messaggi', $_REQUEST, 'insert');
			//echo '<br />'.$query;
			$res=$db->query($query);
			$lastid=mysql_insert_id();
		}	
		$goPage->go_to('index.php?page='.$_REQUEST['page'].'&msg_type=draft&act=list');
			
	break;
	
	case "del": // delete => stato = C
		
		if (!empty($_REQUEST['id_messaggio'])) {

			# prima verifico se l'utente ha i permessi di modifica
			$qry="select * from messaggi where id_messaggio=".$_REQUEST['id_messaggio']." and (id_cliente_destinatario = ".$_SESSION['utente']['id_cliente']." and id_cliente_mittente=1) or (id_cliente_mittente = ".$_SESSION['utente']['id_cliente']." and id_cliente_destinatario=1)";

			$res=$db->query($qry);
			$rows=$res->numRows();
			if ($rows==0) { 
				$goPage->alertback(ACCESSO_NEGATO, false);
				exit();
			}

			$qry="select * from messaggi where id_messaggio=".$_REQUEST['id_messaggio'];
			$res=$db->query($qry);
			$record = $res->fetchRow();
			echo '<h1>'.DELETE.'</h1>';
			if ($record['id_cliente_mittente'] == $_SESSION['utente']['id_cliente']) 
				$qry="update messaggi set stato_messaggio_mittente='E' where id_messaggio=".$_REQUEST['id_messaggio'];
			else
				$qry="update messaggi set stato_messaggio_destinatario='E' where id_messaggio=".$_REQUEST['id_messaggio'];
				
			$res=$db->query($qry);
				$goPage->go_to('index.php?page='.$_REQUEST['page'].'&msg_type='.$_REQUEST['msg_type'].'&act=list');
		} else $goPage->alertback(NO_RECORD, false);
	
	break;
	
	case "trash": // trash => stato = C
		
		if (!empty($_REQUEST['id_messaggio'])) {
			# prima verifico se l'utente ha i permessi di modifica
			$qry="select * from messaggi where id_messaggio=".$_REQUEST['id_messaggio']." and (id_cliente_destinatario = ".$_SESSION['utente']['id_cliente']." and id_cliente_mittente=1) or (id_cliente_mittente = ".$_SESSION['utente']['id_cliente']." and id_cliente_destinatario=1)";

			$res=$db->query($qry);
			$rows=$res->numRows();
			if ($rows==0) { 
				$goPage->alertback(ACCESSO_NEGATO, false);
				exit();
			}
			echo '<h1>'.TRASH_MES.'</h1>';
			$qry="select * from messaggi where id_messaggio=".$_REQUEST['id_messaggio'];
			$res=$db->query($qry);
			$record = $res->fetchRow();
			echo '<h1>'.DELETE.'</h1>';
			if ($record['id_cliente_mittente'] == $_SESSION['utente']['id_cliente']) 
				$qry="update messaggi set stato_messaggio_mittente='C' where id_messaggio=".$_REQUEST['id_messaggio'];
			else
				$qry="update messaggi set stato_messaggio_destinatario='C' where id_messaggio=".$_REQUEST['id_messaggio'];
				
			$res=$db->query($qry);
			$goPage->go_to('index.php?page='.$_REQUEST['page'].'&msg_type='.$_REQUEST['msg_type'].'&act=list');
		} else $goPage->alertback(NO_RECORD, false);
	
	break;
	
	default: // list
		
		switch ($_REQUEST['msg_type']) {
		
			case "sent":
				// messaggi inviati
				echo '<h1>'.SENT_MES.' ('.TO_ADMIN.')</h1>';
				
				$qry="select * from messaggi where id_cliente_mittente = ".$_SESSION['utente']['id_cliente']." and stato_messaggio_mittente ='I'";
				
				# filtri
				//$qry .=(!empty($_REQUEST['filtra_oggetto']) && $_REQUEST['filtra_oggetto']!=''?" and oggetto_messaggio like '%".$_REQUEST['filtra_oggetto']."%'":'');
				
				//$qry .=(!empty($_REQUEST['filtra_data']) && $_REQUEST['filtra_data']!=''?" and data_invio_messaggio = '".$_REQUEST['filtra_data']."'":'');
				
				
				$orderby =(!empty($_REQUEST['orderby'])?$_REQUEST['orderby']:"data_invio_messaggio");
				$orderhow =(!empty($_REQUEST['orderhow'])?$_REQUEST['orderhow']:"desc");

				$qry .=" order by ".$orderby." ".$orderhow;
				
				$res=$db->query($qry);
				
				$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list&amp;msg_type='.$_REQUEST['msg_type'];
				//$link_extra_param .=(!empty($_REQUEST['filtra_oggetto']) && $_REQUEST['filtra_oggetto']!=''?"&amp;filtra_oggetto=".$_REQUEST['filtra_oggetto']:'');
				//$link_extra_param .=(!empty($_REQUEST['filtra_data']) && $_REQUEST['filtra_data']!=''?"&amp;filtra_data=".$_REQUEST['filtra_data']:'');
				$link_extra_param .='&amp;orderby='.$orderby.'&amp;orderhow='.$orderhow;
				
			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
					
				?>
				<form name="fFast_upd" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" onsubmit="return controllaForm('fFast_upd');">
				<input type="hidden" name="msg_type" value="inbox" />
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
				<input type="hidden" name="filter" value="0" />
				<table class="tblAdmin">
				<tr>
					<th style="width:30px;"></th>
					<th style="width:30px;"></th>
					<th><a href="<?php echo $path_web; ?>?page=<?php echo $_REQUEST['page']; ?>&amp;msg_type=<?php echo $_REQUEST['msg_type']; ?>&amp;act=list&amp;orderby=oggetto_messaggio&orderhow=<?php echo ($_REQUEST['orderby']=='oggetto_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" title="order by <?php echo OBJECT; ?> <?php echo ($_REQUEST['orderby']=='oggetto_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" class="linkBold"><?php echo OBJECT; ?></a></th>
					<th><a href="<?php echo $path_web; ?>?page=<?php echo $_REQUEST['page']; ?>&amp;msg_type=<?php echo $_REQUEST['msg_type']; ?>&amp;act=list&amp;orderby=data_invio_messaggio&orderhow=<?php echo ($_REQUEST['orderby']=='data_invio_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" title="order by <?php echo DATA_INVIO; ?> <?php echo ($_REQUEST['orderby']=='data_invio_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" class="linkBold"><?php echo DATA_INVIO; ?></a></th>
					<!--<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>-->
				</tr>
				
				<!-- filtri -->
			<!--	<tr>
					<td colspan="2" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
					<td></td>
					<td></td>
					<td></td>
					
				</tr>-->
			<?php   
			
				$class="tdRow1";
				while ($record =& $res->fetchRow()) {

				//	print_r($record);
					echo '
					<tr>
						<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;msg_type='.$_REQUEST['msg_type'].'&amp;act=read&amp;id_messaggio='.$record['id_messaggio'].'" title="'.LABEL_LEGGI.'"><img src="'.$path_web.'img/icone/leggi.gif" width="16" height="18" border="0" alt="'.LABEL_LEGGI.'" /></a></td>
						<td class="'.$class.'">';
					
					echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;msg_type=".$_REQUEST['msg_type']."&amp;act=trash&amp;id_messaggio=".$record['id_messaggio']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/bt_cancella.gif\" width=\"20\" height=\"20\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
					
					echo "</td>";
					echo "\n".'
						<td class="'.$class.'">'.$record['oggetto_messaggio'].'</td>
						<td class="'.$class.'">'.$func->formatData($record['data_invio_messaggio'], "d-m-Y H:i").'</td>
						<!--<td class="'.$class.'">'.$arrayImgStatiMsg[$record['stato_messaggio_mittente']].'</td>-->
					</tr>';
					
					$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
					
				} // end while
			?>
				</table>
				</form>
			
			<?php	
					echo $view_links;
		
				} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
			
		
			break; // end sent
			
			case "draft":
			
				// messaggi bozze
				echo '<h1>'.DRAFT_MES.'</h1>';
				
				$qry="select * from messaggi where id_cliente_mittente = ".$_SESSION['utente']['id_cliente']." and stato_messaggio_mittente='D'";
				
				# filtri
				//$qry .=(!empty($_REQUEST['filtra_oggetto']) && $_REQUEST['filtra_oggetto']!=''?" and oggetto_messaggio like '%".$_REQUEST['filtra_oggetto']."%'":'');
				
				//$qry .=(!empty($_REQUEST['filtra_data']) && $_REQUEST['filtra_data']!=''?" and data_invio_messaggio = '".$_REQUEST['filtra_data']."'":'');
				
				
				$orderby =(!empty($_REQUEST['orderby'])?$_REQUEST['orderby']:"data_invio_messaggio");
				$orderhow =(!empty($_REQUEST['orderhow'])?$_REQUEST['orderhow']:"desc");

				$qry .=" order by ".$orderby." ".$orderhow;
				
				$res=$db->query($qry);
				
				$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list&amp;msg_type='.$_REQUEST['msg_type'];
				//$link_extra_param .=(!empty($_REQUEST['filtra_oggetto']) && $_REQUEST['filtra_oggetto']!=''?"&amp;filtra_oggetto=".$_REQUEST['filtra_oggetto']:'');
				//$link_extra_param .=(!empty($_REQUEST['filtra_data']) && $_REQUEST['filtra_data']!=''?"&amp;filtra_data=".$_REQUEST['filtra_data']:'');
				$link_extra_param .='&amp;orderby='.$orderby.'&amp;orderhow='.$orderhow;
				
			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
				?>
				<form name="fFast_upd" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" onsubmit="return controllaForm('fFast_upd');">
				<input type="hidden" name="msg_type" value="inbox" />
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
				<input type="hidden" name="filter" value="0" />
				<table class="tblAdmin">
				<tr>
					<th style="width:30px;"></th>
					<th style="width:30px;"></th>
					<th><a href="<?php echo $path_web; ?>?page=<?php echo $_REQUEST['page']; ?>&amp;msg_type=<?php echo $_REQUEST['msg_type']; ?>&amp;act=list&amp;orderby=oggetto_messaggio&orderhow=<?php echo ($_REQUEST['orderby']=='oggetto_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" title="order by <?php echo OBJECT; ?> <?php echo ($_REQUEST['orderby']=='oggetto_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" class="linkBold"><?php echo OBJECT; ?></a></th>
					<th><a href="<?php echo $path_web; ?>?page=<?php echo $_REQUEST['page']; ?>&amp;msg_type=<?php echo $_REQUEST['msg_type']; ?>&amp;act=list&amp;orderby=data_creazione_messaggio&orderhow=<?php echo ($_REQUEST['orderby']=='data_creazione_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" title="order by <?php echo DATA_CREAZIONE; ?> <?php echo ($_REQUEST['orderby']=='data_creazione_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" class="linkBold"><?php echo DATA_CREAZIONE; ?></a></th>
				</tr>
			<?php   
			
				$class="tdRow1";
				while ($record =& $res->fetchRow()) {

				//	print_r($record);
					echo '
					<tr>
						<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;msg_type='.$_REQUEST['msg_type'].'&amp;act=form&amp;id_messaggio='.$record['id_messaggio'].'" title="'.MODIFY.'"><img src="'.$path_web.'img/icone/bt_modifica.gif" width="21" height="20" border="0" alt="'.MODIFY.'" /></a></td>
						<td class="'.$class.'">';
					
					echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;msg_type=".$_REQUEST['msg_type']."&amp;act=trash&amp;id_messaggio=".$record['id_messaggio']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/bt_cancella.gif\" width=\"20\" height=\"20\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
					
					echo "</td>";
					echo "\n".'
						<td class="'.$class.'">'.$record['oggetto_messaggio'].'</td>
						<td class="'.$class.'">'.$func->formatData($record['data_creazione_messaggio'], "d-m-Y H:i").'</td>
					</tr>';
					
					$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
					
				} // end while
			?>
				</table>
				</form>
			
			<?php	
					echo $view_links;
		
				} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);

			break; // end draft

			case "trash":
			// stato_messaggio_destinatario
				// messaggi ricevuti
				echo '<h1>'.TRASH_MES.'</h1>';
				
				$qry="select * from messaggi where id_cliente_destinatario = ".$_SESSION['utente']['id_cliente']." and stato_messaggio_destinatario ='C'";
				
				# filtri
				//$qry .=(!empty($_REQUEST['filtra_oggetto']) && $_REQUEST['filtra_oggetto']!=''?" and oggetto_messaggio like '%".$_REQUEST['filtra_oggetto']."%'":'');
				
				//$qry .=(!empty($_REQUEST['filtra_data']) && $_REQUEST['filtra_data']!=''?" and data_invio_messaggio = '".$_REQUEST['filtra_data']."'":'');
				
				
				$orderby =(!empty($_REQUEST['orderby'])?$_REQUEST['orderby']:"data_invio_messaggio");
				$orderhow =(!empty($_REQUEST['orderhow'])?$_REQUEST['orderhow']:"desc");

				$qry .=" order by ".$orderby." ".$orderhow;
				
				$res=$db->query($qry);
				
				$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list&amp;msg_type='.$_REQUEST['msg_type'];
				//$link_extra_param .=(!empty($_REQUEST['filtra_oggetto']) && $_REQUEST['filtra_oggetto']!=''?"&amp;filtra_oggetto=".$_REQUEST['filtra_oggetto']:'');
				//$link_extra_param .=(!empty($_REQUEST['filtra_data']) && $_REQUEST['filtra_data']!=''?"&amp;filtra_data=".$_REQUEST['filtra_data']:'');
				$link_extra_param .='&amp;orderby='.$orderby.'&amp;orderhow='.$orderhow;
			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
					
				?>
				<form name="fFast_upd" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" onsubmit="return controllaForm('fFast_upd');">
				<input type="hidden" name="msg_type" value="inbox" />
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
				<input type="hidden" name="filter" value="0" />
				<table class="tblAdmin">
				<tr>
					<th style="width:30px;"></th>
					<th style="width:30px;"></th>
					<th><a href="<?php echo $path_web; ?>?page=<?php echo $_REQUEST['page']; ?>&amp;msg_type=<?php echo $_REQUEST['msg_type']; ?>&amp;act=list&amp;orderby=oggetto_messaggio&orderhow=<?php echo ($_REQUEST['orderby']=='oggetto_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" title="order by <?php echo OBJECT; ?> <?php echo ($_REQUEST['orderby']=='oggetto_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" class="linkBold"><?php echo OBJECT; ?></a></th>
					<th><a href="<?php echo $path_web; ?>?page=<?php echo $_REQUEST['page']; ?>&amp;msg_type=<?php echo $_REQUEST['msg_type']; ?>&amp;act=list&amp;orderby=data_invio_messaggio&orderhow=<?php echo ($_REQUEST['orderby']=='data_invio_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" title="order by <?php echo DATA_MESSAGGIO; ?> <?php echo ($_REQUEST['orderby']=='data_invio_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" class="linkBold"><?php echo DATA_MESSAGGIO; ?></a></th>
					<!--<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>-->
				</tr>
				
				<!-- filtri -->
			<!--	<tr>
					<td colspan="2" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
					<td></td>
					<td></td>
					<td></td>
					
				</tr>-->
			<?php   
			
				$class="tdRow1";
				while ($record =& $res->fetchRow()) {

				//	print_r($record);
					echo '
					<tr>
						<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;msg_type='.$_REQUEST['msg_type'].'&amp;act=read&amp;id_messaggio='.$record['id_messaggio'].'" title="'.LABEL_LEGGI.'"><img src="'.$path_web.'img/icone/leggi.gif" width="16" height="18" border="0" alt="'.LABEL_LEGGI.'" /></a></td>
						<td class="'.$class.'">';
					
					echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;msg_type=".$_REQUEST['msg_type']."&amp;act=del&amp;id_messaggio=".$record['id_messaggio']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/bt_cancella.gif\" width=\"20\" height=\"20\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
					
					echo "</td>";
					echo "\n".'
						<td class="'.$class.'">'.$record['oggetto_messaggio'].'</td>
						<td class="'.$class.'">'.$func->formatData($record['data_invio_messaggio'], "d-m-Y H:i").'</td>
						<!--<td class="'.$class.'">'.$arrayImgStatiMsg[$record['stato_messaggio_destinatario']].'</td>-->
					</tr>';
					
					$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
					
				} // end while
			?>
				</table>
				</form>
			
			<?php	
					echo $view_links;
		
				} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);

			break; // end trash
			
			default:
				$_REQUEST['msg_type']='inbox';
				// messaggi ricevuti
				echo '<h1>'.INBOX_MES.' ('.FROM_ADMIN.')</h1>';
				
				$qry="select * from messaggi where id_cliente_destinatario = ".$_SESSION['utente']['id_cliente']." and stato_messaggio_destinatario in ('L', 'N', 'R')";
				
				# filtri
				//$qry .=(!empty($_REQUEST['filtra_oggetto']) && $_REQUEST['filtra_oggetto']!=''?" and oggetto_messaggio like '%".$_REQUEST['filtra_oggetto']."%'":'');
				
				//$qry .=(!empty($_REQUEST['filtra_data']) && $_REQUEST['filtra_data']!=''?" and data_invio_messaggio = '".$_REQUEST['filtra_data']."'":'');
				
				
				$orderby =(!empty($_REQUEST['orderby'])?$_REQUEST['orderby']:"data_invio_messaggio");
				$orderhow =(!empty($_REQUEST['orderhow'])?$_REQUEST['orderhow']:"desc");

				$qry .=" order by ".$orderby." ".$orderhow;
				
				$res=$db->query($qry);
				
				$link_extra_param ='page='.$_REQUEST['page'].'&amp;act=list&amp;msg_type='.$_REQUEST['msg_type'];
				//$link_extra_param .=(!empty($_REQUEST['filtra_oggetto']) && $_REQUEST['filtra_oggetto']!=''?"&amp;filtra_oggetto=".$_REQUEST['filtra_oggetto']:'');
				//$link_extra_param .=(!empty($_REQUEST['filtra_data']) && $_REQUEST['filtra_data']!=''?"&amp;filtra_data=".$_REQUEST['filtra_data']:'');
				$link_extra_param .='&amp;orderby='.$orderby.'&amp;orderhow='.$orderhow;
				
			/*$res=$db->query($qry);
			//	echo $qry;
			// ... se si verifica un errore, lo scriviamo
			if( DB::isError($res) ) { 
				print "Attenzione! Si � verificato un errore durante l'esecuzione della query ".$qry."."; die($res->getMessage()); 
			}
				
			$rows = $res->numRows();*/
			$tot_record=0;
			$res=$sql->sqlPage($qry,$records_pp,$_REQUEST['pg'],$link_extra_param,$nr_pg_per_view);
			
				
			if ($tot_record>0) {
			
				echo NUM_RECORDS.': '.$tot_record.'<br /><br />';
					
			echo $view_links;
		?><br /><br />
		<form name="fFast_upd" method="post" action="index.php" onsubmit="return controllaForm('fFast_upd');">
				<input type="hidden" name="msg_type" value="inbox" />
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
				<input type="hidden" name="filter" value="0" />
				<table class="tblAdmin">
				<tr>
					<th style="width:30px;"></th>
					<th style="width:30px;"></th>
					<th><a href="<?php echo $path_web; ?>?page=<?php echo $_REQUEST['page']; ?>&amp;msg_type=<?php echo $_REQUEST['msg_type']; ?>&amp;act=list&amp;orderby=oggetto_messaggio&orderhow=<?php echo ($_REQUEST['orderby']=='oggetto_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" title="order by <?php echo OBJECT; ?> <?php echo ($_REQUEST['orderby']=='oggetto_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" class="linkBold"><?php echo OBJECT; ?></a></th>
					<th><a href="<?php echo $path_web; ?>?page=<?php echo $_REQUEST['page']; ?>&amp;msg_type=<?php echo $_REQUEST['msg_type']; ?>&amp;act=list&amp;orderby=data_invio_messaggio&orderhow=<?php echo ($_REQUEST['orderby']=='data_invio_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" title="order by <?php echo DATA_RICEZIONE; ?> <?php echo ($_REQUEST['orderby']=='data_invio_messaggio' && $_REQUEST['orderhow']=='asc'?'desc':'asc'); ?>" class="linkBold"><?php echo DATA_RICEZIONE; ?></a></th>
					<th style="width:30px;"><?php echo LABEL_STATUS; ?></th>
				</tr>
				
				<!-- filtri -->
			<!--	<tr>
					<td colspan="2" style="text-align:center; padding:4px;"><input type="button" value="<?php echo FILTRA; ?>" class="button" onclick="document.forms['fFast_upd'].act.value='list'; document.forms['fFast_upd'].filter.value='1'; document.forms['fFast_upd'].submit();" /></td>
					<td></td>
					<td></td>
					<td></td>
					
				</tr>-->
			<?php   
			
				$class="tdRow1";
				while ($record =& $res->fetchRow()) {

				//	print_r($record);
					echo '
					<tr>
						<td class="'.$class.'"><a href="'.$path_web.'?page='.$_REQUEST['page'].'&amp;msg_type='.$_REQUEST['msg_type'].'&amp;act=read&amp;id_messaggio='.$record['id_messaggio'].'" title="'.LABEL_LEGGI.'"><img src="'.$path_web.'img/icone/leggi.gif" width="16" height="18" border="0" alt="'.LABEL_LEGGI.'" /></a></td>
						<td class="'.$class.'">';
					
					echo "<a href=\"".$path_web."?page=".$_REQUEST['page']."&amp;msg_type=".$_REQUEST['msg_type']."&amp;act=trash&amp;id_messaggio=".$record['id_messaggio']."\" onclick=\"if (confirm(confirm_del)) window.open(this.href, '_self', ''); return false;\" title=\"".DELETE."\"><img src=\"".$path_web."img/icone/bt_cancella.gif\" width=\"20\" height=\"20\" style=\"border: 0px;\" alt=\"".DELETE."\" /></a>";
					
					echo "</td>";
					echo "\n".'
						<td class="'.$class.'">'.$record['oggetto_messaggio'].'</td>
						<td class="'.$class.'">'.$func->formatData($record['data_invio_messaggio'], "d-m-Y H:i").'</td>
						<td class="'.$class.'">'.$arrayImgStatiMsg[$record['stato_messaggio_destinatario']].'</td>
					</tr>';
					
					$class=($class=='tdRow1'?$class='tdRow2':$class='tdRow1');
					
				} // end while
			?>
				</table>
				</form>
			
			<?php	
					echo $view_links;
		
				} else echo ($_REQUEST['filter']==1?NOT_FOUND:TABELLA_VUOTA);
			
			// end inbox
			
		}	// ens switch msg_type

}
	
?>
