function xmlhttpPost(strURL,query_string) {
	
    var xmlHttpReq = false;
    var self = this;
    // Mozilla/Safari
    if (window.XMLHttpRequest) {
        self.xmlHttpReq = new XMLHttpRequest();
    }
    // IE
    else if (window.ActiveXObject) {
        self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
    }
    self.xmlHttpReq.open('POST', strURL, true);
    self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	
    self.xmlHttpReq.onreadystatechange = function() {
		
        if (self.xmlHttpReq.readyState == 4) {

            	updatepage(self.xmlHttpReq.responseText);

        }
    };
    self.xmlHttpReq.send(query_string);
}


function setquerystring(form) {
   
   var querystring = '';
   var numberElement = form.elements.length;

    for(var i=0; i<numberElement; i++){
	    if(i<numberElement-1){ 
		   querystring+= form.elements[i].name+"="+encodeURIComponent(form.elements[i].value)+"&";
		}else{
			querystring+= form.elements[i].name+"="+encodeURIComponent(form.elements[i].value);
		}
	}
	
	return querystring;
   
}

function updatepage(str){
    document.getElementById("serviziEsterni").innerHTML = str;
}

function get_browser(){
	var ie  = (document.all && document.getElementById);
	var mozilla = (!document.all && document.getElementById);
	var opera = (document.all && document.getElementById);
	
	if(ie) return('ie');
	if(mozilla) return('mozilla');
	if(opera) return('opera');
}

function loading(){
	//document.getElementById('booking').style.backgroundColor = '#cccccc';
	document.getElementById('serviziEsterni').innerHTML = '<br /><br /><img src="http://www.abc.sm/business_booking/engine/img/loading.gif" /><br /><br />'; 
}

function loading_todiv(divname,messaggio){
	document.getElementById(divname).innerHTML = messaggio + '<br /><img src="http://www.abc.sm/business_booking/engine/img/loading11.gif" />'; 
}