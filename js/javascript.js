// JavaScript Document

function mostra_attesa()
{
	jQuery('#loading').show();		
}


function controllaForm (nome_form) {
	
	
	switch (nome_form) {
		
		case "fFast_upd":
			
			// controllo ordinamento
			
			for (i=0;i<document.forms[nome_form].length;i++) {
				nome_campo=document.forms[nome_form].elements[i].name;
				if (nome_campo.indexOf('ordinamento[')==0) {
					valore_campo=document.forms[nome_form].elements[i].value;
					//alert(valore_campo);
					if (valore_campo!=parseInt(valore_campo)) { alert(alert_num); return false; }
				}
			}
			
		break;


	}
	//return false;
	
} // end func controllaForm


function popolaMailAddress() {
	
	var nomeForm='FormUpdate';
	if (document.forms[nomeForm].elements['mail_same_physical'].checked) {
		
		document.forms[nomeForm].elements['nome_mail'].value=document.forms[nomeForm].elements['nome_physical'].value;
		document.forms[nomeForm].elements['indirizzo_mail'].value=document.forms[nomeForm].elements['indirizzo_physical'].value;
		document.forms[nomeForm].elements['cap_mail'].value=document.forms[nomeForm].elements['cap_physical'].value;
		document.forms[nomeForm].elements['citta_mail'].value=document.forms[nomeForm].elements['citta_physical'].value;
		document.forms[nomeForm].elements['provincia_stato_mail'].value=document.forms[nomeForm].elements['provincia_stato_physical'].value;
		document.forms[nomeForm].elements['codice_paese_mail'].value=document.forms[nomeForm].elements['codice_paese_physical'].value;
		document.forms[nomeForm].elements['telefono_mail'].value=document.forms[nomeForm].elements['telefono_physical'].value;
	
	}
	
} // end func popolaMailAddress

function inserisciValoreQuota() {
	
	var nomeForm='FormUpdate';
	if (document.forms[nomeForm].elements['id_tipo_investimento'].value!='') {
		document.forms[nomeForm].elements['valore_quota_iniziale'].value=arrayInvValQuote[document.forms[nomeForm].elements['id_tipo_investimento'].value]; 
		document.forms[nomeForm].elements['id_valuta'].value=arrayInvValuta[document.forms[nomeForm].elements['id_tipo_investimento'].value];  
	} else {
		document.forms[nomeForm].elements['valore_quota_iniziale'].value=''; 
		document.forms[nomeForm].elements['id_valuta'].value=''; 
	}
} // end func inserisciValoreQuota

function inserisciValutaConto() {

	var nomeForm='FormUpdate';
	if (document.forms[nomeForm].elements['conto[1]'].value!='') document.forms[nomeForm].elements['id_valuta_conto'].value=arrayContiValute[document.forms[nomeForm].elements['conto[1]'].value]; else document.forms[nomeForm].elements['id_valuta_conto'].value='';
	
} // end func inserisciValutaConto

function inserisciValutaConto2() {

	var nomeForm='FormUpdate';
	if (document.forms[nomeForm].elements['id_conto'].value!='') document.forms[nomeForm].elements['id_valuta_conto'].value=arrayContiValute[document.forms[nomeForm].elements['id_conto'].value]; else document.forms[nomeForm].elements['id_valuta_conto'].value='';
	
} // end func inserisciValutaConto


function generaPsw(nomeForm) {
	
	//var nomeForm='FormUpdate';
	if (document.forms[nomeForm].elements['psw[genera]'].checked) {
/*
		N_Caratteri = 15;
		Stringa = "";
		for (I=0;I<N_Caratteri;I++){
			do{
				N = Math.floor(Math.random()*74)+48;
			}while(!(((N >= 48) && (N <= 57)) || ((N >= 65) && (N <= 90)) || ((N >= 97) && (N <= 122))));
			
			Stringa = Stringa+String.fromCharCode(N);
		}
*/
		var template = "1234567890QWERTYUIPASDFGHJKLZXCVBNM1234567890QWERTYUIPASDFGHJKLZXCVBNM";
		var N_Caratteri = 6;
		var rndstring = '';
		var a = 0;
		var b=0;
	//	b= Math.random();
	//	alert(b);
		for (I=0;I<N_Caratteri;I++){
			b = Math.floor((Math.random()*100)%25);
			rndstring += template.substring(b,b+1);
		}
		document.forms[nomeForm].elements['psw[password]'].value=rndstring;
		document.forms[nomeForm].elements['psw[genera]'].checked=false;
	}
	
} // end func generaPsw

function inserisciQuote() {
	
	var nomeForm='FormUpdate';
	if (document.forms[nomeForm].elements['importo'].value!='' && document.forms[nomeForm].elements['valore_quota_iniziale'].value!='') {

		document.forms[nomeForm].elements['numero_quote'].value=(document.forms[nomeForm].elements['importo'].value/document.forms[nomeForm].elements['valore_quota_iniziale'].value);
	}
	
}

function verificaDeposito() {

	var nomeForm='FormUpdate';
	var importo=0;
	
	if (document.forms[nomeForm].elements['numero_quote'].value>0) {
		
		if (document.forms[nomeForm].elements['valore_quota_iniziale'].value>0) {
			importo=document.forms[nomeForm].elements['numero_quote'].value*document.forms[nomeForm].elements['valore_quota_iniziale'].value;
			if (importo>arrayContiGiacenza[document.forms[nomeForm].elements['conto[1]'].value]) alert(giacenza_insuff);
			
		} else alert(select_invest);	
		
	} else alert(insert_num_quote);	
	
} // end func verificaDeposito


function copia(da, a, nomeForm) {
	
		
	switch (nomeForm) {
		case "FormTipi":
			
			// descrizione
			document.forms[nomeForm].elements['nome_tipo['+a+']'].value=document.forms[nomeForm].elements['nome_tipo['+da+']'].value;
			
		break;
		
	}

} // end func copia


/**
 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */
// Declaring valid date character, minimum year and maximum year
var dtCh= "-";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31;
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30;}
		if (i==2) {this[i] = 29;}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12);
	var pos1=dtStr.indexOf(dtCh);
	var pos2=dtStr.indexOf(dtCh,pos1+1);
	var strDay=dtStr.substring(0,pos1);
	var strMonth=dtStr.substring(pos1+1,pos2);
	var strYear=dtStr.substring(pos2+1);
	strYr=strYear;
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1);
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1);
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1);
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : dd-mm-yyyy");
		return false;
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month");
		return false;
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day");
		return false;
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear);
		return false;
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date");
		return false;
	}
return true
}

function ValidateForm(){
	var dt1=document.formCerca.from_date;
	var dt2=document.formCerca.since_date;
	
	if (dt1.value!='') {
		
		if (isDate(dt1.value)==false){
			dt1.focus();
			return false;
		}
		
	}
	
	if (dt2.value!='') {
		
		if (isDate(dt2.value)==false){
			dt2.focus();
			return false;
		}
		
	}
    return true;
 }
 
 
 
 function confermaElimina(nomeForm) {
	 
	if (confirm(confirm_del)) { document.forms[nomeForm].act.value="del_multiple"; return true; } else return false;
	
	return true;
	
}




/* iscrizione utente */

function insertData(campo) {
	
	var modulo = document.forms['FormApply'];
	var campo2 = campo.replace(/_/g,'');
	//alert(campo2);
	
	if (modulo.elements[campo2+'[Y]'].value.length==4 && modulo.elements[campo2+'[m]'].value.length > 0 && modulo.elements[campo2+'[d]'].value.length > 0) modulo.elements[campo].value = modulo.elements[campo2+'[Y]'].value + '-' + modulo.elements[campo2+'[m]'].value + '-' + modulo.elements[campo2+'[d]'].value;
	else modulo.elements[campo].value = '';
	
	
} // end insertData
