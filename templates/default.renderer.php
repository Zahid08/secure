<?php

/**
 * Libreria di appoggio a PEAR::QuickForm necessaria per la
 * personalizzazzione dell' aspetto estetico di tutti i forms.
 * Non utilizza un template engine ma il renderer di default.
 * @author  Michela Cucchi
 * @package
 * @version $Revision: 1.0 $
 */

$renderer=&HTML_QuickForm::defaultRenderer();
$renderer->setFormTemplate('
	<form{attributes}>
	<div class="divForm">
			{content}
	</div>
	</form>
');

$renderer->setHeaderTemplate('
<div class="titoloForm">{header}</div>
');

$renderer->setElementTemplate('
<div class="labelForm"><!-- BEGIN required --><span class="red">*</span><!-- END required -->{label}</div>
<div class="fieldForm"><!-- BEGIN error --><span class="red">{error}</span><br /><!-- END error -->{element}</div>
<div class="clearBoth"></div>
');

$renderer->setRequiredNoteTemplate('
<div style="text-align:left" class="red">{requiredNote}</div>
');

$renderer->setGroupTemplate('{content}','bottoniera');
$renderer->setGroupElementTemplate('&nbsp;{element}&nbsp;','bottoniera');

?>