-- phpMyAdmin SQL Dump
-- version 2.8.2.4
-- http://www.phpmyadmin.net
-- 
-- Host: localhost:3306
-- Generato il: 29 Set, 2009 at 09:03 AM
-- Versione MySQL: 5.0.22
-- Versione PHP: 5.2.3
-- 
-- Database: `titanka_wolfsparkasse`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `carte`
-- 

CREATE TABLE `carte` (
  `id_carta` int(11) NOT NULL auto_increment,
  `id_tipo_carta` int(11) NOT NULL default '0',
  `id_cliente` int(11) NOT NULL default '0',
  `id_conto` int(11) NOT NULL default '0',
  `id_valuta` int(11) NOT NULL default '0',
  `data_creazione` datetime default NULL,
  `numero_carta` varchar(50) default NULL,
  `data_richiesta` datetime default NULL,
  `data_attivazione` datetime default NULL,
  `attivo` tinyint(1) NOT NULL default '0',
  `stato` char(1) default NULL,
  `debito_credito` enum('D','C') default NULL,
  PRIMARY KEY  (`id_carta`,`id_tipo_carta`,`id_cliente`,`id_conto`,`id_valuta`),
  KEY `tipi_carte_carte` (`id_tipo_carta`),
  KEY `conti_carte` (`id_conto`,`id_cliente`,`id_valuta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `carte`
-- 

- --------------------------------------------------------

-- 
-- Struttura della tabella `clienti`
-- 

CREATE TABLE `clienti` (
  `id_cliente` int(11) NOT NULL auto_increment,
  `id_tipo_titolo` int(11) NOT NULL default '0',
  `nome` varchar(125) NOT NULL,
  `cognome` varchar(125) NOT NULL,
  `ragione_sociale` varchar(125) default NULL,
  `data_di_nascita` date default NULL,
  `id_tipo_documento` int(11) NOT NULL default '0',
  `codice_documento` varchar(125) default NULL,
  `email` varchar(125) default NULL,
  `telefono_casa` varchar(125) default NULL,
  `telefono_ufficio` varchar(125) default NULL,
  `telefono_cellulare` varchar(125) default NULL,
  `fax` varchar(125) default NULL,
  `codice_paese_cittadinanza` char(2) default NULL,
  `codice_paese_residenza` char(2) default NULL,
  `sigla_lingua` char(2) NOT NULL,
  `attivo` tinyint(1) NOT NULL default '0',
  `id_tipo_utente` int(11) NOT NULL default '0',
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `chiave` varchar(15) default NULL,
  `note` text,
  `data_richiesta` datetime default NULL,
  `data_attivazione` datetime default NULL,
  `nome_physical` varchar(40) default NULL,
  `indirizzo_physical` varchar(125) default NULL,
  `cap_physical` varchar(15) default NULL,
  `citta_physical` varchar(125) default NULL,
  `provincia_stato_physical` varchar(125) default NULL,
  `codice_paese_physical` char(2) NOT NULL,
  `telefono_physical` varchar(40) default NULL,
  `nome_mail` varchar(40) default NULL,
  `indirizzo_mail` varchar(125) default NULL,
  `cap_mail` varchar(15) default NULL,
  `citta_mail` varchar(125) default NULL,
  `provincia_stato_mail` varchar(125) default NULL,
  `codice_paese_mail` char(2) NOT NULL,
  `telefono_mail` varchar(40) default NULL,
  `data_aggiornamento_psw` datetime default NULL,
  `note_admin` text,
  `skype` varchar(125) default NULL,
  `segnalatore` varchar(125) default NULL,
  PRIMARY KEY  (`id_cliente`,`id_tipo_titolo`,`id_tipo_documento`,`sigla_lingua`,`id_tipo_utente`),
  KEY `lingue_clienti` (`sigla_lingua`),
  KEY `tipi_documenti_clienti` (`id_tipo_documento`),
  KEY `tipi_titoli_clienti` (`id_tipo_titolo`),
  KEY `tipi_utenti_clienti` (`id_tipo_utente`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `clienti`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `clienti_tipi_gruppi`
-- 

CREATE TABLE `clienti_tipi_gruppi` (
  `id_cliente` int(11) NOT NULL default '0',
  `id_tipo_gruppo` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id_cliente`,`id_tipo_gruppo`),
  KEY `tipi_gruppi_clienti_tipi_gruppi` (`id_tipo_gruppo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dump dei dati per la tabella `clienti_tipi_gruppi`
-- 

- --------------------------------------------------------

-- 
-- Struttura della tabella `conti`
-- 

CREATE TABLE `conti` (
  `id_conto` int(11) NOT NULL auto_increment,
  `id_tipo_conto` int(11) NOT NULL default '0',
  `id_cliente` int(11) NOT NULL default '0',
  `id_valuta` int(11) NOT NULL default '0',
  `intestatario` varchar(125) default NULL,
  `data_creazione` datetime default NULL,
  `saldo_iniziale` decimal(16,2) default NULL,
  `numero_conto` varchar(8) default NULL,
  `descrizione` varchar(255) default NULL,
  `abilita_prelievo` tinyint(1) default NULL,
  `abilita_deposito` tinyint(1) default NULL,
  `saldo_attuale` decimal(16,2) default NULL,
  `data_richiesta` datetime default NULL,
  `data_attivazione` datetime default NULL,
  `attivo` tinyint(1) NOT NULL default '0',
  `stato` char(1) NOT NULL,
  PRIMARY KEY  (`id_conto`,`id_tipo_conto`,`id_cliente`,`id_valuta`),
  KEY `tipi_conti_conti` (`id_tipo_conto`),
  KEY `valute_conti` (`id_valuta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `conti`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `gestione_offshore`
-- 

CREATE TABLE `gestione_offshore` (
  `id_gestione_offshore` int(11) NOT NULL auto_increment,
  `id_cliente` int(11) NOT NULL default '0',
  `nome_ba_in` varchar(255) default NULL,
  `nome_ba_out` varchar(255) default NULL,
  `swift_ba_in` varchar(255) default NULL,
  `swift_ba_out` varchar(255) default NULL,
  `indirizzo_ba_in` varchar(255) default NULL,
  `indirizzo_ba_out` varchar(255) default NULL,
  `cap_ba_in` varchar(255) default NULL,
  `cap_ba_out` varchar(255) default NULL,
  `citta_ba_in` varchar(255) default NULL,
  `citta_ba_out` varchar(255) default NULL,
  `codice_paese_ba_in` char(2) default NULL,
  `codice_paese_ba_out` char(2) default NULL,
  `ncn_ba_in` varchar(255) default NULL,
  `ncn_ba_out` varchar(255) default NULL,
  `aba_ba_in` varchar(255) default NULL,
  `aba_ba_out` varchar(255) default NULL,
  `numero_conto_ba_in` varchar(255) default NULL,
  `numero_conto_ba_out` varchar(255) default NULL,
  `iban_ba_in` varchar(255) default NULL,
  `iban_ba_out` varchar(255) default NULL,
  `id_transazione` int(11) NOT NULL default '0',
  `intestatario_ba_in` varchar(255) default NULL,
  `intestatario_ba_out` varchar(255) default NULL,
  PRIMARY KEY  (`id_gestione_offshore`,`id_cliente`,`id_transazione`),
  KEY `clienti_gestione_offshore` (`id_cliente`),
  KEY `transazioni_gestione_offshore` (`id_transazione`,`id_gestione_offshore`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `gestione_offshore`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `lingue`
-- 

CREATE TABLE `lingue` (
  `sigla_lingua` char(2) NOT NULL,
  `nome_lingua` varchar(40) NOT NULL,
  `charset` varchar(40) default NULL,
  `ordinamento` int(11) default NULL,
  `attivo` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`sigla_lingua`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dump dei dati per la tabella `lingue`
-- 

INSERT INTO `lingue` (`sigla_lingua`, `nome_lingua`, `charset`, `ordinamento`, `attivo`) VALUES ('en', 'English', NULL, 1, 1),
('it', 'Italiano', 'iso-8859-1', 2, 1);

-- --------------------------------------------------------

-- 
-- Struttura della tabella `log_accessi`
-- 

CREATE TABLE `log_accessi` (
  `id_log` int(11) NOT NULL auto_increment,
  `id_cliente` int(11) NOT NULL default '0',
  `indirizzo_ip` varchar(40) default NULL,
  `data_accesso` datetime default NULL,
  PRIMARY KEY  (`id_log`,`id_cliente`),
  KEY `clienti_log_accessi` (`id_cliente`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `log_accessi`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `messaggi`
-- 

CREATE TABLE `messaggi` (
  `id_messaggio` int(11) NOT NULL auto_increment,
  `id_cliente_mittente` int(11) NOT NULL default '0',
  `id_cliente_destinatario` int(11) NOT NULL default '0',
  `email_mittente` varchar(255) default NULL,
  `email_destinatario` varchar(255) default NULL,
  `oggetto_messaggio` varchar(125) default NULL,
  `testo_messaggio` text,
  `stato_messaggio_mittente` char(1) default NULL,
  `stato_messaggio_destinatario` char(1) default NULL,
  `sigla_lingua` char(2) NOT NULL,
  `data_creazione_messaggio` datetime default NULL,
  `data_invio_messaggio` datetime default NULL,
  `data_lettura_messaggio` datetime default NULL,
  `notifica_automatica` tinyint(1) NOT NULL default '0',
  `id_messaggio_risposta` int(11) default NULL,
  `notifica_email` tinyint(1) NOT NULL default '0',
  `invio_multiplo` varchar(15) default NULL,
  `allegato` varchar(225) default NULL,
  PRIMARY KEY  (`id_messaggio`,`sigla_lingua`),
  KEY `lingue_messaggi` (`sigla_lingua`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `messaggi`
-- 


-- --------------------------------------------------------

-- 
-- Struttura della tabella `movimenti_carte`
-- 

CREATE TABLE `movimenti_carte` (
  `id_movimento` int(11) NOT NULL auto_increment,
  `id_carta` int(11) NOT NULL default '0',
  `id_valuta` int(11) NOT NULL default '0',
  `data_richiesta` datetime default NULL,
  `data_attivazione` datetime default NULL,
  `debito_credito` enum('D','C') default NULL,
  `importo` decimal(16,2) default NULL,
  `causale` varchar(255) default NULL,
  `stato` char(1) default NULL,
  PRIMARY KEY  (`id_movimento`,`id_carta`,`id_valuta`),
  KEY `carte_movimenti_carte` (`id_carta`,`id_valuta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `movimenti_carte`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `paesi`
-- 

CREATE TABLE `paesi` (
  `codice_iso2` char(2) NOT NULL,
  `nome_paese` varchar(125) NOT NULL,
  PRIMARY KEY  (`codice_iso2`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dump dei dati per la tabella `paesi`
-- 

INSERT INTO `paesi` (`codice_iso2`, `nome_paese`) VALUES ('AF', 'Afghanistan'),
('AL', 'Albania'),
('DZ', 'Algeria'),
('AS', 'American Samoa'),
('AD', 'Andorra'),
('AO', 'Angola'),
('AI', 'Anguilla'),
('AQ', 'Antarctica'),
('AG', 'Antigua and Barbuda'),
('AR', 'Argentina'),
('AM', 'Armenia'),
('AW', 'Aruba'),
('AU', 'Australia'),
('AT', 'Austria'),
('AZ', 'Azerbaijan'),
('BS', 'Bahamas'),
('BH', 'Bahrain'),
('BD', 'Bangladesh'),
('BB', 'Barbados'),
('BY', 'Belarus'),
('BE', 'Belgium'),
('BZ', 'Belize'),
('BJ', 'Benin'),
('BM', 'Bermuda'),
('BT', 'Bhutan'),
('BO', 'Bolivia'),
('BA', 'Bosnia and Herzegowina'),
('BW', 'Botswana'),
('BV', 'Bouvet Island'),
('BR', 'Brazil'),
('IO', 'British Indian Ocean Territory'),
('BN', 'Brunei Darussalam'),
('BG', 'Bulgaria'),
('BF', 'Burkina Faso'),
('BI', 'Burundi'),
('KH', 'Cambodia'),
('CM', 'Cameroon'),
('CA', 'Canada'),
('CV', 'Cape Verde'),
('KY', 'Cayman Islands'),
('CF', 'Central African Republic'),
('TD', 'Chad'),
('CL', 'Chile'),
('CN', 'China'),
('CX', 'Christmas Island'),
('CC', 'Cocos (Keeling) Islands'),
('CO', 'Colombia'),
('KM', 'Comoros'),
('CG', 'Congo'),
('CK', 'Cook Islands'),
('CR', 'Costa Rica'),
('CI', 'Cote D''Ivoire'),
('HR', 'Croatia'),
('CU', 'Cuba'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('DK', 'Denmark'),
('DJ', 'Djibouti'),
('DM', 'Dominica'),
('DO', 'Dominican Republic'),
('TP', 'East Timor'),
('EC', 'Ecuador'),
('EG', 'Egypt'),
('SV', 'El Salvador'),
('GQ', 'Equatorial Guinea'),
('ER', 'Eritrea'),
('EE', 'Estonia'),
('ET', 'Ethiopia'),
('FK', 'Falkland Islands (Malvinas)'),
('FO', 'Faroe Islands'),
('FJ', 'Fiji'),
('FI', 'Finland'),
('FR', 'France'),
('FX', 'France, Metropolitan'),
('GF', 'French Guiana'),
('PF', 'French Polynesia'),
('TF', 'French Southern Territories'),
('GA', 'Gabon'),
('GM', 'Gambia'),
('GE', 'Georgia'),
('DE', 'Germany'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GR', 'Greece'),
('GL', 'Greenland'),
('GD', 'Grenada'),
('GP', 'Guadeloupe'),
('GU', 'Guam'),
('GT', 'Guatemala'),
('GN', 'Guinea'),
('GW', 'Guinea-bissau'),
('GY', 'Guyana'),
('HT', 'Haiti'),
('HM', 'Heard and Mc Donald Islands'),
('HN', 'Honduras'),
('HK', 'Hong Kong'),
('HU', 'Hungary'),
('IS', 'Iceland'),
('IN', 'India'),
('ID', 'Indonesia'),
('IR', 'Iran (Islamic Republic of)'),
('IQ', 'Iraq'),
('IE', 'Ireland'),
('IL', 'Israel'),
('IT', 'Italy'),
('JM', 'Jamaica'),
('JP', 'Japan'),
('JO', 'Jordan'),
('KZ', 'Kazakhstan'),
('KE', 'Kenya'),
('KI', 'Kiribati'),
('KP', 'Korea, Democratic People''s Republic of'),
('KR', 'Korea, Republic of'),
('KW', 'Kuwait'),
('KG', 'Kyrgyzstan'),
('LA', 'Lao People''s Democratic Republic'),
('LV', 'Latvia'),
('LB', 'Lebanon'),
('LS', 'Lesotho'),
('LR', 'Liberia'),
('LY', 'Libyan Arab Jamahiriya'),
('LI', 'Liechtenstein'),
('LT', 'Lithuania'),
('LU', 'Luxembourg'),
('MO', 'Macau'),
('MK', 'Macedonia, The Former Yugoslav Republic of'),
('MG', 'Madagascar'),
('MW', 'Malawi'),
('MY', 'Malaysia'),
('MV', 'Maldives'),
('ML', 'Mali'),
('MT', 'Malta'),
('MH', 'Marshall Islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MU', 'Mauritius'),
('YT', 'Mayotte'),
('MX', 'Mexico'),
('FM', 'Micronesia, Federated States of'),
('MD', 'Moldova, Republic of'),
('MC', 'Monaco'),
('MN', 'Mongolia'),
('MS', 'Montserrat'),
('MA', 'Morocco'),
('MZ', 'Mozambique'),
('MM', 'Myanmar'),
('NA', 'Namibia'),
('NR', 'Nauru'),
('NP', 'Nepal'),
('NL', 'Netherlands'),
('AN', 'Netherlands Antilles'),
('NC', 'New Caledonia'),
('NZ', 'New Zealand'),
('NI', 'Nicaragua'),
('NE', 'Niger'),
('NG', 'Nigeria'),
('NU', 'Niue'),
('NF', 'Norfolk Island'),
('MP', 'Northern Mariana Islands'),
('NO', 'Norway'),
('OM', 'Oman'),
('PK', 'Pakistan'),
('PW', 'Palau'),
('PA', 'Panama'),
('PG', 'Papua New Guinea'),
('PY', 'Paraguay'),
('PE', 'Peru'),
('PH', 'Philippines'),
('PN', 'Pitcairn'),
('PL', 'Poland'),
('PT', 'Portugal'),
('PR', 'Puerto Rico'),
('QA', 'Qatar'),
('RE', 'Reunion'),
('RO', 'Romania'),
('RU', 'Russian Federation'),
('RW', 'Rwanda'),
('KN', 'Saint Kitts and Nevis'),
('LC', 'Saint Lucia'),
('VC', 'Saint Vincent and the Grenadines'),
('WS', 'Samoa'),
('SM', 'San Marino'),
('ST', 'Sao Tome and Principe'),
('SA', 'Saudi Arabia'),
('SN', 'Senegal'),
('SC', 'Seychelles'),
('SL', 'Sierra Leone'),
('SG', 'Singapore'),
('SK', 'Slovakia (Slovak Republic)'),
('SI', 'Slovenia'),
('SB', 'Solomon Islands'),
('SO', 'Somalia'),
('ZA', 'South Africa'),
('GS', 'South Georgia and the South Sandwich Islands'),
('ES', 'Spain'),
('LK', 'Sri Lanka'),
('SH', 'St. Helena'),
('PM', 'St. Pierre and Miquelon'),
('SD', 'Sudan'),
('SR', 'Suriname'),
('SJ', 'Svalbard and Jan Mayen Islands'),
('SZ', 'Swaziland'),
('SE', 'Sweden'),
('CH', 'Switzerland'),
('SY', 'Syrian Arab Republic'),
('TW', 'Taiwan'),
('TJ', 'Tajikistan'),
('TZ', 'Tanzania, United Republic of'),
('TH', 'Thailand'),
('TG', 'Togo'),
('TK', 'Tokelau'),
('TO', 'Tonga'),
('TT', 'Trinidad and Tobago'),
('TN', 'Tunisia'),
('TR', 'Turkey'),
('TM', 'Turkmenistan'),
('TC', 'Turks and Caicos Islands'),
('TV', 'Tuvalu'),
('UG', 'Uganda'),
('UA', 'Ukraine'),
('AE', 'United Arab Emirates'),
('GB', 'United Kingdom'),
('US', 'United States'),
('UM', 'United States Minor Outlying Islands'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VU', 'Vanuatu'),
('VA', 'Vatican City State (Holy See)'),
('VE', 'Venezuela'),
('VN', 'Viet Nam'),
('VG', 'Virgin Islands (British)'),
('VI', 'Virgin Islands (U.S.)'),
('WF', 'Wallis and Futuna Islands'),
('EH', 'Western Sahara'),
('YE', 'Yemen'),
('YU', 'Yugoslavia'),
('ZR', 'Zaire'),
('ZM', 'Zambia'),
('ZW', 'Zimbabwe');

-- --------------------------------------------------------

-- 
-- Struttura della tabella `portfolio`
-- 

CREATE TABLE `portfolio` (
  `id_portfolio` int(11) NOT NULL auto_increment,
  `id_cliente` int(11) NOT NULL default '0',
  `id_conto` int(11) NOT NULL default '0',
  `id_tipo_investimento` int(11) NOT NULL default '0',
  `id_valuta` int(11) NOT NULL default '0',
  `numero_quote` decimal(16,4) NOT NULL default '0.0000',
  `valore_quota_iniziale` decimal(16,4) NOT NULL default '0.0000',
  `data_richiesta` datetime default NULL,
  `data_attivazione` datetime default NULL,
  `stato` char(1) default NULL,
  PRIMARY KEY  (`id_portfolio`,`id_cliente`,`id_conto`,`id_tipo_investimento`,`id_valuta`),
  KEY `clienti_portfolio` (`id_cliente`),
  KEY `conti_portfolio` (`id_conto`,`id_cliente`),
  KEY `tipi_investimenti_portfolio` (`id_tipo_investimento`,`id_valuta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `portfolio`
-- 


-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_carte`
-- 

CREATE TABLE `tipi_carte` (
  `id_tipo_carta` int(11) NOT NULL auto_increment,
  `ordinamento` int(11) default NULL,
  `attivo` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_tipo_carta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_carte`
-- 


-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_carte_lingue`
-- 

CREATE TABLE `tipi_carte_lingue` (
  `id_tipo_carta_lingua` int(11) NOT NULL auto_increment,
  `id_tipo_carta` int(11) NOT NULL default '0',
  `sigla_lingua` char(2) NOT NULL,
  `nome_tipo` varchar(125) NOT NULL,
  PRIMARY KEY  (`id_tipo_carta_lingua`,`id_tipo_carta`,`sigla_lingua`),
  KEY `tipi_carte_tipi_carte_lingue` (`id_tipo_carta`),
  KEY `lingue_tipi_carte_lingue` (`sigla_lingua`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_carte_lingue`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_commissioni`
-- 

CREATE TABLE `tipi_commissioni` (
  `id_tipo_commissione` int(11) NOT NULL auto_increment,
  `ordinamento` int(11) default NULL,
  `attivo` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_tipo_commissione`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_commissioni`
-- 


-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_commissioni_lingue`
-- 

CREATE TABLE `tipi_commissioni_lingue` (
  `id_tipo_commissione_lingua` int(11) NOT NULL auto_increment,
  `id_tipo_commissione` int(11) NOT NULL default '0',
  `sigla_lingua` char(2) NOT NULL,
  `nome_tipo` varchar(125) NOT NULL,
  PRIMARY KEY  (`id_tipo_commissione_lingua`,`id_tipo_commissione`,`sigla_lingua`),
  KEY `tipi_commissioni_tipi_commissioni_lingue` (`id_tipo_commissione`),
  KEY `lingue_tipi_commissioni_lingue` (`sigla_lingua`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_commissioni_lingue`
-- 
-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_conti`
-- 

CREATE TABLE `tipi_conti` (
  `id_tipo_conto` int(11) NOT NULL auto_increment,
  `ordinamento` int(11) default NULL,
  `attivo` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_tipo_conto`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_conti`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_conti_lingue`
-- 

CREATE TABLE `tipi_conti_lingue` (
  `id_tipo_conto_lingua` int(11) NOT NULL auto_increment,
  `id_tipo_conto` int(11) NOT NULL default '0',
  `sigla_lingua` char(2) NOT NULL,
  `nome_tipo` varchar(125) NOT NULL,
  PRIMARY KEY  (`id_tipo_conto_lingua`,`id_tipo_conto`,`sigla_lingua`),
  KEY `tipi_conti_tipi_conti_lingue` (`id_tipo_conto`),
  KEY `lingue_tipi_conti_lingue` (`sigla_lingua`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- 
-- Dump dei dati per la tabella `tipi_conti_lingue`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_documenti`
-- 

CREATE TABLE `tipi_documenti` (
  `id_tipo_documento` int(11) NOT NULL auto_increment,
  `ordinamento` int(11) default NULL,
  `attivo` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_tipo_documento`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_documenti`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_documenti_lingue`
-- 

CREATE TABLE `tipi_documenti_lingue` (
  `id_tipo_documento_lingua` int(11) NOT NULL auto_increment,
  `id_tipo_documento` int(11) NOT NULL default '0',
  `sigla_lingua` char(2) NOT NULL,
  `nome_tipo` varchar(125) NOT NULL,
  PRIMARY KEY  (`id_tipo_documento_lingua`,`id_tipo_documento`,`sigla_lingua`),
  KEY `lingue_tipi_documenti_lingue` (`sigla_lingua`),
  KEY `tipi_documenti_tipi_documenti_lingue` (`id_tipo_documento`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_documenti_lingue`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_gruppi`
-- 

CREATE TABLE `tipi_gruppi` (
  `id_tipo_gruppo` int(11) NOT NULL auto_increment,
  `ordinamento` int(11) default NULL,
  `attivo` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_tipo_gruppo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_gruppi`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_gruppi_lingue`
-- 

CREATE TABLE `tipi_gruppi_lingue` (
  `id_tipo_gruppo_lingua` int(11) NOT NULL auto_increment,
  `id_tipo_gruppo` int(11) NOT NULL default '0',
  `sigla_lingua` char(2) NOT NULL,
  `nome_tipo` varchar(125) NOT NULL,
  PRIMARY KEY  (`id_tipo_gruppo_lingua`,`id_tipo_gruppo`,`sigla_lingua`),
  KEY `tipi_gruppi_tipi_gruppi_lingue` (`id_tipo_gruppo`),
  KEY `lingue_tipi_gruppi_lingue` (`sigla_lingua`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_gruppi_lingue`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_investimenti`
-- 

CREATE TABLE `tipi_investimenti` (
  `id_tipo_investimento` int(11) NOT NULL auto_increment,
  `id_valuta` int(11) NOT NULL default '0',
  `valore` float(6,2) default NULL,
  `attivo` tinyint(1) default NULL,
  `ordinamento` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id_tipo_investimento`,`id_valuta`),
  KEY `valute_tipi_investimenti` (`id_valuta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_investimenti`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_investimenti_lingue`
-- 

CREATE TABLE `tipi_investimenti_lingue` (
  `id_tipo_investimento_lingua` int(11) NOT NULL auto_increment,
  `id_tipo_investimento` int(11) NOT NULL default '0',
  `sigla_lingua` char(2) NOT NULL,
  `nome_tipo` varchar(125) NOT NULL,
  `descrizione` varchar(255) default NULL,
  PRIMARY KEY  (`id_tipo_investimento_lingua`,`id_tipo_investimento`,`sigla_lingua`),
  KEY `tipi_investimenti_tipi_investimenti_lingue` (`id_tipo_investimento`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_investimenti_lingue`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_titoli`
-- 

CREATE TABLE `tipi_titoli` (
  `id_tipo_titolo` int(11) NOT NULL auto_increment,
  `ordinamento` int(11) default NULL,
  `attivo` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_tipo_titolo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_titoli`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_titoli_lingue`
-- 

CREATE TABLE `tipi_titoli_lingue` (
  `id_tipo_titolo_lingua` int(11) NOT NULL auto_increment,
  `id_tipo_titolo` int(11) NOT NULL default '0',
  `sigla_lingua` char(2) NOT NULL,
  `nome_tipo` varchar(125) NOT NULL,
  PRIMARY KEY  (`id_tipo_titolo_lingua`,`id_tipo_titolo`,`sigla_lingua`),
  KEY `tipi_titoli_tipi_titoli_lingue` (`id_tipo_titolo`),
  KEY `lingue_tipi_titoli_lingue` (`sigla_lingua`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_titoli_lingue`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_transazioni`
-- 

CREATE TABLE `tipi_transazioni` (
  `id_tipo_transazione` int(11) NOT NULL auto_increment,
  `ordinamento` int(11) default NULL,
  `attivo` tinyint(1) NOT NULL default '0',
  `fra_utenti` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_tipo_transazione`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_transazioni`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_transazioni_lingue`
-- 

CREATE TABLE `tipi_transazioni_lingue` (
  `id_tipo_transazione_lingua` int(11) NOT NULL auto_increment,
  `id_tipo_transazione` int(11) NOT NULL default '0',
  `sigla_lingua` char(2) NOT NULL,
  `nome_tipo` varchar(125) NOT NULL,
  PRIMARY KEY  (`id_tipo_transazione_lingua`,`id_tipo_transazione`,`sigla_lingua`),
  KEY `lingue_tipi_transazioni_lingue` (`sigla_lingua`),
  KEY `tipi_transazioni_tipi_transazioni_lingue` (`id_tipo_transazione`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_transazioni_lingue`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_utenti`
-- 

CREATE TABLE `tipi_utenti` (
  `id_tipo_utente` int(11) NOT NULL auto_increment,
  `ordinamento` int(11) default NULL,
  `attivo` tinyint(1) default NULL,
  `pagina_default` varchar(40) NOT NULL default '0',
  PRIMARY KEY  (`id_tipo_utente`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_utenti`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `tipi_utenti_lingue`
-- 

CREATE TABLE `tipi_utenti_lingue` (
  `id_tipo_utente_lingua` int(11) NOT NULL auto_increment,
  `id_tipo_utente` int(11) NOT NULL default '0',
  `sigla_lingua` char(2) NOT NULL,
  `nome_tipo` varchar(125) NOT NULL,
  PRIMARY KEY  (`id_tipo_utente_lingua`,`id_tipo_utente`,`sigla_lingua`),
  KEY `tipi_utenti_tipi_utenti_lingue` (`id_tipo_utente`),
  KEY `lingue_tipi_utenti_lingue` (`sigla_lingua`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `tipi_utenti_lingue`
-- 

-- --------------------------------------------------------

-- 
-- Struttura della tabella `transazioni`
-- 

CREATE TABLE `transazioni` (
  `id_transazione` int(11) NOT NULL auto_increment,
  `id_tipo_transazione` int(11) NOT NULL default '0',
  `id_tipo_commissione` int(11) NOT NULL default '0',
  `id_valuta` int(11) NOT NULL default '0',
  `importo_transazione` decimal(16,4) default NULL,
  `descrizione` text,
  `data_richiesta` datetime default NULL,
  `data_attivazione` datetime default NULL,
  `data_transazione` datetime default NULL,
  `id_cliente_da` int(11) NOT NULL default '0',
  `id_conto_da` int(11) NOT NULL default '0',
  `id_cliente_a` int(11) NOT NULL default '0',
  `id_conto_a` int(11) NOT NULL default '0',
  `id_gestione_offshore` int(11) NOT NULL default '0',
  `stato` char(1) default NULL,
  `id_tipo_investimento` int(11) NOT NULL default '0',
  `valore_quota` decimal(16,4) default NULL,
  `numero_quote` decimal(16,4) default NULL,
  `buy_sell` enum('B','S') default NULL,
  `normal_express` enum('N','E') default NULL,
  `id_richiedente` int(11) NOT NULL default '0',
  `cliente_a` varchar(125) default NULL,
  `conto_a` varchar(125) default NULL,
  `id_carta_a` int(11) NOT NULL default '0',
  `id_carta_da` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id_transazione`,`id_tipo_transazione`,`id_tipo_commissione`,`id_valuta`,`id_gestione_offshore`,`id_tipo_investimento`),
  KEY `gestione_offshore_transazioni` (`id_gestione_offshore`),
  KEY `tipi_transazioni_transazioni` (`id_tipo_transazione`),
  KEY `tipi_commissioni_transazioni` (`id_tipo_commissione`),
  KEY `valute_transazioni` (`id_valuta`),
  KEY `tipi_investimenti_transazioni` (`id_tipo_investimento`,`id_valuta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `transazioni`
-- 


-- --------------------------------------------------------

-- 
-- Struttura della tabella `valute`
-- 

CREATE TABLE `valute` (
  `id_valuta` int(11) NOT NULL auto_increment,
  `nome_valuta` varchar(15) default NULL,
  `ordinamento` int(11) default NULL,
  `attivo` tinyint(1) default NULL,
  `valore` decimal(16,4) NOT NULL default '0.0000',
  PRIMARY KEY  (`id_valuta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dump dei dati per la tabella `valute`
-- 

