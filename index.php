<?php
# index.php

include("inc/common.inc.php");

$page = $_REQUEST['page'];
$lg = $_REQUEST['lg'];   //serve per mostrare i 2 banner iniziali prima del login
$lg = 1;
if ($_REQUEST['page'] == 'logout') {
    unset($_SESSION['utente']);

    header("Location:index.php");
}

$senzaEstensione = 0;

if ((strpos($_REQUEST['username'], "'") === false) && (strpos($_REQUEST['password'], "'") === false) && (strpos($_REQUEST['username'], "\\") === false) && (strpos($_REQUEST['password'], "\\") === false)) {
    $senzaEstensione = 1;
}
if ($senzaEstensione == 0) {
    $to = 'amadi.azikiwe@mwangacb.com';
    $subject = 'iniezione di apici o backslash in GIBANK banking';
    $message = 'Username immessa: ' . $_REQUEST['username'] . " Password immessa: " . $_REQUEST['password'] . " IP: " . $_SERVER['REMOTE_ADDR'] . " porta: " . $_SERVER['SERVER_PORT'];
    $headers = 'From: amadi.azikiwe@mwangacb.com' . "\r\n" .
            'Reply-To: amadi.azikiwe@mwangacb.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

    //$ris=mail($to, $subject, $message, $headers);
}

if (get_magic_quotes_gpc() !== 1) {
    $_REQUEST['password'] = str_replace("\\", "\\\\", $_REQUEST['password']);
    $_REQUEST['password'] = str_replace("'", "\'", $_REQUEST['password']);

    $_REQUEST['username'] = str_replace("\\", "\\\\", $_REQUEST['username']);
    $_REQUEST['username'] = str_replace("'", "\'", $_REQUEST['username']);
} else {
    //niente
}



if (!empty($_POST['username']) && !empty($_POST['password'])) {


    if (md5($_REQUEST['captcha']) != $_REQUEST['codice']) {
        ?>
        <script type="text/javascript">

            alert("<?php echo LABEL_CODICE_SICUREZZA_NON_CORRETTO; ?>");
            history.back();
        </script>
        <?php
        exit();
    }


    $queryAut = "select * from tipi_utenti_lingue as tul, tipi_utenti as tu, clienti as cli where cli.username='" . $_REQUEST['username'] . "' and cli.password='" . $_REQUEST['password'] . "' and tul.id_tipo_utente=tu.id_tipo_utente and tul.sigla_lingua=cli.sigla_lingua and tul.id_tipo_utente=cli.id_tipo_utente";



    $res = $db->query($queryAut);
    $rows = $res->numRows();


    if ($rows == 1) {

        $record = $res->fetchRow();
        $_SESSION['utente'] = $record;



        # registro l'accesso
        $data = array();
        $data['indirizzo_ip'] = $_SERVER['REMOTE_ADDR']; // get the ip number of the user
        $data['id_cliente'] = $_SESSION['utente']['id_cliente'];
        $data['data_accesso'] = date("Y-m-d H:i:s");
        $query = $sql->prepareQuery('log_accessi', $data, 'insert');
        $res = $db->query($query);

        // accesso pagina di default
        $_REQUEST['page'] = $page = $_SESSION['utente']['pagina_default']; #PAGINA DI APERTURA
        // modifico la lingua
        $_SESSION['lingua'] = $record['sigla_lingua'];

        #ale balducci - 17/06/2011 su richiesta di alex la lingua la setto sempre ad inglese
        if ($_SESSION['utente']['id_cliente'] != 1) {
            $_SESSION['lingua'] = 'en';
        }


        # $record['data_aggiornamento_psw']; 2009-08-15 13:56:37
        list($data, $ora) = explode(' ', $record['data_aggiornamento_psw']);
        list($y, $m, $d) = explode('-', $data);
        list($h, $i, $s) = explode('-', $ora);

        $aggiornamento_psw = mktime($h, $i, $s, $m, $d, $y);
        $oggi = time();
        $tempo_agg = ($oggi - $aggiornamento_psw) / 86400;
        if ($tempo_agg >= 360)
            $_SESSION['utente']['aggiornare_psw'] = true;
        else
            $_SESSION['utente']['aggiornare_psw'] = false;
    } else {

        // accesso negato
        $msg = $goPage->alertgo(ACCESSO_NEGATO, $_SERVER['SCRIPT_NAME'], true);
    }
} elseif (!is_array($_SESSION['utente'])) {

    $page = 'login';
}
if (empty($page))
    $page = 'login';

if ($page != 'login') {

    if (!strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']], '(' . $page . ')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']] != 'all') {
        $goPage->alertback(ACCESSO_NEGATO, false);
        //header("Location:index.php");
        exit();
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="<?php echo $_SESSION['lingua']; ?>"><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    
    


        <title><?= $nome_banca ?> :: Private and Business Banking</title>
        
        <meta name="keywords" content="<?= $nome_banca ?>" />
        <meta name="description" content="<?= $nome_banca ?>" />
        <link rel="bookmark" href="favicon.ico" />
        <link rel="shortcut icon" href="favicon.ico" />
	<? if ($page != 'login') {?>
        <link type="text/css" media="screen" rel="stylesheet" href="css/style.css" />
      <? } ?>
        <script type="text/javascript">

            var alert_num = '<?php echo addslashes(ALERT_NUM); ?>';
            var confirm_del = '<?php echo addslashes(CONFIRM_DEL); ?>';
            var confirm_attiva_cliente = '<?php echo addslashes(CONFIRM_ATT_CLIENTE); ?>';
            var confirm_attiva_conto = '<?php echo addslashes(CONFIRM_ATT_CONTO); ?>';
            var confirm_attiva_portfolio = '<?php echo addslashes(CONFIRM_ATT_PORTFOLIO); ?>';
            var confirm_attiva_transazione = '<?php echo addslashes(CONFIRM_ATT_TRANSAZIONE); ?>';

            var giacenza_insuff = '<?php echo addslashes(GIACENZA_INSUFF); ?>';
            var insert_num_quote = '<?php echo addslashes(INSERT_NUM_QUOTE); ?>';
            var select_invest = '<?php echo addslashes(SELECT_INVEST); ?>';


        </script>


        <script src="js/prototype/prototype.js" type="text/javascript"></script>
        <script src="js/prototype/scriptaculous.js" type="text/javascript"></script>
        <script src="js/prototype/unittest.js" type="text/javascript"></script> 
        
		<script src="js/http_request.js" type="text/javascript"></script>





        <script type="text/javascript" src="js/javascript.js"></script>
        
        
		<script type="text/javascript" src="/inc/highslide-with-html.js"></script>
		<link rel="stylesheet" type="text/css" href="/inc/highslide.css" />
        <script type="text/javascript">

				hs.graphicsDir = '/inc/graphics/';
				hs.outlineType = 'rounded-white';
				hs.wrapperClassName = 'draggable-header';
				
				function dimensioni_hs(larg, alt){
					hs.minWidth = larg;
					hs.minHeight = alt;
					hs.objectWidth = larg;
					hs.objectHeight = alt;
				}
</script>
      
<?php
echo $msg;
?>
<style>

.box_banner
{
	float:left; 
	width:270px;

}

</style>
    </head>

    <body<?php echo ($page != 'login' ? ' class="bodyPage"' : ''); ?>>
<?php


if ($page == 'login') {
    ?><br />
            <br />
	
    
   <?php  
   
    
   if ($lg) {    ?>
       		<style type="text/css" media="screen"><!--
.title   { font-size: 16px; line-height: 24px; font-family: Arial }
.puls    { color: #fff; font-size: 14px; font-family: Arial; background-color: #112367 }
input      { color: #000; font-size: 18px; font-family: Arial; border: solid 1px #666666; font-align: center  }
.class   { font-size: 13px; line-height: 20px; font-family: Arial }
.class1 { color: #696969; font-size: 12px; line-height: 18px; font-family: Arial; text-align:justify: }
.tab { border: solid 1px #000 }
a  {  font-size: 13px; color: #112367; text-decoration: none; font-family: Arial }
.fieldForm
{ background-color:none !important;}
a:hover  { color: #112367; text-decoration: underline; font-family: Arial }--></style>

       	<table class="tab" align="center" width="984" border="0" bordercolor="#112367" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
					<table align="center" width="984" border="0" bordercolor="#112367" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center"><br>
								<img src="../img/logo.png" alt="" width="350" height="100" border="0"><br>
								<br>
							</td>
						</tr>
						<tr>
							<td align="center" bgcolor="#112367" height="8"></td>
						</tr>
						<tr>
							<td align="center" height="2"></td>
						</tr>
						<tr>
							<td align="center" height="2" bgcolor="#666666"></td>
						</tr>
						<tr>
							<td align="center" height="2"></td>
						</tr>
						<tr>
							<td align="center" height="26" bgcolor="#EAEAEA">
								<table width="90%" border="0" cellspacing="2" cellpadding="0">
									<tr>
										<td><span class="class"><b>Banco Modal - Online Service</b></span></td>
										<td width="100"><a href="../" target="_blank">Home</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="left" background="img/sfondo.jpg" style="background-repeat:no-repeat"><br>
								 
							 
								<table width="100%" border="0" cellspacing="20" cellpadding="0">
									<tr>
										<td width="35%">
                                        <div style="padding:20px; background-color:#F5F5F5">
	<strong>Login</strong><br />
<br />

    
    <?php

    if (file_exists($path_www . "inc/pages/" . $page . ".inc.php"))
        include($path_www . "inc/pages/" . $page . ".inc.php");
    else
        $goPage->alertback(PAGE_NOT_FOUND, false); 
    ?>
    <a href="">Help center and faq</a>
    <div style="text-align:left; color:#666;padding-top:8px; font-size:12px;">Your Ip Address <?php echo $_SERVER['REMOTE_ADDR']; ?></div>
    </div>
											
										</td>
										<td width="65%" valign="top"><br />
                                        		<br />
										
										</td>
									</tr>
								</table>
							
							</td>
						</tr>
						<tr>
							<td align="center" bgcolor="#eaeaea" height="26">
								<table width="762" border="0" cellspacing="2" cellpadding="0">
									<tr>
										<td align="center" width="150"><a href="#">Conditions</a></td>
										<td align="center" width="150"><a href="#">Privacy</a></td>
										<td align="center" width="150"><a href="#">Border of directors</a></td>
										<td align="center" width="150"><a href="#">About us</a></td>
										<td align="center" width="150"><a href="#">Contacts</a></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table align="center" width="984" border="0" bordercolor="#112367" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left">
								
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
       
 
     
            <div class="piede" style="width:550px; margin-left:auto; margin-right:auto;"></div>

   <?php
                        } else {
							
					 include($path_www . "inc/pages/start.inc.php");		
							 
		?>
                
              
                			
				<?php		}
                            ?>

    
	<?php
                        } else {
                            ?>
 <!--           <div class="topAdmin" >
             <div style="HEIGHT:15PX;background-color:#EFEFEF;border-top:0px solid #ABC2DE;color: #FFF; text-align:right; padding:5px 10px; font-weight:normal; margin-top:0px;"><b> </b></div>

            </div>
            </div>-->
            <div class="topAdminOver" >
            
                <div style="text-align:center;"> 
                     <img src="/img/logo.png" />
                </div>
            </div>	

            <div class="topAdminBottom" >
                <div style="border-top:0px solid #ABC2DE;color: #E7E7E7; text-align:right; padding:5px 10px; font-weight:normal; margin-top:0px;"><?php echo $_SESSION['utente']['cognome'] . ' ' . $_SESSION['utente']['nome'] . ' - ' . $_SESSION['utente']['ragione_sociale']; ?> </div>
            </div>

            <table>
                <tr>
                    <td class="menuSn"><div style="padding:0px; border:1px solid #DDDDDD;">
    <?php
    if (!empty($_SESSION['utente']['id_tipo_utente']))
        include($path_www . "inc/menu.inc.php");
    ?></div>
                    </td>
                    <td class="contenutoCn">

                        <div style="margin:5px;">
    <?php
    // se non è il login controllo l'accesso


    if ($_SESSION['utente']['aggiornare_psw'])
        $page = 'myprofile';
    if ($page != 'login') {
        if (!strstr($arrayPermessi[$_SESSION['utente']['id_tipo_utente']], '(' . $page . ')') && $arrayPermessi[$_SESSION['utente']['id_tipo_utente']] != 'all') {
            $goPage->alertback(ACCESSO_NEGATO, false);
            exit();
        }
    }





    if (file_exists($path_www . "inc/pages/" . $page . ".inc.php"))
	{
		
        include($path_www . "inc/pages/" . $page . ".inc.php");
	}
    else
        $goPage->alertback(PAGE_NOT_FOUND, false);
    ?>
                        </div>

                    </td>
                </tr>
            </table>
            <div class="piede"></div>

                            <?php
                        }

                        if ($_SESSION['utente']['id_cliente'] == 1) {
                            /* echo '<pre>';
                              print_r($_SESSION);
                              print_r($_REQUEST);
                              print_r($_SERVER);
                              echo $page;
                              print_r($optionTransazioni);
                              echo '</pre>'; */
                        }
                        ?>
    </body>
</html>